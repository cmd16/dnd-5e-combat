### The following creatures are copyright WOTC or otherwise not included in the SRD, and as such will not be included in the public code:

- Aarakocra
- Banshee
- Beholders (Beholder, Death Tyrant, Spectator, Beholder Zombie)
- Blights (Needle Blight, Twig Blight, Vine Blight)
- Bugbear Chief
- Bullywug
- Cambion
- Carrion Crawler
- Crawling Claw
- Cyclops
- Death Knight
- Demilich
- some Demons (Barlgura, Chasme, Goristro, Manes, Shadow Demon, Yochlol)
- Spined Devil
- some Dinosaurs (Allosaurus, Ankylosaurus, Pteranadon)
- Displacer Beast
- Dracolich
- Shadow Dragon
- some Drow (Drow Elite Warrior, Drow Mage, Drow Priestess of Lolth)
- Empyrean
- Faerie Dragon
- Flameskull
- Flumph
- Fomorian
- some Fungi (Gas Spore)
- Galeb Duhr
- some Genies (Dao, Marid)
- Gith (Githyanki Warrior, Githyanki Knight, Githzerai Monk, Githzerai Zerth)
- some Gnoll (Gnoll Pack Lord, Gnoll Fang of Yeenoghu)
- some Goblins (Goblin Boss)
- Grell
- some Grick (Grick Alpha)
- Helmed Horror
- some Hobgoblins (Hobgoblin, Hobgoblin Captain, Hobgoblin Warlord)
- Hook Horror
- Intellect Devourer
- Jackalwere
- Kenku
- some Kobolds (Winged Kobold)
- Kuo-Toa (Kuo-Toa, Kuo-Toa Archpriest, Kuo-Toa Whip)
- some Lizardfolk (Lizardfolk Shaman, Lizard King/Queen)
- some Mephits (Mud Mephit)
- Mind Flayer
- Modrons (Monodrone, Duodrone, Tridrone, Quadrone, Pentadrone)
- Myconids (Myconid Sprout, Quaggoth Spore Servant, Myconid Adult, Myconid Sovereign)
- some Nagas (Bone Naga)
- Nothic
- Half-Ogre
- some Orcs (Orc War Chief, Orc Eye of Gruumush, Orog)
- Peryton
- Piercer
- Pixie
- Quaggoth
- some Remorhazes (Young Remorhaz)
- Revenant
- Rust Monster
- some Sahuagin (Sahuagin Priestess, Sahuagin Baron)
- some Salamanders (Fire Snake)
- Satyr
- Scarecrow
- some Slaadi (Red Slaad, Blue Slaad, Green Slaad, Gray Slaad, Death Slaad)
- Thri-Kreen
- Troglodyte
- Umber Hulk
- Water Weird
- Yeti
- Yuan-Ti (Yuan-Ti Abomination, Yuan-Ti Malison, Yuan-Ti Pureblood)
- Yugoloths (ArcanaLoth, Mezzoloth, Nycaloth, Ultroloth)
