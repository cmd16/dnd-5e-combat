DnD\_5e package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   DnD_5e.armor
   DnD_5e.armory
   DnD_5e.attack_class
   DnD_5e.battlemap
   DnD_5e.bestiary
   DnD_5e.character_classes
   DnD_5e.combatant
   DnD_5e.dice
   DnD_5e.encounter
   DnD_5e.features
   DnD_5e.read_from_web
   DnD_5e.simulation
   DnD_5e.spell_list
   DnD_5e.tactics
   DnD_5e.team
   DnD_5e.utility_methods_dnd
   DnD_5e.weapons

Module contents
---------------

.. automodule:: DnD_5e
   :members:
   :undoc-members:
   :show-inheritance:
