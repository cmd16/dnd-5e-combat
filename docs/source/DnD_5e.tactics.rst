DnD\_5e.tactics package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   DnD_5e.tactics.attack_tactics
   DnD_5e.tactics.combatant_tactics

Module contents
---------------

.. automodule:: DnD_5e.tactics
   :members:
   :undoc-members:
   :show-inheritance:
