import subprocess
import shutil
import os


# TODO: update to reflect current state of codebase
module_classes = {"DnD_5e.armor": ["DnD_5e.armor.Armor", "DnD_5e.armor.Shield"],
                  "DnD_5e.armory": ["DnD_5e.weapons.Weapon"],
                  "DnD_5e.attack_class": ["DnD_5e.attack_class.Attack", "DnD_5e.attack_class.MultiAttack",
                                          "DnD_5e.attack_class.SavingThrowSpell", ],
                  "DnD_5e.bestiary": [],
                  "DnD_5e.character_classes": ["DnD_5e.combatant.Combatant", "DnD_5e.combatant.Character",
                                               "DnD_5e.combatant.SpellCaster"],
                  "DnD_5e.combatant": ["DnD_5e.combatant.Combatant", "DnD_5e.combatant.Creature",
                                       "DnD_5e.combatant.Character", "DnD_5e.combatant.SpellCaster", ],
                  "DnD_5e.dice": ["DnD_5e.dice.DamageDiceBag", ],
                  "DnD_5e.encounter": ["DnD_5e.encounter.Encounter"],
                  "DnD_5e.features": ["DnD_5e.features.Feature"],
                  "DnD_5e.read_from_web": [],
                  "DnD_5e.simulation": ["DnD_5e.simulation.Simulation", "DnD_5e.encounter.Encounter"],
                  "DnD_5e.spell_list": [],
                  "DnD_5e.tactics": ["DnD_5e.tactics.Tactic"],
                  "DnD_5e.tactics.combatant_tactics": ["DnD_5e.tactics.combatant_tactics.CombatantTactic"],
                  "DnD_5e.tactics.attack_tactics": ["DnD_5e.tactics.attack_tactics.AttackTactic"],
                  "DnD_5e.team": ["DnD_5e.team.Team"], "DnD_5e.tests": [], "DnD_5e.utility_methods_dnd": [],
                  "DnD_5e.weapons": ["DnD_5e.weapons.Weapon"]}

# names of modules that need to be updated (e.g., because we're making a new image file)
update_modules = ["DnD_5e.encounter", "DnD_5e.simulation", "DnD_5e.spell_list"]

for module_name in module_classes:
    print(module_name)
    to_add = ""
    for class_name in module_classes[module_name]:
        filename = "%s.png" % (class_name)
        subprocess.run(["pyreverse", "-o", "png", "-p", module_name, "-f", "ALL", "-c", class_name, "DnD_5e"])
        if not os.path.isfile("docs/source/%s" % filename):
            shutil.move(filename, "docs/source")
        if module_name in update_modules:
            to_add += ".. figure:: %s\n" % filename
    if to_add:
        write_name = "docs/source/%s.rst" % module_name
        if not os.path.isfile(write_name):
            print("Can't find file: %s" % write_name)
            continue
        read_name = write_name.replace(".rst", "_nofig.rst")
        shutil.copy(write_name, read_name)
        with open(write_name, "w") as write_file:
            with open(read_name, "r") as read_file:
                for line in read_file:
                    if line in to_add:  # don't write duplicate lines
                        continue
                    write_file.write(line)
                    if line == ".. inheritance-diagram:: %s\n" % module_name:
                        write_file.write(to_add)
        os.remove(read_name)
