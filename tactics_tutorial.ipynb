{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tactics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are you tired of your combatants acting stupid, targeting whoever they feel like and choosing attacks at random? No fear, that's what tactics are for. You can (and probably should) take advantage of this awesome feature!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What are Tactics?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```Tactic``` is a class created in the ```tactics``` module. You can read more about it in the [online documentation](https://cmd16.gitlab.io/dnd-5e-combat/source/DnD_5e.tactics.html#module-DnD_5e.tactics), but this tutorial will cover the key points you need to know."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The real meat of a ```Tactic``` is its ```run_tactic``` method. ```run_tactic``` takes a ```list``` of choices, plus any keyword arguments you specify. It then uses its algorithm to select from the choices it is given. ```run_tactic```  returns a list that is a subset of the options it was given. The list may contain 0, 1, some, or all of the options it was given. This method is overridden in each subclass - when you make a new ```Tactic```, this is the main way you establish your ```Tactic```'s behavior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But wait, don't you eventually need a single choice? Yes, but you won't need to worry about that. ```make_choice``` deals with that logic, and you shouldn't override that."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So then what happens if your ```run_tactic``` doesn't return exactly 1 item? That's where tiebreakers come in. Each ```Tactic``` has an instance variable called ```_tiebreakers```: a list of other ```Tactic```s to help narrow down the indefinitive result. (Note: if ```run_tactic``` returns 0 item, the tiebreakers are evaluated the same as if ```run_tactic``` returned every possible result.) This variable can be accessed using ```get_tiebreakers```. To append another tactic to the end of ```_tiebreakers```, use ```append_tiebreaker(self, tiebreaker)```. If you want to add all the tiebreakers of another tactic, you can use ```extend_tiebreaker(self, tie_breaker)```."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What Tactics are available?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default (i.e., what you get from ```tactics.Tactic()```), just chooses one item from the list at random. But there are plenty of other ```Tactic```s as well. There are a few general-purpose ```Tactic```s (primarily for use to create other ```Tactic```s), and there are two main subclasses based on what class of items you could be choosing from (stored in submodules of ```tactics```)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ConditionTactic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Probably the simplest to understand, this relies on a simple concept. For each item in ```choices```, call the method ```check_condition(item, **kwargs)``` to see if the item meets those conditions. If it does, it is appended to the result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ThresholdTactic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you create a ```ThresholdTactic```, you need to pass in a ```threshold```. This value is checked by ```validate_threshold``` (which by default rejects anything that's not a non-negative integer). If you don't provide a ```threshold```, one will be calculated from the keyword arguments passed in by calling ```calculate_threshold(self, **kwargs)``` (by default, ```calculate_threshold``` just throws an error, so you'll have to override it if you want to use it). Generally, you would create ```Tactic```s that inherit from ```ThresholdTactic``` and ```ConditionTactic```: \"High\" and/or \"Low\" ```Tactic```s to select items above or below the given threshold."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### MinTactic and MaxTactic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For both of these ```Tactic```s, you need to get some value from each item in ```choices``` by calling ```get_value(item, **kwargs)```. ```MinTactic``` and ```MaxTactic``` select the item(s) with the lowest and highest values returned by ```get_value```, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### CombatantTactic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For ```Tactic```s that choose from ```Combatant```s. To learn more, check out the [documentation](https://cmd16.gitlab.io/dnd-5e-combat/source/DnD_5e.tactics.combatant_tactics.html#module-DnD_5e.tactics.combatant_tactics)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### AttackTactic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For ```Tactic```s that choose from ```Attack```s. To learn more, check out the [documentation](https://cmd16.gitlab.io/dnd-5e-combat/source/DnD_5e.tactics.attack_tactics.html#module-DnD_5e.tactics.attack_tactics)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How do you use Tactics?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several places you can assign ```Tactic```s:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Combatant"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from DnD_5e import combatant\n",
    "from DnD_5e.tactics import combatant_tactics, attack_tactics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "comb = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, speed=20, vision='darkvision',\n",
    "                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,\n",
    "                             name='comb', enemy_tactic=combatant_tactics.LowestAcTactic())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key point there is the ```enemy_tactic``` keyword argument. ```enemy_tactic``` decides who the ```Combatant``` will target for an attack. This can also be set after construction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<DnD_5e.tactics.combatant_tactics.HighHpTactic at 0x7feb1090eeb8>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "comb.set_enemy_tactic(combatant_tactics.HighHpTactic(threshold=10))\n",
    "comb.get_enemy_tactic()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's worth noting that if you don't provide an enemy tactic, the default is ```combatant_tactics.IsConsciousTactic```, so that you don't attack unconcious enemies. You may well still want this to be the first thing you check for when selecting an enemy. If that's what you want, then don't include ```enemy_tactic``` in the constructor and instead call ```comb.get_enemy_tactic().append_tiebreaker(your_tactic)``` (where ```comb``` is your ```Combatant``` and ```your_tactic``` is the ```Tactic``` you want to use to select between conscious enemies)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also set ```heal_tactic``` so that our ```Combatant``` chooses wisely who to heal. Like ```enemy_tactic```, this can also be set at construction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<DnD_5e.tactics.combatant_tactics.LowestHpTactic at 0x7feb1090ee48>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "comb.set_heal_tactic(combatant_tactics.LowestHpTactic())\n",
    "comb.get_heal_tactic()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may also want a ```Tactic``` to choose which ```Attack``` to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "comb.set_attack_tactic(attack_tactics.DprMaxTactic())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Team"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's reuse our team from the basic tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from DnD_5e import armor, armory, bestiary, team, character_classes, encounter, simulation\n",
    "from DnD_5e.utility_methods_dnd import NullLogger"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Created a SpellCaster with no spells\n"
     ]
    }
   ],
   "source": [
    "fighter = character_classes.Fighter(strength=8, dexterity=15, constitution=14, intelligence=10, wisdom=13, charisma=12,\n",
    "          level=1, fighting_style=\"archery\", armor=armor.ChainMailArmor, weapons=[armory.Longbow()], name=\"Archie\")\n",
    "monk = character_classes.Monk(strength=12, dexterity=15, constitution=14, intelligence=10, wisdom=13, charisma=8,\n",
    "       level=1, weapons=[armory.Spear()], name=\"Ninja\")\n",
    "rogue = character_classes.Rogue(strength=8, dexterity=15, constitution=14, intelligence=10, wisdom=13, charisma=12,\n",
    "        level=1, armor=armor.LeatherArmor, weapons=[armory.Rapier()], name=\"Stabby\")\n",
    "paladin = character_classes.Paladin(strength=15, dexterity=8, constitution=15, intelligence=8, wisdom=8, charisma=15,\n",
    "          level=1, armor=armor.ChainMailArmor, weapons=[armory.Greatsword()], name=\"Smitey McSmiteFace\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "party = team.Team(combatant_list=[fighter, monk, rogue, paladin], name=\"The cool kids\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you recall from our basic tutorial, the following Goblin encounter was pretty easy. Does it get harder if we make the Goblins smarter? Let's give the Goblin team a ```CombatantTactic``` for selecting enemies. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "boblin = bestiary.Goblin(name=\"Boblin\")\n",
    "moblin = bestiary.Goblin(name=\"Moblin\")\n",
    "soblin = bestiary.Goblin(name=\"Soblin\")\n",
    "hoblin = bestiary.Goblin(name=\"Hoblin\")\n",
    "foes = team.Team(combatant_list=[boblin, moblin, soblin, hoblin], \n",
    "                 enemy_tactic=combatant_tactics.LowestHpTactic(), name=\"Goblins\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tactics in action - simulation time!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "e1 = encounter.Encounter(name=\"e1\", teams=[party, foes])\n",
    "s1 = simulation.Simulation(encounter=e1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "null_logger = NullLogger()\n",
    "s1.get_encounter().set_logger(null_logger)\n",
    "for comb in s1.get_encounter().get_combatants():\n",
    "    comb.set_logger(null_logger)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Combatant stats:\n",
      "Archie {'Average damage dealt': 10.16, 'Average damage taken': 8.9, 'Times gone unconscious': 561, 'Times ended conscious': 437, 'Probability of ending conscious': 0.437, 'Times ended unconscious': 558, 'Probability of ending unconscious': 0.558, 'Times ended dead': 5, 'Probability of ending dead': 0.005, 'Average hp at encounter end': 4.598}\n",
      "\n",
      "Ninja {'Average damage dealt': 4.411, 'Average damage taken': 11.767, 'Times gone unconscious': 896, 'Times ended conscious': 98, 'Probability of ending conscious': 0.098, 'Times ended unconscious': 836, 'Probability of ending unconscious': 0.836, 'Times ended dead': 66, 'Probability of ending dead': 0.066, 'Average hp at encounter end': 0.612}\n",
      "\n",
      "Stabby {'Average damage dealt': 4.203, 'Average damage taken': 11.78, 'Times gone unconscious': 887, 'Times ended conscious': 109, 'Probability of ending conscious': 0.109, 'Times ended unconscious': 826, 'Probability of ending unconscious': 0.826, 'Times ended dead': 65, 'Probability of ending dead': 0.065, 'Average hp at encounter end': 0.614}\n",
      "\n",
      "Smitey McSmiteFace {'Average damage dealt': 14.919, 'Average damage taken': 8.961, 'Times gone unconscious': 565, 'Times ended conscious': 435, 'Probability of ending conscious': 0.435, 'Times ended unconscious': 563, 'Probability of ending unconscious': 0.563, 'Times ended dead': 2, 'Probability of ending dead': 0.002, 'Average hp at encounter end': 4.529}\n",
      "\n",
      "Boblin {'Average damage dealt': 10.776, 'Average damage taken': 8.418, 'Times gone unconscious': 0, 'Times ended conscious': 244, 'Probability of ending conscious': 0.244, 'Times ended unconscious': 0, 'Probability of ending unconscious': 0.0, 'Times ended dead': 756, 'Probability of ending dead': 0.756, 'Average hp at encounter end': 1.255}\n",
      "\n",
      "Moblin {'Average damage dealt': 10.215, 'Average damage taken': 8.234, 'Times gone unconscious': 0, 'Times ended conscious': 253, 'Probability of ending conscious': 0.253, 'Times ended unconscious': 0, 'Probability of ending unconscious': 0.0, 'Times ended dead': 747, 'Probability of ending dead': 0.747, 'Average hp at encounter end': 1.339}\n",
      "\n",
      "Soblin {'Average damage dealt': 10.187, 'Average damage taken': 8.307, 'Times gone unconscious': 0, 'Times ended conscious': 246, 'Probability of ending conscious': 0.246, 'Times ended unconscious': 0, 'Probability of ending unconscious': 0.0, 'Times ended dead': 754, 'Probability of ending dead': 0.754, 'Average hp at encounter end': 1.307}\n",
      "\n",
      "Hoblin {'Average damage dealt': 10.23, 'Average damage taken': 8.734, 'Times gone unconscious': 0, 'Times ended conscious': 214, 'Probability of ending conscious': 0.214, 'Times ended unconscious': 0, 'Probability of ending unconscious': 0.0, 'Times ended dead': 786, 'Probability of ending dead': 0.786, 'Average hp at encounter end': 1.165}\n",
      "\n",
      "Team stats:\n",
      "The cool kids {'Average number of conscious members': 1.079, 'Average number of unconscious members': 2.783, 'Average number of dead members': 0.138}\n",
      "\n",
      "Goblins {'Average number of conscious members': 0.957, 'Average number of unconscious members': 0.0, 'Average number of dead members': 3.043}\n",
      "\n",
      "Encounter stats:\n",
      "Average rounds 3.794\n",
      "\n"
     ]
    }
   ],
   "source": [
    "s1.run(n=1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, what changed since the version we saw in the basic tutorial? Well, the encounter may take a little longer - closer to 4 rounds now. The probability that the goblins lose is now 76.075%, which is better for them than the 88.55% we calculated when both sides weren't smart about their targets. This use of tactics brings that average number of unconscious party members closer to 3. Ouch! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So as you can see, tactics do matter. This tool will help you understand how different tactics affect the outcome of an encounter, so that you can plan smarter enemies and better account for the strategizing (or lack thereof) of your players."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
