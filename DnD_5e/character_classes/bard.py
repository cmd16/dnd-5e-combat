from DnD_5e.combatant import Combatant
from DnD_5e.combatant.character import Character
from DnD_5e.combatant.spellcaster import SpellCaster
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level, TYPE_DICE_TUPLE


class Bard(SpellCaster, Character):
    """
    Bard character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("hand crossbow")
        proficiencies.add("longsword")
        proficiencies.add("rapier")
        proficiencies.add("shortsword")
        proficiencies.add("dexterity")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        feature_seq = kwargs.get('features', set())
        if isinstance(feature_seq, (list, tuple, set)):
            feature_seq = set(feature_seq)
        elif feature_seq is None:
            feature_seq = set()
        else:
            raise ValueError("Features must be a set (or a list or tuple to convert to a set)")

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3}
            feature_seq.add("countercharm")
            if level == 7:
                spell_slots.update({4: 1})
            elif level == 8:
                spell_slots.update({4: 2})
            elif level == 9:
                spell_slots.update({4: 3, 5: 1})
            elif level == 10:
                spell_slots.update({4: 3, 5: 2})
            elif level < 13:
                spell_slots.update({4: 3, 5: 2, 6: 1})
            elif level < 15:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1})
            elif level < 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1})
            elif level == 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 18:
                spell_slots.update({4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 19:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1})
            elif level == 20:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_ability": "charisma", "spell_slots": spell_slots,
                       "features": feature_seq})

        super().__init__(**kwargs)

        if self.get_level() < 5:
            self._inspiration_dice = (1, 6)
        elif self.get_level() < 10:
            self._inspiration_dice = (1, 8)
        elif self.get_level() < 15:
            self._inspiration_dice = (1, 10)
        else:
            self._inspiration_dice = (1, 12)

        self._inspiration_slots = self._charisma

    def __eq__(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on
        what is checked in the superclass method as well as inspiration dice

        :param other: the Bard to be compared
        :type other: Bard
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) \
            and self.get_inspiration_dice() == other.get_inspiration_dice()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: inspiration slots

        :param other: the Bard to compare
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_inspiration_slots() == other.get_inspiration_slots()

    def get_inspiration_dice(self) -> TYPE_DICE_TUPLE:
        """
        :return: inspiration dice
        :rtype: TYPE_DICE_TUPLE
        """
        return self._inspiration_dice

    def get_inspiration_slots(self) -> int:
        """
        :return: inspiration slots
        :rtype: non-negative integer
        """
        return self._inspiration_slots

    def send_inspiration(self, target: Combatant):
        """
        Give an inspiration die to *target*. NOT IMPLEMENTED YET (i.e., nobody can take inspiration)

        :param target: the Combatant to give inspiration to
        :type target: Combatant
        :return: None
        """
        if target is self or not isinstance(target, Combatant):
            self.get_logger().error("Can only give inspiration to a Combatant other than yourself", stack_info=True)
            raise ValueError("Can only give inspiration to a Combatant other than yourself")
        if not self._inspiration_slots:
            self.get_logger().error("Cannot inspire %s because you have no inspiration slots left", target.get_name())
            raise ValueError("Cannot inspire %s because you have no inspiration slots left" % target.get_name())
        self._inspiration_slots -= 1
