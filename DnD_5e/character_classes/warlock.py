from typing import Optional

from DnD_5e.combatant.character import Character
from DnD_5e.combatant.spellcaster import SpellCaster
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level


class Warlock(SpellCaster, Character):
    """
    Warlock character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)

        Class specific parameters:
        :param eldritch_invocations: the Eldritch Invocations that *self* knows
        :type eldritch_invocations: set of strings
        :param pact_boon: Pact Boon
        :type pact_boon: str

        :raise: ValueError if input is invalid
        """
        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 1}
        elif level == 2:
            spell_slots = {1: 2}
        elif level < 5:
            spell_slots = {2: 2}
        elif level < 7:
            spell_slots = {3: 2}
        elif level < 9:
            spell_slots = {4: 2}
        elif level < 11:
            spell_slots = {5: 2}
        elif level < 17:
            spell_slots = {5: 3}
        else:
            spell_slots = {5: 4}
        if level > 10:
            spell_slots.update({6: 1})  # combine Mystic Arcanum with regular spell slots because they are about the same
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("eldritch invocations")
            self._eldritch_invocations = kwargs.get("eldritch_invocations")
            # TODO: validation of number of invocations for level
            if isinstance(self._eldritch_invocations, (tuple, list)):
                self._eldritch_invocations = set(self._eldritch_invocations)
            elif not isinstance(self._eldritch_invocations, set):
                raise ValueError("Eldritch invocations must be a set (or a list or tuple to convert to set)")
        if self.get_level() > 2:
            self.add_feature("pact boon")
            self._pact_boon = kwargs.get("pact_boon")
            if not self._pact_boon:
                raise ValueError("Must provide a pact boon")

    def __eq__(self, other):
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        eldritch invocations, pact boon

        :param other: the Warlock to compare
        :type other: Warlock
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) \
            and self.get_eldritch_invocations() == other.get_eldritch_invocations() \
            and self.get_pact_boon() == other.get_pact_boon()

    def get_pact_boon(self) -> Optional[str]:
        """
        :return: pact boon
        :rtype: str (or None)
        """
        if self.has_feature("pact boon"):
            return self._pact_boon
        return None

    def get_eldritch_invocations(self) -> Optional[set]:
        """
        :return: eldritch invocations
        :rtype: set of strings (or None)
        """
        if self.has_feature("eldritch invocations"):
            return self._eldritch_invocations
        return None

    def has_eldritch_invocation(self, invocation: str) -> bool:
        """
        Determine if *self* has a given eldritch invocation

        :param invocation: the eldritch invocation to look for
        :type invocation: str
        :return: True if *self* has the eldritch invocation *invocation*, False otherwise
        :rtype: bool
        """
        if self.has_feature("eldritch invocations"):
            return invocation in self.get_eldritch_invocations()
        return False
