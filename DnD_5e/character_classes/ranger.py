from DnD_5e.combatant.character import Character
from DnD_5e.combatant.creature import Creature
from DnD_5e.combatant.spellcaster import SpellCaster
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level


class Ranger(SpellCaster, Character):
    """
    Ranger character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :param fighting_style: the fighting style that *self* knows
        :type fighting_style: str

        Class specific parameters:
        :param favored_enemies: the kind of enemies that *self* knows well
        :type favored_enemies: set of strings (each string is one of these: "aberration", "beast", "celestial",
        "construct", "dragon", "elemental", "fey", "fiend", "giant", "monstrosity", "ooze", "plant", "undead")
        :param favored_terrains: the terrains that *self* knows well
        :type favored_terrains: set of strings

        :raise: ValueError if input is invalid

        """
        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 10 + constitution_mod
        if level > 1:
            max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("dexterity")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 0}
        if level > 1:
            spell_slots.update({1: 2})
            if level > 2:
                spell_slots.update({1: 3})
            if level > 4:
                spell_slots.update({1: 4, 2: 2})
            if level > 6:
                spell_slots.update({2: 3})
            if level > 8:
                spell_slots.update({3: 2})
            if level > 10:
                spell_slots.update({3: 3})
            if level > 12:
                spell_slots.update({4: 1})
            if level > 14:
                spell_slots.update({4: 2})
            if level > 16:
                spell_slots.update({4: 3, 5: 1})
            if level > 19:
                spell_slots.update({5: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "wisdom", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        self.add_feature("favored enemy")
        self._favored_enemies = kwargs.get("favored_enemies")
        if isinstance(self._favored_enemies, (list, tuple, set)):
            self._favored_enemies = set(self._favored_enemies)
        else:
            self._favored_enemies = set()
            favored_enemy = kwargs.get("favored_enemy")
            self._favored_enemies.add(favored_enemy)

        self.add_feature("favored terrain")
        self._favored_terrains = kwargs.get("favored_terrains")
        if isinstance(self._favored_terrains, (list, tuple, set)):
            self._favored_terrains = set(self._favored_terrains)
        else:
            self._favored_terrains = set()
            favored_terrain = kwargs.get("favored_terrain")
            self._favored_terrains.add(favored_terrain)

        if self.get_level() > 1:
            self.add_feature("fighting style")
            fighting_style = kwargs.get("fighting_style")
            self.add_fighting_style(fighting_style)
        if self.get_level() > 2:
            self.add_feature("ranger archetype")
            self.add_feature("primeval awareness")
        if self.get_level() > 4:
            self.add_feature("extra attack")
        if self.get_level() > 7:
            self.add_feature("land's stride")
        if self.get_level() > 9:
            self.add_feature("hide in plain sight")
        if self.get_level() > 13:
            self.add_feature("vanish")
        if self.get_level() > 17:
            self.add_feature("feral senses")
        if self.get_level() > 19:
            self.add_feature("foe slayer")

    def __eq__(self, other):
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        favored enemies, favored terrains

        :param other: the Ranger to compare
        :type other: Ranger
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) \
            and self.get_favored_enemies() == other.get_favored_enemies() \
            and self.get_favored_terrains() == other.get_favored_terrains()

    def get_favored_enemies(self):
        """
        :return: favored enemies
        :rtype: set of strings
        """
        return self._favored_enemies

    def has_favored_enemy(self, enemy) -> bool:
        """
        Determine whether the given enemy is favored or not

        :param enemy: the enemy or name of the enemy
        :type enemy: Creature or str
        :return: True if *enemy* is a favored enemy, False otherwise
        :rtype: bool
        """
        if isinstance(enemy, Creature):
            enemy = enemy.get_creature_type()
        elif isinstance(enemy, str):
            pass
        else:
            self.get_logger().error("Enemy must be a Creature or a string", stack_info=True)
            raise ValueError("Enemy must be a Creature or a string")
        return enemy in self.get_favored_enemies()

    def get_favored_terrains(self) -> set:
        """
        :return: favored terrains
        :rtype: set of strings
        """
        return self._favored_terrains

    def has_favored_terrain(self, terrain) -> bool:
        """
        Determine whether the given terrain is favored or not

        :param terrain: the terrain to look at
        :type terrain: str
        :return: True if *terrain* is a favored terrain, False otherwise
        :rtype: bool
        """
        return terrain in self.get_favored_terrains()
