from DnD_5e.combatant.character import Character
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level, roll_dice


class Fighter(Character):
    """
    Fighter character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :param fighting_style: the fighting style that *self* uses
        :type fighting_style: set, list, or tuple of strings (will be converted to set of strings)
        :raise: ValueError if input is invalid
        """
        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 10 + constitution_mod
        if level > 1:
            max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', None)
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("constitution")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod, "hit_dice": hit_dice})

        super().__init__(**kwargs)

        self.add_feature("fighting style")
        fighting_style = kwargs.get("fighting_style")
        self.add_fighting_style(fighting_style)

        self.add_feature("second wind")
        self._second_wind_slots = 1
        if self.get_level() > 1:
            self.add_feature("action surge")
            self._action_surge_slots = 1
        if self.get_level() > 2:
            self.add_feature("martial archetype")
        if self.get_level() > 4:
            self.add_feature("extra attack")
            self._extra_attack_num = 1
        if self.get_level() > 8:
            self.add_feature("indomitable")
            self._indomitable_slots = 1
        if self.get_level() > 10:
            self._extra_attack_num = 2
        if self.get_level() > 12:
            self._indomitable_slots = 2
        if self.get_level() > 16:
            self._action_surge_slots = 2
            self._indomitable_slots = 3
        if self.get_level() > 19:
            self._extra_attack_num = 3

    def __eq__(self, other) -> bool:
        """
        Check equality based on superclass method and these attributes: extra attack num

        :param other: the Fighter to compare
        :type other: Fighter
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) \
            and self.get_extra_attack_num() == other.get_extra_attack_num()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: action surge slots, indomitable

        :param other: the Fighter to compare
        :type other: Fighter
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_action_surge_slots() == other.get_action_surge_slots() \
            and self.get_indomitable_slots() == other.get_indomitable_slots()

    def get_second_wind_slots(self) -> int:
        """
        :return: second wind slots
        :rtype: non-negative integer
        """
        return self._second_wind_slots

    def get_action_surge_slots(self) -> int:
        """
        :return: action surge slots
        :rtype: non-negative integer
        """
        try:
            return self._action_surge_slots
        except AttributeError:
            return 0

    def get_extra_attack_num(self) -> int:
        """
        :return: the number of extra attacks (additional attacks allowed when using the attack action)
        :rtype: non-negative integer
        """
        try:
            return self._extra_attack_num
        except AttributeError:
            return 0

    def get_indomitable_slots(self) -> int:
        """
        :return: indomitable slots
        :rtype: non-negative integer
        """
        try:
            return self._indomitable_slots
        except AttributeError:
            return 0

    def take_action_surge(self):
        """
        Do an action surge. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* has no action surge slots left
        """
        if self.get_action_surge_slots():
            self._action_surge_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to action surge", stack_info=True)
            raise ValueError("You don't have the slots to action surge")

    def take_extra_attack(self):
        """
        Make an extra attack. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* has no extra attack slots left
        """
        if self.get_extra_attack_num():
            self._extra_attack_num -= 1
        else:
            self.get_logger().error("You don't have the slots for an extra attack", stack_info=True)
            raise ValueError("You don't have the slots for an extra attack")

    def take_indomitable(self):
        """
        Use the indomitable feature. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* doesn't have indomitable slots
        """
        if self.get_indomitable_slots():
            self._indomitable_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to use indomitable", stack_info=True)
            raise ValueError("You don't have the slots to use indomitable")

    def take_second_wind(self):
        """
        Use the second wind feature. NOT IMPLEMENTED YET (i.e., this isn't called anywhere).

        :return: None
        :raise: ValueError if *self* doesn't have second wind slots
        """
        # TODO: make this a bonus action
        if self.get_second_wind_slots():
            self.get_logger().info("%s uses second wind!", self.get_name())
            healing = roll_dice(10, modifier=self.get_level())[0]
            self.take_healing(healing)
            self._second_wind_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to use second wind", stack_info=True)
            raise ValueError("You don't have the slots to use second wind")
