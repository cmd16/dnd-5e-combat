from DnD_5e.combatant.creature import Creature
from DnD_5e import attack_class, combatant, dice

# pylint: disable=pointless-string-statement, trailing-whitespace, trailing-newlines

class Couatl(Creature):  # missing from dnd5einfo
    def __init__(self, **kwargs):  # errata: cr improperly reported
        default_kwargs = {"size": "medium", "creature_type": "celestial",
            "ac": 19, "max_hp": 97, "hit_dice": "13d8", "speed": 30,
            "strength": 16, "dexterity": 20, "constitution": 17, "intelligence": 18, "wisdom": 20, "charisma": 18}
        default_kwargs.update({"proficiencies": {'charisma', 'constitution', 'wisdom'}})
        default_kwargs.update({'resistances': ['radiant']})
        default_kwargs.update({'immunities': {'psychic', 'bludgeoning', 'piercing', 'slashing'}})
        default_kwargs.update({'vision': "truesight", 'cr': 4})

        # Attacks/Features
        """
        Innate Spellcasting. The solar’s spellcasting ability is Charisma (spell save DC 25). It can innately cast the following spells, requiring no material components:
            At will: detect evil and good, detect magic, detect thoughts
            3/day each: bless, create food and water, cure wounds, lesser restoration, protection from poison, sanctuary, shield
            1/day each: dream, greater restoration, scrying
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=5, damage_type='piercing'), attack_mod=8, melee_range=5, name="Bite")
            # the target must succeed on a DC 13 Constitution saving throw or be poisoned for 24 hours. Until this poison ends, the target is unconscious. Another creature can use an action to shake the target awake.
        constrict = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='bludgeoning'), attack_mod=6, melee_range=10, name="Constrict")
            # the target is grappled (escape DC 15). Until this grapple ends, the target is restrained, and the couatl can’t constrict another target.
        Change Shape. The couatl magically polymorphs into a humanoid or beast that has a challenge rating equal to or less than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the couatl’s choice).
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Aboleth(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration", 'ac': 17, 'max_hp': 135, 'speed': 10, 'swim_speed': 40, 
            "strength": 21, "dexterity": 9, "constitution": 15, "intelligence": 18, "wisdom": 15, "charisma": 18}
        default_kwargs.update({"proficiencies": {'constitution', 'wisdom', 'intelligence'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 10})
        # Features
        """
        Amphibious. The aboleth can breathe air and water.
        Mucous Cloud. While underwater, the aboleth is surrounded by transformative mucus. A creature that touches the aboleth or that hits it with a melee attack while within 5 ft. of it must make a DC 14 Constitution saving throw. On a failure, the creature is diseased for 1d4 hours. The diseased creature can breathe only underwater.
        Probing Telepathy. If a creature communicates telepathically with the aboleth, the aboleth learns the creature’s greatest desires if the aboleth can see the creature.
        """
        # Actions
        """
        tentacle = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='bludgeoning'), attack_mod=9, melee_range=10, name='Tentacle')
            #  If the target is a creature, it must succeed on a DC 14 Constitution saving throw or become diseased. The disease has no effect for 1 minute and can be removed by any magic that cures disease. After 1 minute, the diseased creature’s skin becomes translucent and slimy, the creature can’t regain hit points unless it is underwater, and the disease can be removed only by heal or another disease-curing spell of 6th level or higher. When the creature is outside a body of water, it takes 6 (1d12) acid damage every 10 minutes unless moisture is applied to the skin before 10 minutes have passed.
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=5, damage_type='bludgeoning'), attack_mod=9, melee_range=10, name='Tail')
        Enslave (3/day). The aboleth targets one creature it can see within 30 ft. of it. The target must succeed on a DC 14 Wisdom saving throw or be magically charmed by the aboleth until the aboleth dies or until it is on a different plane of existence from the target. The charmed target is under the aboleth’s control and can’t take reactions, and the aboleth and the target can communicate telepathically with each other over any distance. Whenever the charmed target takes damage, the target can repeat the saving throw. On a success, the effect ends. No more than once every 24 hours, the target can also repeat the saving throw when it is at least 1 mile away from the aboleth.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[tentacle, tentacle, tentacle])
        """
        # Legendary Actions
        """
        Detect. The aboleth makes a Wisdom (Perception) check.
        Tail Swipe. The aboleth makes one tail attack.
        Psychic Drain (Costs 2 Actions). One creature charmed by the aboleth takes 10 (3d6) psychic damage, and the aboleth regains hit points equal to the damage the creature takes.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class AngelDeva(Creature):   # complex
    def __init__(self, **kwargs):  # errata: mace instead of melee in multiattack
        default_kwargs = {"size": "medium", "creature_type": "celestial", 'ac': 17, 'max_hp': 136, 'speed': 30, 'fly_speed': 90, 
            "strength": 18, "dexterity": 18, "constitution": 18, "intelligence": 17, "wisdom": 20, "charisma": 20}
        default_kwargs.update({"proficiencies": {'charisma', 'wisdom'}})
        default_kwargs.update({"resistances": {' radiant', 'piercing', 'bludgeoning', 'slashing'}})
        default_kwargs.update({"immunities": {' charmed', 'exhaustion', 'frightened'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 10})
        # Features
        """
        Angelic Weapons. The deva’s weapon attacks are magical. When the deva hits with any weapon, the weapon deals an extra 4d8 radiant damage (included in the attack).
        Innate Spellcasting. The deva’s spellcasting ability is Charisma (spell save DC 17). The deva can innately cast the following spells, requiring only verbal components:
        At will: detect evil and good
        1/day each: commune, raise dead
        Magic Resistance. The deva has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        mace = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=6, modifier=4, damage_type='bludgeoning'), dice.DamageDice(dice_num=4, dice_type=8, modifier=0, damage_type='radiant')]), attack_mod=8, melee_range=5, name='Mace')
        The deva touches another creature. The target magically regains 20 (4d8 + 2) hit points and is freed from any curse, disease, poison, blindness, or deafness.
        The deva magically polymorphs into a humanoid or beast that has a challenge rating equal to or less than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the deva’s choice). <p>In a new form, the deva retains its game statistics and ability to speak, but its AC, movement modes, Strength, Dexterity, and special senses are replaced by those of the new form, and it gains any statistics and capabilities (except class features, legendary actions, and lair actions) that the new form has but that it lacks.</p>
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[mace, mace])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class AngelPlanetar(Creature):  # needs spell
    def __init__(self, **kwargs):  # errata: greatsword instead of melee
        default_kwargs = {"size": "large", "creature_type": "celestial", 'ac': 19, 'max_hp': 200, 'speed': 40, 'fly_speed': 120, 
            "strength": 24, "dexterity": 20, "constitution": 24, "intelligence": 19, "wisdom": 22, "charisma": 25}
        default_kwargs.update({"proficiencies": {'charisma', 'constitution', 'wisdom'}})
        default_kwargs.update({"resistances": {'piercing', 'bludgeoning', ' radiant', 'slashing'}})
        default_kwargs.update({"immunities": {'frightened', 'exhaustion', ' charmed'}})
        default_kwargs.update({'vision': "normal", 'cr': 16})
        # Features
        """
        Angelic Weapons. The planetar’s weapon attacks are magical. When the planetar hits with any weapon, the weapon deals an extra 5d8 radiant damage (included in the attack).
        Divine Awareness. The planetar knows if it hears a lie.
        Innate Spellcasting. The planetar’s spellcasting ability is Charisma (spell save DC 20). The planetar can innately cast the following spells, requiring no material components:
        At will: detect evil and good, invisibility (self only)
        3/day each: blade barrier, dispel evil and good, flame strike, raise dead
        1/day each: commune, control weather, insect plague
        Magic Resistance. The planetar has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        greatsword = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=4, dice_type=6, modifier=7, damage_type='slashing'), dice.DamageDice(dice_num=5, dice_type=8, modifier=0, damage_type='radiant')]), attack_mod=12, melee_range=5, name='Greatsword')
        Healing Touch (4/Day). The planetar touches another creature. The target magically regains 30 (6d8 + 3) hit points and is freed from any curse, disease, poison, blindness, or deafness.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatsword, greatsword])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class AngelSolar(Creature):  # needs spell, needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "celestial", 'ac': 21, 'max_hp': 243, 'speed': 50, 'fly_speed': 150, 
            "strength": 26, "dexterity": 22, "constitution": 26, "intelligence": 25, "wisdom": 25, "charisma": 30}
        default_kwargs.update({"proficiencies": {'charisma', 'wisdom', 'intelligence'}})
        default_kwargs.update({"resistances": {'slashing', 'bludgeoning', 'piercing', ' radiant'}})
        default_kwargs.update({"immunities": {'poison', 'exhaustion', 'frightened', 'poisoned', ' necrotic', ' charmed'}})
        default_kwargs.update({'vision': "truesight", 'cr': 21})
        # Features
        """
        Angelic Weapons. The solar’s weapon attacks are magical. When the solar hits with any weapon, the weapon deals an extra 6d8 radiant damage (included in the attack).
        Divine Awareness. The solar knows if it hears a lie.
        Innate Spellcasting. The solar’s spell casting ability is Charisma (spell save DC 25). It can innately cast the following spells, requiring no material components:
        At will: detect evil and good, invisibility (self only)
        3/day each: blade barrier, dispel evil and good, resurrection
        1/day each: commune, control weather
        Magic Resistance. The solar has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        greatsword = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=4, dice_type=6, modifier=8, damage_type='slashing'), dice.DamageDice(dice_num=6, dice_type=8, modifier=0, damage_type='radiant')]), attack_mod=15, melee_range=5, name='Greatsword')
        slaying_longbow_range = attack_class.SaveOrDie(dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=6, dice_type=8, modifier=0, damage_type='radiant')]), attack_mod=13, range=150, name='Slaying Longbow_range', threshold=191, dc=15, save_type='constitution')
        slaying_longbow_disadvantage = attack_class.SaveOrDie(dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=6, dice_type=8, modifier=0, damage_type='radiant')]), attack_mod=13, range=600, name='Slaying Longbow_range_disadvantage', threshold=191, dc=15, save_type='constitution')
        The solar releases its greatsword to hover magically in an unoccupied space within 5 ft. of it. If the solar can see the sword, the solar can mentally command it as a bonus action to fly up to 50 ft. and either make one attack against a target or return to the solar’s hands. If the hovering sword is targeted by any effect, the solar is considered to be holding it. The hovering sword falls if the solar dies.
        The solar touches another creature. The target magically regains 40 (8d8 + 4) hit points and is freed from any curse, disease, poison, blindness, or deafness.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatsword, greatsword])
        """
        # Legendary Actions
        """
        The solar magically teleports, along with any equipment it is wearing or carrying, up to 120 ft. to an unoccupied space it can see.
        The solar emits magical, divine energy. Each creature of its choice in a 10 -foot radius must make a DC 23 Dexterity saving throw, taking 14 (4d6) fire damage plus 14 (4d6) radiant damage on a failed save, or half as much damage on a successful one.
        The solar targets one creature it can see within 30 ft. of it. If the target can see it, the target must succeed on a DC 15 Constitution saving throw or be blinded until magic such as the
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class AnimatedArmor(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: change two melee attacks to two slam attacks
        default_kwargs = {"size": "medium", "creature_type": "construct", 'ac': 18, 'max_hp': 33, 'speed': 25, 
            "strength": 14, "dexterity": 11, "constitution": 13, "intelligence": 1, "wisdom": 3, "charisma": 1}
        default_kwargs.update({"immunities": {'blinded', 'deafened', 'paralyzed', 'psychic', 'petrified', 'charmed', 'frightened', 'poison', 'poisoned', 'exhaustion'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        Antimagic Susceptibility. The armor is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the armor must succeed on a Constitution saving throw against the caster’s spell save DC or fall unconscious for 1 minute.
        False Appearance. While the armor remains motionless, it is indistinguishable from a normal suit of armor.
        """
        # Actions
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='bludgeoning'), attack_mod=4, melee_range=5, name='Slam')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        default_kwargs.update(attacks=[multiattack, slam])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ankheg(Creature):  # needs movement, needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration", 'ac': 14, 'max_hp': 39, 'speed': 30, 
            "strength": 17, "dexterity": 11, "constitution": 13, "intelligence": 1, "wisdom": 13, "charisma": 6}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='slashing'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='acid')]), attack_mod=5, melee_range=5, name='Bite')
            #  If the target is a Large or smaller creature, it is grappled (escape DC 13). Until this grapple ends, the ankheg can bite only the grappled creature and has advantage on attack rolls to do so.
        Acid Spray (Recharge 6). The ankheg spits acid in a line that is 30 ft. long and 5 ft. wide, provided that it has no creature grappled. Each creature in that line must make a DC 13 Dexterity saving throw, taking 10 (3d6) acid damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Basilisk(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 15, 'max_hp': 52, 'speed': 20, 
            "strength": 16, "dexterity": 8, "constitution": 15, "intelligence": 2, "wisdom": 8, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Petrifying Gaze. If a creature starts its turn within 30 ft. of the basilisk and the two of them can see each other, the basilisk can force the creature to make a DC 12 Constitution saving throw if the basilisk isn’t incapacitated. On a failed save, the creature magically begins to turn to stone and is restrained. It must repeat the saving throw at the end of its next turn. On a success, the effect ends. On a failure, the creature is petrified until freed by the greater restoration spell or other magic.
        A creature that isn’t surprised can avert its eyes to avoid the saving throw at the start of its turn. If it does so, it can’t see the basilisk until the start of its next turn, when it can avert its eyes again. If it looks at the basilisk in the meantime, it must immediately make the save.
        If the basilisk sees its reflection within 30 ft. of it in bright light, it mistakes itself for a rival and targets itself with its gaze.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=5, melee_range=5, name='Bite')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Behir(Creature):  # needs movement, needs grapple, needs recharge
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "monstrosity", 'ac': 17, 'max_hp': 168, 'speed': 50, 'climb_speed': 40, 
            "strength": 23, "dexterity": 16, "constitution": 18, "intelligence": 7, "wisdom": 14, "charisma": 12}
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=6, damage_type='piercing'), attack_mod=10, melee_range=10, name='Bite')
        constrict = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='bludgeoning'), dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='slashing')]), attack_mod=10, melee_range=5, name='Constrict')
            #  The target is grappled (escape DC 16) if the behir isn’t already constricting a creature, and the target is restrained until this grapple ends.
        Lightning Breath (Recharge 5-6). The behir exhales a line of lightning that is 20 ft. long and 5 ft. wide. Each creature in that line must make a DC 16 Dexterity saving throw, taking 66 (12d10) lightning damage on a failed save, or half as much damage on a successful one.
        Swallow. The behir makes one bite attack against a Medium or smaller target it is grappling. If the attack hits, the target is also swallowed, and the grapple ends. While swallowed, the target is blinded and restrained, it has total cover against attacks and other effects outside the behir, and it takes 21 (6d6) acid damage at the start of each of the behir’s turns. A behir can have only one creature swallowed at a time. <p>If the behir takes 30 damage or more on a single turn from the swallowed creature, the behir must succeed on a DC 14 Constitution saving throw at the end of that turn or regurgitate the creature, which falls prone in a space within 10 ft. of the behir. If the behir dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 15 ft. of movement, exiting prone.</p>
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, constrict])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class BlackPudding(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "ooze", 'ac': 7, 'max_hp': 85, 'speed': 20, 'climb_speed': 20, 
            "strength": 16, "dexterity": 5, "constitution": 16, "intelligence": 1, "wisdom": 6, "charisma": 1}
        default_kwargs.update({"immunities": {'prone', 'frightened', 'lightning', ' blinded', 'slashing', 'charmed', 'deafened', ' acid', 'exhaustion', 'cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 4})
        # Features
        """
        Amorphous. The pudding can move through a space as narrow as 1 inch wide without squeezing.
        Corrosive Form. A creature that touches the pudding or hits it with a melee attack while within 5 feet of it takes 4 (1d8) acid damage. Any nonmagical weapon made of metal or wood that hits the pudding corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition made of metal or wood that hits the pudding is destroyed after dealing damage. The pudding can eat through 2-inch-thick, nonmagical wood or metal in 1 round.
        Spider Climb. The pudding can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        """
        # Actions
        """
        pseudopod = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='bludgeoning'), dice.DamageDice(dice_num=4, dice_type=8, modifier=0, damage_type='acid')]), attack_mod=5, melee_range=5, name='Pseudopod')
            #  In addition, nonmagical armor worn by the target is partly dissolved and takes a permanent and cumulative -1 penalty to the AC it offers. The armor is destroyed if the penalty reduces its AC to 10.
        """
        # Reactions
        """
        Split. When a pudding that is Medium or larger is subjected to lightning or slashing damage, it splits into two new puddings if it has at least 10 hit points. Each new pudding has hit points equal to half the original pudding’s, rounded down. New puddings are one size smaller than the original pudding. 
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Bugbear(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (goblinoid)", 'ac': 16, 'max_hp': 27, 'speed': 30,
            "strength": 15, "dexterity": 14, "constitution": 13, "intelligence": 8, "wisdom": 11, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Brute. A melee weapon deals one extra die of its damage when the bugbear hits with it (included in the attack).
        Surprise Attack. If the bugbear surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 7 (2d6) damage from the attack.
        """
        # Actions
        """
        morningstar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Morningstar')
        javelin = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Javelin')
        javelin_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Javelin_range')
        javelin_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Javelin_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Bulette(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 17, 'max_hp': 94, 'speed': 40, 
            "strength": 19, "dexterity": 11, "constitution": 21, "intelligence": 2, "wisdom": 10, "charisma": 5}
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Standing Leap. The bulette’s long jump is up to 30 ft. and its high jump is up to 15 ft., with or without a running start.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=12, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
        Deadly Leap. If the bulette jumps at least 15 ft. as part of its movement, it can then use this action to land on its ft. in a space that contains one or more other creatures. Each of those creatures must succeed on a DC 16 Strength or Dexterity saving throw (target’s choice) or be knocked prone and take 14 (3d6 + 4) bludgeoning damage plus 14 (3d6 + 4) slashing damage. On a successful save, the creature takes only half the damage, isn’t knocked prone, and is pushed 5 ft. out of the bulette’s space into an unoccupied space of the creature’s choice. If no unoccupied space is within range, the creature instead falls prone in the bulette’s space.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Centaur(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 12, 'max_hp': 45, 'speed': 50, 
            "strength": 18, "dexterity": 14, "constitution": 14, "intelligence": 9, "wisdom": 13, "charisma": 11}
        default_kwargs.update({'vision': "normal", 'cr': 2})
        # Features
        """
        Charge. If the centaur moves at least 30 ft. straight toward a target and then hits it with a pike attack on the same turn, the target takes an extra 10 (3d6) piercing damage.
        """
        # Actions
        """
        pike = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=10, name='Pike')
        hooves = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Hooves')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=600, name='Longbow_range_disadvantage')
        multiattack_alt = attack_class.MultiAttack(name="Multiattack (alternate)", attack_list=[longbow, longbow]
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[pike, hooves])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Chimera(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 14, 'max_hp': 114, 'speed': 30, 'fly_speed': 60, 
            "strength": 19, "dexterity": 11, "constitution": 19, "intelligence": 3, "wisdom": 14, "charisma": 10}
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
        horns = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=12, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Horns')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claws')
        Fire Breath (Recharge 5-6). The dragon head exhales fire in a 15-foot cone. Each creature in that area must make a DC 15 Dexterity saving throw, taking 31 (7d8) fire damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, horns, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Chuul(Creature):  # needs grapple, needs poison
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration", 'ac': 16, 'max_hp': 93, 'speed': 30, 'swim_speed': 30, 
            "strength": 19, "dexterity": 10, "constitution": 16, "intelligence": 5, "wisdom": 11, "charisma": 5}
        default_kwargs.update({"immunities": {' poisoned', ' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 4})
        # Features
        """
        Amphibious. The chuul can breathe air and water.
        Sense Magic. The chuul senses magic within 120 feet of it at will. This trait otherwise works like the detect magic spell but isn’t itself magical.
        """
        # Actions
        """
        pincer = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=10, name='Pincer')
            #  The target is grappled (escape DC 14) if it is a Large or smaller creature and the chuul doesn’t have two other creatures grappled.
        Tentacles. One creature grappled by the chuul must succeed on a DC 13 Constitution saving throw or be poisoned for 1 minute. Until this poison ends, the target is paralyzed. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[pincer, pincer])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Cloaker(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration", 'ac': 14, 'max_hp': 78, 'speed': 10, 'fly_speed': 40, 
            "strength": 17, "dexterity": 15, "constitution": 12, "intelligence": 13, "wisdom": 12, "charisma": 14}
        default_kwargs.update({'vision': "darkvision", 'cr': 8})
        # Features
        """
        Damage Transfer. While attached to a creature, the cloaker takes only half the damage dealt to it (rounded down). and that creature takes the other half.
        False Appearance. While the cloaker remains motionless without its underside exposed, it is indistinguishable from a dark leather cloak.
        Light Sensitivity. While in bright light, the cloaker has disadvantage on attack rolls and Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
            # if the target is Large or smaller, the cloaker attaches to it. If the cloaker has advantage against the target, the cloaker attaches to the target’s head, and the target is blinded and unable to breathe while the cloaker is attached. While attached, the cloaker can make this attack only against the target and has advantage on the attack roll. The cloaker can detach itself by spending 5 feet of its movement. A creature, including the target, can take its action to detach the cloaker by succeeding on a DC 16 Strength check.
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=10, name='Tail')
        Moan. Each creature within 60 feet of the cloaker that can hear its moan and that isn’t an aberration must succeed on a DC 13 Wisdom saving throw or become frightened until the end of the cloaker’s next turn. If a creature’s saving throw is successful, the creature is immune to the cloaker’s moan for the next 24 hours.
        Phantasms (Recharges after a Short or Long Rest). The cloaker magically creates three illusory duplicates of itself if it isn’t in bright light. The duplicates move with it and mimic its actions, shifting position so as to make it impossible to track which cloaker is the real one. If the cloaker is ever in an area of bright light, the duplicates disappear. <p>Whenever any creature targets the cloaker with an attack or a harmful spell while a duplicate remains, that creature rolls randomly to determine whether it targets the cloaker or one of the duplicates. A creature is unaffected by this magical effect if it can’t see or if it relies on senses other than sight.</p> <p>A duplicate has the cloaker’s AC and uses its saving throws. If an attack hits a duplicate, or if a duplicate fails a saving throw against an effect that deals damage, the duplicate disappears.</p>
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, tail])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Cockatrice(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "monstrosity", 'ac': 11, 'max_hp': 27, 'speed': 20, 'fly_speed': 40, 
            "strength": 6, "dexterity": 12, "constitution": 12, "intelligence": 2, "wisdom": 13, "charisma": 5}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='piercing'), attack_mod=3, melee_range=5, name='Bite')
            # the target must succeed on a DC 11 Constitution saving throw against being magically petrified. On a failed save, the creature begins to turn to stone and is restrained. It must repeat the saving throw at the end of its next turn. On a success, the effect ends. On a failure, the creature is petrified for 24 hours.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Darkmantle(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "monstrosity", 'ac': 11, 'max_hp': 22, 'speed': 10, 'fly_speed': 30, 
            "strength": 16, "dexterity": 12, "constitution": 13, "intelligence": 2, "wisdom": 10, "charisma": 5}
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        Echolocation. The darkmantle can’t use its blindsight while deafened.
        False Appearance. While the darkmantle remains motionless, it is indistinguishable from a cave formation such as a stalactite or stalagmite.
        """
        # Actions
        """
        crush = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='bludgeoning'), attack_mod=5, melee_range=5, name='Crush')
            # the darkmantle attaches to the target. If the target is Medium or smaller and the darkmantle has advantage on the attack roll, it attaches by engulfing the target’s head, and the target is also blinded and unable to breathe while the darkmantle is attached in this way.While attached to the target, the darkmantle can attack no other creature except the target but has advantage on its attack rolls. The darkmantle’s speed also becomes 0, it can’t benefit from any bonus to its speed, and it moves with the target.A creature can detach the darkmantle by making a successful DC 13 Strength check as an action. On its turn, the darkmantle can detach itself from the target by using 5 feet of movement.
        Darkness Aura (1/day). A 15-foot radius of magical darkness extends out from the darkmantle, moves with it, and spreads around corners. The darkness lasts as long as the darkmantle maintains concentration, up to 10 minutes (as if concentrating on a spell). Darkvision can’t penetrate this darkness, and no natural light can illuminate it. If any of the darkness overlaps with an area of light created by a spell of 2nd level or lower, the spell creating the light is dispelled.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonBalor(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "fiend (demon)", 'ac': 19, 'max_hp': 262, 'speed': 40, 'fly_speed': 80, 
            "strength": 26, "dexterity": 15, "constitution": 22, "intelligence": 20, "wisdom": 16, "charisma": 22}
        default_kwargs.update({"proficiencies": {'charisma', 'strength', 'constitution', 'wisdom'}})
        default_kwargs.update({"resistances": {'piercing', 'slashing', ' cold', 'lightning', 'bludgeoning'}})
        default_kwargs.update({"immunities": {'poison', ' fire', ' poisoned'}})
        default_kwargs.update({'vision': "truesight", 'cr': 19})
        # Features
        """
        Death Throes. When the balor dies, it explodes, and each creature within 30 feet of it must make a DC 20 Dexterity saving throw, taking 70 (20d6) fire damage on a failed save, or half as much damage on a successful one. The explosion ignites flammable objects in that area that aren’t being worn or carried, and it destroys the balor’s weapons.
        Fire Aura. At the start of each of the balor’s turns, each creature within 5 feet of it takes 10 (3d6) fire damage, and flammable objects in the aura that aren’t being worn or carried ignite. A creature that touches the balor or hits it with a melee attack while within 5 feet of it takes 10 (3d6) fire damage.
        Magic Resistance. The balor has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The balor’s weapon attacks are magical.
        """
        # Actions
        """
        longsword = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=3, dice_type=8, modifier=8, damage_type='slashing'), dice.DamageDice(dice_num=3, dice_type=8, modifier=0, damage_type='lightning')]), attack_mod=14, melee_range=10, name='Longsword')
            #  If the balor scores a critical hit, it rolls damage dice three times, instead of twice.
        whip = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=14, melee_range=30, name='Whip')
            # the target must succeed on a DC 20 Strength saving throw or be pulled up to 25 feet toward the balor.
        Teleport. The balor magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[longsword, whip])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonDretch(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "fiend (demon)", 'ac': 11, 'max_hp': 18, 'speed': 20, 
            "strength": 11, "dexterity": 11, "constitution": 12, "intelligence": 5, "wisdom": 8, "charisma": 3}
        default_kwargs.update({"resistances": {' cold', 'lightning', 'fire'}})
        default_kwargs.update({"immunities": {' poisoned', ' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='piercing'), attack_mod=2, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=0, damage_type='slashing'), attack_mod=2, melee_range=5, name='Claws')
        Fetid Cloud (1/Day). A 10-foot radius of disgusting green gas extends out from the dretch. The gas spreads around corners, and its area is lightly obscured. It lasts for 1 minute or until a strong wind disperses it. Any creature that starts its turn in that area must succeed on a DC 11 Constitution saving throw or be poisoned until the start of its next turn. While poisoned in this way, the target can take either an action or a bonus action on its turn, not both, and can’t take reactions.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonGlabrezu(Creature):  # needs spell, needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (demon)", 'ac': 17, 'max_hp': 157, 'speed': 40, 
            "strength": 20, "dexterity": 15, "constitution": 21, "intelligence": 19, "wisdom": 17, "charisma": 16}
        default_kwargs.update({"proficiencies": {'strength', 'wisdom', 'constitution', 'charisma'}})
        default_kwargs.update({"resistances": {'fire', ' cold', 'bludgeoning', 'lightning', 'slashing', 'piercing'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "truesight", 'cr': 9})
        # Features
        """
        Innate Spellcasting. The glabrezu’s spellcasting ability is Intelligence (spell save DC 16). The glabrezu can innately cast the following spells, requiring no material components:
        At will: darkness, detect magic, dispel magic
        1/day each: confusion, fly, power word stun
        Magic Resistance. The glabrezu has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        pincer = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=5, damage_type='bludgeoning'), attack_mod=9, melee_range=10, name='Pincer')
            #  If the target is a Medium or smaller creature, it is grappled (escape DC 15). The glabrezu has two pincers, each of which can grapple only one target.
        fist = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=2, damage_type='bludgeoning'), attack_mod=9, melee_range=5, name='Fist')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[pincers, pincers, fists, fists])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonHezrou(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (demon)", 'ac': 16, 'max_hp': 136, 'speed': 30, 
            "strength": 19, "dexterity": 17, "constitution": 20, "intelligence": 5, "wisdom": 12, "charisma": 13}
        default_kwargs.update({"proficiencies": {'wisdom', 'strength', 'constitution'}})
        default_kwargs.update({"resistances": {'bludgeoning', ' cold', 'fire', 'lightning', 'piercing', 'slashing'}})
        default_kwargs.update({"immunities": {' poisoned', ' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 8})
        # Features
        """
        Magic Resistance. The hezrou has advantage on saving throws against spells and other magical effects.
        Stench. Any creature that starts its turn within 10 feet of the hezrou must succeed on a DC 14 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to the hezrou’s stench for 24 hours.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claws')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonMarilith(Creature):  # needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (demon)", 'ac': 18, 'max_hp': 189, 'speed': 40, 
            "strength": 18, "dexterity": 20, "constitution": 20, "intelligence": 18, "wisdom": 16, "charisma": 20}
        default_kwargs.update({"proficiencies": {'constitution', 'strength', 'charisma', 'wisdom'}})
        default_kwargs.update({"resistances": {'bludgeoning', 'lightning', 'fire', ' cold', 'slashing', 'piercing'}})
        default_kwargs.update({"immunities": {' poisoned', ' poison'}})
        default_kwargs.update({'vision': "truesight", 'cr': 16})
        # Features
        """
        Magic Resistance. The marilith has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The marilith’s weapon attacks are magical.
        Reactive. The marilith can take one reaction on every turn in combat.
        """
        # Actions
        """
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=9, melee_range=5, name='Longsword')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='bludgeoning'), attack_mod=9, melee_range=10, name='Tail')
            #  If the target is Medium or smaller, it is grappled (escape DC 19). Until this grapple ends, the target is restrained, the marilith can automatically hit the target with its tail, and the marilith can’t make tail attacks against other targets.
        Teleport. The marilith magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.
        """
        # Reactions
        """
        Parry. The marilith adds 5 to its AC against one melee attack that would hit it. To do so, the marilith must see the attacker and be wielding a melee weapon. 
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonNalfeshnee(Creature):  # needs movement, needs recharge
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (demon)", 'ac': 18, 'max_hp': 184, 'speed': 20, 'fly_speed': 30, 
            "strength": 21, "dexterity": 10, "constitution": 22, "intelligence": 19, "wisdom": 12, "charisma": 15}
        default_kwargs.update({"proficiencies": {'charisma', 'wisdom', 'intelligence', 'constitution'}})
        default_kwargs.update({"resistances": {'bludgeoning', 'piercing', 'lightning', ' cold', 'slashing', 'fire'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "truesight", 'cr': 13})
        # Features
        """
        Magic Resistance. The nalfeshnee has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=5, dice_type=10, modifier=5, damage_type='piercing'), attack_mod=10, melee_range=5, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=5, damage_type='slashing'), attack_mod=10, melee_range=10, name='Claw')
        Horror Nimbus (Recharge 5-6). The nalfeshnee magically emits scintillating, multicolored light. Each creature within 15 feet of the nalfeshnee that can see the light must succeed on a DC 15 Wisdom saving throw or be frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the nalfeshnee’s Horror Nimbus for the next 24 hours.
        Teleport. The nalfeshnee magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonQuasit(Creature):  # needs poison, needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "fiend (demon)", 'ac': 13, 'max_hp': 7, 'speed': 40, 
            "strength": 5, "dexterity": 17, "constitution": 10, "intelligence": 7, "wisdom": 10, "charisma": 10}
        default_kwargs.update({"resistances": {'piercing', 'lightning', 'bludgeoning', 'fire', ' cold', 'slashing'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Shapechanger. The quasit can use its action to polymorph into a beast form that resembles a bat (speed 10 ft. fly 40 ft.), a centipede (40 ft., climb 40 ft.), or a toad (40 ft., swim 40 ft.), or back into its true form . Its statistics are the same in each form, except for the speed changes noted. Any equipment it is wearing or carrying isn’t transformed . It reverts to its true form if it dies.
        Magic Resistance. The quasit has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        claw_bite_in_beast_form = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=3, damage_type='piercing'), attack_mod=4, melee_range=5, name='Claw (Bite in Beast Form)')
            # the target must succeed on a DC 10 Constitution saving throw or take 5 (2d4) poison damage and become poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        Scare (1/day). One creature of the quasit’s choice within 20 ft. of it must succeed on a DC 10 Wisdom saving throw or be frightened for 1 minute. The target can repeat the saving throw at the end of each of its turns, with disadvantage if the quasit is within line of sight, ending the effect on itself on a success.
        Invisibility. The quasit magically turns invisible until it attacks or uses Scare, or until its concentration ends (as if concentrating on a spell). Any equipment the quasit wears or carries is invisible with it.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DemonVrock(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (demon)", 'ac': 15, 'max_hp': 104, 'speed': 40, 'fly_speed': 60, 
            "strength": 17, "dexterity": 15, "constitution": 18, "intelligence": 8, "wisdom": 13, "charisma": 8}
        default_kwargs.update({"proficiencies": {'wisdom', 'charisma', 'dexterity'}})
        default_kwargs.update({"resistances": {'lightning', 'slashing', 'bludgeoning', 'fire', ' cold', 'piercing'}})
        default_kwargs.update({"immunities": {' poisoned', ' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        Magic Resistance. The vrock has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Beak')
        talons = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=5, name='Talons')
        Spores (Recharge 6). A 15-foot-radius cloud of toxic spores extends out from the vrock. The spores spread around corners. Each creature in that area must succeed on a DC 14 Constitution saving throw or become poisoned. While poisoned in this way, a target takes 5 (1d10) poison damage at the start of each of its turns. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Emptying a vial of holy water on the target also ends the effect on it.
        Stunning Screech (1/Day). The vrock emits a horrific screech. Each creature within 20 feet of it that can hear it and that isn’t a demon must succeed on a DC 14 Constitution saving throw or be stunned until the end of the vrock’s next turn .
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beak, talons])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilBarbed(Creature):  # needs work
    def __init__(self, **kwargs):  # errata: scraper missed some of the description of Hurl Flame
        default_kwargs = {"size": "medium", "creature_type": "fiend (devil)", 'ac': 15, 'max_hp': 110, 'speed': 30, 
            "strength": 16, "dexterity": 17, "constitution": 18, "intelligence": 12, "wisdom": 14, "charisma": 14}
        default_kwargs.update({"proficiencies": {'wisdom', 'charisma', 'constitution', 'strength'}})
        default_kwargs.update({"resistances": {'piercing', 'bludgeoning', ' cold', "slashing that aren't silvered"}})
        default_kwargs.update({"immunities": {' poisoned', 'poison', ' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Barbed Hide. At the start of each of its turns, the barbed devil deals 5 (1d10) piercing damage to any creature grappling it.
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Tail')
        Hurl Flame. Ranged Spell Attack: +5 to hit, range 150 ft., one target.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[tail, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilBearded(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend (devil)", 'ac': 13, 'max_hp': 52, 'speed': 30, 
            "strength": 16, "dexterity": 15, "constitution": 15, "intelligence": 9, "wisdom": 11, "charisma": 11}
        default_kwargs.update({"proficiencies": {'constitution', 'wisdom', 'strength'}})
        default_kwargs.update({"resistances": {' cold', 'bludgeoning', "slashing that aren't silvered", 'piercing'}})
        default_kwargs.update({"immunities": {'poison', ' poisoned', ' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        Steadfast. The devil can’t be frightened while it can see an allied creature within 30 feet of it.
        """
        # Actions
        """
        beard = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=5, melee_range=5, name='Beard')
            # the target must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, the target can’t regain hit points. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        glaive = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=10, name='Glaive')
            #  If the target is a creature other than an undead or a construct, it must succeed on a DC 12 Constitution saving throw or lose 5 (1d10) hit points at the start of each of its turns due to an infernal wound. Each time the devil hits the wounded target with this attack, the damage dealt by the wound increases by 5 (1d10). Any creature can take an action to stanch the wound with a successful DC 12 Wisdom (Medicine) check. The wound also closes if the target receives magical healing.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beard, glaive])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilBone(Creature):  # needs poison
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (devil)", 'ac': 19, 'max_hp': 142, 'speed': 40, 'fly_speed': 40, 
            "strength": 18, "dexterity": 16, "constitution": 18, "intelligence": 13, "wisdom": 14, "charisma": 16}
        default_kwargs.update({"proficiencies": {'intelligence', 'charisma', 'wisdom'}})
        default_kwargs.update({"resistances": {"slashing that aren't silvered", 'bludgeoning', 'piercing', ' cold'}})
        default_kwargs.update({"immunities": {' poisoned', 'poison', ' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 12})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=8, melee_range=10, name='Claw')
        sting = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=5, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=8, melee_range=10, name='Sting')
            # the target must succeed on a DC 14 Constitution saving throw or become poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success .
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[claw, claw, sting])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilChain(Creature):  # needs grapple, needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend (devil)", 'ac': 16, 'max_hp': 85, 'speed': 30, 
            "strength": 18, "dexterity": 15, "constitution": 18, "intelligence": 11, "wisdom": 12, "charisma": 14}
        default_kwargs.update({"resistances": {'piercing', ' cold', "slashing that aren't silvered", 'bludgeoning'}})
        default_kwargs.update({"immunities": {'poison', ' fire', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        chain = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=8, melee_range=10, name='Chain')
            #  The target is grappled (escape DC 14) if the devil isn’t already grappling a creature. Until this grapple ends, the target is restrained and takes 7 (2d6) piercing damage at the start of each of its turns.
        """
        # Reactions
        """
        Unnerving Mask. When a creature the devil can see starts its turn within 30 feet of the devil, the devil can create the illusion that it looks like one of the creature’s departed loved ones or bitter enemies. If the creature can see the devil, it must succeed on a DC 14 Wisdom saving throw or be frightened until the end of its turn.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilErinyes(Creature):  # needs poison
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend (devil)", 'ac': 18, 'max_hp': 153, 'speed': 30, 'fly_speed': 60, 
            "strength": 18, "dexterity": 16, "constitution": 18, "intelligence": 14, "wisdom": 14, "charisma": 18}
        default_kwargs.update({"proficiencies": {'constitution', 'wisdom', 'charisma', 'dexterity'}})
        default_kwargs.update({"resistances": {"slashing", ' cold', 'bludgeoning', 'piercing'}})
        default_kwargs.update({"immunities": {' fire', ' poisoned', 'poison'}})
        default_kwargs.update({'vision': "truesight", 'cr': 12})
        # Features
        """
        Hellish Weapons. The erinyes’s weapon attacks are magical and deal an extra 13 (3d8) poison damage on a hit (included in the attacks).
        Magic Resistance. The erinyes has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        longsword_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=4, damage_type='slashing'), attack_mod=8, melee_range=5, name='Longsword_versatile')
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=8, melee_range=5, name='Longsword')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=7, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=7, range=600, name='Longbow_range_disadvantage')
            # the target must succeed on a DC 14 Constitution saving throw or be poisoned. The poison lasts until it is removed by the lesser restoration spell or similar magic.
        """
        # Reactions
        """
        Parry. The erinyes adds 4 to its AC against one melee attack that would hit it. To do so, the erinyes must see the attacker and be wielding a melee weapon. 
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilHorned(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (devil)", 'ac': 18, 'max_hp': 148, 'speed': 20, 'fly_speed': 60, 
            "strength": 22, "dexterity": 17, "constitution": 21, "intelligence": 12, "wisdom": 16, "charisma": 17}
        default_kwargs.update({"proficiencies": {'strength', 'charisma', 'wisdom', 'dexterity'}})
        default_kwargs.update({"resistances": {"slashing", ' cold', 'piercing', 'bludgeoning'}})
        default_kwargs.update({"immunities": {' poisoned', 'poison', ' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        fork = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='piercing'), attack_mod=10, melee_range=10, name='Fork')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=6, damage_type='piercing'), attack_mod=10, melee_range=10, name='Tail')
            #  If the target is a creature other than an undead or a construct, it must succeed on a DC 17 Constitution saving throw or lose 10 (3d6) hit points at the start of each of its turns due to an infernal wound. Each time the devil hits the wounded target with this attack, the damage dealt by the wound increases by 10 (3d6). Any creature can take an action to stanch the wound with a successful DC 12 Wisdom (Medicine) check. The wound also closes if the target receives magical healing.
        Hurl Flame. Ranged Spell Attack: +7 to hit, range 150 ft., one target. Hit: 14 (4d6) fire damage. If the target is a flammable object that isn’t being worn or carried, it also catches fire.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[fork, fork, tail])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilIce(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (devil)", 'ac': 18, 'max_hp': 180, 'speed': 40, 
            "strength": 21, "dexterity": 14, "constitution": 18, "intelligence": 18, "wisdom": 15, "charisma": 18}
        default_kwargs.update({"proficiencies": {'wisdom', 'dexterity', 'constitution', 'charisma'}})
        default_kwargs.update({"resistances": {' bludgeoning', 'piercing', "slashing that aren't silvered"}})
        default_kwargs.update({"immunities": {'poison', ' poisoned', ' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 14})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the devil’s darkvision.
        Magic Resistance. The devil has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='cold')]), attack_mod=10, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=4, modifier=5, damage_type='slashing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='cold')]), attack_mod=10, melee_range=5, name='Claws')
        tail = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='bludgeoning'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='cold')]), attack_mod=10, melee_range=10, name='Tail')
        Wall of Ice. The devil magically forms an opaque wall of ice on a solid surface it can see within 60 feet of it. The wall is 1 foot thick and up to 30 feet long and 10 feet high, or it’s a hemispherical dome up to 20 feet in diameter. When the wall appears, each creature in its space is pushed out of it by the shortest route. The creature chooses which side of the wall to end up on, unless the creature is incapacitated. The creature then makes a DC 17 Dexterity saving throw, taking 35 (10d6) cold damage on a failed save, or half as much damage on a successful one. The wall lasts for 1 minute or until the devil is incapacitated or dies. The wall can be damaged and breached; each 10-foot section has AC 5, 30 hit points, vulnerability to fire damage, and immunity to acid, cold, necrotic, poison, and psychic damage. If a section is destroyed, it leaves behind a sheet of frigid air in the space the wall occupied. Whenever a creature finishes moving through the frigid air on a turn, willingly or otherwise, the creature must make a DC 17 Constitution saving throw, taking 17 (5d6) cold damage on a failed save, or half as much damage on a successful one. The frigid air dissipates when the rest of the wall vanishes.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, tail])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilImp(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "fiend (devil)", 'ac': 13, 'max_hp': 10, 'speed': 20, 'fly_speed': 40,
            "strength": 6, "dexterity": 17, "constitution": 13, "intelligence": 11, "wisdom": 12, "charisma": 14}
        default_kwargs.update({"resistances": {'piercing', ' cold', 'bludgeoning', 'and slashing from nonmagical/nonsilver weapons'}})
        default_kwargs.update({"immunities": {' poisoned', ' fire', 'poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Shapechanger. The imp can use its action to polymorph into a beast form that resembles a rat (speed 20 ft.), a raven (20 ft., fly 60 ft.), or a spider (20 ft., climb 20 ft.), or back into its true form. Its statistics are the same in each form, except for the speed changes noted. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Devil’s Sight. Magical darkness doesn’t impede the imp’s darkvision.
        Magic Resistance. The imp has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        sting_bite_in_beast_form = attack_class.HitAndSaveAttack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Sting (Bite in Beast Form)', dc=11, save_type='constitution', save_damage_dice='3d6', save_damage_type='poison', damage_on_success=True)
        Invisibility. The imp magically turns invisible until it attacks, or until its concentration ends (as if concentrating on a spell). Any equipment the imp wears or carries is invisible with it.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilLemure(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend (devil)", 'ac': 7, 'max_hp': 13, 'speed': 15, 
            "strength": 10, "dexterity": 5, "constitution": 11, "intelligence": 1, "wisdom": 11, "charisma": 3}
        default_kwargs.update({"resistances": {' cold'}})
        default_kwargs.update({"immunities": {'poison', 'poisoned', 'frightened', ' charmed', ' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 0})
        # Features
        """
        Devil’s Sight. Magical darkness doesn’t impede the lemure’s darkvision.
        Hellish Rejuvenation. A lemure that dies in the Nine Hells comes back to life with all its hit points in 1d10 days unless it is killed by a good-aligned creature with a bless spell cast on that creature or its remains are sprinkled with holy water.
        """
        # Actions
        """
        fist = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='bludgeoning'), attack_mod=3, melee_range=5, name='Fist')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DevilPitFiend(Creature):  # needs poison, needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend (devil)", 'ac': 19, 'max_hp': 300, 'speed': 30, 'fly_speed': 60, 
            "strength": 26, "dexterity": 14, "constitution": 24, "intelligence": 22, "wisdom": 18, "charisma": 24}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity'}})
        default_kwargs.update({"resistances": {'piercing', "slashing that aren't silvered", 'bludgeoning', ' cold'}})
        default_kwargs.update({"immunities": {'poison', ' fire', ' poisoned'}})
        default_kwargs.update({'vision': "truesight", 'cr': 20})
        # Features
        """
        Fear Aura. Any creature hostile to the pit fiend that starts its turn within 20 feet of the pit fiend must make a DC 21 Wisdom saving throw, unless the pit fiend is incapacitated. On a failed save, the creature is frightened until the start of its next turn. If a creature’s saving throw is successful, the creature is immune to the pit fiend’s Fear Aura for the next 24 hours.
        Magic Resistance. The pit fiend has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The pit fiend’s weapon attacks are magical.
        Innate Spellcasting. The pit fiend’s spellcasting ability is Charisma (spell save DC 21). The pit fiend can innately cast the following spells, requiring no material components:
        At will: detect magic, fireball
        3/day each: hold monster, wall of fire
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=6, modifier=8, damage_type='piercing'), attack_mod=14, melee_range=5, name='Bite')
            #  The target must succeed on a DC 21 Constitution saving throw or become poisoned. While poisoned in this way, the target can’t regain hit points, and it takes 21 (6d6) poison damage at the start of each of its turns. The poisoned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='slashing'), attack_mod=14, melee_range=10, name='Claw')
        mace = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='bludgeoning'), dice.DamageDice(dice_num=6, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=14, melee_range=10, name='Mace')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=8, damage_type='bludgeoning'), attack_mod=14, melee_range=10, name='Tail')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Doppelganger(Creature):  # needs work
    def __init__(self, **kwargs):  # errata: two melee should be two slam
        default_kwargs = {"size": "medium", "creature_type": "monstrosity (shapechanger)", 'ac': 14, 'max_hp': 52, 'speed': 30, 
            "strength": 11, "dexterity": 18, "constitution": 14, "intelligence": 11, "wisdom": 12, "charisma": 14}
        default_kwargs.update({"immunities": {' charmed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Shapechanger. The doppelganger can use its action to polymorph into a Small or Medium humanoid it has seen, or back into its true form. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Ambusher. The doppelganger has advantage on attack rolls against any creature it has surprised.
        Surprise Attack. If the doppelganger surprises a creature and hits it with an attack during the first round of combat, the target takes an extra 10 (3d6) damage from the attack.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Slam')
        Read Thoughts. The doppelganger magically reads the surface thoughts of one creature within 60 ft. of it. The effect can penetrate barriers, but 3 ft. of wood or dirt, 2 ft. of stone, 2 inches of metal, or a thin sheet of lead blocks it. While the target is in range, the doppelganger can continue reading its thoughts, as long as the doppelganger’s concentration isn’t broken (as if concentrating on a spell). While reading the target’s mind, the doppelganger has advantage on Wisdom (Insight) and Charisma (Deception, Intimidation, and Persuasion) checks against the target.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlackAncient(Creature):  # needs recharge
    def __init__(self, **kwargs):  # errata: multiattack starts with Frightful Presence (optional)
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 367, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 27, "dexterity": 14, "constitution": 25, "intelligence": 15, "wisdom": 15, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'dexterity', 'wisdom', 'charisma'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 21})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=8, modifier=0, damage_type='acid')]), attack_mod=15, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=15, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=15, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 19 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Acid Breath (Recharge 5-6). The dragon exhales acid in a 90-foot line that is 10 feet wide. Each creature in that line must make a DC 22 Dexterity saving throw, taking 67 (15d8) acid damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claw, claw])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 23 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlackAdult(Creature):
    def __init__(self, **kwargs):  # errata: improper formatting of saving throws, multiattack starts with frightful presence
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 195, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 23, "dexterity": 14, "constitution": 21, "intelligence": 14, "wisdom": 13, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'wisdom', 'dexterity'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 14})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='acid')]), attack_mod=11, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=11, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=11, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Acid Breath (Recharge 5-6). The dragon exhales acid in a 60-foot line that is 5 feet wide. Each creature in that line must make a DC 18 Dexterity saving throw, taking 54 (12d8) acid damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, 'claws', 'claws'])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 19 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlackYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 127, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 19, "dexterity": 14, "constitution": 17, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'charisma', 'dexterity'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 7})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='acid')]), attack_mod=7, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        Acid Breath (Recharge 5-6). The dragon exhales acid in a 30-foot line that is 5 feet wide. Each creature in that line must make a DC 14 Dexterity saving throw, taking 49 (11d8) acid damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlackWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 33, 'speed': 30, 'fly_speed': 60, 'swim_speed': 30, 
            "strength": 15, "dexterity": 14, "constitution": 13, "intelligence": 10, "wisdom": 11, "charisma": 13}
        default_kwargs.update({"proficiencies": {'wisdom', 'dexterity', 'charisma', 'constitution'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='acid')]), attack_mod=4, melee_range=5, name='Bite')
        Acid Breath (Recharge 5-6). The dragon exhales acid in a 15-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 22 (5d8) acid damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlueAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 481, 'speed': 40,
            "strength": 29, "dexterity": 10, "constitution": 27, "intelligence": 18, "wisdom": 17, "charisma": 21}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 23})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=9, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=10, modifier=0, damage_type='lightning')]), attack_mod=16, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=9, damage_type='slashing'), attack_mod=16, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=9, damage_type='bludgeoning'), attack_mod=16, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 20 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Lightning Breath (Recharge 5-6). The dragon exhales lightning in a 120-foot line that is 10 feet wide. Each creature in that line must make a DC 23 Dexterity saving throw, taking 88 (16d10) lightning damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 24 Dexterity saving throw or take 16 (2d6 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlueAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 225, 'speed': 40, 
            "strength": 25, "dexterity": 10, "constitution": 23, "intelligence": 16, "wisdom": 15, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 16})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=7, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=10, modifier=0, damage_type='lightning')]), attack_mod=12, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=7, damage_type='slashing'), attack_mod=12, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=7, damage_type='bludgeoning'), attack_mod=12, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 ft. of the dragon and aware of it must succeed on a DC 17 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Lightning Breath (Recharge 5-6). The dragon exhales lightning in a 90-foot line that is 5 ft. wide. Each creature in that line must make a DC 19 Dexterity saving throw, taking 66 (12d10) lightning damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 20 Dexterity saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlueYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 152, 'speed': 40, 
            "strength": 21, "dexterity": 10, "constitution": 19, "intelligence": 14, "wisdom": 13, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 9})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=5, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=10, modifier=0, damage_type='lightning')]), attack_mod=9, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='slashing'), attack_mod=9, melee_range=5, name='Claw')
        Lightning Breath (Recharge 5-6). The dragon exhales lightning in an 60-foot line that is 5 feet wide. Each creature in that line must make a DC 16 Dexterity saving throw, taking 55 (10d10) lightning damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBlueWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 52, 'speed': 30,
            "strength": 17, "dexterity": 10, "constitution": 15, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 3})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='lightning')]), attack_mod=5, melee_range=5, name='Bite')
        Lightning Breath (Recharge 5-6). The dragon exhales lightning in a 30-foot line that is 5 feet wide. Each creature in that line must make a DC 12 Dexterity saving throw, taking 22 (4d10) lightning damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBrassAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 20, 'max_hp': 297, 'speed': 40,
            "strength": 27, "dexterity": 10, "constitution": 25, "intelligence": 16, "wisdom": 15, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 20})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), attack_mod=14, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=14, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=14, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons: Fire Breath. The dragon exhales fire in an 90-foot line that is 10 feet wide. Each creature in that line must make a DC 21 Dexterity saving throw, taking 56 (16d6) fire damage on a failed save, or half as much damage on a successful one. Sleep Breath. The dragon exhales sleep gas in a 90-foot cone. Each creature in that area must succeed on a DC 21 Constitution saving throw or fall unconscious for 10 minutes. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.
        Change Shape. The dragon magically polymorphs into a humanoid or beast that has a challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon’s choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 22 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBrassAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 18, 'max_hp': 172, 'speed': 40, 
            "strength": 23, "dexterity": 10, "constitution": 21, "intelligence": 14, "wisdom": 13, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 13})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=11, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=11, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in an 60-foot line that is 5 feet wide. Each creature in that line must make a DC 18 Dexterity saving throw, taking 45 (13d6) fire damage on a failed save, or half as much damage on a successful one. Sleep Breath. The dragon exhales sleep gas in a 60-foot cone. Each creature in that area must succeed on a DC 18 Constitution saving throw or fall unconscious for 10 minutes. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)


class DragonBrassYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 17, 'max_hp': 110, 'speed': 40, 
            "strength": 19, "dexterity": 10, "constitution": 17, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 6})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in a 40-foot line that is 5 feet wide. Each creature in that line must make a DC 14 Dexterity saving throw, taking 42 (12d6) fire damage on a failed save, or half as much damage on a successful one. Sleep Breath. The dragon exhales sleep gas in a 30-foot cone. Each creature in that area must succeed on a DC 14 Constitution saving throw or fall unconscious for 5 minutes. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBrassWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 16, 'max_hp': 16, 'speed': 30,
            "strength": 15, "dexterity": 10, "constitution": 13, "intelligence": 10, "wisdom": 11, "charisma": 13}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in an 20-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 14 (4d6) fire damage on a failed save, or half as much damage on a successful one. Sleep Breath. The dragon exhales sleep gas in a 15-foot cone. Each creature in that area must succeed on a DC 11 Constitution saving throw or fall unconscious for 1 minute. This effect ends for a creature if the creature takes damage or someone uses an action to wake it.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBronzeAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 444, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40,
            "strength": 29, "dexterity": 10, "constitution": 27, "intelligence": 18, "wisdom": 17, "charisma": 21}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 22})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=9, damage_type='piercing'), attack_mod=16, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=9, damage_type='slashing'), attack_mod=16, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=9, damage_type='bludgeoning'), attack_mod=16, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 20 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Lightning Breath. The dragon exhales lightning in a 120-foot line that is 10 feet wide. Each creature in that line must make a DC 23 Dexterity saving throw, taking 88 (16d10) lightning damage on a failed save, or half as much damage on a successful one. Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 23 Strength saving throw. On a failed save, the creature is pushed 60 feet away from the dragon.
        Change Shape. The dragon magically polymorphs into a humanoid or beast that has a challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon’s choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 24 Dexterity saving throw or take 16 (2d6 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBronzeAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 212, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 25, "dexterity": 10, "constitution": 23, "intelligence": 16, "wisdom": 15, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 15})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=7, damage_type='piercing'), attack_mod=12, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=7, damage_type='slashing'), attack_mod=12, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=7, damage_type='bludgeoning'), attack_mod=12, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 17 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Lightning Breath. The dragon exhales lightning in a 90-foot line that is 5 feet wide. Each creature in that line must make a DC 19 Dexterity saving throw, taking 66 (12d10) lightning damage on a failed save, or half as much damage on a successful one. Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 19 Strength saving throw. On a failed save, the creature is pushed 60 feet away from the dragon.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 20 Dexterity saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBronzeYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 142, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 21, "dexterity": 10, "constitution": 19, "intelligence": 14, "wisdom": 13, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 8})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=5, damage_type='piercing'), attack_mod=8, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='slashing'), attack_mod=8, melee_range=5, name='Claw')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Lightning Breath. The dragon exhales lightning in a 60-foot line that is 5 feet wide. Each creature in that line must make a DC 15 Dexterity saving throw, taking 55 (10d10) lightning damage on a failed save, or half as much damage on a successful one. Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 15 Strength saving throw. On a failed save, the creature is pushed 40 feet away from the dragon.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonBronzeWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 32, 'speed': 30, 'fly_speed': 60, 'swim_speed': 30,
            "strength": 17, "dexterity": 10, "constitution": 15, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Bite')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Lightning Breath. The dragon exhales lightning in a 40-foot line that is 5 feet wide. Each creature in that line must make a DC 12 Dexterity saving throw, taking 16 (3d10) lightning damage on a failed save, or half as much damage on a successful one. Repulsion Breath. The dragon exhales repulsion energy in a 30-foot cone. Each creature in that area must succeed on a DC 12 Strength saving throw. On a failed save, the creature is pushed 30 feet away from the dragon.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonCopperAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 21, 'max_hp': 350, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80, 
            "strength": 27, "dexterity": 12, "constitution": 25, "intelligence": 20, "wisdom": 17, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 21})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), attack_mod=15, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=15, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=15, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 19 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Acid Breath. The dragon exhales acid in an 90-foot line that is 10 feet wide. Each creature in that line must make a DC 22 Dexterity saving throw, taking 63 (14d8) acid damage on a failed save, or half as much damage on a successful one. Slowing Breath. The dragon exhales gas in a 90-foot cone. Each creature in that area must succeed on a DC 22 Constitution saving throw. On a failed save, the creature can’t use reactions, its speed is halved, and it can’t make more than one attack on its turn. In addition, the creature can use either an action or a bonus action on its turn, but not both. These effects last for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.
        Change Shape. The dragon magically polymorphs into a humanoid or beast that has a challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon’s choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 23 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonCopperAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 18, 'max_hp': 184, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80,
            "strength": 23, "dexterity": 12, "constitution": 21, "intelligence": 18, "wisdom": 15, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 14})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), attack_mod=11, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=11, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=11, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Acid Breath. The dragon exhales acid in an 60-foot line that is 5 feet wide. Each creature in that line must make a DC 18 Dexterity saving throw, taking 54 (12d8) acid damage on a failed save, or half as much damage on a successful one. Slowing Breath. The dragon exhales gas in a 60-foot cone. Each creature in that area must succeed on a DC 18 Constitution saving throw. On a failed save, the creature can’t use reactions, its speed is halved, and it can’t make more than one attack on its turn. In addition, the creature can use either an action or a bonus action on its turn, but not both. These effects last for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 19 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonCopperYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 17, 'max_hp': 119, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80,
            "strength": 19, "dexterity": 12, "constitution": 17, "intelligence": 16, "wisdom": 13, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 7})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Acid Breath. The dragon exhales acid in an 40-foot line that is 5 feet wide. Each creature in that line must make a DC 14 Dexterity saving throw, taking 40 (9d8) acid damage on a failed save, or half as much damage on a successful one. Slowing Breath. The dragon exhales gas in a 30-foot cone. Each creature in that area must succeed on a DC 14 Constitution saving throw. On a failed save, the creature can’t use reactions, its speed is halved, and it can’t make more than one attack on its turn. In addition, the creature can use either an action or a bonus action on its turn, but not both. These effects last for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonCopperWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 16, 'max_hp': 22, 'speed': 30, 'climb_speed': 30, 'fly_speed': 60, 
            "strength": 15, "dexterity": 12, "constitution": 13, "intelligence": 14, "wisdom": 11, "charisma": 13}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' acid'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Acid Breath. The dragon exhales acid in an 20-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 18 (4d8) acid damage on a failed save, or half as much damage on a successful one. Slowing Breath. The dragon exhales gas in a 1 5-foot cone. Each creature in that area must succeed on a DC 11 Constitution saving throw. On a failed save, the creature can’t use reactions, its speed is halved, and it can’t make more than one attack on its turn. In addition, the creature can use either an action or a bonus action on its turn, but not both. These effects last for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself with a successful save.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGoldAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 546, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40,
            "strength": 30, "dexterity": 14, "constitution": 29, "intelligence": 18, "wisdom": 17, "charisma": 28}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 24})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=10, damage_type='piercing'), attack_mod=17, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=10, damage_type='slashing'), attack_mod=17, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=10, damage_type='bludgeoning'), attack_mod=17, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 24 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in a 90-foot cone. Each creature in that area must make a DC 24 Dexterity saving throw, taking 71 (13d10) fire damage on a failed save, or half as much damage on a successful one. Weakening Breath. The dragon exhales gas in a 90-foot cone. Each creature in that area must succeed on a DC 24 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        Change Shape. The dragon magically polymorphs into a humanoid or beast that has a challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon’s choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 25 Dexterity saving throw or take 17 (2d6 + 10) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGoldAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 256, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 27, "dexterity": 14, "constitution": 25, "intelligence": 16, "wisdom": 15, "charisma": 24}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 17})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), attack_mod=14, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=14, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=14, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in a 60-foot cone. Each creature in that area must make a DC 21 Dexterity saving throw, taking 66 (12d10) fire damage on a failed save, or half as much damage on a successful one. Weakening Breath. The dragon exhales gas in a 60-foot cone. Each creature in that area must succeed on a DC 21 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 22 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGoldYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 178, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40,
            "strength": 23, "dexterity": 14, "constitution": 21, "intelligence": 16, "wisdom": 13, "charisma": 20}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 10})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), attack_mod=10, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=10, melee_range=5, name='Claw')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in a 30-foot cone. Each creature in that area must make a DC 17 Dexterity saving throw, taking 55 (10d10) fire damage on a failed save, or half as much damage on a successful one. Weakening Breath. The dragon exhales gas in a 30-foot cone. Each creature in that area must succeed on a DC 17 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGoldWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 60, 'speed': 30, 'fly_speed': 60, 'swim_speed': 30, 
            "strength": 19, "dexterity": 14, "constitution": 17, "intelligence": 14, "wisdom": 11, "charisma": 16}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 3})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Fire Breath. The dragon exhales fire in a 15-foot cone. Each creature in that area must make a DC 13 Dexterity saving throw, taking 22 (4d10) fire damage on a failed save, or half as much damage on a successful one. Weakening Breath. The dragon exhales gas in a 15-foot cone. Each creature in that area must succeed on a DC 13 Strength saving throw or have disadvantage on Strength-based attack rolls, Strength checks, and Strength saving throws for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGreenAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 21, 'max_hp': 385, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 27, "dexterity": 12, "constitution": 25, "intelligence": 20, "wisdom": 17, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 22})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=15, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=15, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=15, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 19 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Poison Breath (Recharge 5-6). The dragon exhales poisonous gas in a 90-foot cone. Each creature in that area must make a DC 22 Constitution saving throw, taking 77 (22d6) poison damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 23 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGreenAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 207, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40,
            "strength": 23, "dexterity": 12, "constitution": 21, "intelligence": 18, "wisdom": 15, "charisma": 17}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 15})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=11, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=11, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=11, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours .
        Poison Breath (Recharge 5-6). The dragon exhales poisonous gas in a 60-foot cone. Each creature in that area must make a DC 18 Constitution saving throw, taking 56 (16d6) poison damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 19 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGreenYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 136, 'speed': 40, 'fly_speed': 80, 'swim_speed': 40, 
            "strength": 19, "dexterity": 12, "constitution": 17, "intelligence": 16, "wisdom": 13, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 8})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=7, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        Poison Breath (Recharge 5-6). The dragon exhales poisonous gas in a 30-foot cone. Each creature in that area must make a DC 14 Constitution saving throw, taking 42 (12d6) poison damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonGreenWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 38, 'speed': 30, 'fly_speed': 60, 'swim_speed': 30,
            "strength": 15, "dexterity": 12, "constitution": 13, "intelligence": 14, "wisdom": 11, "charisma": 13}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Amphibious. The dragon can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=4, melee_range=5, name='Bite')
        Poison Breath (Recharge 5-6). The dragon exhales poisonous gas in a 15-foot cone. Each creature in that area must make a DC 11 Constitution saving throw, taking 21 (6d6) poison damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonRedAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 546, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80, 
            "strength": 30, "dexterity": 10, "constitution": 29, "intelligence": 18, "wisdom": 15, "charisma": 23}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 24})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=10, damage_type='piercing'), dice.DamageDice(dice_num=4, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=17, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=10, damage_type='slashing'), attack_mod=17, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=10, damage_type='bludgeoning'), attack_mod=17, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Fire Breath (Recharge 5-6). The dragon exhales fire in a 90-foot cone. Each creature in that area must make a DC 24 Dexterity saving throw, taking 91 (26d6) fire damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 25 Dexterity saving throw or take 17 (2d6 + 10) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonRedAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 256, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80,
            "strength": 27, "dexterity": 10, "constitution": 25, "intelligence": 16, "wisdom": 13, "charisma": 21}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 17})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=14, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=14, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=14, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 ft. of the dragon and aware of it must succeed on a DC 19 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Fire Breath (Recharge 5-6). The dragon exhales fire in a 60-foot cone. Each creature in that area must make a DC 21 Dexterity saving throw, taking 63 (18d6) fire damage on a failed save, or half as much damage on a successful one.
        Lair Actions. On initiative count 20 (losing initiative ties), the dragon takes a lair action to cause one of the following effects: the dragon can’t use the same effect two rounds in a row:
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 22 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonRedYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 178, 'speed': 40, 'climb_speed': 40, 'fly_speed': 80, 
            "strength": 23, "dexterity": 10, "constitution": 21, "intelligence": 14, "wisdom": 11, "charisma": 19}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 10})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=10, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=10, melee_range=5, name='Claw')
        Fire Breath (Recharge 5-6). The dragon exhales fire in a 30-foot cone. Each creature in that area must make a DC 17 Dexterity saving throw, taking 56 (16d6) fire damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonRedWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 75, 'speed': 30, 'climb_speed': 30, 'fly_speed': 60,
            "strength": 19, "dexterity": 10, "constitution": 17, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 4})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=10, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=6, melee_range=5, name='Bite')
        Fire Breath (Recharge 5-6). The dragon exhales fire in a 15-foot cone. Each creature in that area must make a DC l3 Dexterity saving throw, taking 24 (7d6) fire damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonSilverAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 22, 'max_hp': 487, 'speed': 40, 'fly_speed': 80,
            "strength": 30, "dexterity": 10, "constitution": 29, "intelligence": 18, "wisdom": 15, "charisma": 23}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'charisma', 'dexterity'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 23})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=10, damage_type='piercing'), attack_mod=17, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=10, damage_type='slashing'), attack_mod=17, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=10, damage_type='bludgeoning'), attack_mod=17, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Cold Breath. The dragon exhales an icy blast in a 90-foot cone. Each creature in that area must make a DC 24 Constitution saving throw, taking 67 (15d8) cold damage on a failed save, or half as much damage on a successful one. Paralyzing Breath. The dragon exhales paralyzing gas in a 90- foot cone. Each creature in that area must succeed on a DC 24 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        Change Shape. The dragon magically polymorphs into a humanoid or beast that has a challenge rating no higher than its own, or back into its true form. It reverts to its true form if it dies. Any equipment it is wearing or carrying is absorbed or borne by the new form (the dragon’s choice). In a new form, the dragon retains its alignment, hit points, Hit Dice, ability to speak, proficiencies, Legendary Resistance, lair actions, and Intelligence, Wisdom, and Charisma scores, as well as this action. Its statistics and capabilities are otherwise replaced by those of the new form, except any class features or legendary actions of that form.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 15 ft. of the dragon must succeed on a DC 25 Dexterity saving throw or take 17 (2d6 + 10) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonSilverAdult(Creature):
    def __init__(self, **kwargs):  # errata: saving throws improperly formatted
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 19, 'max_hp': 243, 'speed': 40, 'fly_speed': 80, 
            "strength": 27, "dexterity": 10, "constitution": 25, "intelligence": 16, "wisdom": 13, "charisma": 21}
        default_kwargs.update({"proficiencies": {'constitution', 'charisma', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 16})
        # Features
        """
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), attack_mod=13, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=13, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=13, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 18 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Cold Breath. The dragon exhales an icy blast in a 60-foot cone. Each creature in that area must make a DC 20 Constitution saving throw, taking 58 (13d8) cold damage on a failed save, or half as much damage on a successful one. Paralyzing Breath. The dragon exhales paralyzing gas in a 60-foot cone. Each creature in that area must succeed on a DC 20 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 22 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonSilverYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 18, 'max_hp': 168, 'speed': 40, 'fly_speed': 80, 
            "strength": 23, "dexterity": 10, "constitution": 21, "intelligence": 14, "wisdom": 11, "charisma": 19}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 9})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), attack_mod=10, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=10, melee_range=5, name='Claw')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Cold Breath. The dragon exhales an icy blast in a 30-foot cone. Each creature in that area must make a DC 17 Constitution saving throw, taking 54 (12d8) cold damage on a failed save, or half as much damage on a successful one. Paralyzing Breath. The dragon exhales paralyzing gas in a 30-foot cone. Each creature in that area must succeed on a DC 17 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonSilverWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 17, 'max_hp': 45, 'speed': 30, 'fly_speed': 60,
            "strength": 19, "dexterity": 10, "constitution": 17, "intelligence": 12, "wisdom": 11, "charisma": 15}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
        Breath Weapons (Recharge 5-6). The dragon uses one of the following breath weapons. Cold Breath. The dragon exhales an icy blast in a 15-foot cone. Each creature in that area must make a DC 13 Constitution saving throw, taking 18 (4d8) cold damage on a failed save, or half as much damage on a successful one. Paralyzing Breath. The dragon exhales paralyzing gas in a 15-foot cone. Each creature in that area must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonWhiteAncient(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "dragon", 'ac': 20, 'max_hp': 333, 'speed': 40, 
            "strength": 26, "dexterity": 10, "constitution": 26, "intelligence": 10, "wisdom": 13, "charisma": 14}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 20})
        # Features
        """
        Ice Walk. The dragon can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn’t cost it extra moment.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=8, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=8, modifier=0, damage_type='cold')]), attack_mod=14, melee_range=15, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=8, damage_type='slashing'), attack_mod=14, melee_range=10, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=8, damage_type='bludgeoning'), attack_mod=14, melee_range=20, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours .
        Cold Breath (Recharge 5-6). The dragon exhales an icy blast in a 90-foot cone. Each creature in that area must make a DC 22 Constitution saving throw, taking 72 (16d8) cold damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 22 Dexterity saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonWhiteAdult(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "dragon", 'ac': 18, 'max_hp': 200, 'speed': 40,
            "strength": 22, "dexterity": 10, "constitution": 22, "intelligence": 8, "wisdom": 12, "charisma": 12}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 13})
        # Features
        """
        Ice Walk. The dragon can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn’t cost it extra moment.
        Legendary Resistance (3/Day). If the dragon fails a saving throw, it can choose to succeed instead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='cold')]), attack_mod=11, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=6, damage_type='slashing'), attack_mod=11, melee_range=5, name='Claw')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=11, melee_range=15, name='Tail')
        Frightful Presence. Each creature of the dragon’s choice that is within 120 ft. of the dragon and aware of it must succeed on a DC 14 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the dragon’s Frightful Presence for the next 24 hours.
        Cold Breath (Recharge 5-6). The dragon exhales an icy blast in a 60-foot cone. Each creature in that area must make a DC 19 Constitution saving throw, taking 54 (12d8) cold damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        # Legendary Actions
        """
        Detect. The dragon makes a Wisdom (Perception) check.
        Tail Attack. The dragon makes a tail attack.
        Wing Attack (Costs 2 Actions). The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 19 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonWhiteYoung(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 17, 'max_hp': 133, 'speed': 40, 
            "strength": 18, "dexterity": 10, "constitution": 18, "intelligence": 6, "wisdom": 11, "charisma": 12}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 6})
        # Features
        """
        Ice Walk. The dragon can move across and climb icy surfaces without needing to make an ability check. Additionally, difficult terrain composed of ice or snow doesn’t cost it extra moment.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='cold')]), attack_mod=7, melee_range=10, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        Cold Breath (Recharge 5-6). The dragon exhales an icy blast in a 30-foot cone. Each creature in that area must make a DC 15 Constitution saving throw, taking 45 (10d8) cold damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class DragonWhiteWyrmling(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "dragon", 'ac': 16, 'max_hp': 32, 'speed': 30,
            "strength": 14, "dexterity": 10, "constitution": 14, "intelligence": 5, "wisdom": 10, "charisma": 11}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'dexterity', 'charisma'}})
        default_kwargs.update({"immunities": {' cold'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='cold')]), attack_mod=4, melee_range=5, name='Bite')
        Cold Breath (Recharge 5-6). The dragon exhales an icy blast of hail in a 15-foot cone. Each creature in that area must make a DC 12 Constitution saving throw, taking 22 (5d8) cold damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Drider(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 19, 'max_hp': 123, 'speed': 30, 'climb_speed': 30, 
            "strength": 16, "dexterity": 16, "constitution": 18, "intelligence": 13, "wisdom": 14, "charisma": 12}
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        Fey Ancestry. The drider has advantage on saving throws against being charmed, and magic can’t put the drider to sleep.
        Innate Spellcasting. The drider’s innate spellcasting ability is Wisdom (spell save DC 13). The drider can innately cast the following spells, requiring no material components:
            At will: dancing lights
            1/day each: darkness, faerie fire
        Spider Climb. The drider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        Sunlight Sensitivity. While in sunlight, the drider has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        Web Walker. The drider ignores movement restrictions caused by webbing.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=6, melee_range=5, name='Bite')
        longsword_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=5, name='Longsword_versatile')
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=5, name='Longsword')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=6, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=6, range=600, name='Longbow_range_disadvantage')
        multiattack_longsword = attack_class.MultiAttack(name="Multiattack (longsword)", attack_list=[longsword, longsword, longsword])
        multiattack_longbow = attack_class.MultiAttack(name="Multiattack (longbow)", attack_list=[longbow, longbow, longbow])
        multiattack_longsword_bite = attack_class.MultiAttack(name="Multiattack (longsword and bite)", attack_list=[longsword, longsword, bite])
        multiattack_longbow_bite = attack_class.MultiAttack(name="Multiattack (longbow and bite)", attack_list=[longbow, longbow, bite])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Dryad(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fey", 'ac': 11, 'max_hp': 22, 'speed': 30, 
            "strength": 10, "dexterity": 12, "constitution": 11, "intelligence": 14, "wisdom": 15, "charisma": 18}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Innate Spellcasting. The dryad’s innate spellcasting ability is Charisma (spell save DC 14). The dryad can innately cast the following spells, requiring no material components:
        At will: druidcraft
        3/day each: entangle, goodberry
        1/day each: barkskin, pass without trace, shillelagh
        Magic Resistance. The dryad has advantage on saving throws against spells and other magical effects.
        Speak with Beasts and Plants. The dryad can communicate with beasts and plants as if they shared a language.
        Tree Stride. Once on her turn, the dryad can use 10 ft. of her movement to step magically into one living tree within her reach and emerge from a second living tree within 60 ft. of the first tree, appearing in an unoccupied space within 5 ft. of the second tree. Both trees must be large or bigger.
        """
        # Actions
        """
        club = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='bludgeoning'), attack_mod=2, melee_range=5, name='Club')
        Fey Charm. The dryad targets one humanoid or beast that she can see within 30 feet of her. If the target can see the dryad, it must succeed on a DC 14 Wisdom saving throw or be magically charmed. The charmed creature regards the dryad as a trusted friend to be heeded and protected. Although the target isn’t under the dryad’s control, it takes the dryad’s requests or actions in the most favorable way it can. Each time the dryad or its allies do anything harmful to the target, it can repeat the saving throw, ending the effect on itself on a success. Otherwise, the effect lasts 24 hours or until the dryad dies, is on a different plane of existence from the target, or ends the effect as a bonus action. If a target’s saving throw is successful, the target is immune to the dryad’s Fey Charm for the next 24 hours. The dryad can have no more than one humanoid and up to three beasts charmed at a time.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Duergar(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (dwarf)", 'ac': 16, 'max_hp': 26, 'speed': 25, 
            "strength": 14, "dexterity": 11, "constitution": 14, "intelligence": 11, "wisdom": 10, "charisma": 9}
        default_kwargs.update({"resistances": {' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Duergar Resilience. The duergar has advantage on saving throws against poison, spells, and illusions, as well as to resist being charmed or paralyzed.
        Sunlight Sensitivity. While in sunlight, the duergar has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        Enlarge (Recharges after a Short or Long Rest). For 1 minute, the duergar magically increases in size, along with anything it is wearing or carrying. While enlarged, the duergar is Large, doubles its damage dice on Strength-based weapon attacks (included in the attacks), and makes Strength checks and Strength saving throws with advantage. If the duergar lacks the room to become Large, it attains the maximum size possible in the space available.
        war_pick = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='War Pick')
        javelin = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Javelin')
        javelin_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Javelin_range')
        javelin_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Javelin_range_disadvantage')
        Invisibility (Recharges after a Short or Long Rest). The duergar magically turns invisible until it attacks, casts a spell, or uses its Enlarge, or until its concentration is broken, up to 1 hour (as if concentrating on a spell). Any equipment the duergar wears or carries is invisible with it .
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ElementalAir(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 15, 'max_hp': 90, 'speed': 90, 
            "strength": 14, "dexterity": 20, "constitution": 14, "intelligence": 6, "wisdom": 10, "charisma": 6}
        default_kwargs.update({"resistances": {'piercing', ' lightning', 'thunder', 'slashing', 'bludgeoning'}})
        default_kwargs.update({"immunities": {'paralyzed', ' exhaustion', 'prone', 'poisoned', ' poison', 'grappled', 'restrained', 'unconscious', 'petrified'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Air Form. The elemental can enter a hostile creature’s space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=5, damage_type='bludgeoning'), attack_mod=8, melee_range=5, name='Slam')
        Whirlwind (Recharge 4-6). Each creature in the elemental’s space must make a DC 13 Strength saving throw. On a failure, a target takes 15 (3d8 + 2) bludgeoning damage and is flung up 20 feet away from the elemental in a random direction and knocked prone. If a thrown target strikes an object, such as a wall or floor, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If the target is thrown at another creature, that creature must succeed on a DC 13 Dexterity saving throw or take the same damage and be knocked prone. If the saving throw is successful, the target takes half the bludgeoning damage and isn’t flung away or knocked prone.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ElementalEarth(Creature):  # needs special resistance
    def __init__(self, **kwargs):  # note: resistant to nonmagical bludgeoning, piercing, and slashing
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 17, 'max_hp': 126, 'speed': 30, 
            "strength": 20, "dexterity": 8, "constitution": 20, "intelligence": 5, "wisdom": 10, "charisma": 5}
        default_kwargs.update({"resistances": {'piercing', ' bludgeoning', 'slashing'}})
        default_kwargs.update({"immunities": {'paralyzed', 'unconscious', 'petrified', 'poison', 'poisoned', 'exhaustion'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Earth Glide. The elemental can burrow through nonmagical, unworked earth and stone. While doing so, the elemental doesn’t disturb the material it moves through.
        Siege Monster. The elemental deals double damage to objects and structures.
        """
        # Actions
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=5, damage_type='bludgeoning'), attack_mod=8, melee_range=10, name='Slam')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        kwargs.update(attacks=[slam, multiattack])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ElementalFire(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 13, 'max_hp': 102, 'speed': 50, 
            "strength": 10, "dexterity": 17, "constitution": 16, "intelligence": 6, "wisdom": 10, "charisma": 7}
        default_kwargs.update({"resistances": {' bludgeoning', 'piercing', 'slashing'}})
        default_kwargs.update({"immunities": {'paralyzed', 'grappled', ' fire', 'restrained', 'unconscious', ' exhaustion', 'prone', 'petrified', 'poison', 'poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Fire Form. The elemental can move through a space as narrow as 1 inch wide without squeezing. A creature that touches the elemental or hits it with a melee attack while within 5 ft. of it takes 5 (1d10) fire damage. In addition, the elemental can enter a hostile creature’s space and stop there. The first time it enters a creature’s space on a turn, that creature takes 5 (1d10) fire damage and catches fire; until someone takes an action to douse the fire, the creature takes 5 (1d10) fire damage at the start of each of its turns.
        Illumination. The elemental sheds bright light in a 30-foot radius and dim light in an additional 30 ft..
        Water Susceptibility. For every 5 ft. the elemental moves in water, or for every gallon of water splashed on it, it takes 1 cold damage.
        """
        # Actions
        """
        touch = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='fire'), attack_mod=6, melee_range=5, name='Touch')
            #  If the target is a creature or a flammable object, it ignites. Until a creature takes an action to douse the fire, the target takes 5 (1d10) fire damage at the start of each of its turns.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[touch, touch])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ElementalWater(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 14, 'max_hp': 114, 'speed': 30, 'swim_speed': 90, 
            "strength": 18, "dexterity": 14, "constitution": 18, "intelligence": 5, "wisdom": 10, "charisma": 8}
        default_kwargs.update({"resistances": {'bludgeoning', 'slashing', ' acid', 'piercing'}})
        default_kwargs.update({"immunities": {'petrified', ' poison', 'poisoned', ' exhaustion', 'paralyzed', 'prone', 'grappled', 'unconscious', 'restrained'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Water Form. The elemental can enter a hostile creature’s space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.
        Freeze. If the elemental takes cold damage, it partially freezes; its speed is reduced by 20 ft. until the end of its next turn.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Slam')
        Whelm (Recharge 4-6). Each creature in the elemental’s space must make a DC 15 Strength saving throw. On a failure, a target takes 13 (2d8 + 4) bludgeoning damage. If it is Large or smaller, it is also grappled (escape DC 14). Until this grapple ends, the target is restrained and unable to breathe unless it can breathe water. If the saving throw is successful, the target is pushed out of the elemental’s space. The elemental can grapple one Large creature or up to two Medium or smaller creatures at one time. At the start of each of the elemental’s turns, each target grappled by it takes 13 (2d8 + 4) bludgeoning damage. A creature within 5 feet of the elemental can pull a creature or object out of it by taking an action to make a DC 14 Strength and succeeding.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ElfDrow(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (elf)", 'ac': 15, 'max_hp': 13, 'speed': 30, 
            "strength": 10, "dexterity": 14, "constitution": 10, "intelligence": 11, "wisdom": 11, "charisma": 12}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Fey Ancestry. The drow has advantage on saving throws against being charmed, and magic can’t put the drow to sleep.
        Innate Spellcasting. The drow’s spellcasting ability is Charisma (spell save DC 11). It can innately cast the following spells, requiring no material components:
        At will: dancing lights
        1/day each: darkness, faerie fire
        Sunlight Sensitivity. While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        shortsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Shortsword')
        hand_crossbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Hand Crossbow_range')
        hand_crossbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Hand Crossbow_range_disadvantage')
            # the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target wakes up if it takes damage or if another creature takes an action to shake it awake.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ettercap(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 13, 'max_hp': 44, 'speed': 30, 'climb_speed': 30, 
            "strength": 14, "dexterity": 15, "constitution": 13, "intelligence": 7, "wisdom": 12, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Spider Climb. The ettercap can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        Web Sense. While in contact with a web, the ettercap knows the exact location of any other creature in contact with the same web.
        Web Walker. The ettercap ignores movement restrictions caused by webbing.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='poison')]), attack_mod=4, melee_range=5, name='Bite')
            #  The target must succeed on a DC 11 Constitution saving throw or be poisoned for 1 minute. The creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Claws')
        Web (Recharge 5-6). Ranged Weapon Attack: +4 to hit, range 30/60 ft., one Large or smaller creature. Hit: The creature is restrained by webbing. As an action, the restrained creature can make a DC 11 Strength check, escaping from the webbing on a success. The effect ends if the webbing is destroyed. The webbing has AC 10, 5 hit points, is vulnerable to fire damage and immune to bludgeoning damage.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ettin(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "giant", 'ac': 12, 'max_hp': 85, 'speed': 40, 
            "strength": 21, "dexterity": 8, "constitution": 17, "intelligence": 6, "wisdom": 10, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 4})
        # Features
        """
        Two Heads. The ettin has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, and knocked unconscious.
        Wakeful. When one of the ettin’s heads is asleep, its other head is awake.
        """
        # Actions
        """
        battleaxe = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=5, damage_type='slashing'), attack_mod=7, melee_range=5, name='Battleaxe')
        morningstar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=5, damage_type='piercing'), attack_mod=7, melee_range=5, name='Morningstar')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[battleaxe, morningstar])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class FlyingSword(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: cr is 1/4, not 1
        default_kwargs = {"size": "small", "creature_type": "construct", 'ac': 17, 'max_hp': 17, 'speed': 0, 'fly_speed': 50, 
            "strength": 12, "dexterity": 15, "constitution": 11, "intelligence": 1, "wisdom": 5, "charisma": 1}
        default_kwargs.update({"proficiencies": {'dexterity'}})
        default_kwargs.update({"immunities": {'deafened', 'psychic', 'petrified', 'charmed', ' blinded', 'poisoned', 'frightened', 'paralyzed', 'poison'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 0.25})
        # Features
        """
        Antimagic Susceptibility. The sword is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the sword must succeed on a Constitution saving throw against the caster’s spell save DC or fall unconscious for 1 minute.
        False Appearance. While the sword remains motionless and isn’t flying, it is indistinguishable from a normal sword.
        """
        # Actions
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='slashing'), attack_mod=3, melee_range=5, name='Longsword')
        default_kwargs.update(attacks=[longsword])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Gargoyle(Creature):  # needs resistance
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "elemental", 'ac': 15, 'max_hp': 52, 'speed': 30, 'fly_speed': 60, 
            "strength": 15, "dexterity": 11, "constitution": 16, "intelligence": 6, "wisdom": 11, "charisma": 7}
        default_kwargs.update({"resistances": {"slashing", 'piercing', ' bludgeoning'}})
        default_kwargs.update({"immunities": {' exhaustion', ' poison', 'poisoned', 'petrified'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        False Appearance. While the gargoyle remains motion less, it is indistinguishable from an inanimate statue.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Claws')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GelatinousCube(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "ooze", 'ac': 6, 'max_hp': 84, 'speed': 15, 
            "strength": 14, "dexterity": 3, "constitution": 20, "intelligence": 1, "wisdom": 6, "charisma": 1}
        default_kwargs.update({"immunities": {'deafened', 'exhaustion', 'prone', 'charmed', ' blinded', 'frightened'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Ooze Cube. The cube takes up its entire space. Other creatures can enter the space, but a creature that does so is subjected to the cube’s Engulf and has disadvantage on the saving throw.
        Creatures inside the cube can be seen but have total cover.
        A creature within 5 feet of the cube can take an action to pull a creature or object out of the cube. Doing so requires a successful DC 12 Strength check, and the creature making the attempt takes 10 (3d6) acid damage.
        The cube can hold only one Large creature or up to four Medium or smaller creatures inside it at a time.
        Transparent. Even when the cube is in plain sight, it takes a successful DC 15 Wisdom (Perception) check to spot a cube that has neither moved nor attacked. A creature that tries to enter the cube’s space while unaware of the cube is surprised by the cube.
        """
        # Actions
        """
        pseudopod = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='acid'), attack_mod=4, melee_range=5, name='Pseudopod')
        Engulf. The cube moves up to its speed. While doing so, it can enter Large or smaller creatures’ spaces. Whenever the cube enters a creature’s space, the creature must make a DC 12 Dexterity saving throw. On a successful save, the creature can choose to be pushed 5 feet back or to the side of the cube. A creature that chooses not to be pushed suffers the consequences of a failed saving throw. On a failed save, the cube enters the creature’s space, and the creature takes 10 (3d6) acid damage and is engulfed. The engulfed creature can’t breathe, is restrained, and takes 21 (6d6) acid damage at the start of each of the cube’s turns. When the cube moves, the engulfed creature moves with it. An engulfed creature can try to escape by taking an action to make a DC 12 Strength check. On a success, the creature escapes and enters a space of its choice within 5 feet of the cube.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GenieDjinni(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 17, 'max_hp': 161, 'speed': 30, 'fly_speed': 90, 
            "strength": 21, "dexterity": 15, "constitution": 22, "intelligence": 15, "wisdom": 16, "charisma": 20}
        default_kwargs.update({"proficiencies": {'dexterity', 'charisma', 'wisdom'}})
        default_kwargs.update({"immunities": {' lightning', 'thunder'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        Elemental Demise. If the djinni dies, its body disintegrates into a warm breeze, leaving behind only equipment the djinni was wearing or carrying.
        Innate Spellcasting. The djinni’s innate spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). It can innately cast the following spells, requiring no material components:
        At will: detect evil and good, detect magic, thunderwave
        3/day each: create food and water (can create wine instead of water), tongues, wind walk
        1/day each: conjure elemental (air elemental only), creation, gaseous form, invisibility, major image, plane shift
        """
        # Actions
        """
        scimitar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=5, damage_type='slashing'), attack_mod=9, melee_range=5, name='Scimitar')
        Create Whirlwind. A 5-foot-radius, 30-foot-tall cylinder of swirling air magically forms on a point the djinni can see within 120 feet of it. The whirlwind lasts as long as the djinni maintains concentration (as if concentrating on a spell). Any creature but the djinni that enters the whirlwind must succeed on a DC 18 Strength saving throw or be restrained by it. The djinni can move the whirlwind up to 60 feet as an action, and creatures restrained by the whirlwind move with it. The whirlwind ends if the djinni loses sight of it. A creature can use its action to free a creature restrained by the whirlwind, including itself, by succeeding on a DC 18 Strength check. If the check succeeds, the creature is no longer restrained and moves to the nearest space outside the whirlwind.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[scimitar, scimitar, scimitar])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GenieEfreeti(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 17, 'max_hp': 200, 'speed': 40, 'fly_speed': 60, 
            "strength": 22, "dexterity": 12, "constitution": 24, "intelligence": 16, "wisdom": 15, "charisma": 16}
        default_kwargs.update({"proficiencies": {'wisdom', 'intelligence', 'charisma'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        Elemental Demise. If the efreeti dies, its body disintegrates in a flash of fire and puff of smoke, leaving behind only equipment the djinni was wearing or carrying.
        Innate Spellcasting. The efreeti’s innate spell casting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:
        At will: detect magic
        3/day: enlarge/reduce, tongues
        1/day each: conjure elemental (fire elemental only), gaseous form, invisibility, major image, plane shift, wall of fire
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ghast(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 13, 'max_hp': 36, 'speed': 30, 
            "strength": 16, "dexterity": 17, "constitution": 10, "intelligence": 11, "wisdom": 10, "charisma": 8}
        default_kwargs.update({"immunities": {' necrotic', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Stench. Any creature that starts its turn within 5 ft. of the ghast must succeed on a DC 10 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to the ghast’s Stench for 24 hours.
        Turn Defiance. The ghast and any ghouls within 30 ft. of it have advantage on saving throws against effects that turn undead.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=3, damage_type='piercing'), attack_mod=3, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claws')
            #  If the target is a creature other than an undead, it must succeed on a DC 10 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ghost(Creature):
    def __init__(self, **kwargs):  # errata: missing full description of Possession
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 11, 'max_hp': 45, 'speed': 0, 'fly_speed': 40, 
            "strength": 7, "dexterity": 13, "constitution": 10, "intelligence": 10, "wisdom": 12, "charisma": 17}
        default_kwargs.update({"resistances": {'fire', 'piercing', ' acid', 'bludgeoning', 'thunder', 'lightning', 'slashing'}})
        default_kwargs.update({"immunities": {'poison', 'poisoned', 'grappled', 'paralyzed', ' cold', 'frightened', 'necrotic', 'exhaustion', 'petrified', 'restrained', ' charmed', 'prone'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 4})
        # Features
        """
        Ethereal Sight. The ghost can see 60 ft. into the Ethereal Plane when it is on the Material Plane, and vice versa.
        Incorporeal Movement. The ghost can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.
        """
        # Actions
        """
        withering_touch = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=6, modifier=3, damage_type='necrotic'), attack_mod=5, melee_range=5, name='Withering Touch')
        Etherealness. The ghost enters the Ethereal Plane from the Material Plane, or vice versa. It is visible on the Material Plane while it is in the Border Ethereal, and vice versa, yet it can’t affect or be affected by anything on the other plane.
        Horrifying Visage. Each non-undead creature within 60 ft. of the ghost that can see it must succeed on a DC 13 Wisdom saving throw or be frightened for 1 minute. If the save fails by 5 or more, the target also ages 1d4 × 10 years. A frightened target can repeat the saving throw at the end of each of its turns, ending the frightened condition on itself on a success. If a target’s saving throw is successful or the effect ends for it, the target is immune to this ghost’s Horrifying Visage for the next 24 hours. The aging effect can be reversed with a greater restoration spell, but only within 24 hours of it occurring.
        Possession (Recharge 6). One humanoid that the ghost can see within 5 ft. of it must succeed on a DC 13 Charisma saving throw or be possessed by the ghost; the ghost then disappears, and the target is incapacitated and loses control of its body. The ghost now controls the body but doesn’t deprive the target of awareness. The ghost can’t be targeted by any attack, spell, or other effect, except ones that turn undead, and it retains its alignment, Intelligence, Wisdom, Charisma, and immunity to being charmed and frightened. It otherwise uses the possessed target’s statistics, but doesn’t gain access to the target’s knowledge, class features, or proficiencies.
        dispel evil and good. The possession lasts until the body drops to 0 hit points, the ghost ends it as a bonus action, or the ghost is turned or forced out by an effect like the  spell. When the possession ends, the ghost reappears in an unoccupied space within 5 ft. of the body. The target is immune to this ghost’s Possession for 24 hours after succeeding on the saving throw or after the possession ends.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ghoul(Creature):  # needs poison
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 12, 'max_hp': 22, 'speed': 30, 
            "strength": 13, "dexterity": 15, "constitution": 10, "intelligence": 7, "wisdom": 10, "charisma": 6}
        default_kwargs.update({"immunities": {' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=2, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Claws')
            #  If the target is a creature other than an elf or undead, it must succeed on a DC 10 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantCloud(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 14, 'max_hp': 200, 'speed': 40, 
            "strength": 27, "dexterity": 10, "constitution": 22, "intelligence": 12, "wisdom": 16, "charisma": 16}
        default_kwargs.update({"proficiencies": {'wisdom', 'charisma', 'constitution'}})
        default_kwargs.update({'vision': "normal", 'cr': 9})
        # Features
        """
        Keen Smell. The giant has advantage on Wisdom (Perception) checks that rely on smell.
        Innate Spellcasting. The giant’s innate spellcasting ability is Charisma. It can innately cast the following spells, requiring no material components:
        At will: detect magic, fog cloud, light
        3/day each: feather fall, fly, misty step, telekinesis
        1/day each: control weather, gaseous form
        """
        # Actions
        """
        morningstar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=8, damage_type='piercing'), attack_mod=12, melee_range=10, name='Morningstar')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=8, damage_type='bludgeoning'), attack_mod=12, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=8, damage_type='bludgeoning'), attack_mod=12, range=240, name='Rock_range_disadvantage')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[morningstar, morningstar])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantFire(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 18, 'max_hp': 162, 'speed': 30, 
            "strength": 25, "dexterity": 9, "constitution": 23, "intelligence": 10, "wisdom": 14, "charisma": 13}
        default_kwargs.update({"proficiencies": {'charisma', 'dexterity', 'constitution'}})
        default_kwargs.update({"immunities": {'fire'}})
        default_kwargs.update({'vision': "normal", 'cr': 9})
        # Features
        """
        """
        # Actions

        greatsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=6, dice_type=6, modifier=7, damage_type='slashing'), attack_mod=11, melee_range=10, name='Greatsword')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=7, damage_type='bludgeoning'), attack_mod=11, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=7, damage_type='bludgeoning'), attack_mod=11, range=240, name='Rock_range_disadvantage')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatsword, greatsword])
        default_kwargs.update(attacks=[multiattack, greatsword, rock_range, rock_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantFrost(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 15, 'max_hp': 138, 'speed': 40, 
            "strength": 23, "dexterity": 9, "constitution": 21, "intelligence": 9, "wisdom": 10, "charisma": 12}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'charisma'}})
        default_kwargs.update({"immunities": {'cold'}})
        default_kwargs.update({'vision': "normal", 'cr': 8})
        # Features
        """
        """
        # Actions
        greataxe = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=12, modifier=6, damage_type='slashing'), attack_mod=9, melee_range=10, name='Greataxe')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=9, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=9, range=240, name='Rock_range_disadvantage')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greataxe, greataxe])
        default_kwargs.update(attacks=[multiattack, greataxe, rock_range, rock_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantHill(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 13, 'max_hp': 105, 'speed': 40, 
            "strength": 21, "dexterity": 8, "constitution": 19, "intelligence": 5, "wisdom": 9, "charisma": 6}
        default_kwargs.update({'vision': "normal", 'cr': 5})
        # Features
        """
        """
        # Actions
        greatclub = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=5, damage_type='bludgeoning'), attack_mod=8, melee_range=10, name='Greatclub')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=5, damage_type='bludgeoning'), attack_mod=8, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=5, damage_type='bludgeoning'), attack_mod=8, range=240, name='Rock_range_disadvantage')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatclub, greatclub])
        default_kwargs.update(attacks=[multiattack, greatclub, rock_range, rock_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantStone(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 17, 'max_hp': 126, 'speed': 40, 
            "strength": 23, "dexterity": 15, "constitution": 20, "intelligence": 10, "wisdom": 12, "charisma": 9}
        default_kwargs.update({"proficiencies": {'dexterity', 'wisdom', 'constitution'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 7})
        # Features
        """
        Stone Camouflage. The giant has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.
        """
        # Actions
        """
        greatclub = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=9, melee_range=15, name='Greatclub')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=9, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=9, range=240, name='Rock_range_disadvantage')
            #  If the target is a creature, it must succeed on a DC 17 Strength saving throw or be knocked prone.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatclub, greatclub])
        """
        # Reactions
        """
        Rock Catching. If a rock or similar object is hurled at the giant, the giant can, with a successful DC 10 Dexterity saving throw, catch the missile and take no bludgeoning damage from it. <div class="sharedaddy sd-sharing-enabled"><div class="robots-nocontent sd-block sd-social sd-social-icon-text sd-sharing"><h3 class="sd-title">Share this:</h3><div class="sd-content"><ul><li class="share-facebook"><a class="share-facebook sd-button share-icon" data-shared="sharing-facebook-1739" href="https://dnd5e.info/monsters/monster/giant-stone/?share=facebook" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on Facebook"><span>Facebook</span></a></li><li class="share-twitter"><a class="share-twitter sd-button share-icon" data-shared="sharing-twitter-1739" href="https://dnd5e.info/monsters/monster/giant-stone/?share=twitter" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on Twitter"><span>Twitter</span></a></li><li class="share-linkedin"><a class="share-linkedin sd-button share-icon" data-shared="sharing-linkedin-1739" href="https://dnd5e.info/monsters/monster/giant-stone/?share=linkedin" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on LinkedIn"><span>LinkedIn</span></a></li><li class="share-email"><a class="share-email sd-button share-icon" data-shared="" href="https://dnd5e.info/monsters/monster/giant-stone/?share=email" rel="nofollow noopener noreferrer" target="_blank" title="Click to email this to a friend"><span>Email</span></a></li><li class="share-print"><a class="share-print sd-button share-icon" data-shared="" href="https://dnd5e.info/monsters/monster/giant-stone/#print" rel="nofollow noopener noreferrer" target="_blank" title="Click to print"><span>Print</span></a></li><li class="share-end"></li></ul></div></div></div> <div class="sharedaddy sd-block sd-like jetpack-likes-widget-wrapper jetpack-likes-widget-unloaded" data-name="like-post-frame-148321651-1739-5dbcd55497945" data-src="https://widgets.wp.com/likes/#blog_id=148321651&amp;post_id=1739&amp;origin=dnd5e.info&amp;obj_id=148321651-1739-5dbcd55497945" id="like-post-wrapper-148321651-1739-5dbcd55497945"><h3 class="sd-title">Like this:</h3><div class="likes-widget-placeholder post-likes-widget-placeholder" style="height: 55px;"><span class="button"><span>Like</span></span> <span class="loading">Loading...</span></div><span class="sd-text-color"></span><a class="sd-link-color"></a></div> <nav class="pagination group"></nav> /.pagination <div class="clear"></div>
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GiantStorm(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "giant", 'ac': 16, 'max_hp': 230, 'speed': 50, 'swim_speed': 50, 
            "strength": 29, "dexterity": 14, "constitution": 20, "intelligence": 16, "wisdom": 18, "charisma": 18}
        default_kwargs.update({"proficiencies": {'charisma', 'constitution', 'strength', 'wisdom'}})
        default_kwargs.update({"resistances": {' cold'}})
        default_kwargs.update({"immunities": {' lightning', 'thunder'}})
        default_kwargs.update({'vision': "normal", 'cr': 13})
        # Features
        """
        Amphibious. The giant can breathe air and water.
        Innate Spellcasting. The giant’s innate spellcasting ability is Charisma (spell save DC 17). It can innately cast the following spells, requiring no material components:
        At will: detect magic, feather fall, levitate, light
        3/day each: control weather, water breathing
        """
        # Actions
        """
        greatsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=6, dice_type=6, modifier=9, damage_type='slashing'), attack_mod=14, melee_range=10, name='Greatsword')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=12, modifier=9, damage_type='bludgeoning'), attack_mod=14, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=12, modifier=9, damage_type='bludgeoning'), attack_mod=14, range=240, name='Rock_range_disadvantage')
        Lightning Strike (Recharge 5-6). The giant hurls a magical lightning bolt at a point it can see within 500 feet of it. Each creature within 10 feet of that point must make a DC 17 Dexterity saving throw, taking 54 (12d8) lightning damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[greatsword, greatsword])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GibberingMouther(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "aberration", 'ac': 9, 'max_hp': 67, 'speed': 10, 'swim_speed': 10, 
            "strength": 10, "dexterity": 8, "constitution": 16, "intelligence": 3, "wisdom": 10, "charisma": 6}
        default_kwargs.update({"immunities": {' prone'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Aberrant Ground. The ground in a 10-foot radius around the mouther is doughlike difficult terrain. Each creature that starts its turn in that area must succeed on a DC 10 Strength saving throw or have its speed reduced to 0 until the start of its next turn.
        Gibbering. The mouther babbles incoherently while it can see any creature and isn’t incapacitated. Each creature that starts its turn within 20 feet of the mouther and can hear the gibbering must succeed on a DC 10 Wisdom saving throw. On a failure, the creature can’t take reactions until the start of its next turn and rolls a d8 to determine what it does during its turn. On a 1 to 4, the creature does nothing. On a 5 or 6, the creature takes no action or bonus action and uses all its movement to move in a randomly determined direction. On a 7 or 8, the creature makes a melee attack against a randomly determined creature within its reach or does nothing if it can’t make such an attack.
        """
        # Actions
        """
        bites = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=5, dice_type=6, modifier=0, damage_type='piercing'), attack_mod=2, melee_range=5, name='Bites')
            #  If the target is Medium or smaller, it must succeed on a DC 10 Strength saving throw or be knocked prone. If the target is killed by this damage, it is absorbed into the mouther.
        Blinding Spittle (Recharge 5-6). The mouther spits a chemical glob at a point it can see within 15 feet of it. The glob explodes in a blinding flash of light on impact. Each creature within 5 feet of the flash must succeed on a DC 13 Dexterity saving throw or be blinded until the end of the mouther’s next turn.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Gnoll(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (gnoll)", 'ac': 15, 'max_hp': 22, 'speed': 30, 
            "strength": 14, "dexterity": 12, "constitution": 11, "intelligence": 6, "wisdom": 10, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Rampage. When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        spear_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Spear_versatile')
        spear = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Spear')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, range=600, name='Longbow_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GnomeDeepSvirfneblin(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "humanoid (gnome)", 'ac': 15, 'max_hp': 16, 'speed': 20, 
            "strength": 15, "dexterity": 14, "constitution": 14, "intelligence": 12, "wisdom": 10, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Stone Camouflage. The gnome has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.
        Gnome Cunning. The gnome has advantage on Intelligence, Wisdom, and Charisma saving throws against magic.
        Innate Spellcasting. The gnome’s innate spellcasting ability is Intelligence (spell save DC 11). It can innately cast the following spells, requiring no material components:
        At will: nondetection (self only)
        1/day each: blindness/deafness, blur, disguise self
        """
        # Actions
        """
        war_pick = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='War Pick')
        poisoned_dart_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Poisoned Dart_range')
        poisoned_dart_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Poisoned Dart_range_disadvantage')
            # the target must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Goblin(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "humanoid (goblinoid)", 'ac': 15, 'max_hp': 7, 'speed': 30, 
            "strength": 8, "dexterity": 14, "constitution": 10, "intelligence": 10, "wisdom": 8, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Nimble Escape. The goblin can take the Disengage or Hide action as a bonus action on each of its turns.
        """
        # Actions
        scimitar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Scimitar')
        shortbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=80, name='Shortbow_range')
        shortbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=320, name='Shortbow_range_disadvantage')
        default_kwargs.update(attacks=[scimitar, shortbow_range, shortbow_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GolemClay(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "construct", 'ac': 14, 'max_hp': 133, 'speed': 20, 
            "strength": 20, "dexterity": 9, "constitution": 18, "intelligence": 3, "wisdom": 8, "charisma": 1}
        default_kwargs.update({"immunities": {"slashing", 'piercing', ' acid', 'poison', 'frightened', 'charmed', 'paralyzed', 'petrified', 'psychic', 'exhaustion', 'poisoned', 'bludgeoning'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 9})
        # Features
        """
        Acid Absorption. Whenever the golem is subjected to acid damage, it takes no damage and instead regains a number of hit points equal to the acid damage dealt.
        Berserk. Whenever the golem starts its turn with 60 hit points or fewer, roll a d6. On a 6, the golem goes berserk. On each of its turns while berserk, the golem attacks the nearest creature it can see. If no creature is near enough to move to and attack, the golem attacks an object, with preference for an object smaller than itself. Once the golem goes berserk, it continues to do so until it is destroyed or regains all its hit points.
        Immutable Form. The golem is immune to any spell or effect that would alter its form.
        Magic Resistance. The golem has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The golem’s weapon attacks are magical.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=5, damage_type='bludgeoning'), attack_mod=8, melee_range=5, name='Slam')
            #  If the target is a creature, it must succeed on a DC 15 Constitution saving throw or have its hit point maximum reduced by an amount equal to the damage taken. The target dies if this attack reduces its hit point maximum to 0. The reduction lasts until removed by the greater restoration spell or other magic.
        Haste (Recharge 5-6). Until the end of its next turn, the golem magically gains a +2 bonus to its AC, has advantage on Dexterity saving throws, and can use its slam attack as a bonus action.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GolemFlesh(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "construct", 'ac': 9, 'max_hp': 93, 'speed': 30, 
            "strength": 19, "dexterity": 9, "constitution": 18, "intelligence": 6, "wisdom": 10, "charisma": 5}
        default_kwargs.update({"immunities": {"slashing", 'exhaustion', 'frightened', 'charmed', 'poisoned', 'piercing', ' lightning', 'bludgeoning', 'paralyzed', 'petrified', 'poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Berserk. Whenever the golem starts its turn with 40 hit points or fewer, roll a d6. On a 6, the golem goes berserk. On each of its turns while berserk, the golem attacks the nearest creature it can see. If no creature is near enough to move to and attack, the golem attacks an object, with preference for an object smaller than itself. Once the golem goes berserk, it continues to do so until it is destroyed or regains all its hit points.
        The golem’s creator, if within 60 feet of the berserk golem, can try to calm it by speaking firmly and persuasively. The golem must be able to hear its creator, who must take an action to make a DC 15 Charisma (Persuasion) check. If the check succeeds, the golem ceases being berserk. If it takes damage while still at 40 hit points or fewer, the golem might go berserk again.
        Aversion of Fire. If the golem takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.
        Immutable Form. The golem is immune to any spell or effect that would alter its form.
        Lightning Absorption. Whenever the golem is subjected to lightning damage, it takes no damage and instead regains a number of hit points equal to the lightning damage dealt.
        Magic Resistance. The golem has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The golem’s weapon attacks are magical.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Slam')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GolemIron(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "construct", 'ac': 20, 'max_hp': 210, 'speed': 30, 
            "strength": 24, "dexterity": 9, "constitution": 20, "intelligence": 3, "wisdom": 11, "charisma": 1}
        default_kwargs.update({"immunities": {'bludgeoning', 'paralyzed', ' fire', "slashing", 'charmed', 'piercing', 'frightened', 'psychic', 'poison', 'poisoned', 'petrified', 'exhaustion'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 16})
        # Features
        """
        Fire Absorption. Whenever the golem is subjected to fire damage, it takes no damage and instead regains a number of hit points equal to the fire damage dealt.
        Immutable Form. The golem is immune to any spell or effect that would alter its form.
        Magic Resistance. The golem has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The golem’s weapon attacks are magical.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=7, damage_type='bludgeoning'), attack_mod=13, melee_range=5, name='Slam')
        sword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=7, damage_type='slashing'), attack_mod=13, melee_range=10, name='Sword')
        Poison Breath (Recharge 5-6). The golem exhales poisonous gas in a 15-foot cone. Each creature in that area must make a DC 19 Constitution saving throw, taking 45 (l0d8) poison damage on a failed save, or half as much damage on a successful one.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GolemStone(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "construct", 'ac': 17, 'max_hp': 178, 'speed': 30, 
            "strength": 22, "dexterity": 9, "constitution": 20, "intelligence": 3, "wisdom": 11, "charisma": 1}
        default_kwargs.update({"immunities": {'exhaustion', 'piercing', 'charmed', 'petrified', 'poisoned', "slashing", 'poison', 'frightened', 'psychic', 'bludgeoning', 'paralyzed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 10})
        # Features
        """
        Immutable Form. The golem is immune to any spell or effect that would alter its form.
        Magic Resistance. The golem has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The golem’s weapon attacks are magical.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=6, damage_type='bludgeoning'), attack_mod=10, melee_range=5, name='Slam')
        Slow (Recharge 5-6). The golem targets one or more creatures it can see within 10 ft. of it. Each target must make a DC 17 Wisdom saving throw against this magic. On a failed save, a target can’t use reactions, its speed is halved, and it can’t make more than one attack on its turn. In addition, the target can take either an action or a bonus action on its turn, not both. These effects last for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Gorgon(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 19, 'max_hp': 114, 'speed': 40, 
            "strength": 20, "dexterity": 11, "constitution": 18, "intelligence": 2, "wisdom": 12, "charisma": 7}
        default_kwargs.update({"immunities": {' petrified'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Trampling Charge. If the gorgon moves at least 20 feet straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 16 Strength saving throw or be knocked prone. If the target is prone, the gorgon can make one attack with its hooves against it as a bonus action.
        """
        # Actions
        """
        gore = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=12, modifier=5, damage_type='piercing'), attack_mod=8, melee_range=5, name='Gore')
        hooves = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=5, damage_type='bludgeoning'), attack_mod=8, melee_range=5, name='Hooves')
        Petrifying Breath (Recharge 5-6). The gorgon exhales petrifying gas in a 30-foot cone. Each creature in that area must succeed on a DC 13 Constitution saving throw. On a failed save, a target begins to turn to stone and is restrained. The restrained target must repeat the saving throw at the end of its next turn. On a success, the effect ends on the target. On a failure, the target is petrified until freed by the greater restoration spell or other magic.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class GrayOoze(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "ooze", 'ac': 8, 'max_hp': 22, 'speed': 10, 'climb_speed': 10,
            "strength": 12, "dexterity": 6, "constitution": 16, "intelligence": 1, "wisdom": 6, "charisma": 2}
        default_kwargs.update({"resistances": {' acid', 'cold', 'fire'}})
        default_kwargs.update({"immunities": {'prone', ' blinded', 'frightened', 'exhaustion', 'deafened', 'charmed'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        Amorphous. The ooze can move through a space as narrow as 1 inch wide without squeezing.
        Corrode Metal. Any nonmagical weapon made of metal that hits the ooze corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Nonmagical ammunition made of metal that hits the ooze is destroyed after dealing damage.
        The ooze can eat through 2-inch-thick, nonmagical metal in 1 round.
        False Appearance. While the ooze remains motionless, it is indistinguishable from an oily pool or wet rock.
        """
        # Actions
        """
        pseudopod = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=6, modifier=1, damage_type='bludgeoning'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='acid')]), attack_mod=3, melee_range=5, name='Pseudopod')
            # if the target is wearing nonmagical metal armor, its armor is partly corroded and takes a permanent and cumulative -1 penalty to the AC it offers. The armor is destroyed if the penalty reduces its AC to 10.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Grick(Creature):  # needs resistance
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 14, 'max_hp': 27, 'speed': 30, 'climb_speed': 30, 
            "strength": 14, "dexterity": 14, "constitution": 11, "intelligence": 3, "wisdom": 14, "charisma": 5}
        default_kwargs.update({"resistances": {'bludgeoning', 'slashing', 'piercing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Stone Camouflage. The grick has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.
        """
        # Actions
        """
        tentacles = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Tentacles')
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Beak')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Griffon(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 12, 'max_hp': 59, 'speed': 30, 'fly_speed': 80, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 2, "wisdom": 13, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Keen Sight. The griffon has advantage on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Beak')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=6, melee_range=5, name='Claws')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beak, claws])
        default_kwargs.update(attacks=[multiattack, beak, claws])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Grimlock(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: cr is 1/4, not 1
        default_kwargs = {"size": "medium", "creature_type": "humanoid (grimlock)", 'ac': 11, 'max_hp': 11, 'speed': 30, 
            "strength": 16, "dexterity": 12, "constitution": 12, "intelligence": 9, "wisdom": 8, "charisma": 6}
        default_kwargs.update({"immunities": {' blinded'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 0.25})
        # Features
        """
        Blind Senses. The grimlock can’t use its blindsight while deafened and unable to smell.
        Keen Hearing and Smell. The grimlock has advantage on Wisdom (Perception) checks that rely on hearing or smell.
        Stone Camouflage. The grimlock has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.
        """
        # Actions
        spiked_bone_club = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=3, damage_type='bludgeoning'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='piercing')]), attack_mod=5, melee_range=5, name='Spiked Bone Club')
        default_kwargs.update(attacks=[spiked_bone_club])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class HagGreen(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fey", 'ac': 17, 'max_hp': 82, 'speed': 30, 
            "strength": 18, "dexterity": 12, "constitution": 16, "intelligence": 13, "wisdom": 14, "charisma": 14}
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Amphibious. The hag can breathe air and water.
        Innate Spellcasting. The hag’s innate spellcasting ability is Charisma (spell save DC 12). She can innately cast the following spells, requiring no material components:
        At will: dancing lights, minor illusion, vicious mockery
        Mimicry. The hag can mimic animal sounds and humanoid voices. A creature that hears the sounds can tell they are imitations with a successful DC 14 Wisdom (Insight) check.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=6, melee_range=5, name='Claws')
        Illusory Appearance. The hag covers herself and anything she is wearing or carrying with a magical illusion that makes her look like another creature of her general size and humanoid shape. The illusion ends if the hag takes a bonus action to end it or if she dies. The changes wrought by this effect fail to hold up to physical inspection. For example, the hag could appear to have smooth skin, but someone touching her would feel her rough flesh. Otherwise, a creature must take an action to visually inspect the illusion and succeed on a DC 20 Intelligence (Investigation) check to discern that the hag is disguised.
        Invisible Passage. The hag magically turns invisible until she attacks or casts a spell, or until her concentration ends (as if concentrating on a spell). While invisible, she leaves no physical evidence of her passage, so she can be tracked only by magic. Any equipment she wears or carries is invisible with her.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class HagNight(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend", 'ac': 17, 'max_hp': 112, 'speed': 30, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 16, "wisdom": 14, "charisma": 16}
        default_kwargs.update({"resistances": {'piercing', ' cold', 'bludgeoning', 'fire', "slashing that aren't silvered"}})
        default_kwargs.update({"immunities": {' charmed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Innate Spellcasting. The hag’s innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). She can innately cast the following spells, requiring no material components:
        At will: detect magic, magic missile
        2/day each: plane shift (self only), ray of enfeeblement, sleep
        Magic Resistance. The hag has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        """
        claws_hag_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claws (Hag Form Only)')
        Change Shape. The hag magically polymorphs into a Small or Medium female humanoid, or back into her true form. Her statistics are the same in each form. Any equipment she is wearing or carrying isn’t transformed. She reverts to her true form if she dies.
        Etherealness. The hag magically enters the Ethereal Plane from the Material Plane, or vice versa. To do so, the hag must have a heartstone in her possession.
        Nightmare Haunting (1/Day). While on the Ethereal Plane, the hag magically touches a sleeping humanoid on the Material Plane. A protection from evil and good spell cast on the target prevents this contact, as does a magic circle. As long as the contact persists, the target has dreadful visions. If these visions last for at least 1 hour, the target gains no benefit from its rest, and its hit point maximum is reduced by 5 (1d10). If this effect reduces the target’s hit point maximum to 0, the target dies, and if the target was evil, its soul is trapped in the hag’s soul bag. The reduction to the target’s hit point maximum lasts until removed by the greater restoration spell or similar magic.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class HagSea(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fey", 'ac': 14, 'max_hp': 52, 'speed': 30, 'swim_speed': 40, 
            "strength": 16, "dexterity": 13, "constitution": 16, "intelligence": 12, "wisdom": 12, "charisma": 13}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Amphibious. The hag can breathe air and water.
        Horrific Appearance. Any humanoid that starts its turn within 30 feet of the hag and can see the hag’s true form must make a DC 11 Wisdom saving throw. On a failed save, the creature is frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, with disadvantage if the hag is within line of sight, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the hag’s Horrific Appearance for the next 24 hours.
        Unless the target is surprised or the revelation of the hag’s true form is sudden, the target can avert its eyes and avoid making the initial saving throw. Until the start of its next turn, a creature that averts its eyes has disadvantage on attack rolls against the hag.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claws')
        Death Glare. The hag targets one frightened creature she can see within 30 ft. of her. If the target can see the hag, it must succeed on a DC 11 Wisdom saving throw against this magic or drop to 0 hit points.
        Illusory Appearance. The hag covers herself and anything she is wearing or carrying with a magical illusion that makes her look like an ugly creature of her general size and humanoid shape. The effect ends if the hag takes a bonus action to end it or if she dies. The changes wrought by this effect fail to hold up to physical inspection. For example, the hag could appear to have no claws, but someone touching her hand might feel the claws. Otherwise, a creature must take an action to visually inspect the illusion and succeed on a DC 16 Intelligence (Investigation) check to discern that the hag is disguised.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Harpy(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 11, 'max_hp': 38, 'speed': 20, 'fly_speed': 40, 
            "strength": 12, "dexterity": 13, "constitution": 12, "intelligence": 7, "wisdom": 10, "charisma": 13}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=1, damage_type='slashing'), attack_mod=3, melee_range=5, name='Claws')
        club = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='bludgeoning'), attack_mod=3, melee_range=5, name='Club')
        Luring Song. The harpy sings a magical melody. Every humanoid and giant within 300 ft. of the harpy that can hear the song must succeed on a DC 11 Wisdom saving throw or be charmed until the song ends. The harpy must take a bonus action on its subsequent turns to continue singing. It can stop singing at any time. The song ends if the harpy is incapacitated. While charmed by the harpy, a target is incapacitated and ignores the songs of other harpies. If the charmed target is more than 5 ft. away from the harpy, the must move on its turn toward the harpy by the most direct route. It doesn’t avoid opportunity attacks, but before moving into damaging terrain, such as lava or a pit, and whenever it takes damage from a source other than the harpy, a target can repeat the saving throw. A creature can also repeat the saving throw at the end of each of its turns. If a creature’s saving throw is successful, the effect ends on it. A target that successfully saves is immune to this harpy’s song for the next 24 hours.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[claws, club])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class HellHound(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend", 'ac': 15, 'max_hp': 45, 'speed': 50, 
            "strength": 17, "dexterity": 12, "constitution": 14, "intelligence": 6, "wisdom": 13, "charisma": 6}
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Keen Hearing and Smell. The hound has advantage on Wisdom (Perception) checks that rely on hearing or smell.
        Pack Tactics. The hound has advantage on an attack roll against a creature if at least one of the hound’s allies is within 5 ft. of the creature and the ally isn’t incapacitated.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=5, melee_range=5, name='Bite')
        Fire Breath (Recharge 5-6). The hound exhales fire in a 15-foot cone. Each creature in that area must make a DC 12 Dexterity saving throw, taking 21 (6d6) fire damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Hippogriff(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 11, 'max_hp': 19, 'speed': 40, 'fly_speed': 60, 
            "strength": 17, "dexterity": 13, "constitution": 13, "intelligence": 2, "wisdom": 12, "charisma": 8}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        Keen Sight. The hippogriff has advantage on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Beak')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claws')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beak, claws])
        default_kwargs.update(attacks=[multiattack, beak, claws])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Hobgoblin(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (goblinoid)", 'ac': 18, 'max_hp': 11, 'speed': 30, 
            "strength": 13, "dexterity": 12, "constitution": 12, "intelligence": 10, "wisdom": 10, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Martial Advantage. Once per turn, the hobgoblin can deal an extra 7 (2d6) damage to a creature it hits with a weapon attack if that creature is within 5 ft. of an ally of the hobgoblin that isn’t incapacitated.
        """
        # Actions
        """
        longsword_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=1, damage_type='slashing'), attack_mod=3, melee_range=5, name='Longsword_versatile')
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='slashing'), attack_mod=3, melee_range=5, name='Longsword')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, range=600, name='Longbow_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Homunculus(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "construct", 'ac': 13, 'max_hp': 5, 'speed': 20, 'fly_speed': 40, 
            "strength": 4, "dexterity": 15, "constitution": 11, "intelligence": 10, "wisdom": 10, "charisma": 7}
        default_kwargs.update({"immunities": {'poisoned', ' poison', ' charmed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 0})
        # Features
        """
        Telepathic Bond. While the homunculus is on the same plane of existence as its master, it can magically convey what it senses to its master, and the two can communicate telepathically.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=1, modifier=0, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
            # the target must succeed on a DC 10 Constitution saving throw or be poisoned for 1 minute. If the saving throw fails by 5 or more, the target is instead poisoned for 5 (1d10) minutes and unconscious while poisoned in this way.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Hydra(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "monstrosity", 'ac': 15, 'max_hp': 172, 'speed': 30, 'swim_speed': 30, 
            "strength": 20, "dexterity": 12, "constitution": 20, "intelligence": 2, "wisdom": 10, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 8})
        # Features
        """
        Hold Breath. The hydra can hold its breath for 1 hour.
        Multiple Heads. The hydra has five heads. While it has more than one head, the hydra has advantage on saving throws against being blinded, charmed, deafened, frightened, stunned, and knocked unconscious.
        Whenever the hydra takes 25 or more damage in a single turn, one of its heads dies. If all its heads die, the hydra dies.
        At the end of its turn, it grows two heads for each of its heads that died since its last turn, unless it has taken fire damage since its last turn. The hydra regains 10 hit points for each head regrown in this way.
        Reactive Heads. For each head the hydra has beyond one, it gets an extra reaction that can be used only for opportunity attacks.
        Wakeful. While the hydra sleeps, at least one of its heads is awake.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=5, damage_type='piercing'), attack_mod=8, melee_range=10, name='Bite')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class InvisibleStalker(Creature):  # needs special resistance
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "elemental", 'ac': 14, 'max_hp': 104, 'speed': 50, 'fly_speed': 50, 
            "strength": 16, "dexterity": 19, "constitution": 14, "intelligence": 10, "wisdom": 15, "charisma": 11}
        default_kwargs.update({"resistances": {' bludgeoning', 'slashing', 'piercing'}})
        default_kwargs.update({"immunities": {'petrified', 'paralyzed', ' exhaustion', ' poison', 'grappled', 'restrained', 'unconscious', 'prone', 'poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        Invisibility. The stalker is invisible.
        Faultless Tracker. The stalker is given a quarry by its summoner. The stalker knows the direction and distance to its quarry as long as the two of them are on the same plane of existence. The stalker also knows the location of its summoner.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Slam')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Kobold(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "humanoid (kobold)", 'ac': 12, 'max_hp': 5, 'speed': 30, 
            "strength": 7, "dexterity": 15, "constitution": 9, "intelligence": 8, "wisdom": 7, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Sunlight Sensitivity. While in sunlight, the kobold has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        Pack Tactics. The kobold has advantage on an attack roll against a creature if at least one of the kobold’s allies is within 5 ft. of the creature and the ally isn’t incapacitated.
        """
        # Actions
        """
        dagger = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Dagger')
        sling_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='bludgeoning'), attack_mod=4, range=30, name='Sling_range')
        sling_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='bludgeoning'), attack_mod=4, range=120, name='Sling_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Kraken(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "monstrosity (titan)", 'ac': 18, 'max_hp': 472, 'speed': 20, 'swim_speed': 60, 
            "strength": 30, "dexterity": 11, "constitution": 25, "intelligence": 22, "wisdom": 18, "charisma": 20}
        default_kwargs.update({"proficiencies": {'constitution', 'intelligence', 'dexterity', 'wisdom', 'strength'}})
        default_kwargs.update({"immunities": {'piercing', 'paralyzed', ' lightning', 'slashing', ' frightened', 'bludgeoning'}})
        default_kwargs.update({'vision': "truesight", 'cr': 23})
        # Features
        """
        Amphibious. The kraken can breathe air and water.
        Freedom of Movement. The kraken ignores difficult terrain, and magical effects can’t reduce its speed or cause it to be restrained. It can spend 5 feet of movement to escape from nonmagical restraints or being grappled.
        Siege Monster. The kraken deals double damage to objects and structures.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=10, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
            #  If the target is a Large or smaller creature grappled by the kraken, that creature is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the kraken, and it takes 42 (12d6) acid damage at the start of each of the kraken’s turns. If the kraken takes 50 damage or more on a single turn from a creature inside it, the kraken must succeed on a DC 25 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the kraken. If the kraken dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 15 feet of movement, exiting prone.
        tentacle = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=10, damage_type='bludgeoning'), attack_mod=7, melee_range=30, name='Tentacle')
            # the target is grappled (escape DC 18). Until this grapple ends, the target is restrained. The kraken has ten tentacles, each of which can grapple one target.
        Fling. One Large or smaller object held or creature grappled by the kraken is thrown up to 60 feet in a random direction and knocked prone. If a thrown target strikes a solid surface, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If the target is thrown at another creature, that creature must succeed on a DC 18 Dexterity saving throw or take the same damage and be knocked prone.
        Lightning Storm. The kraken magically creates three bolts of lightning, each of which can strike a target the kraken can see within 120 feet of it. A target must make a DC 23 Dexterity saving throw, taking 22 (4d10) lightning damage on a failed save, or half as much damage on a successful one.
        """
        # Legendary Actions
        """
        Tentacle Attack or Fling. The kraken makes one tentacle attack or uses its Fling.
        Lightning Storm (Costs 2 Actions). The kraken uses Lightning Storm.
        Ink Cloud (Costs 3 Actions). While underwater, the kraken expels an ink cloud in a 60-foot radius. The cloud spreads around corners, and that area is heavily obscured to creatures other than the kraken. Each creature other than the kraken that ends its turn there must succeed on a DC 23 Constitution saving throw, taking 16 (3d10) poison damage on a failed save, or half as much damage on a successful one. A strong current disperses the cloud, which otherwise disappears at the end of the kraken’s next turn.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

# TODO: figure out why I'm having trouble reading in Lamia

class Lich(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 17, 'max_hp': 135, 'speed': 30, 
            "strength": 11, "dexterity": 16, "constitution": 16, "intelligence": 20, "wisdom": 14, "charisma": 16}
        default_kwargs.update({"proficiencies": {'constitution', 'wisdom', 'intelligence'}})
        default_kwargs.update({"resistances": {'lightning', 'necrotic', ' cold'}})
        default_kwargs.update({"immunities": {'exhaustion', 'piercing', ' charmed', 'poisoned', 'slashing', 'bludgeoning', 'paralyzed', 'frightened', ' poison'}})
        default_kwargs.update({'vision': "truesight", 'cr': 21})
        # Features
        """
        Legendary Resistance (3/Day). If the lich fails a saving throw, it can choose to succeed instead.
        Rejuvenation. If it has a phylactery, a destroyed lich gains a new body in 1d10 days, regaining all its hit points and becoming active again. The new body appears within 5 feet of the phylactery.
        Spellcasting. The lich is an 18th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). The lich has the following wizard spells prepared:
        Cantrips (at will): mage hand, prestidigitation, ray of frost
        1st level (4 slots): detect magic, magic missile, shield, thunderwave
        2nd level (3 slots): acid arrow, detect thoughts, invisibility, mirror image
        3rd level (3 slots): animate dead, counterspell, dispel magic, fireball
        4th level (3 slots): blight, dimension door
        5th level (3 slots): cloudkill, scrying
        6th level (1 slot): disintegrate, globe of invulnerability
        7th level (1 slot): finger of death, plane shift
        8th level (1 slot): dominate monster, power word stun
        9th level (1 slot): power word kill
        Turn Resistance. The lich has advantage on saving throws against any effect that turns undead.
        """
        # Actions
        """
        Paralyzing Touch. Melee Spell Attack: +12 to hit, reach 5 ft., one creature. Hit: 10 (3d6) cold damage. The target must succeed on a DC 18 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        # Legendary Actions
        """
        Cantrip. The lich casts a cantrip.
        Paralyzing Touch (Costs 2 Actions). The lich uses its Paralyzing Touch.
        Frightening Gaze (Costs 2 Actions). The lich fixes its gaze on one creature it can see within 10 feet of it. The target must succeed on a DC 18 Wisdom saving throw against this magic or become frightened for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a target’s saving throw is successful or the effect ends for it, the target is immune to the lich’s gaze for the next 24 hours.
        Disrupt Life (Costs 3 Actions). Each living creature within 20 feet of the lich must make a DC 18 Constitution saving throw against this magic, taking 21 (6d6) necrotic damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Lizardfolk(Creature):  # needs work
    def __init__(self, **kwargs):  # note: "multiattack: two melee attacks, each one with a different weapon"
        default_kwargs = {"size": "medium", "creature_type": "humanoid (lizardfolk)", 'ac': 15, 'max_hp': 22, 'speed': 30, 'swim_speed': 30, 
            "strength": 15, "dexterity": 10, "constitution": 13, "intelligence": 7, "wisdom": 12, "charisma": 7}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        Hold Breath. The lizardfolk can hold its breath for 15 minutes.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        heavy_club = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='bludgeoning'), attack_mod=4, melee_range=5, name='Heavy Club')
        javelin = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Javelin')
        javelin_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Javelin_range')
        javelin_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Javelin_range_disadvantage')
        spiked_shield = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Spiked Shield')
        """

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Magmin(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "elemental", 'ac': 14, 'max_hp': 9, 'speed': 30, 
            "strength": 7, "dexterity": 15, "constitution": 12, "intelligence": 8, "wisdom": 11, "charisma": 10}
        default_kwargs.update({"resistances": {' bludgeoning', 'piercing', 'slashing'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Death Burst. When the magmin dies, it explodes in a burst of fire and magma. Each creature within 10 ft. of it must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save, or half as much damage on a successful one. Flammable objects that aren’t being worn or carried in that area are ignited.
        Ignited Illumination. As a bonus action, the magmin can set itself ablaze or extinguish its flames. While ablaze, the magmin sheds bright light in a 10-foot radius and dim light for an additional 10 ft.
        """
        # Actions
        """
        touch = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='fire'), attack_mod=4, melee_range=5, name='Touch')
            #  If the target is a creature or a flammable object, it ignites. Until a target takes an action to douse the fire, the target takes 3 (1d6) fire damage at the end of each of its turns.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Manticore(Creature):  # needs work
    def __init__(self, **kwargs):  # errata: rename "tail spikes" to "tail_spike" in multiattack
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 14, 'max_hp': 68, 'speed': 30, 'fly_speed': 50, 
            "strength": 17, "dexterity": 16, "constitution": 17, "intelligence": 7, "wisdom": 12, "charisma": 8}
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Tail Spike Regrowth. The manticore has twenty-four tail spikes. Used spikes regrow when the manticore finishes a long rest.
        """
        # Actions
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claw')
        tail_spike_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), attack_mod=5, range=100, name='Tail Spike_range')
        tail_spike_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), attack_mod=5, range=200, name='Tail Spike_range_disadvantage')
        multiattack_alt = attack_class.MultiAttack(name="Multiattack (alternate)", attack_list=[tail_spike_range, tail_spike_range, tail_spike_range])
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claw, claw])
        default_kwargs.update(attacks=[bite, claw, tail_spike_range, tail_spike_disadvantage, multiattack, multiattack_alt])
        
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Medusa(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 15, 'max_hp': 127, 'speed': 30, 
            "strength": 10, "dexterity": 15, "constitution": 15, "intelligence": 12, "wisdom": 13, "charisma": 15}
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        Petrifying Gaze. When a creature that can see the medusa’s eyes starts its turn within 30 ft. of the medusa, the medusa can force it to make a DC 14 Constitution saving throw if the medusa isn’t incapacitated and can see the creature. If the saving throw fails by 5 or more, the creature is instantly petrified. Otherwise, a creature that fails the save begins to turn to stone and is restrained. The restrained creature must repeat the saving throw at the end of its next turn, becoming petrified on a failure or ending the effect on a success. The petrification lasts until the creature is freed by the greater restoration spell or other magic.
        Unless surprised, a creature can avert its eyes to avoid the saving throw at the start of its turn. If the creature does so, it can’t see the medusa until the start of its next turn, when it can avert its eyes again. If the creature looks at the medusa in the meantime, it must immediately make the save.
        If the medusa sees itself reflected on a polished surface within 30 ft. of it and in an area of bright light, the medusa is, due to its curse, affected by its own gaze.
        """
        # Actions
        """
        snake_hair = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=4, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=5, melee_range=5, name='Snake Hair')
        shortsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=5, melee_range=5, name='Shortsword')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=5, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='poison')]), attack_mod=5, range=600, name='Longbow_range_disadvantage')
        multiattack_snake_hair_shortsword = attack_class.MultiAttack(name=Multiattack (snake_hair and shortsword), attack_list=[snake_hair, shortsword, shortsword]
        multiattack_longbow = attack_class.MultiAttack(name=Multiattack (longbow), attack_list=[longbow, longbow]
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class MephitDust(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "elemental", 'ac': 12, 'max_hp': 17, 'speed': 30, 'fly_speed': 30, 
            "strength": 5, "dexterity": 14, "constitution": 10, "intelligence": 9, "wisdom": 10, "charisma": 10}
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Death Burst. When the mephit dies, it explodes in a burst of dust. Each creature within 5 ft. of it must then succeed on a DC 10 Constitution saving throw or be blinded for 1 minute. A blinded creature can repeat the saving throw on each of its turns, ending the effect on itself on a success.
        Innate Spellcasting (1/Day). The mephit can innately cast sleep, requiring no material components. Its innate spellcasting ability is Charisma.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Claws')
        Blinding Breath (Recharge 6). The mephit exhales a 15-foot cone of blinding dust. Each creature in that area must succeed on a DC 10 Dexterity saving throw or be blinded for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class MephitIce(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "elemental", 'ac': 11, 'max_hp': 21, 'speed': 30, 'fly_speed': 30, 
            "strength": 7, "dexterity": 13, "constitution": 10, "intelligence": 9, "wisdom": 11, "charisma": 12}
        default_kwargs.update({"immunities": {' poisoned', ' cold', 'poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Death Burst. When the mephit dies, it explodes in a burst of jagged ice. Each creature within 5 ft. of it must make a DC 10 Dexterity saving throw, taking 4 (1d8) slashing damage on a failed save, or half as much damage on a successful one.
        False Appearance. While the mephit remains motionless, it is indistinguishable from an ordinary shard of ice.
        Innate Spellcasting (1/Day). The mephit can innately cast fog cloud, requiring no material components. Its innate spellcasting ability is Charisma.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='slashing'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='cold')]), attack_mod=3, melee_range=5, name='Claws')
        Frost Breath (Recharge 6). The mephit exhales a 15-foot cone of cold air. Each creature in that area must succeed on a DC 10 Dexterity saving throw, taking 5 (2d4) cold damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class MephitMagma(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "elemental", 'ac': 11, 'max_hp': 22, 'speed': 30, 'fly_speed': 30, 
            "strength": 8, "dexterity": 12, "constitution": 12, "intelligence": 7, "wisdom": 10, "charisma": 10}
        default_kwargs.update({"immunities": {' fire', 'poison', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Death Burst. When the mephit dies, it explodes in a burst of lava. Each creature within 5 ft. of it must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save, or half as much damage on a successful one.
        False Appearance. While the mephit remains motionless, it is indistinguishable from an ordinary mound of magma.
        Innate Spellcasting (1/Day). The mephit can innately cast heat metal (spell save DC 10), requiring no material components. Its innate spellcasting ability is Charisma.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='slashing'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='fire')]), attack_mod=3, melee_range=5, name='Claws')
        Fire Breath (Recharge 6). The mephit exhales a 15-foot cone of fire. Each creature in that area must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class MephitSteam(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "small", "creature_type": "elemental", 'ac': 10, 'max_hp': 21, 'speed': 30, 'fly_speed': 30, 
            "strength": 5, "dexterity": 11, "constitution": 10, "intelligence": 11, "wisdom": 10, "charisma": 12}
        default_kwargs.update({"immunities": {' poisoned', ' fire', 'poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Death Burst. When the mephit dies, it explodes in a cloud of steam. Each creature within 5 ft. of the mephit must succeed on a DC 10 Dexterity saving throw or take 4 (1d8) fire damage.
        Innate Spellcasting (1/Day). The mephit can innately cast blur, requiring no material components. Its innate spellcasting ability is Charisma.
        """
        # Actions
        """
        claws = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='slashing'), dice.DamageDice(dice_num=1, dice_type=4, modifier=0, damage_type='fire')]), attack_mod=2, melee_range=5, name='Claws')
        Steam Breath (Recharge 6). The mephit exhales a 15-foot cone of scalding steam. Each creature in that area must succeed on a DC 10 Dexterity saving throw, taking 4 (1d8) fire damage on a failed save, or half as much damage on a successful one.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Merfolk(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: cr is 1/8, not 1
        default_kwargs = {"size": "medium", "creature_type": "humanoid (merfolk)", 'ac': 11, 'max_hp': 11, 'speed': 10, 'swim_speed': 40, 
            "strength": 10, "dexterity": 13, "constitution": 12, "intelligence": 11, "wisdom": 11, "charisma": 12}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        Amphibious. The merfolk can breathe air and water.
        """
        # Actions
        spear_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='piercing'), attack_mod=2, melee_range=5, name='Spear_versatile')
        spear = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='piercing'), attack_mod=2, melee_range=5, name='Spear')
        default_kwargs.update(attacks=[spear_versatile, spear])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Merrow(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 13, 'max_hp': 45, 'speed': 10, 'swim_speed': 40, 
            "strength": 18, "dexterity": 10, "constitution": 15, "intelligence": 8, "wisdom": 10, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Amphibious. The merrow can breathe air and water.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=4, damage_type='slashing'), attack_mod=6, melee_range=5, name='Claws')
        harpoon = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Harpoon')
            #  If the target is a Huge or smaller creature, it must succeed on a Strength contest against the merrow or be pulled up to 20 feet toward the merrow.
        multiattack_alt = attack_class.MultiAttack(name="Multiattack (alternate)", attack_list=[bite, harpoon]
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Mimic(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity (shapechanger)", 'ac': 12, 'max_hp': 58, 'speed': 15, 
            "strength": 17, "dexterity": 12, "constitution": 15, "intelligence": 5, "wisdom": 13, "charisma": 8}
        default_kwargs.update({"immunities": {' acid', ' prone'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Shapechanger. The mimic can use its action to polymorph into an object or back into its true, amorphous form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn ‘t transformed. It reverts to its true form if it dies.
        Adhesive (Object Form Only). The mimic adheres to anything that touches it. A Huge or smaller creature adhered to the mimic is also grappled by it (escape DC 13). Ability checks made to escape this grapple have disadvantage.
        False Appearance (Object Form Only). While the mimic remains motionless, it is indistinguishable from an ordinary object.
        Grappler. The mimic has advantage on attack rolls against any creature grappled by it.
        """
        # Actions
        """
        pseudopod = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='bludgeoning'), attack_mod=5, melee_range=5, name='Pseudopod')
            #  If the mimic is in object form, the target is subjected to its Adhesive trait.
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='acid')]), attack_mod=5, melee_range=5, name='Bite')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Minotaur(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 14, 'max_hp': 76, 'speed': 40, 
            "strength": 18, "dexterity": 11, "constitution": 16, "intelligence": 6, "wisdom": 16, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Charge. If the minotaur moves at least 10 ft. straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be pushed up to 10 ft. away and knocked prone.
        Labyrinthine Recall. The minotaur can perfectly recall any path it has traveled.
        Reckless. At the start of its turn, the minotaur can gain advantage on all melee weapon attack rolls it makes during that turn, but attack rolls against it have advantage until the start of its next turn.
        """
        # Actions
        """
        greataxe = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=12, modifier=4, damage_type='slashing'), attack_mod=6, melee_range=5, name='Greataxe')
        gore = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Gore')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Mummy(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 11, 'max_hp': 58, 'speed': 20, 
            "strength": 16, "dexterity": 8, "constitution": 15, "intelligence": 6, "wisdom": 10, "charisma": 12}
        default_kwargs.update({"proficiencies": {'wisdom'}})
        default_kwargs.update({"immunities": {'piercing', ' bludgeoning', 'poisoned', ' necrotic', 'slashing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        """
        # Actions
        """
        rotting_fist = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='bludgeoning'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='necrotic')]), attack_mod=5, melee_range=5, name='Rotting Fist')
            #  If the target is a creature, it must succeed on a DC 12 Constitution saving throw or be cursed with mummy rot. The cursed target can’t regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target’s hit point maximum to 0, the target dies, and its body turns to dust. The curse lasts until removed by the remove curse spell or other magic.
        Dreadful Glare. The mummy targets one creature it can see within 60 ft. of it. If the target can see the mummy, it must succeed on a DC 11 Wisdom saving throw against this magic or become frightened until the end of the mummy’s next turn. If the target fails the saving throw by 5 or more, it is also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Dreadful Glare of all mummies (but not mummy lords) for the next 24 hours.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class MummyLord(Creature):  # needs spell, needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 17, 'max_hp': 97, 'speed': 20, 
            "strength": 18, "dexterity": 10, "constitution": 17, "intelligence": 11, "wisdom": 18, "charisma": 16}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution', 'intelligence', 'charisma'}})
        default_kwargs.update({"immunities": {'slashing', 'exhaustion', 'poison', 'paralyzed', 'bludgeoning', ' necrotic', 'piercing', 'poisoned', ' charmed', 'frightened'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 15})
        # Features
        """
        Magic Resistance. The mummy lord has advantage on saving throws against spells and other magical effects.
        Rejuvenation. A destroyed mummy lord gains a new body in 24 hours if its heart is intact, regaining all its hit points and becoming active again. The new body appears within 5 feet of the mummy lord’s heart.
        Spellcasting. The mummy lord is a 10th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell attacks). The mummy lord has the following cleric spells prepared:
        Cantrips (at will): sacred flame, thaumaturgy
        1st level (4 slots): command, guiding bolt, shield of faith
        2nd level (3 slots): hold person, silence, spiritual weapon
        3rd level (3 slots): animate dead, dispel magic
        4th level (3 slots): divination, guardian of faith
        5th level (2 slots): contagion, insect plague
        6th level (1 slot): harm
        """
        # Actions
        """
        rotting_fist = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=3, dice_type=6, modifier=4, damage_type='bludgeoning'), dice.DamageDice(dice_num=6, dice_type=6, modifier=0, damage_type='necrotic')]), attack_mod=9, melee_range=5, name='Rotting Fist')
            #  If the target is a creature, it must succeed on a DC 16 Constitution saving throw or be cursed with mummy rot. The cursed target can’t regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target’s hit point maximum to 0, the target dies, and its body turns to dust. The curse lasts until removed by the remove curse spell or other magic.
        Dreadful Glare. The mummy lord targets one creature it can see within 60 feet of it. If the target can see the mummy lord, it must succeed on a DC 16 Wisdom saving throw against this magic or become frightened until the end of the mummy’s next turn. If the target fails the saving throw by 5 or more, it is also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Dreadful Glare of all mummies and mummy lords for the next 24 hours.
        """
        # Legendary Actions
        """
        Attack. The mummy lord makes one attack with its rotting fist or uses its Dreadful Glare.
        Blinding Dust. Blinding dust and sand swirls magically around the mummy lord. Each creature within 5 feet of the mummy lord must succeed on a DC 16 Constitution saving throw or be blinded until the end of the creature’s next turn.
        Blasphemous Word (Costs 2 Actions). The mummy lord utters a blasphemous word. Each non-undead creature within 10 feet of the mummy lord that can hear the magical utterance must succeed on a DC 16 Constitution saving throw or be stunned until the end of the mummy lord’s next turn.
        Channel Negative Energy (Costs 2 Actions). The mummy lord magically unleashes negative energy. Creatures within 60 feet of the mummy lord, including ones behind barriers and around corners, can’t regain hit points until the end of the mummy lord’s next turn.
        Whirlwind of Sand (Costs 2 Actions). The mummy lord magically transforms into a whirlwind of sand, moves up to 60 feet, and reverts to its normal form. While in whirlwind form, the mummy lord is immune to all damage, and it can’t be grappled, petrified, knocked prone, restrained, or stunned. Equipment worn or carried by the mummy lord remain in its possession.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class NagaGuardian(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 18, 'max_hp': 127, 'speed': 40, 
            "strength": 19, "dexterity": 18, "constitution": 16, "intelligence": 16, "wisdom": 19, "charisma": 18}
        default_kwargs.update({"proficiencies": {'intelligence', 'dexterity', 'wisdom', 'constitution', 'charisma'}})
        default_kwargs.update({"immunities": {' poison', ' charmed', 'poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 10})
        # Features
        """
        Rejuvenation. If it dies, the naga returns to life in 1d6 days and regains all its hit points. Only a wish spell can prevent this trait from functioning.
        Spellcasting. The naga is an 11th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks), and it needs only verbal components to cast its spells. It has the following cleric spells prepared:
        Cantrips (at will): mending, sacred flame, thaumaturgy
        1st level (4 slots): command, cure wounds, shield of faith
        2nd level (3 slots): calm emotions, hold person
        3rd level (3 slots): bestow curse, clairvoyance
        4th level (3 slots): banishment, freedom of movement
        5th level (2 slots): flame strike, geas
        6th level (1 slot): true seeing
        """
        # Actions
        """
        bite = attack_class.HitAndSaveAttack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=8, melee_range=10, name='Bite', dc=15, save_type='constitution', save_damage_dice='10d8', save_damage_type='poison', damage_on_success=True)
        spit_poison_range = attack_class.HitAndSaveAttack(damage_dice=dice.NullDamageDice(), attack_mod=8, range=15, name='Spit Poison_range', dc=15, save_type='constitution', save_damage_dice='10d8', save_damage_type='poison', damage_on_success=True)
        spit_poison_disadvantage = attack_class.HitAndSaveAttack(damage_dice=dice.NullDamageDice(), attack_mod=8, range=30, name='Spit Poison_range_disadvantage', dc=15, save_type='constitution', save_damage_dice='10d8', save_damage_type='poison', damage_on_success=True)
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class NagaSpirit(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 15, 'max_hp': 75, 'speed': 40, 
            "strength": 18, "dexterity": 17, "constitution": 14, "intelligence": 16, "wisdom": 15, "charisma": 16}
        default_kwargs.update({"proficiencies": {'charisma', 'wisdom', 'constitution', 'dexterity'}})
        default_kwargs.update({"immunities": {'poisoned', ' charmed', ' poison'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 8})
        # Features
        """
        Rejuvenation. If it dies, the naga returns to life in 1d6 days and regains all its hit points. Only a wish spell can prevent this trait from functioning.
        Spellcasting. The naga is a 10th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks), and it needs only verbal components to cast its spells. It has the following wizard spells prepared:
        Cantrips (at will): mage hand, minor illusion, ray of frost
        1st level (4 slots): charm person, detect magic, sleep
        2nd level (3 slots): detect thoughts, hold person
        3rd level (3 slots): lightning bolt, water breathing
        4th level (3 slots): blight, dimension door
        5th level (2 slots): dominate person
        """
        # Actions
        """
        bite = attack_class.HitAndSaveAttack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=10, name='Bite', dc=13, save_type='constitution', save_damage_dice='7d8', save_damage_type='poison', damage_on_success=True)
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Nightmare(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "fiend", 'ac': 13, 'max_hp': 68, 'speed': 60, 'fly_speed': 90, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 10, "wisdom": 13, "charisma": 15}
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "normal", 'cr': 3})
        # Features
        """
        Confer Fire Resistance. The nightmare can grant resistance to fire damage to anyone riding it.
        Illumination. The nightmare sheds bright light in a 10-foot radius and dim light for an additional 10 feet.
        """
        # Actions
        """
        Ethereal Stride. The nightmare and up to three willing creatures within 5 feet of it magically enter the Ethereal Plane from the Material Plane, or vice versa.
        """
        hooves = attack_class.Attack(damage_dice=dice.DamageDiceBag(
            dice_list=[dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'),
                       dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=6,
                                     melee_range=5, name='Hooves')
        default_kwargs.update(attacks=[hooves])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class OchreJelly(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "ooze", 'ac': 8, 'max_hp': 45, 'speed': 10, 'climb_speed': 10, 
            "strength": 15, "dexterity": 6, "constitution": 14, "intelligence": 2, "wisdom": 6, "charisma": 1}
        default_kwargs.update({"resistances": {' acid'}})
        default_kwargs.update({"immunities": {' lightning', 'frightened', ' blinded', 'exhaustion', 'charmed', 'slashing', 'prone', 'deafened'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Amorphous. The jelly can move through a space as narrow as 1 inch wide without squeezing.
        Spider Climb. The jelly can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        """
        # Actions
        """
        pseudopod = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='bludgeoning'), dice.DamageDice(dice_num=1, dice_type=6, modifier=0, damage_type='acid')]), attack_mod=4, melee_range=5, name='Pseudopod')
        """
        # Reactions
        """
        Split. When a jelly that is Medium or larger is subjected to lightning or slashing damage, it splits into two new jellies if it has at least 10 hit points. Each new jelly has hit points equal to half the original jelly’s, rounded down. New jellies are one size smaller than the original jelly. <div class="sharedaddy sd-sharing-enabled"><div class="robots-nocontent sd-block sd-social sd-social-icon-text sd-sharing"><h3 class="sd-title">Share this:</h3><div class="sd-content"><ul><li class="share-facebook"><a class="share-facebook sd-button share-icon" data-shared="sharing-facebook-1872" href="https://dnd5e.info/monsters/monster/ochre-jelly/?share=facebook" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on Facebook"><span>Facebook</span></a></li><li class="share-twitter"><a class="share-twitter sd-button share-icon" data-shared="sharing-twitter-1872" href="https://dnd5e.info/monsters/monster/ochre-jelly/?share=twitter" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on Twitter"><span>Twitter</span></a></li><li class="share-linkedin"><a class="share-linkedin sd-button share-icon" data-shared="sharing-linkedin-1872" href="https://dnd5e.info/monsters/monster/ochre-jelly/?share=linkedin" rel="nofollow noopener noreferrer" target="_blank" title="Click to share on LinkedIn"><span>LinkedIn</span></a></li><li class="share-email"><a class="share-email sd-button share-icon" data-shared="" href="https://dnd5e.info/monsters/monster/ochre-jelly/?share=email" rel="nofollow noopener noreferrer" target="_blank" title="Click to email this to a friend"><span>Email</span></a></li><li class="share-print"><a class="share-print sd-button share-icon" data-shared="" href="https://dnd5e.info/monsters/monster/ochre-jelly/#print" rel="nofollow noopener noreferrer" target="_blank" title="Click to print"><span>Print</span></a></li><li class="share-end"></li></ul></div></div></div> <div class="sharedaddy sd-block sd-like jetpack-likes-widget-wrapper jetpack-likes-widget-unloaded" data-name="like-post-frame-148321651-1872-5dbe1a78f0725" data-src="https://widgets.wp.com/likes/#blog_id=148321651&amp;post_id=1872&amp;origin=dnd5e.info&amp;obj_id=148321651-1872-5dbe1a78f0725" id="like-post-wrapper-148321651-1872-5dbe1a78f0725"><h3 class="sd-title">Like this:</h3><div class="likes-widget-placeholder post-likes-widget-placeholder" style="height: 55px;"><span class="button"><span>Like</span></span> <span class="loading">Loading...</span></div><span class="sd-text-color"></span><a class="sd-link-color"></a></div> <nav class="pagination group"></nav> /.pagination <div class="clear"></div>
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Ogre(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "giant", 'ac': 11, 'max_hp': 59, 'speed': 40, 
            "strength": 19, "dexterity": 8, "constitution": 16, "intelligence": 5, "wisdom": 7, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        """
        # Actions
        greatclub = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Greatclub')
        javelin = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Javelin')
        javelin_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=6, range=30, name='Javelin_range')
        javelin_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=6, range=120, name='Javelin_range_disadvantage')
        default_kwargs.update(attacks=[greatclub, javelin, javelin_range, javelin_range_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class OgreZombie(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "undead", 'ac': 8, 'max_hp': 85, 'speed': 30, 
            "strength": 19, "dexterity": 6, "constitution": 18, "intelligence": 3, "wisdom": 6, "charisma": 5}
        default_kwargs.update({"proficiencies": {'wisdom'}})
        default_kwargs.update({"immunities": {' poison', ' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Undead Fortitude. If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5+the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.
        """
        # Actions
        """
        morningstar = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Morningstar')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Oni(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "giant", 'ac': 16, 'max_hp': 110, 'speed': 30, 'fly_speed': 30, 
            "strength": 19, "dexterity": 11, "constitution": 16, "intelligence": 14, "wisdom": 12, "charisma": 15}
        default_kwargs.update({"proficiencies": {'charisma', 'constitution', 'dexterity', 'wisdom'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 7})
        # Features
        """
        Innate Spellcasting. The oni’s innate spellcasting ability is Charisma (spell save DC 13). The oni can innately cast the following spells, requiring no material components:
        At will: darkness, invisibility
        1/day each: charm person, cone of cold, gaseous form, sleep
        Magic Weapons. The oni’s weapon attacks are magical.
        Regeneration. The oni regains 10 hit points at the start of its turn if it has at least 1 hit point.
        """
        # Actions
        """
        claw_oni_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw (Oni Form Only)')
        glaive = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=10, name='Glaive')
        Change Shape. The oni magically polymorphs into a Small or Medium humanoid, into a Large giant, or back into its true form. Other than its size, its statistics are the same in each form. The only equipment that is transformed is its glaive, which shrinks so that it can be wielded in humanoid form. If the oni dies, it reverts to its true form, and its glaive reverts to its normal size.
        multiattack_claws = attack_class.MultiAttack(name="Multiattack (claws)", attack_list=[claws, claws])
        multiattack_glaive = attack_class.MultiAttack(name="Multiattack (glaive)", attack_list=[glaive, glaive])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Orc(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (orc)", 'ac': 13, 'max_hp': 15, 'speed': 30, 
            "strength": 16, "dexterity": 12, "constitution": 16, "intelligence": 7, "wisdom": 11, "charisma": 10}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Aggressive. As a bonus action, the orc can move up to its speed toward a hostile creature that it can see.
        """
        # Actions
        """
        greataxe = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=12, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Greataxe')
        javelin = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Javelin')
        javelin_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, range=30, name='Javelin_range')
        javelin_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, range=120, name='Javelin_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Otyugh(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration", 'ac': 14, 'max_hp': 114, 'speed': 30, 
            "strength": 16, "dexterity": 11, "constitution": 19, "intelligence": 6, "wisdom": 13, "charisma": 6}
        default_kwargs.update({"proficiencies": {'constitution'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Limited Telepathy. The otyugh can magically transmit simple messages and images to any creature within 120 ft. of it that can understand a language. This form of telepathy doesn’t allow the receiving creature to telepathically respond.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
            #  If the target is a creature, it must succeed on a DC 15 Constitution saving throw against disease or become poisoned until the disease is cured. Every 24 hours that elapse, the target must repeat the saving throw, reducing its hit point maximum by 5 (1d10) on a failure. The disease is cured on a success. The target dies if the disease reduces its hit point maximum to 0. This reduction to the target’s hit point maximum lasts until the disease is cured.
        tentacle = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='bludgeoning'), dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='piercing')]), attack_mod=6, melee_range=10, name='Tentacle')
            #  If the target is Medium or smaller, it is grappled (escape DC 13) and restrained until the grapple ends. The otyugh has two tentacles, each of which can grapple one target.
        Tentacle Slam. The otyugh slams creatures grappled by it into each other or a solid surface. Each creature must succeed on a DC 14 Constitution saving throw or take 10 (2d6 + 3) bludgeoning damage and be stunned until the end of the otyugh’s next turn. On a successful save, the target takes half the bludgeoning damage and isn’t stunned.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, tentacles, tentacles])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Owlbear(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 13, 'max_hp': 59, 'speed': 40, 
            "strength": 20, "dexterity": 12, "constitution": 17, "intelligence": 3, "wisdom": 12, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Keen Sight and Smell. The owlbear has advantage on Wisdom (Perception) checks that rely on sight or smell.
        """
        # Actions
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=5, damage_type='piercing'), attack_mod=7, melee_range=5, name='Beak')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=5, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claws')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beak, claws])
        default_kwargs.update(attacks=[multiattack, beak, claws])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Pegasus(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: improperly formatted details
        default_kwargs = {"size": "large", "creature_type": "celestial", 'ac': 12, 'max_hp': 59, 'speed': 60, 'fly_speed': 90, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 10, "wisdom": 15, "charisma": 13}
        default_kwargs.update({'vision': "normal", 'cr': 0})
        # Features
        """
        """
        # Actions
        hooves = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Hooves')
        default_kwargs.update(attacks=[hooves])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Plesiosaurus(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "beast", 'ac': 13, 'max_hp': 68, 'speed': 20, 'swim_speed': 40, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 2, "wisdom": 12, "charisma": 5}
        default_kwargs.update({'vision': "normal", 'cr': 2})
        # Features
        """
        Hold Breath. The plesiosaurus can hold its breath for 1 hour.
        """
        # Actions
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=10, name='Bite')
        default_kwargs.update(attacks=[bite])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Pseudodragon(Creature):  # needs poison
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "dragon", 'ac': 13, 'max_hp': 7, 'speed': 15, 'fly_speed': 60, 
            "strength": 6, "dexterity": 15, "constitution": 13, "intelligence": 10, "wisdom": 12, "charisma": 10}
        default_kwargs.update({'vision': "blindsight", 'cr': 1})
        # Features
        """
        Keen Senses. The pseudodragon has advantage on Wisdom (Perception) checks that rely on sight, hearing, or smell.
        Magic Resistance. The pseudodragon has advantage on saving throws against spells and other magical effects.
        Limited Telepathy. The pseudodragon can magically communicate simple ideas, emotions, and images telepathically with any creature within 100 ft. of it that can understand a language.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite')
        sting = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Sting')
            # the target must succeed on a DC 11 Constitution saving throw or become poisoned for 1 hour. If the saving throw fails by 5 or more, the target falls unconscious for the same duration, or until it takes damage or another creature uses an action to shake it awake.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class PurpleWorm(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "monstrosity", 'ac': 18, 'max_hp': 247, 'speed': 50, 
            "strength": 28, "dexterity": 7, "constitution": 22, "intelligence": 1, "wisdom": 8, "charisma": 4}
        default_kwargs.update({"proficiencies": {'wisdom', 'constitution'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 15})
        # Features
        """
        Tunneler. The worm can burrow through solid rock at half its burrow speed and leaves a 10-foot-diameter tunnel in its wake.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=9, damage_type='piercing'), attack_mod=9, melee_range=10, name='Bite')
            #  If the target is a Large or smaller creature, it must succeed on a DC 19 Dexterity saving throw or be swallowed by the worm. A swallowed creature is blinded and restrained, it has total cover against attacks and other effects outside the worm, and it takes 21 (6d6) acid damage at the start of each of the worm’s turns.If the worm takes 30 damage or more on a single turn from a creature inside it, the worm must succeed on a DC 21 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the worm. If the worm dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 20 feet of movement, exiting prone.
        tail_stinger = attack_class.HitAndSaveAttack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=9, damage_type='piercing'), attack_mod=9, melee_range=10, name='Tail Stinger', dc=19, save_type='constitution', save_damage_dice='12d6', save_damage_type='poison', damage_on_success=True)
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, stinger])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Rakshasa(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend", 'ac': 16, 'max_hp': 110, 'speed': 40, 
            "strength": 14, "dexterity": 17, "constitution": 18, "intelligence": 13, "wisdom": 16, "charisma": 20}
        default_kwargs.update({"immunities": {' bludgeoning', 'piercing', 'slashing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 13})
        # Features
        """
        Limited Magic Immunity. The rakshasa can’t be affected or detected by spells of 6th level or lower unless it wishes to be. It has advantage on saving throws against all other spells and magical effects.
        Innate Spellcasting. The rakshasa’s innate spellcasting ability is Charisma (spell save DC 18, +10 to hit with spell attacks). The rakshasa can innately cast the following spells, requiring no material components:
        At will: detect thoughts, disguise self, mage hand, minor illusion
        3/day each: charm person, detect magic, invisibility, major image, suggestion
        1/day each: dominate person, fly, plane shift, true seeing
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
            # the target is cursed if it is a creature. The magical curse takes effect whenever the target takes a short or long rest, filling the target’s thoughts with horrible images and dreams. The cursed target gains no benefit from finishing a short or long rest. The curse lasts until it is lifted by a remove curse spell or similar magic.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Remorhaz(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "monstrosity", 'ac': 17, 'max_hp': 195, 'speed': 30, 
            "strength": 24, "dexterity": 13, "constitution": 21, "intelligence": 4, "wisdom": 10, "charisma": 5}
        default_kwargs.update({"immunities": {' cold', 'fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 11})
        # Features
        """
        Heated Body. A creature that touches the remorhaz or hits it with a melee attack while within 5 feet of it takes 10 (3d6) fire damage.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=6, dice_type=10, modifier=7, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=11, melee_range=10, name='Bite')
            #  If the target is a creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained, and the remorhaz can’t bite another target.
        Swallow. The remorhaz makes one bite attack against a Medium or smaller creature it is grappling. If the attack hits, that creature takes the bite’s damage and is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the remorhaz, and it takes 21 (6d6) acid damage at the start of each of the remorhaz’s turns. If the remorhaz takes 30 damage or more on a single turn from a creature inside it, the remorhaz must succeed on a DC 15 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet oft he remorhaz. If the remorhaz dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 15 feet of movement, exiting prone.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Roc(Creature):  # needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "monstrosity", 'ac': 15, 'max_hp': 248, 'speed': 20, 'fly_speed': 120, 
            "strength": 28, "dexterity": 10, "constitution": 20, "intelligence": 3, "wisdom": 10, "charisma": 9}
        default_kwargs.update({"proficiencies": {'wisdom', 'charisma', 'constitution', 'dexterity'}})
        default_kwargs.update({'vision': "normal", 'cr': 11})
        # Features
        """
        Keen Sight. The roc has advantage on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        beak = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=8, modifier=9, damage_type='piercing'), attack_mod=13, melee_range=10, name='Beak')
        talons = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=6, modifier=9, damage_type='slashing'), attack_mod=13, melee_range=5, name='Talons')
            # the target is grappled (escape DC 19). Until this grapple ends, the target is restrained, and the roc can’t use its talons on another target.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[beak, talons])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Roper(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 20, 'max_hp': 93, 'speed': 10, 'climb_speed': 10, 
            "strength": 18, "dexterity": 8, "constitution": 17, "intelligence": 7, "wisdom": 16, "charisma": 6}
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        False Appearance. While the roper remains motionless, it is indistinguishable from a normal cave formation, such as a stalagmite.
        Grasping Tendrils. The roper can have up to six tendrils at a time. Each tendril can be attacked (AC 20; 10 hit points; immunity to poison and psychic damage). Destroying a tendril deals no damage to the roper, which can extrude a replacement tendril on its next turn. A tendril can also be broken if a creature takes an action and succeeds on a DC 15 Strength check against it.
        Spider Climb. The roper can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
        Tendril. Melee Weapon Attack: +7 to hit, reach 50 ft., one creature. Hit: The target is grappled (escape DC 15). Until the grapple ends, the target is restrained and has disadvantage on Strength checks and Strength saving throws, and the roper can’t use the same tendril on another target.
        Reel. The roper pulls each creature grappled by it up to 25 ft. straight toward it.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class RugofSmothering(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "construct", 'ac': 12, 'max_hp': 33, 'speed': 10, 
            "strength": 17, "dexterity": 14, "constitution": 10, "intelligence": 1, "wisdom": 3, "charisma": 1}
        default_kwargs.update({"immunities": {'petrified', 'charmed', 'paralyzed', 'psychic', 'poisoned', 'deafened', ' blinded', ' poison', 'frightened'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 2})
        # Features
        """
        Antimagic Susceptibility. The rug is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the rug must succeed on a Constitution saving throw against the caster’s spell save DC or fall unconscious for 1 minute.
        Damage Transfer. While it is grappling a creature, the rug takes only half the damage dealt to it, and the creature grappled by the rug takes the other half.
        False Appearance. While the rug remains motionless, it is indistinguishable from a normal rug.
        """
        # Actions
        """
        smother = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='bludgeoning'), attack_mod=5, melee_range=5, name='Smother')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class RustMonster(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "monstrosity", 'ac': 14, 'max_hp': 27, 'speed': 40, 
            "strength": 13, "dexterity": 12, "constitution": 13, "intelligence": 2, "wisdom": 13, "charisma": 6}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Iron Scent. The rust monster can pinpoint, by scent, the location of ferrous metal within 30 feet of it.
        Rust Metal. Any nonmagical weapon made of metal that hits the rust monster corrodes. After dealing damage, the weapon takes a permanent and cumulative -1 penalty to damage rolls. If its penalty drops to -5, the weapon is destroyed. Non magical ammunition made of metal that hits the rust monster is destroyed after dealing damage.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, melee_range=5, name='Bite')
        Antennae. The rust monster corrodes a nonmagical ferrous metal object it can see within 5 feet of it. If the object isn’t being worn or carried, the touch destroys a 1-foot cube of it. If the object is being worn or carried by a creature, the creature can make a DC 11 Dexterity saving throw to avoid the rust monster’s touch. If the object touched is either metal armor or a metal shield being worn or carried, its takes a permanent and cumulative -1 penalty to the AC it offers. Armor reduced to an AC of 10 or a shield that drops to a +0 bonus is destroyed. If the object touched is a held metal weapon, it rusts as described in the Rust Metal trait.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Sahuagin(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (sahuagin)", 'ac': 12, 'max_hp': 22, 'speed': 30, 'swim_speed': 40, 
            "strength": 13, "dexterity": 11, "constitution": 12, "intelligence": 12, "wisdom": 13, "charisma": 9}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Blood Frenzy. The sahuagin has advantage on melee attack rolls against any creature that doesn’t have all its hit points.
        Limited Amphibiousness. The sahuagin can breathe air and water, but it needs to be submerged at least once every 4 hours to avoid suffocating.
        Shark Telepathy. The sahuagin can magically command any shark within 120 feet of it, using a limited telepathy.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='piercing'), attack_mod=3, melee_range=5, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=1, damage_type='slashing'), attack_mod=3, melee_range=5, name='Claws')
        spear = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=1, damage_type='piercing'), attack_mod=3, melee_range=5, name='Spear')
        spear_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=1, damage_type='piercing'), attack_mod=3, melee_range=5, name='Spear_versatile')
        spear_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=1, damage_type='piercing'), attack_mod=3, range=20, name='Spear_range')
        spear_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=1, damage_type='piercing'), attack_mod=3, range=60, name='Spear_range_disadvantage')
        multiattack_alt = attack_class.MultiAttack(name="Multiattack (alternate)", attack_list=[bite, spear]
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Salamander(Creature):  # needs grapple, needs work
    def __init__(self, **kwargs):  # errata: had to add fire damage manually
        default_kwargs = {"size": "large", "creature_type": "elemental", 'ac': 15, 'max_hp': 90, 'speed': 30, 
            "strength": 18, "dexterity": 14, "constitution": 15, "intelligence": 11, "wisdom": 10, "charisma": 12}
        default_kwargs.update({"resistances": {'slashing', ' bludgeoning', 'piercing'}})
        default_kwargs.update({"immunities": {' fire'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Heated Body. A creature that touches the salamander or hits it with a melee attack while within 5 ft. of it takes 7 (2d6) fire damage.
        Heated Weapons. Any metal melee weapon the salamander wields deals an extra 3 (1d6) fire damage on a hit (included in the attack).
        """
        # Actions
        """
        spear_versatile = attack_class.Attack(damage_dice=dice.DamageDiceBag([dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, damage_type='fire')]), attack_mod=7, melee_range=5, name='Spear_versatile')
        spear = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=1, dice_type=6, damage_type='fire')]), attack_mod=7, melee_range=5, name='Spear')
        tail = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='fire')]), attack_mod=7, melee_range=10, name='Tail')
            # the target is grappled (escape DC 14). Until this grapple ends, the target is restrained, the salamander can automatically hit the target with its tail, and the salamander can’t make tail attacks against other targets.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[spear, tail])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Satyr(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: improper formatting "1 d6" instead of "1d6"
        default_kwargs = {"size": "medium", "creature_type": "fey", 'ac': 14, 'max_hp': 31, 'speed': 40, 
            "strength": 12, "dexterity": 16, "constitution": 11, "intelligence": 12, "wisdom": 10, "charisma": 14}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        Magic Resistance. The satyr has advantage on saving throws against spells and other magical effects.
        """
        # Actions
        ram = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=1, damage_type='bludgeoning'), attack_mod=3, melee_range=5, name='Ram')
        shortsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Shortsword')
        shortbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, range=80, name='Shortbow_range')
        shortbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=5, range=320, name='Shortbow_range_disadvantage')
        default_kwargs.update(attacks=[ram, shortsword, shortbow_range, shortbow_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Shadow(Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 12, 'max_hp': 16, 'speed': 40, 
            "strength": 6, "dexterity": 14, "constitution": 13, "intelligence": 6, "wisdom": 10, "charisma": 8}
        default_kwargs.update({"resistances": {'fire', ' acid', 'lightning', 'slashing', 'thunder', 'piercing', 'cold', 'bludgeoning'}})
        default_kwargs.update({"immunities": {' exhaustion', 'grappled', 'poisoned', 'restrained', 'petrified', 'poison', 'frightened', ' necrotic', 'prone', 'paralyzed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Amorphous. The shadow can move through a space as narrow as 1 inch wide without squeezing.
        Shadow Stealth. While in dim light or darkness, the shadow can take the Hide action as a bonus action. Its stealth bonus is also improved to +6.
        Sunlight Weakness. While in sunlight, the shadow has disadvantage on attack rolls, ability checks, and saving throws.
        """
        # Actions
        """
        strength_drain = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=2, damage_type='necrotic'), attack_mod=4, melee_range=5, name='Strength Drain')
            # the target’s Strength score is reduced by 1d4. The target dies if this reduces its Strength to 0. Otherwise, the reduction lasts until the target finishes a short or long rest.If a non-evil humanoid dies from this attack, a new shadow rises from the corpse 1d4 hours later.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ShamblingMound(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "plant", 'ac': 15, 'max_hp': 136, 'speed': 20, 'swim_speed': 20, 
            "strength": 18, "dexterity": 8, "constitution": 16, "intelligence": 5, "wisdom": 10, "charisma": 5}
        default_kwargs.update({"resistances": {'fire', ' cold'}})
        default_kwargs.update({"immunities": {' blinded', ' lightning', 'exhaustion', 'deafened'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 5})
        # Features
        """
        Lightning Absorption. Whenever the shambling mound is subjected to lightning damage, it takes no damage and regains a number of hit points equal to the lightning damage dealt.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Slam')
        Engulf. The shambling mound engulfs a Medium or smaller creature grappled by it. The engulfed target is blinded, restrained, and unable to breathe, and it must succeed on a DC 14 Constitution saving throw at the start of each of the mound’s turns or take 13 (2d8 + 4) bludgeoning damage. If the mound moves, the engulfed target moves with it. The mound can have only one creature engulfed at a time.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class ShieldGuardian(Creature):  # complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "construct", 'ac': 17, 'max_hp': 142, 'speed': 30, 
            "strength": 18, "dexterity": 8, "constitution": 18, "intelligence": 7, "wisdom": 10, "charisma": 3}
        default_kwargs.update({"immunities": {' poison', 'exhaustion', 'poisoned', ' charmed', 'frightened', 'paralyzed'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 7})
        # Features
        """
        Bound. The shield guardian is magically bound to an amulet. As long as the guardian and its amulet are on the same plane of existence, the amulet’s wearer can telepathically call the guardian to travel to it, and the guardian knows the distance and direction to the amulet. If the guardian is within 60 feet of the amulet’s wearer, half of any damage the wearer takes (rounded up) is transferred to the guardian.
        Regeneration. The shield guardian regains 10 hit points at the start of its turn if it has at least 1 hit. point.
        Spell Storing. A spellcaster who wears the shield guardian’s amulet can cause the guardian to store one spell of 4th level or lower. To do so, the wearer must cast the spell on the guardian. The spell has no effect but is stored within the guardian. When commanded to do so by the wearer or when a situation arises that was predefined by the spellcaster, the guardian casts the stored spell with any parameters set by the original caster, requiring no components. When the spell is cast or a new spell is stored, any previously stored spell is lost.
        """
        # Actions
        """
        fist = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Fist')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[fist, fist])
        """
        # Reactions
        """
        Shield. When a creature makes an attack against the wearer of the guardian’s amulet, the guardian grants a +2 bonus to the wearer’s AC if the guardian is within 5 feet of the wearer.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Skeleton(Creature):  # production-ready
    def __init__(self, **kwargs):  # errata: vulnerable to bludgeoning
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 13, 'max_hp': 13, 'speed': 30, 
            "strength": 10, "dexterity": 14, "constitution": 15, "intelligence": 6, "wisdom": 8, "charisma": 5}
        default_kwargs.update({"vulnerabilities": {"bludgeoning"}})
        default_kwargs.update({"immunities": {'poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 0.25})
        # Features
        """
        """
        # Actions
        shortsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Shortsword')
        shortbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=80, name='Shortbow_range')
        shortbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=320, name='Shortbow_range_disadvantage')
        default_kwargs.update(attacks=[shortsword, shortbow_range, shortbow_disadvantage])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class SkeletonMinotaur(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "undead", 'ac': 12, 'max_hp': 67, 'speed': 40, 
            "strength": 18, "dexterity": 11, "constitution": 15, "intelligence": 6, "wisdom": 8, "charisma": 5}
        default_kwargs.update({"immunities": {' poison', 'poisoned', ' exhaustion'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Charge. If the skeleton moves at least 10 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be pushed up to 10 feet away and knocked prone.
        """
        # Actions
        """
        greataxe = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=12, modifier=4, damage_type='slashing'), attack_mod=6, melee_range=5, name='Greataxe')
        gore = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=6, melee_range=5, name='Gore')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class SkeletonWarhorse(Creature):  # production-ready
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "undead", 'ac': 13, 'max_hp': 22, 'speed': 60, 
            "strength": 18, "dexterity": 12, "constitution": 15, "intelligence": 2, "wisdom": 8, "charisma": 5}
        default_kwargs.update({"immunities": {' poison', ' exhaustion', 'poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        """
        # Actions
        hooves = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=6, melee_range=5, name='Hooves')
        default_kwargs.update(attacks=[hooves])

        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Specter(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 12, 'max_hp': 22, 'speed': 0, 'fly_speed': 50, 
            "strength": 1, "dexterity": 14, "constitution": 11, "intelligence": 10, "wisdom": 10, "charisma": 11}
        default_kwargs.update({"resistances": {'fire', 'slashing', ' acid', 'bludgeoning', 'thunder', 'piercing', 'cold', 'lightning'}})
        default_kwargs.update({"immunities": {'restrained', 'unconscious', ' necrotic', 'prone', 'exhaustion', 'poisoned', 'grappled', 'petrified', 'poison', 'paralyzed', ' charmed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Incorporeal Movement. The specter can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.
        Sunlight Sensitivity. While in sunlight, the specter has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        Life Drain. Melee Spell Attack: +4 to hit, reach 5 ft., one creature. Hit: 10 (3d6) necrotic damage. The target must succeed on a DC 10 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the creature finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class SphinxAndrosphinx(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 17, 'max_hp': 199, 'speed': 40, 'fly_speed': 60, 
            "strength": 22, "dexterity": 10, "constitution": 20, "intelligence": 16, "wisdom": 18, "charisma": 23}
        default_kwargs.update({"proficiencies": {'constitution', 'intelligence', 'dexterity', 'wisdom'}})
        default_kwargs.update({"immunities": {'bludgeoning', 'slashing', 'piercing', ' charmed', ' psychic', 'frightened'}})
        default_kwargs.update({'vision': "truesight", 'cr': 17})
        # Features
        """
        Inscrutable. The sphinx is immune to any effect that would sense its emotions or read its thoughts, as well as any divination spell that it refuses. Wisdom (Insight) checks made to ascertain the sphinx’s intentions or sincerity have disadvantage.
        Magic Weapons. The sphinx’s weapon attacks are magical.
        Spellcasting. The sphinx is a 12th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 18, +10 to hit with spell attacks). It requires no material components to cast its spells. The sphinx has the following cleric spells prepared:
        Cantrips (at will): sacred flame, spare the dying, thaumaturgy
        1st level (4 slots): command, detect evil and good, detect magic
        2nd level (3 slots): lesser restoration, zone of truth
        3rd level (3 slots): dispel magic, tongues
        4th level (3 slots): banishment, freedom of movement
        5th level (2 slots): flame strike, greater restoration
        6th level (1 slot): heroes’ feast
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=6, damage_type='slashing'), attack_mod=12, melee_range=5, name='Claw')
        Roar (3/Day). The sphinx emits a magical roar. Each time it roars before finishing a long rest, the roar is louder and the effect is different, as detailed below. Each creature within 500 feet of the sphinx and able to hear the roar must make a saving throw. First Roar. Each creature that fails a DC 18 Wisdom saving throw is frightened for 1 minute. A frightened creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Second Roar. Each creature that fails a DC 18 Wisdom saving throw is deafened and frightened for 1 minute. A frightened creature is paralyzed and can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Third Roar. Each creature makes a DC 18 Constitution saving throw. On a failed save, a creature takes 44 (8d10) thunder damage and is knocked prone. On a successful save, the creature takes half as much damage and isn’t knocked prone.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[claw, claw])
        """
        # Legendary Actions
        """
        Claw Attack. The sphinx makes one claw attack.
        Teleport (Costs 2 Actions). The sphinx magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.
        Cast a Spell (Costs 3 Actions). The sphinx casts a spell from its list of prepared spells, using a spell slot as normal.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class SphinxGynosphinx(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "monstrosity", 'ac': 17, 'max_hp': 136, 'speed': 40, 'fly_speed': 60, 
            "strength": 18, "dexterity": 15, "constitution": 16, "intelligence": 18, "wisdom": 18, "charisma": 18}
        default_kwargs.update({"resistances": {'piercing', ' bludgeoning', 'slashing'}})
        default_kwargs.update({"immunities": {'frightened', ' psychic', ' charmed'}})
        default_kwargs.update({'vision': "truesight", 'cr': 11})
        # Features
        """
        Inscrutable. The sphinx is immune to any effect that would sense its emotions or read its thoughts, as well as any divination spell that it refuses. Wisdom (Insight) checks made to ascertain the sphinx’s intentions or sincerity have disadvantage.
        Magic Weapons. The sphinx’s weapon attacks are magical.
        Spellcasting. The sphinx is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 16, +8 to hit with spell attacks). It requires no material components to cast its spells. The sphinx has the following wizard spells prepared:
        Cantrips (at will): mage hand, minor illusion, prestidigitation
        1st level (4 slots): detect magic, identify, shield
        2nd level (3 slots): darkness, locate object, suggestion
        3rd level (3 slots): dispel magic, remove curse, tongues
        4th level (3 slots): banishment, greater invisibility
        5th level (1 slot): legend lore
        """
        # Actions
        """
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=9, melee_range=5, name='Claw')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[claw, claw])
        """
        # Legendary Actions
        """
        Claw Attack. The sphinx makes one claw attack.
        Teleport (Costs 2 Actions). The sphinx magically teleports, along with any equipment it is wearing or carrying, up to 120 feet to an unoccupied space it can see.
        Cast a Spell (Costs 3 Actions). The sphinx casts a spell from its list of prepared spells, using a spell slot as normal.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Sprite(Creature):  # needs poison, needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "fey", 'ac': 15, 'max_hp': 2, 'speed': 10, 'fly_speed': 40, 
            "strength": 3, "dexterity": 18, "constitution": 10, "intelligence": 14, "wisdom": 13, "charisma": 11}
        default_kwargs.update({'vision': "normal", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=1, modifier=0, damage_type='slashing'), attack_mod=2, melee_range=5, name='Longsword')
        shortbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=1, modifier=0, damage_type='piercing'), attack_mod=6, range=40, name='Shortbow_range')
        shortbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=1, modifier=0, damage_type='piercing'), attack_mod=6, range=160, name='Shortbow_range_disadvantage')
            # the target must succeed on a DC 10 Constitution saving throw or become poisoned for 1 minute. If its saving throw result is 5 or lower, the poisoned target falls unconscious for the same duration, or until it takes damage or another creature takes an action to shake it awake.
        Heart Sight. The sprite touches a creature and magically knows the creature’s current emotional state. If the target fails a DC 10 Charisma saving throw, the sprite also knows the creature’s alignment. Celestials, fiends, and undead automatically fail the saving throw.
        Invisibility. The sprite magically turns invisible until it attacks or casts a spell, or until its concentration ends (as if concentrating on a spell). Any equipment the sprite wears or carries is invisible with it.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Stirge(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "beast", 'ac': 14, 'max_hp': 2, 'speed': 10, 'fly_speed': 40, 
            "strength": 4, "dexterity": 16, "constitution": 11, "intelligence": 2, "wisdom": 8, "charisma": 6}
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        """
        # Actions
        """
        blood_drain = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Blood Drain')
            # the stirge attaches to the target. While attached, the stirge doesn’t attack. Instead, at the start of each of the stirge’s turns, the target loses 5 (1d4 + 3) hit points due to blood loss.The stirge can detach itself by spending 5 feet of its movement. It does so after it drains 10 hit points of blood from the target or the target dies. A creature, including the target, can use its action to detach the stirge.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class SuccubusIncubus(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "fiend (shapechanger)", 'ac': 15, 'max_hp': 66, 'speed': 30, 'fly_speed': 60, 
            "strength": 8, "dexterity": 17, "constitution": 13, "intelligence": 15, "wisdom": 12, "charisma": 20}
        default_kwargs.update({"resistances": {'poison', 'bludgeoning', 'fire', ' cold', 'lightning', 'slashing', 'piercing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 4})
        # Features
        """
        Telepathic Bond. The fiend ignores the range restriction on its telepathy when communicating with a creature it has charmed. The two don’t even need to be on the same plane of existence.
        Shapechanger. The fiend can use its action to polymorph into a Small or Medium humanoid, or back into its true form. Without wings, the fiend loses its flying speed. Other than its size and speed, its statistics are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        """
        # Actions
        """
        claw_fiend_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claw (Fiend Form Only)')
        Charm. One humanoid the fiend can see within 30 feet of it must succeed on a DC 15 Wisdom saving throw or be magically charmed for 1 day. The charmed target obeys the fiend’s verbal or telepathic commands. If the target suffers any harm or receives a suicidal command, it can repeat the saving throw, ending the effect on a success. If the target successfully saves against the effect, or if the effect on it ends, the target is immune to this fiend’s Charm for the next 24 hours. The fiend can have only one target charmed at a time. If it charms another, the effect on the previous target ends.
        Draining Kiss. The fiend kisses a creature charmed by it or a willing creature. The target must make a DC 15 Constitution saving throw against this magic, taking 32 (5d10 + 5) psychic damage on a failed save, or half as much damage on a successful one. The target’s hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.
        Etherealness. The fiend magically enters the Ethereal Plane from the Material Plane, or vice versa.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Tarrasque(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "gargantuan", "creature_type": "monstrosity (titan)", 'ac': 25, 'max_hp': 676, 'speed': 40, 
            "strength": 30, "dexterity": 11, "constitution": 30, "intelligence": 3, "wisdom": 11, "charisma": 11}
        default_kwargs.update({"proficiencies": {'wisdom', 'charisma', 'intelligence'}})
        default_kwargs.update({"immunities": {' charmed', 'paralyzed', 'poisoned', 'piercing', ' fire', 'frightened', 'bludgeoning', 'poison', 'slashing'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 30})
        # Features
        """
        Legendary Resistance (3/Day). If the tarrasque fails a saving throw, it can choose to succeed instead.
        Magic Resistance. The tarrasque has advantage on saving throws against spells and other magical effects.
        Reflective Carapace. Any time the tarrasque is targeted by a magic missile spell, a line spell, or a spell that requires a ranged attack roll, roll a d6. On a 1 to 5, the tarrasque is unaffected. On a 6, the tarrasque is unaffected, and the effect is reflected back at the caster as though it originated from the tarrasque, turning the caster into the target.
        Siege Monster. The tarrasque deals double damage to objects and structures.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=12, modifier=10, damage_type='piercing'), attack_mod=19, melee_range=10, name='Bite')
            #  If the target is a creature, it is grappled (escape DC 20). Until this grapple ends, the target is restrained, and the tarrasque can’t bite another target.
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=8, modifier=10, damage_type='slashing'), attack_mod=19, melee_range=15, name='Claw')
        horns = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=10, damage_type='piercing'), attack_mod=19, melee_range=10, name='Horns')
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=6, modifier=10, damage_type='bludgeoning'), attack_mod=19, melee_range=20, name='Tail')
            #  If the target is a creature, it must succeed on a DC 20 Strength saving throw or be knocked prone.
        Frightful Presence. Each creature of the tarrasque’s choice within 120 feet of it and aware of it must succeed on a DC 17 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, with disadvantage if the tarrasque is within line of sight, ending the effect on itself on a success. If a creature’s saving throw is successful or the effect ends for it, the creature is immune to the tarrasque’s Frightful Presence for the next 24 hours.
        Swallow. The tarrasque makes one bite attack against a Large or smaller creature it is grappling. If the attack hits, the target takes the bite’s damage, the target is swallowed, and the grapple ends. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects outside the tarrasque, and it takes 56 (16d6) acid damage at the start of each of the tarrasque’s turns. If the tarrasque takes 60 damage or more on a single turn from a creature inside it, the tarrasque must succeed on a DC 20 Constitution saving throw at the end of that turn or regurgitate all swallowed creatures, which fall prone in a space within 10 feet of the tarrasque. If the tarrasque dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 30 feet of movement, exiting prone.
        """
        # Legendary Actions
        """
        Attack. The tarrasque makes one claw attack or tail attack.
        Move. The tarrasque moves up to half its speed.
        Chomp (Costs 2 Actions). The tarrasque makes one bite attack or uses its Swallow.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Treant(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "plant", 'ac': 16, 'max_hp': 138, 'speed': 30, 
            "strength": 23, "dexterity": 8, "constitution": 21, "intelligence": 12, "wisdom": 16, "charisma": 12}
        default_kwargs.update({"resistances": {' bludgeoning', 'piercing'}})
        default_kwargs.update({'vision': "normal", 'cr': 9})
        # Features
        """
        False Appearance. While the treant remains motionless, it is indistinguishable from a normal tree.
        Siege Monster. The treant deals double damage to objects and structures.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=6, damage_type='bludgeoning'), attack_mod=10, melee_range=5, name='Slam')
        rock_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=10, range=60, name='Rock_range')
        rock_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=10, range=180, name='Rock_range_disadvantage')
        Animate Trees (1/Day). The treant magically animates one or two trees it can see within 60 feet of it. These trees have the same statistics as a treant, except they have Intelligence and Charisma scores of 1, they can’t speak, and they have only the Slam action option. An animated tree acts as an ally of the treant. The tree remains animate for 1 day or until it dies; until the treant dies or is more than 120 feet from the tree; or until the treant takes a bonus action to turn it back into an inanimate tree. The tree then takes root if possible.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[slam, slam])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Triceratops(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "beast", 'ac': 13, 'max_hp': 95, 'speed': 50, 
            "strength": 22, "dexterity": 9, "constitution": 17, "intelligence": 2, "wisdom": 11, "charisma": 5}
        default_kwargs.update({'vision': "normal", 'cr': 5})
        # Features
        """
        Trampling Charge. If the triceratops moves at least 20 ft. straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the triceratops can make one stomp attack against it as a bonus action.
        """
        # Actions
        """
        gore = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=8, modifier=6, damage_type='piercing'), attack_mod=9, melee_range=5, name='Gore')
        stomp = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=10, modifier=6, damage_type='bludgeoning'), attack_mod=9, melee_range=5, name='Stomp')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Troll(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "giant", 'ac': 15, 'max_hp': 84, 'speed': 30, 
            "strength": 18, "dexterity": 13, "constitution": 20, "intelligence": 7, "wisdom": 9, "charisma": 7}
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Keen Smell. The troll has advantage on Wisdom (Perception) checks that rely on smell.
        Regeneration. The troll regains 10 hit points at the start of its turn. If the troll takes acid or fire damage, this trait doesn’t function at the start of the troll’s next turn. The troll dies only if it starts its turn with 0 hit points and doesn’t regenerate.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class TyrannosaurusRex(Creature):  # needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "huge", "creature_type": "beast", 'ac': 13, 'max_hp': 136, 'speed': 50, 
            "strength": 25, "dexterity": 10, "constitution": 19, "intelligence": 2, "wisdom": 12, "charisma": 9}
        default_kwargs.update({'vision': "normal", 'cr': 8})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=12, modifier=7, damage_type='piercing'), attack_mod=10, melee_range=10, name='Bite')
            #  If the target is a Medium or smaller creature, it is grappled (escape DC 17). Until this grapple ends, the target is restrained, and the tyrannosaurus can’t bite another target.
        tail = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=8, modifier=7, damage_type='bludgeoning'), attack_mod=10, melee_range=10, name='Tail')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, tail])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Unicorn(Creature):  # needs spell
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "celestial", 'ac': 12, 'max_hp': 67, 'speed': 50, 
            "strength": 18, "dexterity": 14, "constitution": 15, "intelligence": 11, "wisdom": 17, "charisma": 16}
        default_kwargs.update({"immunities": {'poisoned', ' charmed', ' poison', 'paralyzed'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Charge. If the unicorn moves at least 20 ft. straight toward a target and then hits it with a horn attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.
        Innate Spellcasting. The unicorn’s innate spellcasting ability is Charisma (spell save DC 14). The unicorn can innately cast the following spells, requiring no components:
        At will: detect evil and good, druidcraft, pass without trace
        1/day each: calm emotions, dispel evil and good, entangle
        Magic Resistance. The unicorn has advantage on saving throws against spells and other magical effects.
        Magic Weapons. The unicorn’s weapon attacks are magical.
        """
        # Actions
        """
        hooves = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='bludgeoning'), attack_mod=7, melee_range=5, name='Hooves')
        horn = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Horn')
        Healing Touch (3/Day). The unicorn touches another creature with its horn. The target magically regains 11 (2d8 + 2) hit points. In addition, the touch removes all diseases and neutralizes all poisons afflicting the target.
        Teleport (1/Day). The unicorn magically teleports itself and up to three willing creatures it can see within 5 ft. of it, along with any equipment they are wearing or carrying, to a location the unicorn is familiar with, up to 1 mile away.
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[hooves, horn])
        """
        # Legendary Actions
        """
        Hooves. The unicorn makes one attack with its hooves.
        Shimmering Shield (Costs 2 Actions). The unicorn creates a shimmering, magical field around itself or another creature it can see within 60 ft. of it. The target gains a +2 bonus to AC until the end of the unicorn’s next turn.
        Heal Self (Costs 3 Actions). The unicorn magically regains 11 (2d8 + 2) hit points.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Vampire(Creature):  # needs spelll, needs movement, needs grapple
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead (shapechanger)", 'ac': 16, 'max_hp': 144, 'speed': 30, 
            "strength": 18, "dexterity": 18, "constitution": 18, "intelligence": 17, "wisdom": 15, "charisma": 18}
        default_kwargs.update({"proficiencies": {'wisdom', 'dexterity', 'charisma'}})
        default_kwargs.update({"resistances": {' necrotic', 'slashing', 'piercing', 'bludgeoning'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 13})
        # Features
        """
        Shapechanger. If the vampire isn’t in sun light or running water, it can use its action to polymorph into a Tiny bat or a Medium cloud of mist, or back into its true form.
        While in bat form, the vampire can’t speak, its walking speed is 5 feet, and it has a flying speed of 30 feet. Its statistics, other than its size and speed, are unchanged. Anything it is wearing transforms with it, but nothing it is carrying does. It reverts to its true form if it dies.
        While in mist form, the vampire can’t take any actions, speak, or manipulate objects. It is weightless, has a flying speed of 20 feet, can hover, and can enter a hostile creature’s space and stop there. In addition, if air can pass through a space, the mist can do so without squeezing, and it can’t pass through water. It has advantage on Strength, Dexterity, and Constitution saving throws, and it is immune to all nonmagical damage, except the damage it takes from sunlight.
        Legendary Resistance (3/Day). If the vampire fails a saving throw, it can choose to succeed instead.
        Misty Escape. When it drops to 0 hit points outside its resting place, the vampire transforms into a cloud of mist (as in the Shapechanger trait) instead of falling unconscious, provided that it isn’t in sunlight or running water. If it can’t transform, it is destroyed.
        While it has 0 hit points in mist form, it can’t revert to its vampire form, and it must reach its resting place within 2 hours or be destroyed. Once in its resting place, it reverts to its vampire form. It is then paralyzed until it regains at least 1 hit point. After spending 1 hour in its resting place with 0 hit points, it regains 1 hit point.
        Regeneration. The vampire regains 20 hit points at the start of its turn if it has at least 1 hit point and isn’t in sunlight or running water. If the vampire takes radiant damage or damage from holy water, this trait doesn’t function at the start of the vampire’s next turn.
        Spider Climb. The vampire can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        Vampire Weaknesses. The vampire has the following flaws:
        Forbiddance. The vampire can’t enter a residence without an invitation from one of the occupants.
        Harmed by Running Water. The vampire takes 20 acid damage if it ends its turn in running water.
        Stake to the Heart. If a piercing weapon made of wood is driven into the vampire’s heart while the vampire is incapacitated in its resting place, the vampire is paralyzed until the stake is removed.
        Sunlight Hypersensitivity. The vampire takes 20 radiant damage when it starts its turn in sunlight. While in sunlight, it has disadvantage on attack rolls and ability checks.
        """
        # Actions
        """
        unarmed_strike_vampire_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=4, damage_type='bludgeoning'), attack_mod=9, melee_range=5, name='Unarmed Strike (Vampire Form Only)')
            #  Instead of dealing damage, the vampire can grapple the target (escape DC 18).
        bite_bat_or_vampire_form_only = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=6, modifier=4, damage_type='piercing'), dice.DamageDice(dice_num=3, dice_type=6, modifier=0, damage_type='necrotic')]), attack_mod=9, melee_range=5, name='Bite (Bat or Vampire Form Only)')
            #  The target’s hit point maximum is reduced by an amount equal to the necrotic damage taken, and the vampire regains hit points equal to that amount. The reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0. A humanoid slain in this way and then buried in the ground rises the following night as a vampire spawn under the vampire’s control.
        Charm. The vampire targets one humanoid it can see within 30 ft. of it. If the target can see the vampire, the target must succeed on a DC 17 Wisdom saving throw against this magic or be charmed by the vampire. The charmed target regards the vampire as a trusted friend to be heeded and protected. Although the target isn’t under the vampire’s control, it takes the vampire’s requests or actions in the most favorable way it can, and it is a willing target for the vampire’s bit attack. Each time the vampire or the vampire’s companions do anything harmful to the target, it can repeat the saving throw, ending the effect on itself on a success. Otherwise, the effect lasts 24 hours or until the vampire is destroyed, is on a different plane of existence than the target, or takes a bonus action to end the effect.
        Children of the Night (1/Day). The vampire magically calls 2d4 swarms of bats or rats, provided that the sun isn’t up. While outdoors, the vampire can call 3d6 wolves instead. The called creatures arrive in 1d4 rounds, acting as allies of the vampire and obeying its spoken commands. The beasts remain for 1 hour, until the vampire dies, or until the vampire dismisses them as a bonus action.
        """
        # Legendary Actions
        """
        Move. The vampire moves up to its speed without provoking opportunity attacks.
        Unarmed Strike. The vampire makes one unarmed strike.
        Bite (Costs 2 Actions). The vampire makes one bite attack.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class VampireSpawn(Creature):  # needs movement, complex
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 15, 'max_hp': 82, 'speed': 30, 
            "strength": 16, "dexterity": 16, "constitution": 16, "intelligence": 11, "wisdom": 10, "charisma": 12}
        default_kwargs.update({"proficiencies": {'wisdom', 'dexterity'}})
        default_kwargs.update({"resistances": {'slashing', ' necrotic', 'bludgeoning', 'piercing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Regeneration. The vampire regains 10 hit points at the start of its turn if it has at least 1 hit point and isn’t in sunlight or running water. If the vampire takes radiant damage or damage from holy water, this trait doesn’t function at the start of the vampire’s next turn.
        Spider Climb. The vampire can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.
        Vampire Weaknesses. The vampire has the following flaws:
        Forbiddance. The vampire can’t enter a residence without an invitation from one of the occupants.
        Harmed by Running Water. The vampire takes 20 acid damage when it ends its turn in running water.
        Stake to the Heart. The vampire is destroyed if a piercing weapon made of wood is driven into its heart while it is incapacitated in its resting place.
        Sunlight Hypersensitivity. The vampire takes 20 radiant damage when it starts its turn in sunlight. While in sunlight, it has disadvantage on attack rolls and ability checks.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDiceBag(dice_list=[dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='piercing'), dice.DamageDice(dice_num=2, dice_type=6, modifier=0, damage_type='necrotic')]), attack_mod=6, melee_range=5, name='Bite')
            #  The target’s hit point maximum is reduced by an amount equal to the necrotic damage taken, and the vampire regains hit points equal to that amount. The reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=5, name='Claws')
            #  Instead of dealing damage, the vampire can grapple the target (escape DC 13).
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class VioletFungus(Creature):  # complex
    def __init__(self, **kwargs):  # note: "multiattack: 1d4 rotting touch attacks"
        default_kwargs = {"size": "medium", "creature_type": "plant", 'ac': 5, 'max_hp': 18, 'speed': 5, 
            "strength": 3, "dexterity": 1, "constitution": 10, "intelligence": 1, "wisdom": 3, "charisma": 1}
        default_kwargs.update({"immunities": {' blinded', 'frightened', 'deafened'}})
        default_kwargs.update({'vision': "blindsight", 'cr': 0.25})
        # Features
        """
        False Appearance. While the violet fungus remains motionless, it is indistinguishable from an ordinary fungus.
        """
        # Actions
        """
        rotting_touch = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=0, damage_type='necrotic'), attack_mod=2, melee_range=10, name='Rotting Touch')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Werebear(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (human, shapechanger)", 'ac': 10, 'max_hp': 135, 'speed': 30, 
            "strength": 19, "dexterity": 10, "constitution": 17, "intelligence": 11, "wisdom": 12, "charisma": 12}
        default_kwargs.update({"immunities": {'piercing', "slashing", 'bludgeoning'}})
        default_kwargs.update({'vision': "normal", 'cr': 5})
        # Features
        """
        Shapechanger. The werebear can use its action to polymorph into a Large bear-humanoid hybrid or into a Large bear, or back into its true form, which is humanoid. Its statistics, other than its size and AC, are the same in each form. Any equipment it. is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Keen Smell. The werebear has advantage on Wisdom (Perception) checks that rely on smell.
        """
        # Actions
        """
        bite_bear_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=10, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=5, name='Bite (Bear or Hybrid Form Only)')
            #  If the target is a humanoid, it must succeed on a DC 14 Constitution saving throw or be cursed with were bear lycanthropy.
        claw_bear_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claw (Bear or Hybrid Form Only)')
        greataxe_humanoid_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=12, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Greataxe (Humanoid or Hybrid Form Only)')
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[claw, claw])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Wereboar(Creature):  # needs movement
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (human, shapechanger)", 'ac': 10, 'max_hp': 78, 'speed': 30, 
            "strength": 17, "dexterity": 10, "constitution": 15, "intelligence": 10, "wisdom": 11, "charisma": 8}
        default_kwargs.update({"immunities": {'bludgeoning', 'piercing', "slashing"}})
        default_kwargs.update({'vision': "normal", 'cr': 4})
        # Features
        """
        Shapechanger. The wereboar can use its action to polymorph into a boar-humanoid hybrid or into a boar, or back into its true form, which is humanoid. Its statistics, other than its AC, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Charge (Boar or Hybrid Form Only). If the wereboar moves at least 15 feet straight toward a target and then hits it with its tusks on the same turn, the target takes an extra 7 (2d6) slashing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.
        Relentless (Recharges after a Short or Long Rest). If the wereboar takes 14 damage or less that would reduce it to 0 hit points, it is reduced to 1 hit point instead.
        """
        # Actions
        """
        maul_humanoid_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='bludgeoning'), attack_mod=5, melee_range=5, name='Maul (Humanoid or Hybrid Form Only)')
        tusks_boar_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Tusks (Boar or Hybrid Form Only)')
            #  If the target is a humanoid, it must succeed on a DC 12 Constitution saving throw or be cursed with wereboar lycanthropy.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Wererat(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (human, shapechanger)", 'ac': 12, 'max_hp': 33, 'speed': 30, 
            "strength": 10, "dexterity": 15, "constitution": 12, "intelligence": 11, "wisdom": 10, "charisma": 8}
        default_kwargs.update({"immunities": {'piercing', "slashing", ' bludgeoning'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Shapechanger. The wererat can use its action to polymorph into a rat-humanoid hybrid or into a giant rat, or back into its true form, which is humanoid. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Keen Smell. The wererat has advantage on Wisdom (Perception) checks that rely on smell.
        """
        # Actions
        """
        bite_rat_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=4, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite (Rat or Hybrid Form Only)')
            #  If the target is a humanoid, it must succeed on a DC 11 Constitution saving throw or be cursed with wererat lycanthropy.
        shortsword_humanoid_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Shortsword (Humanoid or Hybrid Form Only)')
        hand_crossbow_humanoid_or_hybrid_form_only_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=30, name='Hand Crossbow (Humanoid or Hybrid Form Only)_range')
        hand_crossbow_humanoid_or_hybrid_form_only_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=120, name='Hand Crossbow (Humanoid or Hybrid Form Only)_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Weretiger(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (human, shapechanger)", 'ac': 12, 'max_hp': 120, 'speed': 30, 
            "strength": 17, "dexterity": 15, "constitution": 16, "intelligence": 10, "wisdom": 13, "charisma": 11}
        default_kwargs.update({"immunities": {' bludgeoning', "and slashing damage from nonmagical weapons that aren't silvered", 'piercing'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 4})
        # Features
        """
        Shapechanger. The weretiger can use its action to polymorph into a tiger-humanoid hybrid or into a tiger, or back into its true form, which is humanoid. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Keen Hearing and Smell. The weretiger has advantage on Wisdom (Perception) checks that rely on hearing or smell.
        Pounce (Tiger or Hybrid Form Only). If the weretiger moves at least 15 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 14 Strength saving throw or be knocked prone. If the target is prone, the weretiger can make one bite attack against it as a bonus action.
        """
        # Actions
        """
        bite_tiger_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=3, damage_type='piercing'), attack_mod=5, melee_range=5, name='Bite (Tiger or Hybrid Form Only)')
            #  If the target is a humanoid, it must succeed on a DC 13 Constitution saving throw or be cursed with weretiger lycanthropy.
        claw_tiger_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Claw (Tiger or Hybrid Form Only)')
        scimitar_humanoid_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=5, melee_range=5, name='Scimitar (Humanoid or Hybrid Form Only)')
        longbow_humanoid_or_hybrid_form_only_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=150, name='Longbow (Humanoid or Hybrid Form Only)_range')
        longbow_humanoid_or_hybrid_form_only_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=600, name='Longbow (Humanoid or Hybrid Form Only)_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Werewolf(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "humanoid (human, shapechanger)", 'ac': 11, 'max_hp': 58, 'speed': 30, 
            "strength": 15, "dexterity": 13, "constitution": 14, "intelligence": 10, "wisdom": 11, "charisma": 10}
        default_kwargs.update({"immunities": {"and slashing damage from nonmagical weapons that aren't silvered", 'piercing', ' bludgeoning'}})
        default_kwargs.update({'vision': "normal", 'cr': 3})
        # Features
        """
        Shapechanger. The werewolf can use its action to polymorph into a wolf-humanoid hybrid or into a wolf, or back into its true form, which is humanoid. Its statistics, other than its AC, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.
        Keen Hearing and Smell. The werewolf has advantage on Wisdom (Perception) checks that rely on hearing or smell.
        """
        # Actions
        """
        bite_wolf_or_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Bite (Wolf or Hybrid Form Only)')
            #  If the target is a humanoid, it must succeed on a DC 12 Constitution saving throw or be cursed with werewolf lycanthropy.
        claws_hybrid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=4, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Claws (Hybrid Form Only)')
        spear_humanoid_form_only = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Spear (Humanoid Form Only)')
        spear_humanoid_form_only_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, melee_range=5, name='Spear (Humanoid Form Only)_versatile')
        spear_humanoid_form_only_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=20, name='Spear (Humanoid Form Only)_range')
        spear_humanoid_form_only_range_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='piercing'), attack_mod=4, range=60, name='Spear (Humanoid Form Only)_range_disadvantage')
        multiattack_alt = attack_class.MultiAttack(name="Multiattack (alternate)", attack_list=[bite, spear]
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, claws])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Wight(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 14, 'max_hp': 45, 'speed': 30, 
            "strength": 15, "dexterity": 14, "constitution": 16, "intelligence": 10, "wisdom": 13, "charisma": 15}
        default_kwargs.update({"immunities": {'bludgeoning', ' poisoned', 'piercing', "slashing that aren't silvered", ' necrotic'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 3})
        # Features
        """
        Sunlight Sensitivity. While in sunlight, the wight has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        life_drain = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=2, damage_type='necrotic'), attack_mod=4, melee_range=5, name='Life Drain')
            #  The target must succeed on a DC 13 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.A humanoid slain by this attack rises 24 hours later as a zombie under the wight’s control, unless the humanoid is restored to life or its body is destroyed. The wight can have no more than twelve zombies under its control at one time.
        longsword_versatile = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=10, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Longsword_versatile')
        longsword = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='slashing'), attack_mod=4, melee_range=5, name='Longsword')
        longbow_range = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=150, name='Longbow_range')
        longbow_disadvantage = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=8, modifier=2, damage_type='piercing'), attack_mod=4, range=600, name='Longbow_range_disadvantage')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class WilloWisp(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "tiny", "creature_type": "undead", 'ac': 19, 'max_hp': 22, 'speed': 0, 'fly_speed': 50, 
            "strength": 1, "dexterity": 28, "constitution": 10, "intelligence": 13, "wisdom": 14, "charisma": 11}
        default_kwargs.update({"resistances": {'necrotic', 'fire', 'bludgeoning', 'slashing', 'cold', ' acid', 'thunder', 'piercing'}})
        default_kwargs.update({"immunities": {'poisoned', 'restrained', 'poison', 'unconscious', 'paralyzed', ' exhaustion', 'grappled', ' lightning', 'prone'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 2})
        # Features
        """
        Consume Life. As a bonus action, the will-o’-wisp can target one creature it can see within 5 ft. of it that has 0 hit points and is still alive. The target must succeed on a DC 10 Constitution saving throw against this magic or die. If the target dies, the will-o’-wisp regains 10 (3d6) hit points.
        Ephemeral. The will-o’-wisp can’t wear or carry anything.
        Incorporeal Movement. The will-o’-wisp can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.
        Variable Illumination. The will-o’-wisp sheds bright light in a 5- to 20-foot radius and dim light for an additional number of ft. equal to the chosen radius. The will-o’-wisp can alter the radius as a bonus action.
        """
        # Actions
        """
        Shock. Melee Spell Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d8) lightning damage.
        Invisibility. The will-o’-wisp and its light magically become invisible until it attacks or uses its Consume Life, or until its concentration ends (as if concentrating on a spell).
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Wraith(Creature):  # neeeds work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 13, 'max_hp': 67, 'speed': 0, 'fly_speed': 60, 
            "strength": 6, "dexterity": 16, "constitution": 16, "intelligence": 12, "wisdom": 14, "charisma": 15}
        default_kwargs.update({"resistances": {'thunder', 'bludgeoning', "slashing that aren't silvered", ' acid', 'lightning', 'cold', 'fire', 'piercing'}})
        default_kwargs.update({"immunities": {'exhaustion', 'poison', 'paralyzed', 'prone', 'grappled', 'petrified', ' charmed', 'restrained', 'poisoned', ' necrotic'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Incorporeal Movement. The wraith can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.
        Sunlight Sensitivity. While in sunlight, the wraith has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.
        """
        # Actions
        """
        life_drain = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=4, dice_type=8, modifier=3, damage_type='necrotic'), attack_mod=6, melee_range=5, name='Life Drain')
            #  The target must succeed on a DC 14 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.
        Create Specter. The wraith targets a humanoid within 10 feet of it that has been dead for no longer than 1 minute and died violently. The target’s spirit rises as a specter in the space of its corpse or in the nearest unoccupied space. The specter is under the wraith’s control. The wraith can have no more than seven specters under its control at one time.
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Wyvern(Creature):  # complex
    def __init__(self, **kwargs):  # note: "multiattack: The wyvern makes two attacks: one with its bite and one with its stinger. While flying, it can use its claws in place of one other attack.
        default_kwargs = {"size": "large", "creature_type": "dragon", 'ac': 13, 'max_hp': 110, 'speed': 20, 'fly_speed': 80, 
            "strength": 19, "dexterity": 10, "constitution": 16, "intelligence": 5, "wisdom": 12, "charisma": 6}
        default_kwargs.update({'vision': "darkvision", 'cr': 6})
        # Features
        """
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=10, name='Bite')
        claws = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=2, dice_type=8, modifier=4, damage_type='slashing'), attack_mod=7, melee_range=5, name='Claws')
        stinger = attack_class.HitAndSaveAttack(damage_dice=dice.DamageDice(dice_num=2, dice_type=6, modifier=4, damage_type='piercing'), attack_mod=7, melee_range=10, name='Stinger', dc=15, save_type='constitution', save_damage_dice='7d6', save_damage_type='poison', damage_on_success=True)
        multiattack = attack_class.MultiAttack(name="Multiattack", attack_list=[bite, stinger])
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Xorn(Creature):  # needs special resistance
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "elemental", 'ac': 19, 'max_hp': 73, 'speed': 20, 
            "strength": 17, "dexterity": 10, "constitution": 22, "intelligence": 11, "wisdom": 10, "charisma": 11}
        default_kwargs.update({"resistances": {" piercing slashing that aren't adamantine"}})
        default_kwargs.update({'vision': "darkvision", 'cr': 5})
        # Features
        """
        Earth Glide. The xorn can burrow through nonmagical, unworked earth and stone. While doing so, the xorn doesn’t disturb the material it moves through.
        Stone Camouflage. The xorn has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.
        Treasure Sense. The xorn can pinpoint, by scent, the location of precious metals and stones, such as coins and gems, within 60 ft. of it.
        """
        # Actions
        """
        bite = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=3, dice_type=6, modifier=3, damage_type='piercing'), attack_mod=6, melee_range=5, name='Bite')
        claw = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=3, damage_type='slashing'), attack_mod=6, melee_range=5, name='Claw')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

class Zombie(Creature):  # needs work
    def __init__(self, **kwargs):
        default_kwargs = {"size": "medium", "creature_type": "undead", 'ac': 8, 'max_hp': 22, 'speed': 20, 
            "strength": 13, "dexterity": 6, "constitution": 16, "intelligence": 3, "wisdom": 6, "charisma": 5}
        default_kwargs.update({"proficiencies": {'wisdom'}})
        default_kwargs.update({"immunities": {' poisoned'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 1})
        # Features
        """
        Undead Fortitude. If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5+the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.
        """
        # Actions
        """
        slam = attack_class.Attack(damage_dice=dice.DamageDice(dice_num=1, dice_type=6, modifier=1, damage_type='bludgeoning'), attack_mod=3, melee_range=5, name='Slam')
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)

