"""
:Test module: :py:mod:`team`
:Test class: :py:class:`Team`
:Untested parts: invalid input to constructor, warning messages
:Test dependencies:
    - :py:func:`test_tactics`
    - :py:func:`test_combatant`
:return: None
"""
from copy import copy, deepcopy

from DnD_5e import team
from DnD_5e.combatant import Combatant
from DnD_5e.tactics import combatant_tactics
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.Team', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_combatants():
    combatant_c10 = Combatant(ac=10, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="combatant_c10", logger=TEST_LOGGER)
    combatant_d10 = copy(combatant_c10)
    combatant_d10.set_name("combatant_d10")
    combatant_c11 = Combatant(ac=11, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="combatant_c10", logger=TEST_LOGGER)
    combatant_c12 = Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="combatant_c10", logger=TEST_LOGGER)
    return [combatant_c10, combatant_d10, combatant_c11, combatant_c12]


def get_combatants_without_c11():
    combatant_10 = Combatant(ac=10, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="combatant_10", logger=TEST_LOGGER)
    combatant_d10 = deepcopy(combatant_10)
    combatant_d10.set_name("combatant_d10")
    combatant_c12 = Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="combatant_10", logger=TEST_LOGGER)
    return [combatant_10, combatant_d10, combatant_c12]


def test_constructor_should_work():
    combatant_list = get_combatants()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = combatant_list
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()

    team_0 = team.Team(name="team_0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)
    assert team_0.get_name() == "team_0"
    assert team_0.get_combatants() == combatant_list
    assert team_0.get_combatants() is not combatant_list
    assert team_0.get_enemy_tactic() == lowest_ac_tactic
    assert team_0.has_member(combatant_c10) and team_0.has_member(combatant_d10) and team_0.has_member(combatant_c11) and team_0.has_member(combatant_c12)
    assert combatant_c10.get_team() == team_0 and combatant_d10.get_team() == team_0 and combatant_c11.get_team() == team_0 and combatant_c12.get_team() == team_0
    assert combatant_c11.is_on_my_team(combatant_c10)
    assert team_0.has_all_alive() and team_0.has_some_alive()
    stats = team_0.get_stats()
    assert stats['num_conscious'] == 4
    assert stats['num_unconscious'] == 0
    assert stats['num_dead'] == 0
    assert isinstance(combatant_c10.get_enemy_tactic(), combatant_tactics.IsConsciousTactic)
    assert combatant_c10.get_enemy_tactic().get_tiebreakers() == [lowest_ac_tactic]


def test_deep_copy_should_eq():
    combatant_list = get_combatants()
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    team_0 = team.Team(name="team_0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)

    team_0_deep_copy = deepcopy(team_0)
    assert team_0_deep_copy.get_combatants() is not team_0.get_combatants()
    for i in range(len(team_0.get_combatants())):
        original_item = team_0.get_combatants()[i]
        copied_item = team_0_deep_copy.get_combatants()[i]
        assert original_item == copied_item
        assert copied_item is not original_item
    assert isinstance(team_0_deep_copy.get_enemy_tactic(), combatant_tactics.LowestAcTactic)
    assert team_0_deep_copy.get_enemy_tactic() is not team_0.get_enemy_tactic()
    assert team_0 == team_0_deep_copy


def test_remove_combatant_should_remove_team():
    combatant_list = get_combatants()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = combatant_list
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    team_0 = team.Team(name="team_0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)

    team_0.remove_combatant(combatant_c11)
    assert not team_0.has_member(combatant_c11)
    assert combatant_c11.get_team() is None
    assert not combatant_c11.is_on_my_team(combatant_c10)
    assert team_0.get_combatants() == [combatant_c10, combatant_d10, combatant_c12]


def test_unconscious():
    combatant_list = get_combatants_without_c11()
    [combatant_c10, combatant_d10, combatant_c12] = combatant_list
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    team_0 = team.Team(name="team_0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)

    assert not team_0.has_any_unconscious()
    assert not team_0.has_some_not_all_unconscious()
    assert not team_0.has_all_unconscious()
    assert team_0.has_any_conscious()
    assert team_0.has_all_conscious()
    combatant_c10.become_unconscious()
    assert team_0.has_any_unconscious()
    assert team_0.has_some_not_all_unconscious()
    assert team_0.has_any_conscious()
    stats = team_0.get_stats()
    assert stats['num_conscious'] == 2, stats["num_conscious"]
    assert stats['num_unconscious'] == 1
    assert stats['num_dead'] == 0
    combatant_d10.become_unconscious()
    combatant_c12.become_unconscious()
    assert team_0.has_any_unconscious()
    assert team_0.has_all_unconscious()
    assert not team_0.has_any_conscious()
    assert not team_0.has_all_conscious()


def test_dead():
    combatant_list = get_combatants_without_c11()
    [combatant_c10, combatant_d10, combatant_c12] = combatant_list
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    team_0 = team.Team(name="team_0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)

    assert not team_0.has_any_dead() and not team_0.has_some_not_all_dead() and not team_0.has_all_dead()
    combatant_c10.die()
    assert team_0.has_some_alive()
    assert team_0.has_any_dead() and team_0.has_some_not_all_dead()
    assert not team_0.has_all_dead()
    stats = team_0.get_stats()
    assert stats['num_conscious'] == 2
    assert stats['num_unconscious'] == 0
    assert stats['num_dead'] == 1
    combatant_d10.die()
    combatant_c12.die()
    assert team_0.has_any_dead() and team_0.has_all_dead()
    assert not team_0.has_some_not_all_dead()
    assert not team_0.has_some_alive() and not team_0.has_all_alive()

close_logging(None, CH)
