"""
:Test module: :py:mod:`combatant`
:Test class: :py:class:`Combatant`
:Untested parts:
    - invalid input
    - a few other things that can be found by running nosetests with coverage
:Test dependencies: None
:return: None
"""
from copy import copy

from DnD_5e import armory, armor
from DnD_5e.combatant import Combatant
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.combatant.Combatant', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_Combatant():
    return Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, speed=20, climb_speed=5, fly_speed=10, swim_speed=15,
                     vision='normal', strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                     proficiencies=["dexterity", "charisma"], expertise=["stealth"], proficiency_mod=2,
                     name='combatant_0', vulnerabilities=["bludgeoning"], resistances=["slashing", "slashing", "piercing"],
                     immunities={"fire"}, logger=TEST_LOGGER)


def test_combatant():
    combatant_0 = get_Combatant()
    assert combatant_0.get_name() == 'combatant_0'
    assert combatant_0.get_ac() == 12
    assert combatant_0.get_max_hp() == 20
    assert combatant_0.get_current_hp() == 20
    assert combatant_0.get_hp_to_max() == 0
    assert combatant_0.is_hp_max()
    assert combatant_0.get_temp_hp() == 2
    assert combatant_0.get_speed() == 20
    assert combatant_0.get_climb_speed() == 5
    assert combatant_0.get_fly_speed() == 10
    assert combatant_0.get_swim_speed() == 15
    assert combatant_0.get_vision() == 'normal'
    assert combatant_0.get_strength() == 2
    assert combatant_0.get_dexterity() == 3
    assert combatant_0.get_constitution() == -1
    assert combatant_0.get_intelligence() == 1
    assert combatant_0.get_wisdom() == 0
    assert combatant_0.get_charisma() == -1
    assert combatant_0.has_proficiency("dexterity")
    assert combatant_0.has_proficiency("charisma")
    assert combatant_0.get_proficiency_mod() == 2
    assert combatant_0.get_vulnerabilities() == {"bludgeoning"}
    assert combatant_0.is_vulnerable("bludgeoning")
    assert combatant_0.is_resistant("slashing")
    assert combatant_0.is_resistant("piercing")
    assert combatant_0.is_immune("fire")
    assert combatant_0.get_saving_throw("strength") == 2
    assert combatant_0.get_saving_throw("dexterity") == 5
    assert combatant_0.get_saving_throw("constitution") == -1
    assert combatant_0.get_saving_throw("intelligence") == 1
    assert combatant_0.get_saving_throw("wisdom") == 0
    assert combatant_0.get_saving_throw("charisma") == 1
    assert not combatant_0.get_features()
    assert not combatant_0.get_conditions()
    assert not combatant_0.get_weapons()
    assert combatant_0.get_size() == "medium"
    assert not combatant_0.get_items()
    assert combatant_0.has_expertise("stealth")


def test_copy_should_eq():
    combatant_0 = get_Combatant()
    combatant_10 = copy(combatant_0)
    combatant_10.set_name("combatant_10")
    assert combatant_10.get_name() == 'combatant_10'
    assert combatant_10.get_ac() == 12
    assert combatant_10.get_max_hp() == 20
    assert combatant_10.get_current_hp() == 20
    assert combatant_10.get_hp_to_max() == 0
    assert combatant_10.is_hp_max()
    assert combatant_10.get_temp_hp() == 2
    assert combatant_10.get_speed() == 20
    assert combatant_10.get_climb_speed() == 5
    assert combatant_10.get_fly_speed() == 10
    assert combatant_10.get_swim_speed() == 15
    assert combatant_10.get_vision() == 'normal'
    assert combatant_10.get_strength() == 2
    assert combatant_10.get_dexterity() == 3
    assert combatant_10.get_constitution() == -1
    assert combatant_10.get_intelligence() == 1
    assert combatant_10.get_wisdom() == 0
    assert combatant_10.get_charisma() == -1
    assert combatant_10.has_proficiency("dexterity")
    assert combatant_10.has_proficiency("charisma")
    assert combatant_10.get_proficiency_mod() == 2
    assert combatant_10.get_vulnerabilities() == {"bludgeoning"}
    assert combatant_10.is_vulnerable("bludgeoning")
    assert combatant_10.is_resistant("slashing")
    assert combatant_10.is_resistant("piercing")
    assert combatant_10.is_immune("fire")
    assert combatant_10.get_saving_throw("strength") == 2
    assert combatant_10.get_saving_throw("dexterity") == 5
    assert combatant_10.get_saving_throw("constitution") == -1
    assert combatant_10.get_saving_throw("intelligence") == 1
    assert combatant_10.get_saving_throw("wisdom") == 0
    assert combatant_10.get_saving_throw("charisma") == 1
    assert not combatant_10.get_features()
    assert not combatant_10.get_conditions()
    assert not combatant_10.get_weapons()
    assert combatant_10.get_size() == "medium"
    assert not combatant_10.get_items()
    assert combatant_10.has_expertise("stealth")
    assert combatant_10 is not combatant_0
    assert combatant_10.get_conditions() is not combatant_0.get_conditions()
    assert combatant_10.get_proficiencies() is not combatant_0.get_proficiencies()
    assert combatant_10.get_vulnerabilities() is not combatant_0.get_vulnerabilities()
    assert combatant_10.get_resistances() is not combatant_0.get_resistances()
    assert combatant_10.get_immunities() is not combatant_0.get_immunities()
    assert combatant_10.get_features() is not combatant_0.get_features()

    combatant_11 = copy(combatant_10)
    assert combatant_11.get_ac() == 12
    assert combatant_11.get_max_hp() == 20
    assert combatant_11.get_current_hp() == 20
    assert combatant_11.get_hp_to_max() == 0
    assert combatant_11.is_hp_max()
    assert combatant_11.get_temp_hp() == 2
    assert combatant_11.get_speed() == 20
    assert combatant_11.get_climb_speed() == 5
    assert combatant_11.get_fly_speed() == 10
    assert combatant_11.get_swim_speed() == 15
    assert combatant_11.get_vision() == 'normal'
    assert combatant_11.get_strength() == 2
    assert combatant_11.get_dexterity() == 3
    assert combatant_11.get_constitution() == -1
    assert combatant_11.get_intelligence() == 1
    assert combatant_11.get_wisdom() == 0
    assert combatant_11.get_charisma() == -1
    assert combatant_11.has_proficiency("dexterity")
    assert combatant_11.has_proficiency("charisma")
    assert combatant_11.get_proficiency_mod() == 2
    assert combatant_11.get_vulnerabilities() == {"bludgeoning"}
    assert combatant_11.is_vulnerable("bludgeoning")
    assert combatant_11.is_resistant("slashing")
    assert combatant_11.is_resistant("piercing")
    assert combatant_11.is_immune("fire")
    assert combatant_11.get_saving_throw("strength") == 2
    assert combatant_11.get_saving_throw("dexterity") == 5
    assert combatant_11.get_saving_throw("constitution") == -1
    assert combatant_11.get_saving_throw("intelligence") == 1
    assert combatant_11.get_saving_throw("wisdom") == 0
    assert combatant_11.get_saving_throw("charisma") == 1
    assert not combatant_11.get_features()
    assert not combatant_11.get_conditions()
    assert not combatant_11.get_weapons()
    assert combatant_11.get_size() == "medium"
    assert not combatant_11.get_items()
    assert combatant_11 is not combatant_0
    assert combatant_11.get_conditions() is not combatant_0.get_conditions()
    assert combatant_11.get_proficiencies() is not combatant_0.get_proficiencies()
    assert combatant_11.get_vulnerabilities() is not combatant_0.get_vulnerabilities()
    assert combatant_11.get_resistances() is not combatant_0.get_resistances()
    assert combatant_11.get_immunities() is not combatant_0.get_immunities()
    assert combatant_11.get_features() is not combatant_0.get_features()

    assert combatant_0 == combatant_10  # test equality operator
    assert combatant_10 == combatant_11
    assert combatant_0.current_eq(combatant_10)  # test current equality operator
    assert combatant_10.current_eq(combatant_11)


def test_unarmored_ac_should_be_10_plus_dex():
    combatant_1 = Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="combatant_1")
    assert combatant_1.get_unarmored_ac() == 13
    assert combatant_1.get_ac() == 13


def test_invalid_Combatant_should_raise_exception():
    try:
        Combatant(name="bob", logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded without ability scores")   # pragma: no cover
    except ValueError:
        pass

    try:
        Combatant(ac="10", max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded with non-integer AC")   # pragma: no cover
    except ValueError:
        pass

    try:
        Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, conditions="dog",
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded with non-list conditions")   # pragma: no cover
    except ValueError:
        pass

    try:
        Combatant(ac=18, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded without max hp")   # pragma: no cover
    except ValueError:
        pass

    try:
        Combatant(ac=18, max_hp=30, current_hp=35, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                  strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                  name="t1", logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded with current hp greater than max hp")   # pragma: no cover
    except ValueError:
        pass
    try:
        Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded without name")   # pragma: no cover
    except ValueError:
        pass
    try:
        Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                  strength=-1, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded with low ability score")   # pragma: no cover
    except ValueError:
        pass
    try:
        Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                  strength=7, dexterity=50, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                  logger=TEST_LOGGER)
        raise AssertionError("Create combatant succeeded with high ability score")   # pragma: no cover
    except ValueError:
        pass


def test_Combatant_vision():
    combatant_2 = get_Combatant()
    assert combatant_2.can_see("normal")
    assert not combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.add_condition("blinded")
    assert not combatant_2.can_see("normal")
    assert not combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.remove_condition("blinded")
    assert not combatant_2.has_condition("blinded")

    combatant_2.set_vision("blindsight")
    assert combatant_2.can_see("normal")
    assert combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.add_condition("blinded")
    assert combatant_2.can_see("normal")
    assert combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.remove_condition("blinded")

    combatant_2.set_vision("darkvision")
    assert combatant_2.can_see("normal")
    assert combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.add_condition("blinded")
    assert not combatant_2.can_see("normal")
    assert not combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.remove_condition("blinded")

    combatant_2.set_vision("truesight")
    assert combatant_2.can_see("normal")
    assert combatant_2.can_see("dark")
    assert combatant_2.can_see("magic")
    combatant_2.add_condition("blinded")
    assert not combatant_2.can_see("normal")
    assert not combatant_2.can_see("dark")
    assert not combatant_2.can_see("magic")
    combatant_2.remove_condition("blinded")


def test_Combatant_size():
    combatant_2 = get_Combatant()
    combatant_3 = copy(combatant_2)

    combatant_2.set_size("tiny")
    assert combatant_2.get_size() == "tiny"
    assert combatant_3 == combatant_2
    assert not combatant_3.current_eq(combatant_2)


def test_add_remove_condition():
    combatant_2 = get_Combatant()
    combatant_2.add_condition("bored")
    assert combatant_2.has_condition("bored")
    combatant_2.remove_condition("bored")
    assert not combatant_2.has_condition("bored")


def test_take_damage_should_decrease_hp():
    combatant_2 = get_Combatant()
    # take less than temp hp
    assert combatant_2.take_damage(1) == 1
    assert combatant_2.get_temp_hp() == 1
    assert combatant_2.get_current_hp() == 20
    assert combatant_2.get_damage_taken() == 1
    # take more than temp hp
    assert combatant_2.take_damage(2) == 2
    assert combatant_2.get_temp_hp() == 0
    assert combatant_2.get_current_hp() == 19, combatant_2.get_current_hp()
    assert combatant_2.get_hp_to_max() == 1
    # take damage to current hp
    assert combatant_2.take_damage(2) == 2
    assert combatant_2.get_temp_hp() == 0
    assert combatant_2.get_current_hp() == 17
    assert combatant_2.get_hp_to_max() == 3


def test_take_healing_should_increase_hp():
    combatant_2 = get_Combatant()
    combatant_2.take_damage(12)  # 2 from temp_hp and 10 from current_hp
    assert combatant_2.get_current_hp() == 10
    assert combatant_2.take_healing(2) == 2
    assert combatant_2.get_current_hp() == 12
    # heal above max
    assert combatant_2.take_healing(20) == 8
    assert combatant_2.get_current_hp() == combatant_2.get_max_hp() == 20


def test_modify_adv_to_be_hit():
    combatant_1 = Combatant(ac=18, max_hp=30, current_hp=10, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                   strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_1", logger=TEST_LOGGER, conditions=['paralyzed'])
    combatant_1.modify_adv_to_be_hit(1)


def test_reset():
    combatant_1 = Combatant(ac=18, max_hp=30, current_hp=10, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                            strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                            name="combatant_1", logger=TEST_LOGGER, conditions=['paralyzed'])
    combatant_1.reset()
    assert combatant_1.get_current_hp() == 30
    assert combatant_1.get_conditions() == []
    assert combatant_1.get_adv_to_be_hit() == 0


def test_hands():
    """
    :Test module: :py:mod:`combatant` and :py:mod:`armor`
    :Test class: :py:class:`Combatant` and :py:class:`Shield`
    :Test dependencies: :py:func:`test_combatant`, :py:func:`test_armory`
    :return: None
    """
    sword = armory.Shortsword()
    dagger = armory.Dagger()
    combatant_0 = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                   intelligence=12, wisdom=11, charisma=8, main_hand=sword, off_hand=dagger, ac=12, name='combatant_0',
                   logger=TEST_LOGGER)
    assert combatant_0.get_main_hand() == sword
    assert combatant_0.get_off_hand() == dagger

    mace = armory.Mace()
    combatant_1 = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                   intelligence=12, wisdom=11, charisma=8, main_hand=mace, off_hand=armor.Shield, ac=12, name='combatant_1',
                   logger=TEST_LOGGER)
    assert combatant_1.get_main_hand() == mace
    assert combatant_1.get_off_hand() is armor.Shield
    assert combatant_1.get_ac() == 12 + 2

    combatant_2 = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                   intelligence=12, wisdom=11, charisma=8, main_hand=mace, ac=12, name='combatant_2', logger=TEST_LOGGER)
    assert combatant_2.get_main_hand() == mace
    assert combatant_2.get_off_hand() is None

    combatant_3 = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                   intelligence=12, wisdom=11, charisma=8, off_hand=armor.Shield, ac=12, name='combatant_3', logger=TEST_LOGGER)
    assert combatant_3.get_main_hand() is None
    assert combatant_3.get_off_hand() is armor.Shield


def test_invalid_two_handed_off_hand_should_raise_exception():
    try:
        Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                  intelligence=12, wisdom=11, charisma=8, off_hand=armory.Pike(), ac=12, name='c4',
                  logger=TEST_LOGGER)
        raise AssertionError("allowed a two-handed off hand")
    except ValueError:
        pass


def test_invalid_main_hand_should_raise_exception():
    try:
        Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                  intelligence=12, wisdom=11, charisma=8, main_hand="hi", off_hand=armor.Shield, ac=12, name='c4',
                  logger=TEST_LOGGER)
        raise AssertionError("allowed an invalid main hand")
    except ValueError:
        pass


def test_invalid_off_hand_should_raise_exception():
    try:
        Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                  intelligence=12, wisdom=11, charisma=8, off_hand=2, ac=12, name='c4', logger=TEST_LOGGER)
        raise AssertionError("allowed an invalid off hand")
    except ValueError:
        pass


def test_invalid_non_light_dual_wielding_should_raise_exception():
    try:
        Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                  intelligence=12, wisdom=11, charisma=8, main_hand=armory.Mace(), off_hand=armory.Dagger(), ac=12, name='c4',
                  logger=TEST_LOGGER)
        raise AssertionError("allowed non-light dual wielding")
    except ValueError:
        pass


def test_invalid_non_melee_dual_wielding_should_raise_exception():
    try:
        Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                  intelligence=12, wisdom=11, charisma=8, main_hand=armory.Shortsword(), off_hand=armory.HandCrossbow(),
                  ac=12, name='c4', logger=TEST_LOGGER)
        raise AssertionError("allowed non-melee dual wielding")
    except ValueError:
        pass


close_logging(None, CH)
