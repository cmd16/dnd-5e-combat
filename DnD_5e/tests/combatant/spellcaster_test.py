"""
:Test module: :py:mod:`combatant`
:Test class: :py:class:`SpellCaster`
:Untested parts: invalid input to constructor
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_spell`
:return: None
"""
from copy import deepcopy

from DnD_5e.attack_class.spell_attacks import Spell
from DnD_5e.combatant.spellcaster import SpellCaster

from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.combatant.Character', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_spellcaster():
    spell_0 = Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, name="spell_0")

    spellcaster_0 = SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                     strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8, name="spellcaster_0",
                     spell_ability="wisdom", spell_slots={1:3, 2:2, 3:1}, proficiency_mod=2, spells=[spell_0],
                     logger=TEST_LOGGER)
    assert spellcaster_0.get_spell_ability() == "wisdom"
    assert spellcaster_0.get_spell_ability_mod() == 3
    assert spellcaster_0.get_spell_save_dc() == 13
    assert spellcaster_0.get_spell_attack_mod() == 5
    assert spellcaster_0.get_level_spell_slots(1) == 3
    assert spellcaster_0.get_level_spell_slots(2) == 2
    assert spellcaster_0.get_level_spell_slots(3) == 1
    assert spellcaster_0.get_level_spell_slots(4) == 0
    spellcaster_0.spend_spell_slot(3)
    assert not spellcaster_0.get_level_spell_slots(3)
    spellcaster_0.reset_spell_slots()
    assert spellcaster_0.get_spell_slots() == {1:3, 2:2, 3:1}
    assert spell_0 in spellcaster_0.get_spells()
    assert spell_0.get_attack_mod() == 5

    spellcaster_1 = deepcopy(spellcaster_0)
    assert spellcaster_1.get_spell_ability() == "wisdom"
    assert spellcaster_1.get_spell_ability_mod() == 3
    assert spellcaster_1.get_spell_save_dc() == 13
    assert spellcaster_1.get_spell_attack_mod() == 5
    assert spellcaster_1.get_level_spell_slots(1) == 3
    assert spellcaster_1.get_level_spell_slots(2) == 2
    assert spellcaster_1.get_level_spell_slots(3) == 1
    assert spellcaster_1.get_level_spell_slots(4) == 0
    assert spellcaster_1 is not spellcaster_0
    assert spellcaster_1 == spellcaster_0
    assert spellcaster_1.get_spells() is not spellcaster_0.get_spells()

    spellcaster_1.spend_spell_slot(3)
    assert not spellcaster_1.get_level_spell_slots(3)
    assert not spellcaster_1.current_eq(spellcaster_0)

    spellcaster_1.reset_spell_slots()
    assert spellcaster_1.get_spell_slots() == {1: 3, 2: 2, 3: 1}
    assert spellcaster_1.get_spells()[0].get_attack_mod() == 5

close_logging(None, CH)
