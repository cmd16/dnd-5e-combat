"""
:Test module: :py:mod:`combatant`
:Test class: :py:class:`Creature`
:Untested parts: None
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from copy import copy

from DnD_5e.combatant.creature import Creature

from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.combatant.Creature', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_Creature():
    creature_0 = Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="creature_0",
                  cr=1/2, xp=100, creature_type="elemental", logger=TEST_LOGGER)
    assert creature_0.get_cr() == 1/2
    assert creature_0.get_xp() == 100
    assert creature_0.get_creature_type() == "elemental"

    creature_0 = Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="creature_0",
                  cr=1/4, creature_type="monstrosity", logger=TEST_LOGGER)
    assert creature_0.get_cr() == 1/4
    assert creature_0.get_xp() == 50

    creature_1 = copy(creature_0)
    assert creature_1.get_max_hp() == 70  # just check a stat lol
    assert creature_1.get_cr() == 1/4
    assert creature_1.get_xp() == 50
    assert creature_1.get_creature_type() == "monstrosity"
    assert creature_0 is not creature_1
    assert creature_0 == creature_1

    creature_1.become_unconscious()  # Creatures don't become unconscious, they just die.
    assert creature_1.has_condition("dead")


def test_invalid_Creature_should_raise_exception():
    try:
        Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                 cr="dog", logger=TEST_LOGGER)
        raise AssertionError("Create Creature succeeded for non-number cr")   # pragma: no cover
    except ValueError:
        pass

    try:
        Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                 cr=-2, logger=TEST_LOGGER)
        raise AssertionError("Create Creature succeeded for negative cr")   # pragma: no cover
    except ValueError:
        pass

    try:
        Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                 cr=1/4, creature_type="tacocat", logger=TEST_LOGGER)
        raise AssertionError("Create Creature succeeded for invalid creature type (string)")
    except ValueError:
        pass

    try:
        Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                 cr=1/4, creature_type=2, logger=TEST_LOGGER)
        raise AssertionError("Create Creature succeeded for non-string creature type")
    except ValueError:
        pass

close_logging(None, CH)
