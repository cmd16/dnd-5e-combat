"""
:Test module: :py:mod:`combatant`
:Test class: :py:class:`Character`
:Untested parts: None
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from copy import deepcopy

from DnD_5e.combatant.character import Character
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.combatant.Character', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_Character():
    character_0 = Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="character_0",
                   level=5, logger=TEST_LOGGER)
    assert character_0.get_level() == 5
    assert character_0.get_death_saves() == {0: 0, 1: 0}

    character_1 = deepcopy(character_0)
    assert character_1.get_temp_hp() == 2
    assert character_1.get_level() == 5
    assert character_1.get_death_saves() == {0: 0, 1: 0}
    assert character_1.get_death_saves() is not character_0.get_death_saves()
    assert character_1 is not character_0
    assert character_1 == character_0
    assert character_1.get_features() is not character_0.get_features()
    assert character_1.get_conditions() is not character_0.get_conditions()


def test_invalid_Character_should_raise_exception():
    try:
        Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                  name="c0", level=30, logger=TEST_LOGGER)
        raise AssertionError("Create Character succeeded with level too high")   # pragma: no cover
    except ValueError:
        pass

    try:
        Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                  name="c0", level=-2, logger=TEST_LOGGER)
        raise AssertionError("Create Character succeeded with level too low")   # pragma: no cover
    except ValueError:
        pass

    try:
        Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                  strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                  name="c0", level="stuff", logger=TEST_LOGGER)
        raise AssertionError("Create Character succeeded with non-int level")   # pragma: no cover
    except ValueError:
        pass


def get_Character():
    return Character(ac=12, max_hp=20, current_hp=20, hit_dice='2d10', level=1, speed=20, climb_speed=5, fly_speed=10, swim_speed=15,
                     vision='normal', strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                     name='character_1', logger=TEST_LOGGER)


def test_go_unconscious():
    character_1 = get_Character()
    # go unconscious but don't die
    assert character_1.take_damage(20) == 20
    assert character_1.get_temp_hp() == 0
    assert character_1.get_current_hp() == 0
    assert character_1.has_condition("unconscious")
    assert character_1.has_condition("unstable")
    assert character_1.get_death_saves() == {0: 0, 1: 0}  # 0 failures, 0 successes
    assert character_1.get_times_unconscious() == 1
    character_1.fail_death_save()
    character_1.fail_death_save()
    character_1.succeed_death_save()
    assert character_1.get_death_saves() == {0: 2, 1: 1}  # 2 failures, 1 success
    # heal
    assert character_1.take_healing(10) == 10
    assert character_1.get_current_hp() == 10
    assert not character_1.has_condition("unconscious")
    assert not character_1.has_condition("unstable")
    assert character_1.get_death_saves() == {0: 0, 1: 0}  # ensure death saves have been reset
    # heal above max
    assert character_1.take_healing(20) == 10
    assert character_1.get_current_hp() == character_1.get_max_hp() == 20


def test_become_unconscious_should_make_death_saves():
    character_1 = get_Character()
    character_1.take_damage(20)
    assert character_1.has_condition("unstable")
    for _ in range(3):
        character_1.succeed_death_save()
    assert character_1.has_condition("unconscious")
    assert not character_1.has_condition("unstable")
    assert character_1.get_times_unconscious() == 1
    character_1.take_healing(10)  # become conscious again :)
    character_1.take_damage(10)  # and back down :(
    assert character_1.has_condition("unconscious")
    assert character_1.has_condition("unstable")
    assert not character_1.has_condition("stable")
    assert character_1.get_death_saves() == {0: 0, 1: 0}, character_1.get_death_saves()
    character_1.take_damage(1, is_critical=True)  # critical hit is two failed saves
    assert character_1.get_death_saves() == {0: 2, 1: 0}, character_1.get_death_saves()
    character_1.take_damage(5)  # 1 failed death save from taking damage
    assert character_1.has_condition("dead")
    character_1.reset()
    assert character_1.get_damage_taken() == 0
    assert character_1.get_times_unconscious() == 0
    assert character_1.get_death_saves() == {0: 0, 1: 0}


def test_taking_double_max_hp_should_die():
    combatant_2 = get_Character()
    assert combatant_2.take_damage(40) == 40  # insta-death
    assert combatant_2.get_conditions() == ["dead"]
    assert combatant_2.has_condition("dead")
    assert combatant_2.get_current_hp() == 0
    assert combatant_2.get_temp_hp() == 0

    combatant_2.remove_all_conditions()
    assert combatant_2.get_conditions() == []
    combatant_2.heal_to_max()
    assert combatant_2.get_current_hp() == 20

close_logging(None, CH)
