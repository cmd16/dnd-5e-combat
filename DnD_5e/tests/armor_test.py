"""
:Test module: :py:mod:`armor`
:Test class: :py:class:`Armor` and subclasses
:Untested parts: not all armor is tested, just a sample
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from copy import copy

from DnD_5e import combatant, armor
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.armor', "")
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_Combatant_LightArmor():
    return combatant.Combatant(armor=armor.PaddedArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                               strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                               proficiencies=["light armor"], proficiency_mod=2, name='c0', logger=TEST_LOGGER)


def get_Combatant_MediumArmor_and_PlateArmor():
    return combatant.Combatant(armor=armor.ChainShirtArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                               strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                               proficiencies=["medium armor", "PlateArmor"], proficiency_mod=2, name='c1',
                               logger=TEST_LOGGER)


def test_MediumArmor_and_PlateArmor_should_haveProficiency():
    combatant_1 = get_Combatant_MediumArmor_and_PlateArmor()
    assert combatant_1.get_armor() is armor.ChainShirtArmor
    assert combatant_1.get_ac() == 13 + 2
    assert armor.ChainShirtArmor.get_stealth() == 0
    assert combatant_1.has_armor_proficiency(armor.ChainShirtArmor)
    assert combatant_1.has_armor_proficiency(armor.MediumArmor)
    assert combatant_1.has_armor_proficiency(armor.PlateArmor)
    assert not combatant_1.has_armor_proficiency(armor.SplintArmor)


def test_PaddedArmor_should_haveStealthDisadvantage_haveProficiency():
    combatant_0 = get_Combatant_LightArmor()
    assert combatant_0.get_armor() is armor.PaddedArmor, print(combatant_0.get_armor(), armor.PaddedArmor)
    assert combatant_0.get_ac() == 11 + 3
    assert armor.PaddedArmor.get_stealth() == -1  # TODO: implement penalty on stealth checks
    assert combatant_0.has_armor_proficiency(armor.PaddedArmor)
    assert combatant_0.has_armor_proficiency(armor.LightArmor)


def test_SplintArmor_should_haveStrRestriction():
    combatant_3 = combatant.Combatant(armor=armor.SplintArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                             strength=15, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["heavy armor"], proficiency_mod=2, name='combatant_3', logger=TEST_LOGGER)
    assert combatant_3.get_armor() is armor.SplintArmor
    assert combatant_3.get_ac() == 17
    assert armor.SplintArmor.get_strength() == 2
    assert armor.SplintArmor.get_stealth() == -1
    assert combatant_3.has_armor_proficiency(armor.HeavyArmor)
    assert combatant_3.has_armor_proficiency(armor.SplintArmor)
    assert not combatant_3.has_armor_proficiency(armor.ChainShirtArmor)


def test_copy_armor():
    combatant_0 = get_Combatant_LightArmor()
    combatant_1 = get_Combatant_MediumArmor_and_PlateArmor()

    combatant_0_copy = copy(combatant_0)
    assert combatant_0_copy.get_armor() == combatant_0.get_armor()

    combatant_1.set_armor(armor.BreastplateArmor)
    assert combatant_1.get_armor() is armor.BreastplateArmor
    assert combatant_1.get_ac() == 14 + 2
    combatant_1_copy = copy(combatant_1)
    assert combatant_1_copy.current_eq(combatant_1)
    combatant_1.set_armor(armor.ScaleMailArmor)  # same AC, different armor
    assert combatant_1.get_ac() == combatant_1_copy.get_ac(), combatant_1_copy.get_ac()
    assert not combatant_1.current_eq(combatant_1_copy)
    combatant_1_copy.set_armor(None)
    assert combatant_1_copy.get_ac() == combatant_1_copy.get_unarmored_ac()


def test_setInvalidArmor_should_raiseException():
    combatant_1 = get_Combatant_MediumArmor_and_PlateArmor()

    try:
        combatant_1.set_armor(2)
        raise AssertionError("Allowed invalid armor for set_armor")
    except ValueError:
        pass

close_logging(None, CH)
