"""
:Test module: :py:mod:`tactics`
:Test class: :py:class:`Tactic`
:Untested parts:
    - invalid input to constructor
    - include tiebreakers (but this is tested in :py:func:`test_combatant_tactics`)
:Test dependencies: None
:return: None
"""
from collections import Counter
from copy import copy

from DnD_5e import tactics
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.tactics.Tactics', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_item_list():
    return [1, 3, 2, 4, 5]


def test_constructor_should_work():
    rand_tactic = tactics.Tactic(name="random")
    assert rand_tactic.get_name() == "random"
    assert rand_tactic.get_tiebreakers() == []


def test_copy_should_eq():
    rand_tactic = tactics.Tactic(name="random")
    rand_tactic2 = copy(rand_tactic)
    assert rand_tactic2.get_name() == "random2"
    assert rand_tactic2.get_tiebreakers() == []
    assert rand_tactic is not rand_tactic2
    assert rand_tactic2 == rand_tactic


def test_Tactic_should_run():
    rand_tactic = tactics.Tactic(name="random")
    item_list = get_item_list()
    result = rand_tactic.run_tactic(item_list)
    assert item_list == get_item_list()  # check that the list was not modified
    assert len(result) == 1
    assert result[0] in item_list


def test_make_choice_should_choose_one_random():
    rand_tactic = tactics.Tactic(name="random")
    item_list = get_item_list()
    result_freq = Counter()
    for _ in range(100):  # should be enough times to ensure each element visited at least once
        result = rand_tactic.make_choice(item_list)
        result_freq[result] += 1
    for num in range(1, 6):
        assert num in result_freq
        freq = result_freq[num]
        assert freq > 10, result_freq
        assert freq < 40, result_freq
    print(result_freq)


close_logging(None, CH)
