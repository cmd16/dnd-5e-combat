"""
:Test module: :py:mod:`combatant_tactics`
:Test class:
    :py:class:`LowestAcTactic`, :py:class:`HighestAcTactic`, :py:class:`LowAcTactic`, :py:class:`HighAcTactic`,
    :py:class:`BloodiedTactic`, :py:class:`MaxHpTactic`, :py:class:`HpToMaxHighTactic`,
    :py:class:`MurderHoboTactic`, :py:class:`HasTempHpTactic`, :py:class:`NoTempHsTactic`,
    :py:class:`HasVulnerabilityTactic`, :py:class:`HasNoVulnerabilityTactic`, :py:class:`HasResistanceTactic`,
    :py:class:`HasNoResistanceTactic`, :py:class:`HasImmunityTactic`, :py:class:`HasNoImmunityTactic`
:Untested parts: invalid input to constructor
:Test dependencies:
    - :py:func:`test_tactics`
    - :py:func:`test_combatant`
:return: None
"""
from copy import deepcopy

from DnD_5e.attack_class import Attack
from DnD_5e.attack_class.spell_attacks import HealingSpell
from DnD_5e.combatant import Combatant
from DnD_5e.combatant.character import Character
from DnD_5e.tactics import combatant_tactics
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.tactics.CombatantTactics', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_combatants_c12():
    combatant_c10 = Combatant(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                    strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                    name="combatant_c10",
                    logger=TEST_LOGGER)
    combatant_d10 = deepcopy(combatant_c10)
    combatant_d10.set_name("combatant_d10")
    combatant_c11 = Combatant(ac=11, max_hp=20, current_hp=8, hit_dice='3d6', speed=20, vision='darkvision',
                    strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                    name="combatant_c10",
                    logger=TEST_LOGGER)
    combatant_c12 = Combatant(ac=12, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                    strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                    name="combatant_c10",
                    logger=TEST_LOGGER)
    return [combatant_c10, combatant_d10, combatant_c11, combatant_c12]


def get_combatants_d12():
    result = get_combatants_c12()
    combatant_d12 = deepcopy(result[-1])
    combatant_d12.set_name("combatant_d12")
    result.append(combatant_d12)
    return result


def get_combatants_c13():
    result = get_combatants_d12()
    combatant_c13 = Combatant(ac=12, max_hp=35, current_hp=30, hit_dice='3d6', speed=20, strength=14, dexterity=16,
                    constitution=9, intelligence=12, wisdom=11, charisma=8, name="combatant_c13", logger=TEST_LOGGER)
    result.append(combatant_c13)
    return result


def get_characters_c13():
    character_c10 = Character(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="character_c10", level=1,
                              logger=TEST_LOGGER)
    character_d10 = deepcopy(character_c10)
    character_d10.set_name("character_d10")
    character_c11 = Character(ac=11, max_hp=20, current_hp=8, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="character_c10", level=1,
                              logger=TEST_LOGGER)
    character_c12 = Character(ac=12, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="character_c10", level=1,
                              logger=TEST_LOGGER)
    character_d12 = deepcopy(character_c12)
    character_d12.set_name("character_c12")
    character_c13 = Character(ac=12, max_hp=35, current_hp=30, hit_dice='3d6', speed=20, strength=14, dexterity=16,
                              constitution=9, intelligence=12, wisdom=11, charisma=8, name="character_c13", level=1,
                              logger=TEST_LOGGER)
    return [character_c10, character_d10, character_c11, character_c12, character_d12, character_c13]


def get_hp_combatants():
    combatant_20 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                     strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="combatant_c10",
                     logger=TEST_LOGGER)
    combatant_10 = Combatant(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                     strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="combatant_c10",
                     logger=TEST_LOGGER)
    combatant_05 = Combatant(ac=10, max_hp=20, current_hp=5, hit_dice='3d6', speed=20, vision='darkvision',
                    strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="combatant_c10",
                    logger=TEST_LOGGER)
    combatant_02 = Combatant(ac=10, max_hp=20, current_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                    strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="combatant_c10",
                    logger=TEST_LOGGER)
    return [combatant_02, combatant_05, combatant_10, combatant_20]

# pylint: disable=unbalanced-tuple-unpacking

def test_LowestAcTactic():
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = get_combatants_c12()
    test_list = [combatant_c10, combatant_c11, combatant_c12]
    assert lowest_ac_tactic.run_tactic(test_list) == [combatant_c10]
    combatant_c11.set_enemy_tactic(lowest_ac_tactic)
    assert combatant_c11.select_enemy(test_list) == combatant_c10
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12]
    assert lowest_ac_tactic.run_tactic(test_list) == [combatant_c10, combatant_d10]
    assert lowest_ac_tactic == combatant_tactics.LowestAcTactic()


def test_HighestAcTactic():
    highest_ac_tactic = combatant_tactics.HighestAcTactic()
    test_list = get_combatants_c12()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = test_list
    assert highest_ac_tactic.run_tactic(test_list) == [combatant_c12]
    combatant_d12 = deepcopy(combatant_c12)
    combatant_d12.set_name("combatant_d12")
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12]
    assert highest_ac_tactic.run_tactic(test_list) == [combatant_c12, combatant_d12]
    assert highest_ac_tactic == combatant_tactics.HighestAcTactic()


def test_LowAcTactic():
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12] = get_combatants_d12()
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12]
    low_ac_tactic = combatant_tactics.LowAcTactic(threshold=11)
    low_ac_result = low_ac_tactic.run_tactic(test_list)
    assert len(low_ac_result) == 2 and combatant_c10 in low_ac_result and combatant_d10 in low_ac_result
    assert deepcopy(low_ac_tactic) == low_ac_tactic
    low_ac_tactic2 = combatant_tactics.LowAcTactic(threshold=12)
    assert low_ac_tactic2 != low_ac_tactic
    assert low_ac_tactic2.run_tactic(test_list) == [combatant_c10, combatant_d10, combatant_c11]


def test_HighAcTactic():
    test_list = get_combatants_d12()
    [_, _, combatant_c11, combatant_c12, combatant_d12] = test_list
    high_ac_tactic = combatant_tactics.HighAcTactic(threshold=10)
    assert high_ac_tactic.run_tactic(test_list) == [combatant_c11, combatant_c12, combatant_d12]
    assert deepcopy(high_ac_tactic) == high_ac_tactic
    high_ac_tactic2 = combatant_tactics.HighAcTactic(threshold=11)
    assert high_ac_tactic2 != high_ac_tactic
    assert high_ac_tactic2.run_tactic(test_list) == [combatant_c12, combatant_d12]


def test_tactic_with_Attack_in_constructor():
    attack_0 = Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="attack_0")
    assert combatant_tactics.LowAcTactic(attack=attack_0, use_max=True).get_threshold() == 25 + 1
    assert combatant_tactics.LowAcTactic(attack=attack_0).get_threshold() == 12 + 1


def test_BloodiedTactic():
    bloodied_tactic = combatant_tactics.BloodiedTactic()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = get_combatants_c12()
    test_list = [combatant_c10, combatant_c12]
    assert bloodied_tactic.run_tactic(test_list) == [combatant_c10]
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12]
    assert bloodied_tactic.run_tactic(test_list) == [combatant_c10, combatant_d10, combatant_c11]
    assert bloodied_tactic == combatant_tactics.BloodiedTactic()


def test_LowestHpTactic():
    test_list = get_hp_combatants()
    [combatant_02, combatant_05, combatant_10, _] = test_list
    lowest_hp_tactic = combatant_tactics.LowestHpTactic()
    assert lowest_hp_tactic.run_tactic(test_list) == [combatant_02]
    combatant_02_copy = deepcopy(combatant_02)
    assert lowest_hp_tactic.run_tactic([combatant_02, combatant_02_copy, combatant_05]) == [combatant_02, combatant_02_copy]
    assert lowest_hp_tactic == combatant_tactics.LowestHpTactic()
    combatant_10.set_heal_tactic(lowest_hp_tactic)
    assert combatant_10.select_heal(test_list) == combatant_02


def test_HighestHpTactic():
    test_list = get_hp_combatants()
    [_, _, combatant_10, combatant_20] = test_list
    highest_hp_tactic = combatant_tactics.HighestHpTactic()
    assert highest_hp_tactic.run_tactic(test_list) == [combatant_20]
    combatant_20_copy = deepcopy(combatant_20)
    highest_hp_result = highest_hp_tactic.run_tactic([combatant_20, combatant_20_copy, combatant_10])
    assert len(highest_hp_result) == 2 and combatant_20 in highest_hp_result and combatant_20_copy in highest_hp_result
    assert highest_hp_tactic == combatant_tactics.HighestHpTactic()


def test_LowHpTactic():
    test_list = get_hp_combatants()
    [combatant_02, combatant_05, combatant_10, _] = test_list
    hp_low_tactic = combatant_tactics.LowHpTactic(threshold=15)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 3 and combatant_02 in hp_low_result and combatant_05 in hp_low_result and combatant_10 in hp_low_result
    attack_0 = Attack(damage_dice=(1, 10), damage_type="slashing", range=120, name="attack_0")
    hp_low_tactic = combatant_tactics.LowHpTactic(attack=attack_0)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 2 and combatant_02 in hp_low_result and combatant_05 in hp_low_result
    hp_low_tactic = combatant_tactics.LowHpTactic(attack=attack_0, use_max=True)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 3 and combatant_02 in hp_low_result and combatant_05 in hp_low_result and combatant_10 in hp_low_result


def test_HighHpTactic():
    test_list = get_hp_combatants()
    [_, _, combatant_10, combatant_20] = test_list
    hp_high_tactic = combatant_tactics.HighHpTactic(threshold=15)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 1 and combatant_20 in hp_high_result
    attack_0 = Attack(damage_dice=(1, 10), damage_type="slashing", range=120, name="attack_0")
    hp_high_tactic = combatant_tactics.HighHpTactic(attack=attack_0)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 2 and combatant_10 in hp_high_result and combatant_20 in hp_high_result
    hp_high_tactic = combatant_tactics.HighHpTactic(attack=attack_0, use_max=True)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 1 and combatant_20 in hp_high_result


def test_MaxHpTactic():
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12] = get_combatants_c12()
    test_list = [combatant_c10, combatant_c12]
    max_hp_tactic = combatant_tactics.MaxHpTactic()
    max_hp_result = max_hp_tactic.run_tactic(test_list)
    assert max_hp_result == [combatant_c12], max_hp_result
    combatant_d12 = get_combatants_d12()[-1]
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12]
    max_hp_result = max_hp_tactic.run_tactic(test_list)
    assert len(max_hp_result) == 2 and combatant_c12 in max_hp_result and combatant_d12 in max_hp_result
    assert max_hp_tactic == combatant_tactics.MaxHpTactic()


def test_HighHpToMaxTactic():
    test_list = get_combatants_d12()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, d12] = test_list
    hp_to_max_high_tactic = combatant_tactics.HighHpToMaxTactic(threshold=9)
    hp_to_max_high_result = hp_to_max_high_tactic.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 3 and combatant_c10 in hp_to_max_high_result and combatant_d10 in hp_to_max_high_result and combatant_c11 in hp_to_max_high_result
    healing_spell = HealingSpell(damage_dice=(1, 8), damage_mod=1, name="healing", level=1, casting_time="1 action",
                                              duration="1 minute", components=["v", "s"])
    hp_to_max_high_tactic2 = combatant_tactics.HighHpToMaxTactic(healing=healing_spell)
    assert hp_to_max_high_tactic2 == deepcopy(hp_to_max_high_tactic2)
    assert hp_to_max_high_tactic2 != hp_to_max_high_tactic
    c13 = get_combatants_c13()[-1]
    test_list = [combatant_c10, combatant_d10, combatant_c11, combatant_c12, d12, c13]
    hp_to_max_high_result = hp_to_max_high_tactic2.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 3 and combatant_c10 in hp_to_max_high_result and combatant_d10 in hp_to_max_high_result and combatant_c11 in hp_to_max_high_result
    hp_to_max_high_tactic2 = combatant_tactics.HighHpToMaxTactic(healing=healing_spell, use_max=False)
    hp_to_max_high_result = hp_to_max_high_tactic2.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 4 and combatant_c10 in hp_to_max_high_result and combatant_d10 in hp_to_max_high_result \
           and combatant_c11 in hp_to_max_high_result and c13 in hp_to_max_high_result


def test_IsUnconsciousTactic_and_IsConsciousTactic():
    test_list = get_characters_c13()
    [character_c10, character_d10, character_c11, character_c12, character_d12, character_c13] = test_list
    character_c10.take_damage(10)  # go unconscious
    assert character_c10.has_condition("unconscious")
    character_c11.take_damage(68)  # die instantly
    assert character_c11.has_condition("dead")
    character_c12.take_damage(20)
    character_c12.take_damage(40)  # die
    assert character_c12.has_condition("dead")
    character_c13.take_damage(31)  # go unconscious
    assert character_c13.has_condition("unconscious")
    # character_d10 and character_d12 are the same
    murder_hobo_tactic = combatant_tactics.IsUnconsciousTactic()
    result = murder_hobo_tactic.run_tactic(test_list)
    assert result == [character_c10, character_c13], [char.get_name() for char in result]
    assert murder_hobo_tactic == combatant_tactics.IsUnconsciousTactic()
    is_conscious_tactic = combatant_tactics.IsConsciousTactic()
    assert is_conscious_tactic.run_tactic(test_list) == [character_d10, character_d12]


def test_HasTempHpTactic():
    test_list = get_combatants_c13()
    [combatant_c10, _, _, combatant_c12, _, combatant_c13] = test_list
    has_temp_hp_tactic = combatant_tactics.HasTempHpTactic()
    combatant_c10.set_temp_hp(5)
    combatant_c12.set_temp_hp(1)
    combatant_c13.set_temp_hp(0)
    has_temp_hp_result = has_temp_hp_tactic.run_tactic(test_list)
    assert len(has_temp_hp_result) == 2, has_temp_hp_result
    assert combatant_c10 in has_temp_hp_result
    assert combatant_c12 in has_temp_hp_result
    assert has_temp_hp_tactic == combatant_tactics.HasTempHpTactic()


def test_NoTempHpTactic():
    test_list = get_combatants_c13()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12, combatant_c13] = test_list
    combatant_c10.set_temp_hp(5)
    combatant_c12.set_temp_hp(1)
    no_temp_hp_tactic = combatant_tactics.NoTempHpTactic()
    no_temp_hp_result = no_temp_hp_tactic.run_tactic(test_list)
    assert len(no_temp_hp_result) == 4
    assert combatant_d10 in no_temp_hp_result
    assert combatant_c11 in no_temp_hp_result
    assert combatant_d12 in no_temp_hp_result
    assert combatant_c13 in no_temp_hp_result
    assert no_temp_hp_tactic == combatant_tactics.NoTempHpTactic()


def test_HasVulnerabilityTactic_and_HasNoVulnerabilityTactic():
    test_list = get_combatants_c13()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12, combatant_c13] = test_list
    combatant_c10.add_vulnerability("piercing")
    has_vulnerability_tactic = combatant_tactics.HasVulnerabilityTactic(vulnerability_type="piercing")
    assert has_vulnerability_tactic.run_tactic(test_list) == [combatant_c10]
    assert has_vulnerability_tactic == deepcopy(has_vulnerability_tactic)
    assert has_vulnerability_tactic != combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning")
    has_no_vulnerability_tactic = combatant_tactics.HasNoVulnerabilityTactic(vulnerability_type="piercing")
    assert has_no_vulnerability_tactic.run_tactic(test_list) == [combatant_d10, combatant_c11, combatant_c12, combatant_d12, combatant_c13]
    assert has_no_vulnerability_tactic == deepcopy(has_no_vulnerability_tactic)
    assert has_no_vulnerability_tactic != combatant_tactics.HasNoVulnerabilityTactic(vulnerability_type="bludgeoning")


def test_HasResistanceTactic_and_HasNoResistanceTactic():
    test_list = get_combatants_c13()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12, combatant_c13] = test_list
    combatant_d10.add_resistance("piercing")
    has_resistance_tactic = combatant_tactics.HasResistanceTactic(resistance_type="piercing")
    assert has_resistance_tactic.run_tactic(test_list) == [combatant_d10]
    assert has_resistance_tactic == deepcopy(has_resistance_tactic)
    assert has_resistance_tactic != combatant_tactics.HasResistanceTactic(resistance_type="bludgeoning")
    has_no_resistance_tactic = combatant_tactics.HasNoResistanceTactic(resistance_type="piercing")
    assert has_no_resistance_tactic.run_tactic(test_list) == [combatant_c10, combatant_c11, combatant_c12, combatant_d12, combatant_c13]
    assert has_no_resistance_tactic == deepcopy(has_no_resistance_tactic)
    assert has_no_resistance_tactic != combatant_tactics.HasNoResistanceTactic(resistance_type="bludgeoning")


def test_HasImmunityTactic_and_HasNoImmunityTactic():
    test_list = get_combatants_c13()
    [combatant_c10, combatant_d10, combatant_c11, combatant_c12, combatant_d12, combatant_c13] = test_list
    combatant_c11.add_immunity("piercing")
    has_immunity_tactic = combatant_tactics.HasImmunityTactic(immunity_type="piercing")
    assert has_immunity_tactic.run_tactic(test_list) == [combatant_c11]
    assert has_immunity_tactic == deepcopy(has_immunity_tactic)
    assert has_immunity_tactic != combatant_tactics.HasImmunityTactic(immunity_type="bludgeoning")
    has_no_immunity_tactic = combatant_tactics.HasNoImmunityTactic(immunity_type="piercing")
    assert has_no_immunity_tactic.run_tactic(test_list) == [combatant_c10, combatant_d10, combatant_c12, combatant_d12, combatant_c13]
    assert has_no_immunity_tactic == deepcopy(has_no_immunity_tactic)
    assert has_no_immunity_tactic != combatant_tactics.HasNoImmunityTactic(immunity_type="bludgeoning")


def test_tiebreakers():
    tact1 = combatant_tactics.LowAcTactic(threshold=12,
                                          tiebreakers=[combatant_tactics.BloodiedTactic(), combatant_tactics.IsUnconsciousTactic()])
    tact2 = combatant_tactics.NoTempHpTactic(tiebreakers=[combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning")])
    try:
        tact2.extend_tiebreakers("dog")
        raise AssertionError("Allowed invalid extend tiebreaker")
    except ValueError:
        pass
    tact2.extend_tiebreakers(tact1)
    assert tact2.get_tiebreakers() == [combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning"),
                                       combatant_tactics.BloodiedTactic(), combatant_tactics.IsUnconsciousTactic()]

close_logging(None, CH)
