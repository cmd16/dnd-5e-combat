"""
:Test module: :py:mod:`attack_tactics`
:Test class: :py:class:`AttackTactic` and subclasses
:Test dependencies:
    - :py:func:`test_tactics`
    - :py:func:`test_attack`
    - :py:func:`test_armory`
    - :py:func:`test_dice`
:return: None
"""
from DnD_5e import armory, dice
from DnD_5e.attack_class import Attack
from DnD_5e.attack_class.saving_throw_attacks import SavingThrowAttack
from DnD_5e.combatant import Combatant
from DnD_5e.tactics import attack_tactics
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.tactics.AttackTactics', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')

DAGGER_KWARGS = armory.Dagger().get_attack_kwargs()
DAGGER_RANGE = Attack(**(DAGGER_KWARGS["range"]))
DAGGER_DISADV = Attack(**(DAGGER_KWARGS["range_disadv"]))
DAGGER_MELEE = Attack(**(DAGGER_KWARGS["melee"]))
JAVELIN_KWARGS = armory.Javelin().get_attack_kwargs()
JAVELIN_RANGE = Attack(**(JAVELIN_KWARGS["range"]))
JAVELIN_DISADV = Attack(**(JAVELIN_KWARGS["range_disadv"]))
JAVELIN_MELEE = Attack(**(JAVELIN_KWARGS["melee"]))
CROSSBOW_KWARGS = armory.LightCrossbow().get_attack_kwargs()
CROSSBOW = Attack(**(CROSSBOW_KWARGS["range"]))
CROSSBOW_DISADV = Attack(**(CROSSBOW_KWARGS["range_disadv"]))
GLAIVE = Attack(**(armory.Glaive().get_attack_kwargs()["melee"]))
CLUB = Attack(**(armory.Club().get_attack_kwargs()["melee"]))
TORCH = Attack(name="torch", melee_range=5, damage_dice=dice.DamageDice(str_val="1d6", damage_type="fire"))
HOT_N_COLD_DAMAGE_DICE = dice.DamageDiceBag(
    dice_list=[dice.DamageDice(str_val="1d4", damage_type="fire"),
               dice.DamageDice(str_val="1d8", damage_type="cold")])
assert isinstance(HOT_N_COLD_DAMAGE_DICE, dice.DamageDiceBag)
HOT_N_COLD = Attack(name="hot_n_cold", melee_range=5, damage_dice=HOT_N_COLD_DAMAGE_DICE)
DAGGER_MELEE_ADV = Attack(**(DAGGER_KWARGS["melee"]), adv=1)
DAGGER_MELEE_5 = Attack(**(DAGGER_KWARGS["melee"]), attack_mod=5)
DAGGER_MELEE_3 = Attack(**(DAGGER_KWARGS["melee"]), attack_mod=3)
GREATSWORD = Attack(**(armory.Greatsword().get_attack_kwargs()["melee"]))
DAGGER_MELEE_MOD_2 = Attack(**(DAGGER_KWARGS["melee"]), damage_mod=2)


# this ugly setup is because I wrote this dumb the first time and refactoring this is not a priority
def get_attack_list():
    return [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, JAVELIN_MELEE, JAVELIN_RANGE, JAVELIN_DISADV, CROSSBOW,
            CROSSBOW_DISADV, GLAIVE]


def get_attack_list_club_torch_hotncold():
    test_list_2 = get_attack_list()
    test_list_2.extend([CLUB, TORCH, HOT_N_COLD])
    return test_list_2


def get_attack_list_dagger_melee():
    test_list_2 = get_attack_list()
    test_list_2.append(DAGGER_MELEE_ADV)
    return test_list_2


def get_attack_list_dagger_mods():
    test_list2 = get_attack_list()
    test_list2.extend([DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5])
    return test_list2


def get_attack_list_greatsword_dagger():
    test_list2 = get_attack_list_dagger_mods()
    test_list2.extend([GREATSWORD, DAGGER_MELEE_MOD_2])
    return test_list2


def test_DamageTypeTactic():
    test_list = get_attack_list()
    test_list_without_glaive = get_attack_list()
    test_list_without_glaive.pop()

    piercing_tactic = attack_tactics.DamageTypeTactic(damage_type="piercing")
    assert piercing_tactic.run_tactic(test_list) == test_list_without_glaive


def get_Combatant():
    return Combatant(ac=12, max_hp=50, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                     strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                     proficiencies=["dexterity", "strength"], proficiency_mod=2,
                     name='t0', vulnerabilities=["slashing"],
                     resistances=["bludgeoning", "fire"], immunities=["piercing"], logger=TEST_LOGGER)


def test_DamageVulnerabilityTactic():
    combatant_0 = get_Combatant()

    test_list_2 = get_attack_list_club_torch_hotncold()
    for attack in test_list_2:
        combatant_0.add_attack(attack)
    damage_vulnerability = attack_tactics.DamageVulnerabilityTactic()
    assert damage_vulnerability.run_tactic(test_list_2, target=combatant_0) == [GLAIVE]
    combatant_0.set_attack_tactic(damage_vulnerability)
    assert combatant_0.select_attack(target=combatant_0) == GLAIVE


def test_DamageNoImmuneTactic():
    combatant_0 = get_Combatant()
    test_list_2 = get_attack_list_club_torch_hotncold()
    damage_no_immune = attack_tactics.DamageNoImmuneTactic()
    assert damage_no_immune.run_tactic(test_list_2, target=combatant_0) == [GLAIVE, CLUB, TORCH, HOT_N_COLD]


def test_DamageNoResistanceTactic():
    test_list = get_attack_list()
    combatant_0 = get_Combatant()
    damage_no_resistance = attack_tactics.DamageNoResistanceTactic()
    assert damage_no_resistance.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, JAVELIN_MELEE, JAVELIN_RANGE,
                                                                     JAVELIN_DISADV, CROSSBOW, CROSSBOW_DISADV, GLAIVE]


def test_LowRangeTactic():
    test_list = get_attack_list()
    range_normal_lt_80 = attack_tactics.LowRangeTactic(threshold=80)
    assert range_normal_lt_80.run_tactic(test_list) == [DAGGER_RANGE, DAGGER_DISADV, JAVELIN_RANGE]
    range_normal_gt_20 = attack_tactics.HighRangeTactic(threshold=20)
    assert range_normal_gt_20.run_tactic(test_list) == [DAGGER_DISADV, JAVELIN_RANGE, JAVELIN_DISADV, CROSSBOW, CROSSBOW_DISADV]
    melee_range_lt_10 = attack_tactics.LowMeleeRangeTactic(threshold=10)
    assert melee_range_lt_10.run_tactic(test_list) == [DAGGER_MELEE, JAVELIN_MELEE]
    melee_range_gt_5 = attack_tactics.HighMeleeRangeTactic(threshold=5)
    assert melee_range_gt_5.run_tactic(test_list) == [GLAIVE]


def test_tacticWithCombatant():
    combatant_0 = get_Combatant()
    for attack in get_attack_list():
        combatant_0.add_attack(attack)
    melee_range_gt_5 = attack_tactics.HighMeleeRangeTactic(threshold=5)
    combatant_0.set_attack_tactic(melee_range_gt_5)
    assert combatant_0.select_attack() == GLAIVE


def test_AdvTactic_DisAdvTactic_NoDisadvTactic():
    test_list = get_attack_list_dagger_melee()
    has_adv = attack_tactics.HasAdvTactic()
    assert has_adv.run_tactic(test_list) == [DAGGER_MELEE_ADV]
    has_disadv = attack_tactics.HasDisAdvTactic()
    assert has_disadv.run_tactic(test_list) == [DAGGER_DISADV, JAVELIN_DISADV, CROSSBOW_DISADV]
    has_no_disadv = attack_tactics.HasNoDisadvTactic()
    assert has_no_disadv.run_tactic(test_list) == [DAGGER_MELEE, DAGGER_RANGE, JAVELIN_MELEE, JAVELIN_RANGE,
                                                   CROSSBOW, GLAIVE, DAGGER_MELEE_ADV]


def test_HighestMaxHitTactic():
    test_list = get_attack_list_dagger_mods()
    highest_max_hit = attack_tactics.HighestMaxHitTactic()
    assert highest_max_hit.run_tactic(test_list) == [DAGGER_MELEE_5]
    high_max_hit = attack_tactics.HighMaxHitTactic(threshold=20)
    assert high_max_hit.run_tactic(test_list) == [DAGGER_MELEE_3, DAGGER_MELEE_5]


def test_hit_tactics():
    combatant_0 = get_Combatant()
    combatant_0.set_ac(21)
    test_list = get_attack_list_dagger_mods()
    max_hit_ac = attack_tactics.MaxHitACTactic()
    assert max_hit_ac.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE_3, DAGGER_MELEE_5]
    highest_avg_hit = attack_tactics.HighestAvgHitTactic()
    assert highest_avg_hit.run_tactic(test_list) == [DAGGER_MELEE_5]
    high_avg_hit = attack_tactics.HighAvgHitTactic(threshold=11)
    assert high_avg_hit.run_tactic(test_list) == [DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5]
    combatant_0.set_ac(12)
    avg_hit_ac_high = attack_tactics.AvgHitACHighTactic()
    assert avg_hit_ac_high.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5]


def test_max_damage_tactics():
    test_list = get_attack_list_dagger_mods()
    combatant_0 = get_Combatant()
    highest_max_damage = attack_tactics.HighestMaxDamageTactic()
    assert highest_max_damage.run_tactic(test_list) == [GLAIVE]
    high_max_damage = attack_tactics.HighMaxDamageTactic(threshold=6)
    assert high_max_damage.run_tactic(test_list) == [CROSSBOW, CROSSBOW_DISADV, GLAIVE]

    max_damage_hp_high = attack_tactics.MaxDamageHPHighTactic()
    combatant_0.take_damage(13)  # combatant_0 had 20 hp, so now it has 7 hp
    assert max_damage_hp_high.run_tactic(test_list, target=combatant_0) == [CROSSBOW, CROSSBOW_DISADV, GLAIVE]
    assert max_damage_hp_high.run_tactic(test_list, target=combatant_0)
    low_max_damage = attack_tactics.LowMaxDamageTactic(threshold=6)
    assert low_max_damage.run_tactic(test_list) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5]
    combatant_0.take_damage(1)  # combatant_0 had 7 hp, so now it has 6 hp
    max_damage_hp_low = attack_tactics.MaxDamageHPLowTactic()
    assert max_damage_hp_low.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5]


def test_min_damage_tactics():
    test_list = get_attack_list_greatsword_dagger()
    combatant_0 = get_Combatant()
    highest_min_damage = attack_tactics.HighestMinDamageTactic()
    assert highest_min_damage.run_tactic(test_list) == [DAGGER_MELEE_MOD_2]
    low_min_damage = attack_tactics.LowMinDamageTactic(threshold=2)
    assert low_min_damage.run_tactic(test_list) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, JAVELIN_MELEE,
                                                    JAVELIN_RANGE, JAVELIN_DISADV, CROSSBOW,
                                                    CROSSBOW_DISADV, GLAIVE, DAGGER_MELEE_ADV, DAGGER_MELEE_3,
                                                    DAGGER_MELEE_5]
    combatant_0.take_damage(18)  # combatant_0 had 20 hp, so now it has 2 hp
    min_damage_hp_low = attack_tactics.MinDamageHPLowTactic()
    assert min_damage_hp_low.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV,
                                                                  JAVELIN_MELEE, JAVELIN_RANGE, JAVELIN_DISADV,
                                                                  CROSSBOW, CROSSBOW_DISADV, GLAIVE, DAGGER_MELEE_ADV,
                                                                  DAGGER_MELEE_3, DAGGER_MELEE_5]
    high_min_damage = attack_tactics.HighMinDamageTactic(threshold=1)
    assert high_min_damage.run_tactic(test_list) == [GREATSWORD, DAGGER_MELEE_MOD_2]
    min_damage_hp_high = attack_tactics.MinDamageHPHighTactic()
    assert min_damage_hp_high.run_tactic(test_list, target=combatant_0) == [GREATSWORD, DAGGER_MELEE_MOD_2]


def test_adv_damage_tactics():
    test_list = get_attack_list_greatsword_dagger()
    combatant_0 = get_Combatant()
    highest_avg_damage = attack_tactics.HighestAvgDamageTactic()
    assert highest_avg_damage.run_tactic(test_list) == [GREATSWORD]
    high_avg_damage = attack_tactics.HighAvgDamageTactic(threshold=4)
    assert high_avg_damage.run_tactic(test_list) == [CROSSBOW, CROSSBOW_DISADV, GLAIVE, GREATSWORD, DAGGER_MELEE_MOD_2]
    combatant_0.take_damage(18)
    combatant_0.take_healing(2)  # combatant_0 had 2 hp, so now it has 4 hp
    avg_damage_hp_high = attack_tactics.AvgDamageHPHighTactic()
    assert avg_damage_hp_high.run_tactic(test_list, target=combatant_0) == [CROSSBOW, CROSSBOW_DISADV, GLAIVE, GREATSWORD, DAGGER_MELEE_MOD_2]
    low_avg_damage = attack_tactics.LowAvgDamageTactic(threshold=3)
    assert low_avg_damage.run_tactic(test_list) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV, DAGGER_MELEE_ADV,
                                                    DAGGER_MELEE_3, DAGGER_MELEE_5]
    combatant_0.take_damage(1)  # combatant_0 had 4 hp, so now it has 3 hp
    avg_damage_hp_low = attack_tactics.AvgDamageHPLowTactic()
    assert avg_damage_hp_low.run_tactic(test_list, target=combatant_0) == [DAGGER_MELEE, DAGGER_RANGE, DAGGER_DISADV,
                                                                  DAGGER_MELEE_ADV, DAGGER_MELEE_3, DAGGER_MELEE_5]


def test_dpr_tactics():
    test_list = get_attack_list_greatsword_dagger()
    combatant_0 = get_Combatant()
    combatant_0.take_damage(17)

    dpr_max = attack_tactics.DprMaxTactic()
    assert dpr_max.run_tactic(test_list, target=combatant_0) == [GREATSWORD]
    dpr_high = attack_tactics.DprHighTactic(threshold=2)
    assert dpr_high.run_tactic(test_list, target=combatant_0) == [CROSSBOW, GLAIVE, GREATSWORD, DAGGER_MELEE_MOD_2]
    combatant_0.take_damage(1)  # combatant_0 had 3 hp, so now it has 2 hp
    dpr_hp_high = attack_tactics.DprHpHighTactic()
    assert dpr_hp_high.run_tactic(test_list, target=combatant_0) == [CROSSBOW, GLAIVE, GREATSWORD, DAGGER_MELEE_MOD_2]
    dpr_low = attack_tactics.DprLowTactic(threshold=1)
    assert dpr_low.run_tactic(test_list, target=combatant_0) == [DAGGER_DISADV, JAVELIN_DISADV, CROSSBOW_DISADV]
    combatant_0.take_damage(1)  # combatant_0 had 2 hp, so now it has 1 hp
    dpr_hp_low = attack_tactics.DprHpLowTactic()
    assert dpr_hp_low.run_tactic(test_list, target=combatant_0) == [DAGGER_DISADV, JAVELIN_DISADV, CROSSBOW_DISADV]

    # more likely to hit than greatsword and does the same amount of damage
    cha_save_attack = SavingThrowAttack(save_type="charisma", dc=10, damage_dice=(2, 6), name="cha")
    test_list.append(cha_save_attack)
    assert dpr_max.run_tactic(test_list, target=combatant_0) == [cha_save_attack]
    # less likely to hit than greatsword and does the same amount of damage
    dex_save_attack = SavingThrowAttack(save_type="dexterity", dc=10, damage_dice=(2, 6), name="dex")
    test_list.remove(cha_save_attack)
    test_list.append(dex_save_attack)
    assert dpr_max.run_tactic(test_list, target=combatant_0) == [GREATSWORD]


close_logging(None, CH)
