"""
:Test module: :py:mod:`weapons`
:Test class: :py:class:`Weapon`
:Test functions: :py:func:`weapon_list_equals`
:Untested parts: invalid input
:Test dependencies: :py:meth:`test_combatant`
:return: None
"""
from copy import copy, deepcopy

from DnD_5e.combatant import Combatant
from DnD_5e.tests.test_utils import setup_logging, close_logging

from DnD_5e.weapons import Weapon

LOG_INFO = setup_logging('DnD_5e.tests.weapons', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_invalid_weapon_should_raise_exception():
    try:
        Weapon(name='weapon')
        raise AssertionError("Create weapon succeeded without damage dice")   # pragma: no cover
    except ValueError:
        pass

    try:
        Weapon(damage_dice=(1, 4))
        raise AssertionError("Create weapon succeeded without name")   # pragma: no cover
    except ValueError:
        pass


def get_Dagger():
    return Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_mod=2, damage_mod=1, damage_type="piercing")


def test_constructor_should_work():
    dagger = get_Dagger()
    assert dagger.get_name() == "dagger"
    props = dagger.get_properties()
    assert "finesse" in props
    assert dagger.has_prop("light")
    assert dagger.has_prop("range")
    assert dagger.get_range() == (20, 60)
    assert dagger.has_prop("melee")
    assert dagger.get_melee_range() == 5
    assert dagger.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger.has_prop("heavy")
    assert not dagger.has_prop("load")
    assert not dagger.has_prop("reach")
    assert not dagger.has_prop("two_handed")
    assert not dagger.has_prop("versatile")
    assert dagger.get_attack_mod() == 2
    assert dagger.get_damage_mod() == 1
    assert dagger.get_damage_type() == "piercing"


def test_copy_should_eq():
    dagger = get_Dagger()
    dagger_2 = deepcopy(dagger)
    props = dagger_2.get_properties()
    assert "finesse" in props
    assert dagger_2.has_prop("light")
    assert dagger_2.has_prop("range")
    assert dagger_2.get_range() == (20, 60)
    assert dagger_2.has_prop("melee")
    assert dagger_2.get_melee_range() == 5
    assert dagger_2.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger_2.has_prop("heavy")
    assert not dagger_2.has_prop("load")
    assert not dagger_2.has_prop("reach")
    assert not dagger_2.has_prop("two_handed")
    assert not dagger_2.has_prop("versatile")
    assert dagger_2.get_attack_mod() == 2
    assert dagger_2.get_damage_mod() == 1
    assert dagger_2.get_damage_type() == "piercing"
    assert dagger is not dagger_2  # make sure they aren't the same object
    assert dagger.get_damage_dice() is not dagger_2.get_damage_dice()


def test_weapon_should_work_with_Combatant():
    dagger = get_Dagger()
    dagger_2 = copy(dagger)
    combatant_0 = Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name='combatant_0', logger=TEST_LOGGER)

    assert combatant_0.get_weapon_attack_mod(dagger) == 3
    assert combatant_0.get_weapon_damage_mod(dagger) == 3

    combatant_0.add_weapon(dagger)
    assert combatant_0.get_weapons() == [dagger]
    assert dagger.get_owner() is combatant_0

    combatant_2 = copy(combatant_0)
    combatant_2.set_name("combatant_2")
    assert len(combatant_2.get_weapons()) == 1
    dagger_3 = combatant_2.get_weapons()[0]
    assert dagger_3.get_name() == "dagger"
    props = dagger_3.get_properties()
    assert "finesse" in props
    assert dagger_3.has_prop("light")
    assert dagger_3.has_prop("range")
    assert dagger_3.get_range() == (20, 60)
    assert dagger_3.has_prop("melee")
    assert dagger_3.get_melee_range() == 5
    assert dagger_3.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger_3.has_prop("heavy")
    assert not dagger_3.has_prop("load")
    assert not dagger_3.has_prop("reach")
    assert not dagger_3.has_prop("two_handed")
    assert not dagger_3.has_prop("versatile")
    assert dagger_3.get_attack_mod() == 2
    assert dagger_3.get_damage_mod() == 1
    assert dagger_3.get_damage_type() == "piercing"

    assert dagger == dagger_2
    assert dagger == dagger_3
    dagger.set_attack_mod(3)
    assert dagger != dagger_2


def test_invalid_weapon_for_Combatant_should_raise_exception():
    combatant_0 = Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name='combatant_0', logger=TEST_LOGGER)
    try:
        combatant_0.add_weapon('stuff')
        raise AssertionError("Add weapon succeeded for non-weapon")   # pragma: no cover
    except ValueError:
        pass


close_logging(None, CH)
