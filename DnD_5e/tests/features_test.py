"""
Test Features that don't belong to a specific Character class

:Test module: :py:mod:`features`
:Test class: :py:class:`ArcheryFightingStyle`, :py:class:`DefenseFightingStyle`
:Untested parts:
  - DraconicResilience
:Test dependencies: :py:func:`test_barbarian`, :py:func:`test_armory`
:return: None
"""

from DnD_5e import features, armory, armor
from DnD_5e.combatant import Combatant
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.features', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_fighting_styles():

    archer = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                       strength_mod=2, dexterity_mod=3, constitution_mod=-1, intelligence_mod=1, wisdom_mod=0,
                       charisma_mod=-1, proficiency_mod=2, proficiencies=["shortbow"],
                       name='archer', feature_classes=[features.ArcheryFightingStyle], logger=TEST_LOGGER)
    assert archer.get_weapon_attack_mod(armory.Shortbow()) == 3 + 2 + 2
    # TODO: more tests once the attack mod method is implemented for adding weapon attacks

    defender = Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                         strength_mod=2, dexterity_mod=3, constitution_mod=-1, intelligence_mod=1, wisdom_mod=0,
                         charisma_mod=-1, proficiency_mod=2, proficiencies=["shortbow"],
                         name='archer', feature_classes=[features.DefenseFightingStyle], logger=TEST_LOGGER)
    assert defender.get_ac() == 10 + 3
    defender.set_armor(armor.StuddedLeatherArmor)
    assert defender.get_ac() == 12 + 3 + 1

close_logging(None, CH)
