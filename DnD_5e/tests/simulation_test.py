"""
:Test module: :py:mod:`simulation`
:Test class: :py:class:`Simulation`
:Untested parts: None
:Test dependencies:
    - :py:func:`test_encounter`
:return:
"""
import time
from copy import deepcopy, copy

from DnD_5e import armory, team, encounter, simulation
from DnD_5e.combatant import Combatant
from DnD_5e.team import Team
from DnD_5e.tests.test_utils import setup_logging, close_logging
from DnD_5e.utility_methods_dnd import NullLogger

LOG_INFO = setup_logging('DnD_5e.tests.simulation', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_deep_copy_should_eq():
    combatant_1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_2 = deepcopy(combatant_1)
    combatant_2.set_name("combatant_2")
    team_c = Team(combatant_list=[combatant_1, combatant_2], name="team_c")
    combatant_d1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=16, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_d1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_d2 = deepcopy(combatant_d1)
    combatant_d2.set_name("combatant_d2")
    team_d = team.Team(combatant_list=[combatant_d1, combatant_d2], name="team_d")
    encounter_0 = encounter.Encounter(name="encounter_0", teams=[team_c, team_d], logger=TEST_LOGGER)

    simulation_0 = simulation.Simulation(name="simulation_0", encounter=encounter_0)
    assert simulation_0.get_encounter() == encounter_0
    simulation_1 = deepcopy(simulation_0)
    assert simulation_1.get_encounter() is not simulation_0.get_encounter()
    assert simulation_1.get_encounter() == simulation_0.get_encounter()
    assert simulation_1.get_encounter().get_combatants()[0] is not simulation_0.get_encounter().get_combatants()[0]


def test_simulation():
    combatant_1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_2 = copy(combatant_1)
    combatant_2.set_name("combatant_2")
    team_c = Team(combatant_list=[combatant_1, combatant_2], name="team_c")
    combatant_d1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=16, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_d1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_d2 = copy(combatant_d1)
    combatant_d2.set_name("combatant_d2")
    team_d = team.Team(combatant_list=[combatant_d1, combatant_d2], name="team_d")
    encounter_0 = encounter.Encounter(name="encounter_0", teams=[team_c, team_d], logger=TEST_LOGGER)

    simulation_0 = simulation.Simulation(name="simulation_0", encounter=encounter_0)

    simulation_0.run(5)  # run the simulation 5 times

    # we don't want a bazillion logging statements
    null_logger = NullLogger()
    simulation_0.get_encounter().set_logger(null_logger)
    for comb in simulation_0.get_encounter().get_combatants():
        comb.set_logger(null_logger)

    print("simulation_0")
    start = time.time()
    simulation_0.run(100)
    elapsed_time = time.time() - start
    TEST_LOGGER.info("Time to run simulation s1 100 times: %f", elapsed_time)


def test_simulation_multiprocessing():
    combatant_1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_2 = copy(combatant_1)
    combatant_2.set_name("combatant_2")
    team_c = Team(combatant_list=[combatant_1, combatant_2], name="team_c")
    combatant_d1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                   strength=16, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                   name="combatant_d1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_d2 = copy(combatant_d1)
    combatant_d2.set_name("combatant_d2")
    team_d = team.Team(combatant_list=[combatant_d1, combatant_d2], name="team_d")
    encounter_0 = encounter.Encounter(name="encounter_0", teams=[team_c, team_d], logger=TEST_LOGGER)

    simulation_0 = simulation.Simulation(name="simulation_0", encounter=encounter_0)

    simulation_0.run(5)  # run the simulation 5 times

    # we don't want a bazillion logging statements
    null_logger = NullLogger()
    simulation_0.get_encounter().set_logger(null_logger)
    for comb in simulation_0.get_encounter().get_combatants():
        comb.set_logger(null_logger)

    for process_count in [1, 2, 4, 8]:
        print(f'running with {process_count} processes')
        start = time.time()
        simulation_0.mp_run(1000, process_count)
        elapsed_time = time.time() - start
        TEST_LOGGER.info("Time to run simulation s1 1000 times with %d processes: %f", process_count, elapsed_time)
        print(f"Time to run simulation s1 1000 times with {process_count} processes: {elapsed_time}")

close_logging(None, CH)
