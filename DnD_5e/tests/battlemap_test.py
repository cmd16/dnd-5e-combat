from DnD_5e.attack_class import Attack
from DnD_5e.battlemap import BattleMap
from DnD_5e.combatant import Combatant
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.battlemap', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_normal_dark():
    return {BattleMap.Point(1, 2), BattleMap.Point(2, 2)}


def get_magical_dark():
    return {BattleMap.Point(1, 3), BattleMap.Point(2, 4)}


def get_non_traversable():
    return {BattleMap.Point(3, 4), BattleMap.Point(5, 2), BattleMap.Point(4, 7)}


def get_difficult_terrain():
    return {BattleMap.Point(2, 2), BattleMap.Point(2, 3), BattleMap.Point(2, 4)}


def get_swimmable_terrain():
    return {BattleMap.Point(5, 4), BattleMap.Point(4, 4), BattleMap.Point(5, 3), BattleMap.Point(4, 3)}


def get_damage_terrain_dict():
    return {BattleMap.Point(0, 2): Attack(damage_dice='1d4', name="1d4")}


def get_Combatant():
    return Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, speed=20, climb_speed=5, fly_speed=10, swim_speed=15,
              vision='normal', strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
              proficiencies=["dexterity", "charisma"], expertise=["stealth"], proficiency_mod=2,
              name='combatant_0', vulnerabilities=["bludgeoning"], resistances=["slashing", "slashing", "piercing"],
              immunities={"fire"}, logger=TEST_LOGGER)


def get_BattleMap():
    normal_dark = get_normal_dark()
    magical_dark = get_magical_dark()
    non_traversable = get_non_traversable()
    difficult_terrain = get_difficult_terrain()
    swimmable_terrain = get_swimmable_terrain()
    return BattleMap(dimensions=(0, 10, 0, 12), dark_info={'normal': normal_dark, 'magical': magical_dark},
                     non_traversable_terrain=non_traversable, difficult_terrain=difficult_terrain,
                     swimmable_terrain=swimmable_terrain,
                     occupied_terrain_dict={BattleMap.Point(6, 6): get_Combatant()},
                     damage_terrain_dict=get_damage_terrain_dict())


def test_validate_point_should_succeed():
    BattleMap.validate_point(BattleMap.Point(1, 2))


def test_validate_point_should_raise_exception():
    try:
        BattleMap.validate_point((1, 2))
        raise AssertionError("validate_point should fail without Point type")
    except ValueError:
        pass
    try:
        BattleMap.validate_point(BattleMap.Point("1", "2"))
        raise AssertionError("validate_point should fail when values are not integers")
    except ValueError:
        pass


def test_validate_points_should_succeed():
    BattleMap.validate_points(get_difficult_terrain())


def test_validate_points_should_raise_exception():
    try:
        BattleMap.validate_points(list(get_difficult_terrain()))
        raise AssertionError("validate_points should fail with incorrect type")
    except ValueError:
        pass


def test_constructor_should_raise_exception_when_dimensions_invalid():
    try:
        BattleMap(x_min=0, x_max=0, y_min=0, y_max=12)
        raise AssertionError("Allowed battlemap creation with x_min == x_max")
    except ValueError:
        pass
    try:
        BattleMap(x_min=0, x_max=5, y_min=12, y_max=12)
        raise AssertionError("Allowed battlemap creation with y_min == y_max")
    except ValueError:
        pass
    try:
        BattleMap(x_min=3, x_max=0, y_min=0, y_max=12)
        raise AssertionError("Allowed battlemap creation with x_min > x_max")
    except ValueError:
        pass
    try:
        BattleMap(x_min=0, x_max=5, y_min=12, y_max=5)
        raise AssertionError("Allowed battlemap creation with y_min > y_max")
    except ValueError:
        pass
    try:
        BattleMap(x_min="0", x_max=5, y_min=0, y_max=12)
        raise AssertionError("Allowed battlemap creation with non-int x_min")
    except ValueError:
        pass
    try:
        BattleMap(x_min=0, x_max=5, y_min="2", y_max=12)
        raise AssertionError("Allowed battlemap creation with non-int y_min")
    except ValueError:
        pass
    try:
        BattleMap(x_min=0, x_max="5", y_min=0, y_max=12)
        raise AssertionError("Allowed battlemap creation with non-int x_max")
    except ValueError:
        pass
    try:
        BattleMap(x_min=0, x_max=5, y_min=0, y_max="12")
        raise AssertionError("Allowed battlemap creation with non-int y_max")
    except ValueError:
        pass

    try:
        BattleMap(dimensions="nope")
        raise AssertionError("Allowed battlemap creation with non-list/tuple dimensions")
    except ValueError:
        pass
    try:
        BattleMap(dimensions=[1, 2, 3])
        raise AssertionError("Allowed battlemap creation with incorrect length dimensions")
    except ValueError:
        pass
    try:
        BattleMap(dimensions=[1, "2", None, "4"])
        raise AssertionError("Allowed battlemap creation with non-int dimensions")
    except ValueError:
        pass


def test_constructor_should_raise_exception_when_square_size_invalid():
    try:
        BattleMap(dimensions=(0, 2, 0, 3), square_size=3)
        raise AssertionError("Allowed battlemap creation with square size too small")
    except ValueError:
        pass
    try:
        BattleMap(dimensions=(0, 2, 0, 3), square_size="5")
        raise AssertionError("Allowed battlemap creation with non-integer square size")
    except ValueError:
        pass


def test_constructor_should_raise_exception_when_nontraversable_points_overlap():
    try:
        BattleMap(dimensions=(0, 10, 0, 12), non_traversable_terrain=get_non_traversable(),
                  difficult_terrain=list(get_non_traversable())[1:3],
                  swimmable_terrain=get_swimmable_terrain())
        raise AssertionError("Allowed battlemap with overlap in non-traversable and difficult terrain")
    except ValueError:
        pass
    try:
        BattleMap(dimensions=(0, 10, 0, 12), non_traversable_terrain=get_non_traversable(),
                  difficult_terrain=get_difficult_terrain(),
                  swimmable_terrain=list(get_non_traversable())[0])
        raise AssertionError("Allowed battlemap with overlap in non-traversable and difficult terrain")
    except ValueError:
        pass


def test_traversable_accessors():
    battle_map = get_BattleMap()
    assert battle_map.get_square_size() == 5
    for point in get_non_traversable():
        assert not battle_map.is_traversable(point), f"{point} should not be traversable"
        assert not battle_map.is_walkable(point), f"{point} is not traversable and should not be walkable"
    occupied_point = BattleMap.Point(6, 6)
    assert not battle_map.is_traversable(occupied_point), f"{occupied_point} is occupied and should not be traversable"
    assert not battle_map.is_walkable(occupied_point), f"{occupied_point} is occupied and should not be walkable"
    assert battle_map.get_occupant(occupied_point) == get_Combatant()
    for point in get_difficult_terrain():
        assert battle_map.is_traversable(point), f"{point} is difficult terrain and should be traversable"
        assert not battle_map.is_swimmable(point), f"{point} should not be swimmable"
    for point in get_swimmable_terrain():
        assert battle_map.is_traversable(point), f"{point} is swimmable terrain and should be traversable"
        assert battle_map.is_swimmable(point), f"{point} should be swimmable"
    assert not battle_map.is_swimmable(BattleMap.Point(1, 1))


def test_damage_accessor():
    battle_map = get_BattleMap()
    assert battle_map.get_dpr_for_point(BattleMap.Point(3, 2)) == 0
    damage_point = BattleMap.Point(0, 2)
    assert battle_map.get_dpr_for_point(damage_point) == 0.55 * 2.5  # 55% chance of hitting AC 10
    assert battle_map.get_dpr_for_point(damage_point, 13) == 0.40 * 2.5  # 40% chance of hitting AC 13


def test_damage_setters():
    battle_map = get_BattleMap()
    damage_point = BattleMap.Point(0, 2)
    assert battle_map.get_dpr_for_point(damage_point) == 0.55 * 2.5  # 55% chance of hitting AC 10
    battle_map.clear_damage_for_point(damage_point)
    assert battle_map.get_dpr_for_point(damage_point) == 0

    new_point = BattleMap.Point(1, 4)
    attack = list(get_damage_terrain_dict().values())[0]
    assert battle_map.get_dpr_for_point(new_point) == 0
    battle_map.set_damage_for_point(new_point, attack)
    assert battle_map.get_dpr_for_point(new_point) == 0.55 * 2.5  # 55% chance of hitting AC 10


def test_vision_accessors():
    battle_map = get_BattleMap()
    for point in get_normal_dark():
        assert battle_map.get_light_source(point) == "dark"
    for point in get_magical_dark():
        assert battle_map.get_light_source(point) == "magic"
    assert battle_map.get_light_source(BattleMap.Point(1, 1)) == "normal"
    assert battle_map.is_in_bounds(BattleMap.Point(3, 2))
    assert not battle_map.is_in_bounds(BattleMap.Point(-1, 0))

    battle_map_10 = BattleMap(dimensions=(0, 10, 0, 12), square_size=10)
    assert battle_map_10.get_square_size() == 10


def test_darkness_setters():
    battle_map = get_BattleMap()
    normal_dark_point = list(get_normal_dark())[0]
    assert battle_map.get_light_source(normal_dark_point) == "dark"
    battle_map.dispel_magical_darkness(normal_dark_point)
    assert battle_map.get_light_source(normal_dark_point) == "dark"
    battle_map.dispel_normal_darkness(normal_dark_point)
    assert battle_map.get_light_source(normal_dark_point) == "normal"

    magical_dark_point = list(get_magical_dark())[0]
    assert battle_map.get_light_source(magical_dark_point) == "magic"
    battle_map.dispel_normal_darkness(magical_dark_point)
    assert battle_map.get_light_source(magical_dark_point) == "magic"
    battle_map.dispel_magical_darkness(magical_dark_point)
    assert battle_map.get_light_source(magical_dark_point) == "normal"

    new_point = BattleMap.Point(7, 7)
    assert battle_map.get_light_source(new_point) == "normal"
    battle_map.set_normal_dark_point(new_point)
    assert battle_map.get_light_source(new_point) == "dark"
    battle_map.set_magical_dark_point(new_point)
    assert battle_map.get_light_source(new_point) == "magic"
    battle_map.dispel_magical_darkness(new_point)
    assert battle_map.get_light_source(new_point) == "dark"
    battle_map.dispel_normal_darkness(new_point)
    assert battle_map.get_light_source(new_point) == "normal"

    other_point = BattleMap.Point(6, 8)
    assert battle_map.get_light_source(other_point) == "normal"
    battle_map.set_normal_dark_point(other_point)
    battle_map.set_magical_dark_point(other_point)
    battle_map.dispel_all_darkness(other_point)
    assert battle_map.get_light_source(other_point) == "normal"


def test_set_occupant():
    battle_map = get_BattleMap()
    occupant = get_Combatant()
    point = BattleMap.Point(0, 0)
    battle_map.set_occupant(point, occupant)
    assert battle_map.get_occupant(point) is occupant
    # replace an occupant
    occupant2 = get_Combatant()
    assert occupant2 == occupant
    assert occupant2 is not occupant
    battle_map.set_occupant(point, occupant2)
    assert battle_map.get_occupant(point) is occupant2


def test_get_neighbors():
    battle_map = get_BattleMap()
    neighbors = battle_map.get_neighbors(BattleMap.Point(0, 1))
    expected_neighbors = [BattleMap.Point(0, 2), BattleMap.Point(1, 2), BattleMap.Point(1, 1), BattleMap.Point(1, 0),
                          BattleMap.Point(0, 0)]
    assert neighbors == expected_neighbors, neighbors

    battle_map_0 = BattleMap(dimensions=(-10, 10, -10, 10))
    neighbors_10 = battle_map_0.get_neighbors(BattleMap.Point(0, 0), 10)
    expected_neighbors_10 = [BattleMap.Point(-2, 2), BattleMap.Point(-1, 2), BattleMap.Point(0, 2),
                             BattleMap.Point(1, 2), BattleMap.Point(2, 2), BattleMap.Point(2, 1), BattleMap.Point(2, 0),
                             BattleMap.Point(2, -1), BattleMap.Point(2, -2), BattleMap.Point(1, -2),
                             BattleMap.Point(0, -2), BattleMap.Point(-1, -2), BattleMap.Point(-2, -2),
                             BattleMap.Point(-2, -1), BattleMap.Point(-2, 0), BattleMap.Point(-2, 1)]
    assert neighbors_10 == expected_neighbors_10, neighbors_10


def test_get_traversable_neighbors():
    battle_map = get_BattleMap()
    neighbors = battle_map.get_traversable_neighbors(BattleMap.Point(3, 3))
    # note (3, 4) is missing because it is non-traversable
    expected_neighbors = [BattleMap.Point(2, 4), BattleMap.Point(4, 4), BattleMap.Point(4, 3), BattleMap.Point(4, 2),
                          BattleMap.Point(3, 2), BattleMap.Point(2, 2), BattleMap.Point(2, 3)]
    assert neighbors == expected_neighbors, neighbors


def test_distance_to_traverse():
    battle_map = get_BattleMap()
    assert battle_map.get_distance_to_traverse(BattleMap.Point(1, 2)) == (5, "")
    assert battle_map.get_distance_to_traverse(BattleMap.Point(3, 4) is False)
    assert battle_map.get_distance_to_traverse(BattleMap.Point(2, 2)) == (10, "")
    assert battle_map.get_distance_to_traverse(BattleMap.Point(2, 2), can_ignore_difficult=True) == (5, "")
    assert battle_map.get_distance_to_traverse(BattleMap.Point(5, 4)) == (10, "swim")
    assert battle_map.get_distance_to_traverse(BattleMap.Point(5, 4), can_ignore_swim=True) == (5, "swim")


def test_get_heuristic_distance():
    battle_map = get_BattleMap()
    assert battle_map.get_heuristic_distance(BattleMap.Point(1, 1), BattleMap.Point(1, 3)) == 10
    assert battle_map.get_heuristic_distance(BattleMap.Point(3, 2), BattleMap.Point(4, 5)) == 15


def test_find_straight_path():
    battle_map = get_BattleMap()
    # test a straight path with no difficult terrain or obstructions
    path_info = battle_map.find_path(BattleMap.Point(0, 0), BattleMap.Point(0, 5))
    assert path_info
    assert path_info['path'] == [BattleMap.Point(0, 1), BattleMap.Point(0, 2), BattleMap.Point(0, 3),
                                 BattleMap.Point(0, 4), BattleMap.Point(0, 5)]
    assert path_info['distance'] == 25
    assert path_info['swim_distance'] == 0
    assert path_info['damage'] == 0.55 * 2.5  # 55% chance of hitting AC 10

    path_info_13 = battle_map.find_path(BattleMap.Point(0, 0), BattleMap.Point(0, 5), ac=13)
    assert path_info_13['damage'] == 0.40 * 2.5, path_info_13['damage']  # 40% chance of hitting AC 13


def test_find_diagonal_path_with_difficult_terrain():
    battle_map = get_BattleMap()
    # test a diagonal path with difficult terrain
    path_info = battle_map.find_path(BattleMap.Point(0, 0), BattleMap.Point(2, 2))
    assert path_info
    assert path_info['path'] == [BattleMap.Point(1, 1), BattleMap.Point(2, 2)]
    assert path_info['distance'] == 15
    assert path_info['swim_distance'] == 0
    assert path_info['damage'] == 0


def test_path_divert_difficult_terrain():
    battle_map = get_BattleMap()
    # test a path where we want to divert due to difficult terrain
    path_info = battle_map.find_path(BattleMap.Point(2, 1), BattleMap.Point(2, 3))
    assert path_info
    fork_left = [BattleMap.Point(1, 2), BattleMap.Point(2, 3)]
    fork_right = [BattleMap.Point(3, 2), BattleMap.Point(2, 3)]
    assert path_info['path'] == fork_left or path_info['path'] == fork_right
    assert path_info['distance'] == 15
    assert path_info['swim_distance'] == 0
    assert path_info['damage'] == 0


def test_find_path_returns_false_when_no_path():
    battle_map = get_BattleMap()
    # goal is out of bounds
    assert not battle_map.find_path(BattleMap.Point(0, 0), BattleMap.Point(-3, 3))

    doomed_battle_map = BattleMap(dimensions=(0, 10, 0, 12),
                                  non_traversable_terrain={BattleMap.Point(0, 1), BattleMap.Point(1, 1),
                                                           BattleMap.Point(1, 0), BattleMap.Point(1, 0)})
    # path is blocked by bounds of the map and non-traversable terrain
    assert not doomed_battle_map.find_path(BattleMap.Point(0, 0), BattleMap.Point(5, 10))


close_logging(None, CH)
