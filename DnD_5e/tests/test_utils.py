import logging


def setup_logging(name: str, filename: str):
    logging.basicConfig()
    test_logger = logging.getLogger(name)
    test_logger.setLevel(logging.DEBUG)
    if filename:
        # create file handler which logs even debug messages
        file_handler = logging.FileHandler(filename)
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler = None
    # create console handler with a higher log level
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    if file_handler:
        file_handler.setFormatter(formatter)
        test_logger.addHandler(file_handler)
    console_handler.setFormatter(formatter)
    # add the handlers to the logger
    test_logger.addHandler(console_handler)
    result = {'logger': test_logger, 'FH': file_handler, 'CH': console_handler}
    return result


def close_logging(file_handler, console_handler):
    if file_handler:
        file_handler.close()
    console_handler.close()


def features_should_vary_by_level(constructor, feature_to_min_level: dict):
    levels = set(feature_to_min_level.values())
    levels.add(min(levels) - 1)
    for level in levels:
        item = constructor(level)
        for feature in feature_to_min_level:
            if feature_to_min_level.get(feature) > level:
                assert not item.has_feature(feature), f"should not have {feature} at level {level}"
            else:
                assert item.has_feature(feature), f"should have {feature} at level {level}"
