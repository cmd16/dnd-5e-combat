"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Paladin`
:Untested parts:
    - invalid input to constructor
    - :py:meth:`spend_divine_sense_slot`
    - :py:meth:`spend_cleansing_touch_slot`
    - :py:meth:`send_lay_on_hands`
    - :py:meth:`send_divine_smite`
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e.character_classes.paladin import Paladin
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Paladin', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Paladin(level=14):
    return Paladin(name="p0", level=level, ac=18, strength_mod=1, dexterity_mod=-2, constitution_mod=3,
            wisdom_mod=2, intelligence_mod=0, charisma_mod=4, fighting_style="archery",
            logger=test_logger)


def test_constructor_should_work():
    p0 = get_Paladin()
    assert p0.get_level() == 14
    assert p0.get_hit_dice() == (14, 10)
    assert p0.get_max_hp() == (10 + 3 + (6+3)*13)
    assert p0.has_proficiency("simple weapons")
    assert p0.has_proficiency("martial weapons")
    assert p0.has_proficiency("wisdom")
    assert p0.has_proficiency("charisma")
    assert p0.get_proficiency_mod() == 5
    assert p0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1}
    assert p0.has_feature("divine sense")
    assert p0.get_divine_sense_slots() == 1 + 4
    assert p0.has_feature("lay on hands")
    assert p0.get_lay_on_hands_pool() == 14 * 5
    assert p0.has_feature("divine smite")
    assert p0.has_feature("divine health")
    assert p0.has_feature("sacred oath")
    assert p0.has_feature("extra attack")
    assert p0.has_feature("aura of protection")
    assert p0.has_feature("aura of courage")
    assert p0.get_aura() == 10
    assert p0.has_feature("improved divine smite")
    assert p0.has_feature("cleansing touch")
    assert p0.get_cleansing_touch_slots() == 4


def test_copy_should_eq():
    p0 = get_Paladin()

    p1 = copy(p0)
    assert p1.get_level() == 14
    assert p1.get_hit_dice() == (14, 10)
    assert p1.get_max_hp() == (10 + 3 + (6 + 3) * 13)
    assert p1.has_proficiency("simple weapons")
    assert p1.has_proficiency("martial weapons")
    assert p1.has_proficiency("wisdom")
    assert p1.has_proficiency("charisma")
    assert p1.get_proficiency_mod() == 5
    assert p1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1}
    assert p1.has_feature("divine sense")
    assert p1.get_divine_sense_slots() == 1 + 4
    assert p1.has_feature("lay on hands")
    assert p1.get_lay_on_hands_pool() == 14 * 5
    assert p1.has_feature("divine smite")
    assert p1.has_feature("divine health")
    assert p1.has_feature("sacred oath")
    assert p1.has_feature("extra attack")
    assert p1.has_feature("aura of protection")
    assert p1.has_feature("aura of courage")
    assert p1.get_aura() == 10
    assert p1.has_feature("improved divine smite")
    assert p1.has_feature("cleansing touch")
    assert p1.get_cleansing_touch_slots() == 4
    assert p1 is not p0

    assert p0 == p1
    assert p0.current_eq(p1)
    p1.spend_cleansing_touch_slot()
    assert p1.get_cleansing_touch_slots() == 3
    assert not p0.current_eq(p1)
    assert p0 == p1


def test_features_should_vary_by_level():
    paladin_features = {
        'fighting style': 2,
        'divine smite': 2,
        'divine health': 3,
        'sacred oath': 3,
        'extra attack': 5,
        'aura of protection': 6,
        'aura of courage': 10,
        'improved divine smite': 11,
        'cleansing touch': 14,
    }
    features_should_vary_by_level(get_Paladin, paladin_features)

close_logging(None, ch)
