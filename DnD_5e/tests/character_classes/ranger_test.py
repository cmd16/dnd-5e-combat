"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Ranger`
:Untested parts:
    - invalid input to constructor
    - :py:meth:`has_favored_enemy` with a :py:class:`Creature` as input
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e.character_classes.ranger import Ranger
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Ranger', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Ranger(level=9):
    return Ranger(name="r0", level=level, ac=16, strength_mod=3, dexterity_mod=2, constitution_mod=2, wisdom_mod=1,
                                  intelligence_mod=-1, charisma_mod=-2, favored_enemies=("beast", "undead"),
                                  favored_terrains=("forest", "swamp"), fighting_style="defense", logger=test_logger)


def test_constructor_should_work():
    r0 = get_Ranger()
    assert r0.get_level() == 9
    assert r0.get_hit_dice() == (9, 10)
    assert r0.get_max_hp() == 10 + 2 + (6+2)*8
    assert r0.has_proficiency("simple weapons")
    assert r0.has_proficiency("martial weapons")
    assert r0.has_proficiency("strength")
    assert r0.has_proficiency("dexterity")
    assert r0.get_proficiency_mod() == 4
    assert r0.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert r0.has_feature("favored enemy")
    assert r0.has_favored_enemy("beast")
    assert r0.has_favored_enemy("undead")
    assert r0.has_feature("favored terrain")
    assert r0.has_favored_terrain("forest")
    assert r0.has_favored_terrain("swamp")
    assert r0.has_feature("fighting style")
    assert r0.has_fighting_style("defense")
    assert r0.has_feature("ranger archetype")
    assert r0.has_feature("primeval awareness")
    assert r0.has_feature("extra attack")
    assert r0.has_feature("land's stride")


def test_copy_should_eq():
    r0 = get_Ranger()

    r1 = copy(r0)
    assert r1.get_level() == 9
    assert r1.get_hit_dice() == (9, 10)
    assert r1.get_max_hp() == 10 + 2 + (6 + 2) * 8
    assert r1.has_proficiency("simple weapons")
    assert r1.has_proficiency("martial weapons")
    assert r1.has_proficiency("strength")
    assert r1.has_proficiency("dexterity")
    assert r1.get_proficiency_mod() == 4
    assert r1.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert r1.has_feature("favored enemy")
    assert r1.has_favored_enemy("beast")
    assert r1.has_favored_enemy("undead")
    assert r1.get_favored_enemies() is not r0.get_favored_enemies()
    assert r1.has_feature("favored terrain")
    assert r1.has_favored_terrain("forest")
    assert r1.has_favored_terrain("swamp")
    assert r1.get_favored_terrains() is not r0.get_favored_terrains()
    assert r1.has_feature("fighting style")
    assert r1.has_fighting_style("defense")
    assert r1.has_feature("ranger archetype")
    assert r1.has_feature("primeval awareness")
    assert r1.has_feature("extra attack")
    assert r1.has_feature("land's stride")
    assert r1 is not r0

    assert r0 == r1


def test_features_should_vary_by_level():
    ranger_features = {
        'fighting style': 2,
        'ranger archetype': 3,
        'primeval awareness': 3,
        'extra attack': 5,
        'land\'s stride': 8,
        'hide in plain sight': 10,
        'vanish': 14,
        'feral senses': 18,
        'foe slayer': 20,
    }
    features_should_vary_by_level(get_Ranger, ranger_features)


close_logging(None, ch)
