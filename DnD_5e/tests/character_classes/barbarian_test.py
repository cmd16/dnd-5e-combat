"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Barbarian`
:Untested parts:
    - invalid input to constructor
    - rage
    - reckless attack
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from copy import copy

from DnD_5e import features, armor
from DnD_5e.character_classes.barbarian import Barbarian
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Barbarian', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Barbarian(level=20):
    return Barbarian(name="barb", strength_mod=4, dexterity_mod=4, constitution_mod=4,
                                       wisdom_mod=-2, intelligence_mod=-2, charisma_mod=-3, speed=30, level=level,
                                       logger=test_logger)


def test_constructor_should_work():
    barb = get_Barbarian()
    assert barb.get_level() == 20
    assert barb.get_hit_dice() == (20, 12)
    assert barb.get_max_hp() == 12 + 4 + (7+4)*19
    assert barb.has_proficiency("simple weapons")
    assert barb.has_proficiency("martial weapons")
    assert barb.has_proficiency("strength")
    assert barb.has_proficiency("constitution")
    assert barb.get_proficiency_mod() == 6
    assert barb.get_rage_slots() == 6
    assert barb.get_rage_damage_bonus() == 4
    assert not barb.is_raging()
    # TODO: test for reckless state
    assert barb.has_feature("rage")
    assert barb.has_feature("unarmored defense")
    assert barb.has_feature_class(features.UnarmoredDefenseBarbarian)
    assert barb.has_feature_method("get_unarmored_ac")
    assert barb.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseBarbarian.get_unarmored_ac
    assert barb.get_unarmored_ac() == barb.get_ac() == 10 + 4 + 4
    assert barb.has_feature_class(features.FastMovementBarbarian)
    assert barb.has_feature_method("get_speed")
    assert barb.get_speed() == 30 + 10, barb.get_speed()  # Fast Movement feature


def test_copy_should_eq():
    barb = get_Barbarian()

    barb2 = copy(barb)
    assert barb2.get_level() == 20
    assert barb2.get_hit_dice() == (20, 12)
    assert barb2.get_max_hp() == 12 + 4 + (7 + 4) * 19
    assert barb2.has_proficiency("simple weapons")
    assert barb2.has_proficiency("martial weapons")
    assert barb2.has_proficiency("strength")
    assert barb2.has_proficiency("constitution")
    assert barb2.get_proficiency_mod() == 6
    assert barb2.get_rage_slots() == 6
    assert barb2.get_rage_damage_bonus() == 4
    assert not barb2.is_raging()
    assert barb2.has_feature("rage")
    assert barb2.has_feature("unarmored defense")
    assert barb2.has_feature("reckless attack")
    assert barb2.has_feature("danger sense")
    assert barb2.has_feature("extra attack")
    assert barb2.get_speed() == 40
    assert barb2.has_feature("feral instinct")
    assert barb2.has_feature("brutal critical")
    assert barb2.has_feature("relentless rage")
    assert barb2.has_feature("persistent rage")
    assert barb2.has_feature("indomitable might")
    assert barb2 is not barb
    assert barb2.has_feature_class(features.UnarmoredDefenseBarbarian)
    assert barb2.has_feature_method("get_unarmored_ac")
    assert barb2.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseBarbarian.get_unarmored_ac
    assert barb2.get_unarmored_ac() == barb2.get_ac() == 10 + 4 + 4
    assert barb2.get_speed() == 30 + 10  # Fast Movement feature

    assert barb == barb2
    assert barb.current_eq(barb2)
    barb.start_rage()
    assert barb.is_raging()
    assert not barb.current_eq(barb2)
    assert barb == barb2


def test_heavy_armor_should_disable_FastMovement():
    barb = Barbarian(name="barb", armor=armor.PlateArmor, strength_mod=4, dexterity_mod=4,
                                       constitution_mod=4, wisdom_mod=-2, intelligence_mod=-2, charisma_mod=-3,
                                       speed=30, level=20, logger=test_logger)
    assert barb.get_speed() == barb._speed  # pylint: disable=protected-access
    assert barb.get_speed() == 30, barb.get_speed()  # wearing Heavy Armor, so no Fast Movement
    barb.set_armor(None)
    assert barb.get_speed() == 30 + 10  # Fast Movement
    barb.set_armor(armor.LeatherArmor)
    assert barb.get_speed() == 30 + 10  # Fast Movement
    barb.set_armor(armor.SplintArmor)
    assert barb.get_speed() == 30


def test_rage_should_vary_by_level():
    barb2 = get_Barbarian(2)
    assert barb2.get_rage_slots() == 2
    assert barb2.get_rage_damage_bonus() == 2
    barb3 = get_Barbarian(3)
    assert barb3.get_rage_slots() == 3
    assert barb3.get_rage_damage_bonus() == 2
    barb6 = get_Barbarian(6)
    assert barb6.get_rage_slots() == 4
    assert barb6.get_rage_damage_bonus() == 2
    barb9 = get_Barbarian(9)
    assert barb9.get_rage_slots() == 4
    assert barb9.get_rage_damage_bonus() == 3
    barb12 = get_Barbarian(12)
    assert barb12.get_rage_slots() == 5
    assert barb12.get_rage_damage_bonus() == 3
    barb16 = get_Barbarian(16)
    assert barb16.get_rage_slots() == 5
    assert barb16.get_rage_damage_bonus() == 4
    barb17 = get_Barbarian(17)
    assert barb17.get_rage_slots() == 6
    assert barb17.get_rage_damage_bonus() == 4


def test_features_should_vary_by_level():
    barbarian_features = {
        'reckless attack': 2,
        'danger sense': 2,
        'extra attack': 5,
        'feral instinct': 7,
        'brutal critical': 9,
        'relentless rage': 11,
        'persistent rage': 15,
        'indomitable might': 18,
    }
    features_should_vary_by_level(get_Barbarian, barbarian_features)

close_logging(None, ch)
