"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Druid`
:Untested parts:
    - invalid input to constructor
    - everything involved with changing into a wild shape
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e import bestiary
from DnD_5e.character_classes.druid import Druid
from DnD_5e.tests.test_utils import setup_logging, close_logging

loginfo = setup_logging('DnD_5e.tests.character_classes.Druid', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Druid(level=5):
    return Druid(ac=14, level=level, strength_mod=-1, dexterity_mod=1, constitution_mod=1,
          wisdom_mod=3, intelligence_mod=0, charisma_mod=-1, name="d0", logger=test_logger)


def test_constructor_should_work():
    d0 = get_Druid()
    assert d0.get_level() == 5
    assert d0.get_hit_dice() == (5, 8)
    assert d0.get_max_hp() == 8 + 1 + (5 + 1)*4
    assert d0.has_proficiency("club")
    assert d0.has_proficiency("dagger")
    assert d0.has_proficiency("javelin")
    assert d0.has_proficiency("mace")
    assert d0.has_proficiency("quarterstaff")
    assert d0.has_proficiency("scimitar")
    assert d0.has_proficiency("sickle")
    assert d0.has_proficiency("sling")
    assert d0.has_proficiency("spear")
    assert d0.has_proficiency("intelligence")
    assert d0.has_proficiency("wisdom")
    assert d0.get_proficiency_mod() == 3
    assert d0.get_spell_ability() == "wisdom"
    assert d0.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert d0.get_wild_shape_slots() == 2
    assert d0.get_wild_shapes() == []
    assert d0.get_current_shape() is None


def test_copy_should_eq():
    d0 = get_Druid()

    d1 = copy(d0)
    assert d1.get_level() == 5
    assert d1.get_hit_dice() == (5, 8)
    assert d1.get_max_hp() == 8 + 1 + (5 + 1) * 4
    assert d1.has_proficiency("club")
    assert d1.has_proficiency("dagger")
    assert d1.has_proficiency("javelin")
    assert d1.has_proficiency("mace")
    assert d1.has_proficiency("quarterstaff")
    assert d1.has_proficiency("scimitar")
    assert d1.has_proficiency("sickle")
    assert d1.has_proficiency("sling")
    assert d1.has_proficiency("spear")
    assert d1.has_proficiency("intelligence")
    assert d1.has_proficiency("wisdom")
    assert d1.get_proficiency_mod() == 3
    assert d1.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert d1.get_wild_shape_slots() == 2
    assert d1.get_wild_shapes() == []
    assert d1.get_current_shape() is None
    assert d1 is not d0

    assert d0.current_eq(d1)
    d0.add_wild_shape(bestiary.Aboleth())
    assert not d0.current_eq(d1)


close_logging(None, ch)
