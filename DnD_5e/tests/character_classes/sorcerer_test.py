"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Sorcerer`
:Untested parts:
    - invalid input to constructor
    - Draconic Resilience feature (because that's for a Draconic archetype of Sorcerer)
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""

from DnD_5e.character_classes.sorcerer import Sorcerer
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Sorcerer', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Sorcerer(level=20):
    if level > 2:
        metamagic = ['careful', 'distant']
        if level > 9:
            metamagic.append('empowered')
        if level > 16:
            metamagic.append('heightened')
    else:
        metamagic = None
    return Sorcerer(name="s0", level=level, ac=12, strength_mod=-1, dexterity_mod=3, constitution_mod=2,
                                    wisdom_mod=1, intelligence_mod=-1, charisma_mod=3,
                                    metamagic=metamagic, logger=test_logger)


def test_constructor_should_work():
    s0 = get_Sorcerer()
    assert s0.get_level() == 20
    assert s0.get_hit_dice() == (20, 6)
    assert s0.get_max_hp() == 6 + s0.get_constitution() + (4 + s0.get_constitution())*19
    assert s0.has_proficiency("dagger")
    assert s0.has_proficiency("dart")
    assert s0.has_proficiency("sling")
    assert s0.has_proficiency("quarterstaff")
    assert s0.has_proficiency("light crossbow")
    assert s0.has_proficiency("constitution")
    assert s0.has_proficiency("charisma")
    assert s0.get_spell_ability() == "charisma"
    assert s0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert s0.has_feature("font of magic")
    assert s0.get_sorcery_points() == s0.get_full_sorcery_points() == 20
    assert s0.has_feature("metamagic")
    assert s0.has_metamagic("careful")
    assert s0.has_metamagic("distant")
    assert s0.has_metamagic("empowered")
    assert s0.has_metamagic("heightened")


def test_copy_should_eq():
    s0 = get_Sorcerer()

    s1 = copy(s0)
    assert s1.get_level() == 20
    assert s1.get_max_hp() == 6 + s1.get_constitution() + (4 + s1.get_constitution()) * 19
    assert s1.has_proficiency("dagger")
    assert s1.has_proficiency("dart")
    assert s1.has_proficiency("sling")
    assert s1.has_proficiency("quarterstaff")
    assert s1.has_proficiency("light crossbow")
    assert s1.has_proficiency("constitution")
    assert s1.has_proficiency("charisma")
    assert s1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert s1.has_feature("font of magic")
    assert s1.get_sorcery_points() == s1.get_full_sorcery_points() == 20
    assert s1.has_feature("metamagic")
    assert s1.has_metamagic("careful")
    assert s1.has_metamagic("distant")
    assert s1.has_metamagic("empowered")
    assert s1.has_metamagic("heightened")
    assert s1.get_metamagic() is not s0.get_metamagic()
    assert s1 is not s0

    assert s0 == s1
    assert s0.current_eq(s1)
    assert s1.sorcery_points_to_spell_slot(2) == 3
    assert s1.get_sorcery_points() == 17
    assert s1.get_level_spell_slots(2) == 4
    assert not s0.current_eq(s1)
    assert s0 == s1


def test_sorcery_points_should_spend_and_convert():
    s1 = get_Sorcerer()
    s1.spend_spell_slot(2)
    assert s1.get_level_spell_slots(2) == 2
    assert s1.spend_sorcery_points(5) == 5
    assert s1.get_sorcery_points() == 15  # 20 - 5 = 15

    s1.spell_slot_to_sorcery_points(7)
    assert s1.get_sorcery_points() == 22  # 15 + 7 == 22
    assert s1.get_level_spell_slots(7) == 1  # 2 - 1 == 1

    s1.reset_sorcery_points()
    assert s1.get_sorcery_points() == s1.get_full_sorcery_points() == 20


def test_invalid_spend_sorcery_points_should_raise_exception():
    s1 = get_Sorcerer()
    try:
        s1.spend_sorcery_points(0)
        raise Exception("Spent an invalid amount of sorcery points")
    except ValueError:
        pass

    try:
        s1.spend_sorcery_points(30)
        raise Exception("Spent more sorcery points than you have")
    except ValueError:
        pass


def test_invalid_sorcery_points_to_spell_slot_should_raise_exception():
    s1 = get_Sorcerer()
    try:
        s1.sorcery_points_to_spell_slot(6)
        raise Exception("Converted sorcery points to spell slot of too high a level")
    except ValueError:
        pass


def test_sorcerer_without_sorcery_points_shouldNot_use_sorcery_points():
    s3 = get_Sorcerer(1)
    try:
        s3.reset_sorcery_points()
        raise Exception("Allowed Sorcerer without sorcery points to reset sorcery points")
    except ValueError:
        pass

    try:
        s3.spend_sorcery_points(2)
        raise Exception("Allowed Sorcerer without sorcery points to spend sorcery points")
    except ValueError:
        pass

    try:
        s3.spell_slot_to_sorcery_points(3)
        raise Exception("Allowed Sorcerer without sorcery points to convert spell slot to sorcery points")
    except ValueError:
        pass


def test_features_should_vary_by_level():
    sorcerer_features = {
        'font of magic': 2,
        'metamagic': 3,
    }
    features_should_vary_by_level(get_Sorcerer, sorcerer_features)

close_logging(None, ch)
