"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Monk`
:Untested parts:
    - invalid input to constructor
    - add an :py:class:`Attack` that uses a :py:class:`Monk` weapon when *self's* dexterity is greater than *self's* strength
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_armory`
:return: None
"""
from copy import copy

from DnD_5e import features
from DnD_5e.character_classes.monk import Monk
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Monk', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Monk(level=20):
    return Monk(name="m0", level=level, strength_mod=5, dexterity_mod=5, constitution_mod=4,
         wisdom_mod=1, intelligence_mod=0, charisma_mod=-2, logger=test_logger)


def test_constructor_should_work():
    m0 = get_Monk()
    assert m0.get_level() == 20
    assert m0.get_hit_dice() == (20, 8)
    assert m0.get_max_hp() == (8 + 4) + (5+4)*19
    assert m0.has_proficiency("monk weapons")
    assert m0.has_proficiency("strength")
    assert m0.has_proficiency("dexterity")
    assert m0.has_proficiency("constitution")
    assert m0.has_proficiency("wisdom")
    assert m0.has_proficiency("intelligence")
    assert m0.has_proficiency("charisma")
    assert m0.get_proficiency_mod() == 6
    assert m0.get_martial_arts_dice() == (1, 10)
    assert m0.has_feature("unarmored defense")
    assert m0.get_ki_points() == 20
    assert m0.get_ki_save_dc() == 8 + 6 + 1
    assert m0.get_saving_throw("strength") == m0.get_strength() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("dexterity") == m0.get_dexterity() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("constitution") == m0.get_constitution() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("wisdom") == m0.get_wisdom() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("intelligence") == m0.get_intelligence() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("charisma") == m0.get_charisma() + m0.get_proficiency_mod()
    assert features.UnarmoredDefenseMonk in m0.get_feature_classes()
    assert "get_unarmored_ac" in m0.get_feature_methods()
    assert m0.has_feature_class(features.UnarmoredDefenseMonk)
    assert m0.has_feature_method("get_unarmored_ac")
    assert m0.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseMonk.get_unarmored_ac
    assert m0.get_unarmored_ac() == m0.get_ac() == 10 + 5 + 1


def test_copy_should_eq():
    m0 = get_Monk()

    m1 = copy(m0)
    assert m1.get_hit_dice() == (20, 8)
    assert m1.get_max_hp() == (8 + 4) + (5 + 4) * 19
    assert m1.has_proficiency("monk weapons")
    assert m1.has_proficiency("strength")
    assert m1.has_proficiency("dexterity")
    assert m1.has_proficiency("constitution")
    assert m1.has_proficiency("wisdom")
    assert m1.has_proficiency("intelligence")
    assert m1.has_proficiency("charisma")
    assert m1.get_proficiency_mod() == 6
    assert m1.get_martial_arts_dice() == (1, 10)
    assert m1.has_feature("unarmored defense")
    assert m1.get_ki_points() == 20
    assert m1.get_ki_save_dc() == 8 + 6 + 1
    assert m1.get_saving_throw("strength") == m1.get_strength() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("dexterity") == m1.get_dexterity() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("constitution") == m1.get_constitution() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("wisdom") == m1.get_wisdom() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("intelligence") == m1.get_intelligence() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("charisma") == m1.get_charisma() + m1.get_proficiency_mod()
    assert m1.has_feature_class(features.UnarmoredDefenseMonk)
    assert m1.has_feature_method("get_unarmored_ac")
    assert m1.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseMonk.get_unarmored_ac
    assert m1.get_unarmored_ac() == m1.get_ac() == 10 + 5 + 1
    assert m1 is not m0

    assert m0 == m1
    assert m0.current_eq(m1)
    assert m1.spend_ki_points(5) == 5
    assert m1.get_ki_points() == 15
    assert not m0.current_eq(m1)
    assert m0 == m1


def test_features_should_vary_by_level():
    monk_features = {
        'flurry of blows': 2,
        'patient defense': 2,
        'step of the wind': 2,
        'deflect missiles': 3,
        'slow fall': 4,
        'extra attack': 5,
        'stunning strike': 5,
        'ki-empowered strikes': 6,
        'stillness of mind': 7,
        'evasion': 7,
        'purity of body': 10,
        'tongue of the sun and moon': 13,
        'diamond soul': 14,
        'empty body': 18,
        'perfect soul': 20,
    }
    features_should_vary_by_level(get_Monk, monk_features)


close_logging(None, ch)
