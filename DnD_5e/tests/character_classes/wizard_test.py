"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Wizard`
:Untested parts:
    - invalid input to constructor
    - :py:meth:`has_spell_mastery` and :py:meth:`has_signature_spell` with Spells
    - :py:meth:`spend_spell_slot`
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e.attack_class.spell_attacks import Spell
from DnD_5e.character_classes.wizard import Wizard
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Wizard', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Wizard(level=20):
    return Wizard(name="w0", level=level, ac=12, strength_mod=-2, dexterity_mod=1, constitution_mod=1,
                                  wisdom_mod=3, intelligence_mod=3, charisma_mod=-1, spell_mastery=["a", "b"],
                                  signature_spells=["c", "d"], logger=test_logger)


def test_constructor_should_work():
    w0 = Wizard(name="w0", level=20, ac=12, strength_mod=-2, dexterity_mod=1, constitution_mod=1,
                                  wisdom_mod=3, intelligence_mod=3, charisma_mod=-1, spell_mastery=["a", "b"],
                                  signature_spells=["c", "d"], logger=test_logger)
    assert w0.get_level() == 20
    assert w0.get_hit_dice() == (20, 6)
    assert w0.get_max_hp() == 6 + w0.get_constitution() + (4 + w0.get_constitution())*19
    assert w0.has_proficiency("dagger")
    assert w0.has_proficiency("dart")
    assert w0.has_proficiency("sling")
    assert w0.has_proficiency("light crossbow")
    assert w0.has_proficiency("intelligence")
    assert w0.has_proficiency("wisdom")
    assert w0.get_proficiency_mod() == 6
    assert w0.get_spell_ability() == "intelligence"
    assert w0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert w0.has_feature("spell mastery")
    assert w0.has_spell_mastery("a")
    assert w0.has_spell_mastery("b")
    assert w0.has_feature("signature spells")
    assert w0.has_signature_spell("c")
    assert w0.has_signature_spell("d")
    assert w0.get_signature_spells() == ["c", "d"]


def test_copy_should_eq():
    w0 = get_Wizard()

    w1 = copy(w0)
    assert w1.get_level() == 20
    assert w1.get_hit_dice() == (20, 6)
    assert w1.get_max_hp() == 6 + w1.get_constitution() + (4 + w1.get_constitution()) * 19
    assert w1.has_proficiency("dagger")
    assert w1.has_proficiency("dart")
    assert w1.has_proficiency("sling")
    assert w1.has_proficiency("light crossbow")
    assert w1.has_proficiency("intelligence")
    assert w1.has_proficiency("wisdom")
    assert w1.get_proficiency_mod() == 6
    assert w1.get_spell_ability() == "intelligence"
    assert w1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert w1.get_spell_slots() is not w0.get_spell_slots()
    assert w1.has_feature("spell mastery")
    assert w1.has_spell_mastery("a")
    assert w1.has_spell_mastery("b")
    assert w1.has_feature("signature spells")
    assert w1.has_signature_spell("c")
    assert w1.has_signature_spell("d")
    assert w1.get_signature_spells() == ["c", "d"]
    assert w1.get_signature_spell_slots() is not w0.get_signature_spell_slots()
    assert w1 is not w0

    assert w0 == w1
    assert w0.current_eq(w1)


def test_spell_mastery_should_work_with_Spell():
    w0 = get_Wizard()
    w1 = Wizard(copy=w0, name="w1")

    spell_a = Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                    school="magic", damage_dice=(1, 8), range=60, name="a")
    assert w0.has_spell_mastery(spell_a)
    assert w1.has_spell_mastery(spell_a)

    w1.spend_spell_slot(1, spell=spell_a)
    assert w1.get_spell_slots().get(1) == 4
    assert w1.current_eq(w0)


def test_signature_spell_should_work_with_Spell():
    w0 = get_Wizard()
    w1 = Wizard(copy=w0, name="w1")

    spell_c = Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, name="c")
    assert w0.has_signature_spell(spell_c)
    assert w1.has_signature_spell(spell_c)

    w1.spend_spell_slot(1, spell=spell_c)
    assert w1.get_signature_spell_slots()["c"] == 0
    assert not w0.current_eq(w1)
    assert w0 == w1


def test_features_should_vary_by_level():
    wizard_features = {
        'spell mastery': 18,
        'signature spells': 20,
    }
    features_should_vary_by_level(get_Wizard, wizard_features)

close_logging(None, ch)
