"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Fighter`
:Untested parts:
    - invalid input to constructor
    - action surge (NOT IMPLEMENTED YET)
    - extra attack (NOT IMPLEMENTED YET)
    - second wind
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_armory`
:return: None
"""
from copy import copy

from DnD_5e import armory
from DnD_5e.character_classes.fighter import Fighter
from DnD_5e.tests.test_utils import setup_logging, close_logging

loginfo = setup_logging('DnD_5e.tests.character_classes.Fighter', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Fighter(level=11, fighting_style="archery"):
    return Fighter(level=level, name="f0", ac=18, strength_mod=4, dexterity_mod=2, constitution_mod=4,
            wisdom_mod=-1, intelligence_mod=-2, charisma_mod=-3, fighting_style=fighting_style,
            logger=test_logger)


def test_constructor_should_work():
    f0 = Fighter(level=11, name="f0", ac=18, strength_mod=4, dexterity_mod=2, constitution_mod=4,
                                   wisdom_mod=-1, intelligence_mod=-2, charisma_mod=-3, fighting_style="archery",
                                   logger=test_logger)
    assert f0.get_level() == 11
    assert f0.get_hit_dice() == (11, 10)
    assert f0.get_max_hp() == 10 + 4 + (6+4)*10
    assert f0.has_proficiency("simple weapons")
    assert f0.has_proficiency("martial weapons")
    assert f0.has_proficiency("strength")
    assert f0.has_proficiency("constitution")
    assert f0.get_proficiency_mod() == 4
    assert f0.has_fighting_style("archery")
    assert f0.has_feature("fighting style")
    assert f0.has_feature("second wind")
    assert f0.get_second_wind_slots() == 1
    assert f0.has_feature("action surge")
    assert f0.get_action_surge_slots() == 1
    assert f0.has_feature("martial archetype")
    assert f0.has_feature("extra attack")
    assert f0.get_extra_attack_num() == 2
    assert f0.has_feature("indomitable")
    assert f0.get_indomitable_slots() == 1


def test_copy_should_eq():
    f0 = get_Fighter()

    f1 = copy(f0)
    assert f1.get_hit_dice() == (11, 10)
    assert f1.get_max_hp() == 10 + 4 + (6 + 4) * 10
    assert f1.has_proficiency("simple weapons")
    assert f1.has_proficiency("martial weapons")
    assert f1.has_proficiency("strength")
    assert f1.has_proficiency("constitution")
    assert f1.get_proficiency_mod() == 4
    assert f1.has_fighting_style("archery")
    assert f1.has_feature("fighting style")
    assert f1.has_feature("second wind")
    assert f1.get_second_wind_slots() == 1
    assert f1.has_feature("action surge")
    assert f1.get_action_surge_slots() == 1
    assert f1.has_feature("martial archetype")
    assert f1.has_feature("extra attack")
    assert f1.get_extra_attack_num() == 2
    assert f1.has_feature("indomitable")
    assert f1.get_indomitable_slots() == 1
    assert f1 is not f0

    assert f1 == f0
    assert f1.current_eq(f0)
    f1.take_action_surge()  # decreases action surge slots
    assert not f1.current_eq(f0)
    assert f1 == f0


def test_archer_fighting_style_should_increase_attack_mod():
    f0 = get_Fighter()

    shortbow = armory.Shortbow()
    assert isinstance(shortbow, armory.RangedWeapon)
    assert shortbow.get_range()

    f0.add_weapon(shortbow)
    for attack in f0.get_attacks():
        # both are ranged attacks
        assert attack.get_attack_mod() == f0.get_dexterity() + f0.get_proficiency_mod() + 2  # testing Archer fighting style
    f0.remove_weapon(shortbow)

close_logging(None, ch)
