"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Warlock`
:Untested parts: invalid input to constructor
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""

from DnD_5e.character_classes.warlock import Warlock
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Warlock', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Warlock(level=20):
    return Warlock(name="w0", level=level, ac=16, strength_mod=1, dexterity_mod=3, constitution_mod=2,
                                   wisdom_mod=1, intelligence_mod=-1, charisma_mod=3, pact_boon="blade",
                                   eldritch_invocations=["agonizing blast", "armor of shadows", "ascendant step",
                                                         "beast speech", "beguiling influence", "bewitching whispers",
                                                         "book of ancient secrets", "chains of carcerei"],
                                   logger=test_logger)


def test_constructor_should_work():
    w0 = get_Warlock()
    assert w0.get_level() == 20
    assert w0.get_hit_dice() == (20, 8)
    assert w0.get_max_hp() == 8 + w0.get_constitution() + (5 + w0.get_constitution())*19
    assert w0.has_proficiency("simple weapons")
    assert w0.has_proficiency("wisdom")
    assert w0.has_proficiency("charisma")
    assert w0.get_proficiency_mod() == 6
    assert w0.get_spell_ability() == "charisma"
    assert w0.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1}
    assert w0.has_feature("pact boon")
    assert w0.get_pact_boon() == "blade"
    assert w0.has_feature("eldritch invocations")
    assert w0.get_eldritch_invocations() == {"agonizing blast", "armor of shadows", "ascendant step", "beast speech",
                                             "beguiling influence", "bewitching whispers", "book of ancient secrets",
                                             "chains of carcerei"}
    assert w0.has_eldritch_invocation("chains of carcerei")


def test_copy_should_eq():
    w0 = get_Warlock()

    w1 = copy(w0)
    assert w1.get_level() == 20
    assert w1.get_max_hp() == 8 + w1.get_constitution() + (5 + w1.get_constitution()) * 19
    assert w1.has_proficiency("simple weapons")
    assert w1.has_proficiency("wisdom")
    assert w1.has_proficiency("charisma")
    assert w1.get_proficiency_mod() == 6
    assert w1.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1}
    assert w1.has_feature("pact boon")
    assert w1.get_pact_boon() == "blade"
    assert w1.has_feature("eldritch invocations")
    assert w1.get_eldritch_invocations() == {"agonizing blast", "armor of shadows", "ascendant step", "beast speech",
                                             "beguiling influence", "bewitching whispers", "book of ancient secrets",
                                             "chains of carcerei"}
    assert w1.has_eldritch_invocation("chains of carcerei")
    assert w1.get_eldritch_invocations() is not w0.get_eldritch_invocations()
    assert w1 is not w0

    assert w0 == w1


def test_features_should_vary_by_level():
    warlock_features = {
        'eldritch invocations': 2,
        'pact boon': 3,
    }
    features_should_vary_by_level(get_Warlock, warlock_features)

close_logging(None, ch)
