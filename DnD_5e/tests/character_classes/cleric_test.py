"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Bard`
:Untested parts:
    - invalid input to constructor
    - :py:meth:`send_inspiration`
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e.character_classes.cleric import Cleric
from DnD_5e.tests.test_utils import setup_logging, close_logging

loginfo = setup_logging('DnD_5e.tests.character_classes.Cleric', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Cleric(level=7):
    return Cleric(name="c0", ac=18, strength_mod=0, dexterity_mod=1, constitution_mod=1, wisdom_mod=3,
                                  intelligence_mod=2, charisma_mod=-2, level=level, logger=test_logger)


def test_constructor_should_work():
    c0 = get_Cleric()
    assert c0.get_level() == 7
    assert c0.get_hit_dice() == (7, 8)
    assert c0.get_max_hp() == 8 + 1 + (5+1)*6
    assert c0.has_proficiency("simple weapons")
    assert c0.has_proficiency("wisdom")
    assert c0.has_proficiency("charisma")
    assert c0.get_proficiency_mod() == 3
    assert c0.get_spell_ability() == "wisdom"
    assert c0.get_level_spell_slots(1) == 4
    assert c0.get_level_spell_slots(2) == 3
    assert c0.get_level_spell_slots(3) == 3
    assert c0.get_level_spell_slots(4) == 2
    assert c0.has_feature("channel divinity")
    assert c0.get_channel_divinity_slots() == 2
    assert c0.has_feature("destroy undead")
    assert c0.get_destroy_undead_cr() == 0.5


def test_copy_should_eq():
    c0 = get_Cleric()

    c1 = copy(c0)
    assert c1.get_level() == 7
    assert c1.get_hit_dice() == (7, 8)
    assert c1.get_max_hp() == 8 + 1 + (5 + 1) * 6
    assert c1.has_proficiency("simple weapons")
    assert c1.has_proficiency("wisdom")
    assert c1.has_proficiency("charisma")
    assert c1.get_proficiency_mod() == 3
    assert c1.get_level_spell_slots(1) == 4
    assert c1.get_level_spell_slots(2) == 3
    assert c1.get_level_spell_slots(3) == 3
    assert c1.get_level_spell_slots(4) == 2
    assert c1.has_feature("channel divinity")
    assert c1.get_channel_divinity_slots() == 2
    assert c1.has_feature("destroy undead")
    assert c1.get_destroy_undead_cr() == 0.5
    assert c1 is not c0

    assert c0 == c1
    assert c0.current_eq(c1)
    c1.channel_divinity(use_type="channel undead")
    assert c1.get_channel_divinity_slots() == 1
    assert c1 == c0
    assert not c1.current_eq(c0)

close_logging(None, ch)
