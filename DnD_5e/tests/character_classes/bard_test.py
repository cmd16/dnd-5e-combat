"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Bard`
:Test functions: N/A
:Untested classes/functions: None
:Untested parts:
    - invalid input to constructor
    - :py:meth:`send_inspiration`
:Test dependencies:
    - :py:func:`test_spellcaster`
    - :py:func:`test_character`
:return: None
"""
from copy import copy

from DnD_5e.character_classes.bard import Bard
from DnD_5e.tests.test_utils import setup_logging, close_logging

loginfo = setup_logging('DnD_5e.tests.character_classes.Bard', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Bard(level=13):
    return Bard(name="b0", ac=14, strength_mod=-3, dexterity_mod=3, constitution_mod=1, intelligence_mod=0,
              wisdom_mod=1, charisma_mod=4, level=level, logger=test_logger)


def test_constructor_should_work():
    b0 = get_Bard()
    assert b0.get_level() == 13
    assert b0.get_hit_dice() == (13, 8)
    assert b0.get_max_hp() == 8 + 1 + (5+1)*12
    assert b0.has_proficiency("simple weapons")
    assert b0.has_proficiency("hand crossbow")
    assert b0.has_proficiency("longsword")
    assert b0.has_proficiency("shortsword")
    assert b0.has_proficiency("rapier")
    assert b0.has_proficiency("dexterity")
    assert b0.has_proficiency("charisma")
    assert b0.get_proficiency_mod() == 5
    assert b0.get_spell_ability() == "charisma"
    assert b0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
    assert b0.has_feature("countercharm")
    assert b0.get_inspiration_dice() == (1, 10)
    assert b0.get_inspiration_slots() == 4


def test_copy_should_eq():
    b0 = get_Bard()
    b1 = copy(b0)
    assert b1.get_level() == 13
    assert b1.get_hit_dice() == (13, 8)
    assert b1.get_max_hp() == 8 + 1 + (5 + 1) * 12
    assert b1.has_proficiency("simple weapons")
    assert b1.has_proficiency("hand crossbow")
    assert b1.has_proficiency("longsword")
    assert b1.has_proficiency("shortsword")
    assert b1.has_proficiency("rapier")
    assert b1.has_proficiency("dexterity")
    assert b1.has_proficiency("charisma")
    assert b1.get_proficiency_mod() == 5
    assert b1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
    assert b1.has_feature("countercharm")
    assert b1.get_inspiration_dice() == (1, 10)
    assert b1.get_inspiration_slots() == 4
    assert b1 is not b0

    assert b0 == b1
    assert b0.current_eq(b1)
    b1.send_inspiration(b0)
    assert b1.get_inspiration_slots() == 3
    assert not b0.current_eq(b1)
    assert b0 == b1


close_logging(None, ch)
