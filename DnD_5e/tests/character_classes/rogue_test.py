"""
:Test module: :py:mod:`character_classes`
:Test class: :py:class:`Rogue`
:Untested parts:
    - invalid input to constructor
    - :py:meth:`take_stroke_of_luck` (NOT IMPLEMENTED YET)
    - attack with sneak attack
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from copy import copy

from DnD_5e.character_classes.rogue import Rogue
from DnD_5e.tests.test_utils import setup_logging, close_logging, features_should_vary_by_level

loginfo = setup_logging('DnD_5e.tests.character_classes.Rogue', '')
test_logger = loginfo.get('logger')
ch = loginfo.get('CH')


def get_Rogue(level=20):
    return Rogue(name="r0", level=level, ac=16, strength_mod=5, dexterity_mod=5, constitution_mod=3,
                                 wisdom_mod=2, intelligence_mod=0, charisma_mod=-2, logger=test_logger)


def test_rogue():
    r0 = get_Rogue()
    assert r0.get_level() == 20
    assert r0.get_hit_dice() == (20, 8)
    assert r0.get_max_hp() == 8 + r0.get_constitution() + (5 + r0.get_constitution())*19
    assert r0.has_proficiency("simple weapons")
    assert r0.has_proficiency("hand crossbow")
    assert r0.has_proficiency("longsword")
    assert r0.has_proficiency("rapier")
    assert r0.has_proficiency("shortsword")
    assert r0.has_proficiency("dexterity")
    assert r0.has_proficiency("intelligence")
    assert r0.get_proficiency_mod() == 6
    assert r0.get_sneak_attack_dice() == (10, 6)
    assert r0.has_feature("sneak attack")
    assert r0.has_proficiency("wisdom")
    assert r0.get_saving_throw("wisdom") == r0.get_wisdom() + r0.get_proficiency_mod()
    assert r0.has_feature("elusive")
    assert r0.has_feature("stroke of luck")
    assert r0.get_stroke_of_luck_slots() == 1

    assert r0.can_see("magic")
    r0.add_condition("deafened")
    assert not r0.can_see("magic")
    r0.remove_condition("deafened")

    r0.modify_adv_to_be_hit(1)
    assert r0.get_adv_to_be_hit() == 0
    r0.add_condition("incapacitated")
    assert r0.get_adv_to_be_hit() == 1
    r0.remove_condition("incapacitated")


def test_copy_should_eq():
    r0 = get_Rogue()

    r1 = copy(r0)

    assert r1.get_max_hp() == 8 + r1.get_constitution() + (5 + r1.get_constitution()) * 19

    assert r1.has_proficiency("simple weapons")
    assert r1.has_proficiency("hand crossbow")
    assert r1.has_proficiency("longsword")
    assert r1.has_proficiency("rapier")
    assert r1.has_proficiency("shortsword")
    assert r1.has_proficiency("dexterity")
    assert r1.has_proficiency("intelligence")
    assert r1.get_proficiency_mod() == 6

    assert r1.get_sneak_attack_dice() == (10, 6)
    assert r1.has_feature("sneak attack")
    assert r1.has_proficiency("wisdom")
    assert r1.get_saving_throw("wisdom") == r1.get_wisdom() + r1.get_proficiency_mod()
    assert r1.has_feature("elusive")
    assert r1.has_feature("stroke of luck")
    assert r1.get_stroke_of_luck_slots() == 1

    assert r1 is not r0
    assert r0 == r1
    assert r0.current_eq(r1)
    r1.take_stroke_of_luck()
    assert r1.get_stroke_of_luck_slots() == 0
    assert not r0.current_eq(r1)
    assert r0 == r1


def test_blindsense_should_affect_sight():
    r1 = get_Rogue()
    assert r1.has_feature('blindsense')
    assert r1.can_see("magic")
    assert r1.can_see("dark")
    r1.add_condition("deafened")
    assert not r1.can_see("magic")
    assert not r1.can_see("dark")
    r1.remove_condition("deafened")

    r1.modify_adv_to_be_hit(1)
    assert r1.get_adv_to_be_hit() == 0
    r1.add_condition("incapacitated")
    assert r1.get_adv_to_be_hit() == 1
    r1.remove_condition("incapacitated")


def test_features_should_vary_by_level():
    rogue_features = {
        'cunning action': 2,
        'uncanny dodge': 5,
        'evasion': 7,
        'reliable talent': 11,
        'blindsense': 14,
        'slippery mind': 15,
        'elusive': 18,
        'stroke of luck': 20,
    }
    features_should_vary_by_level(get_Rogue, rogue_features)

close_logging(None, ch)
