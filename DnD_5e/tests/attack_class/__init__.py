"""
:Test module: :py:mod:`attack_class`
:Test class: :py:class:`Attack`
:Untested classes/functions: run SaveOrDieAttack or HitAndSaveAttack
:Untested parts:
    - :py:meth:`shift_attack_mod` (just calls the Dice method, so should be ok)
    - :py:meth:`Combatant.add_attack` (called by `Combatant.add_weapon_attack`, so should be ok)
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_weapon`
:return: None
"""
import traceback
import warnings
from math import isclose
from copy import copy, deepcopy

import DnD_5e.attack_class.saving_throw_attacks
import DnD_5e.attack_class.spell_attacks
from DnD_5e import attack_class, weapons, combatant
from DnD_5e.character_classes.cleric import Cleric
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.attack_class', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_Combatant(name='t0'):
    return combatant.Combatant(ac=12, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                               strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                               name=name, logger=TEST_LOGGER)


def get_dagger_Attack():
    return attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                        melee_range=10, name="attack_0")


def test_invalid_attack_mod_should_raise_exception():
    try:
        attack_class.Attack(damage_dice=(1, 8), attack_mod=5.2)
        raise AssertionError("Create Attack succeeded with non-int attack mod")   # pragma: no cover
    except ValueError:
        pass


def test_invalid_damage_mod_should_raise_exception():
    try:
        attack_class.Attack(damage_dice=(1, 8), damage_mod=["fail"])
        raise AssertionError("Create Attack succeeded with non-int damage mod")   # pragma: no cover
    except ValueError:
        pass


def test_invalid_name_should_raise_exception():
    try:
        attack_class.Attack(damage_dice=(1, 8), name=1)
        raise AssertionError("Create Attack succeeded with non-string name")   # pragma: no cover
    except ValueError:
        pass


def test_invalid_weapon_damage_should_raise_exception():
    try:
        attack_class.Attack(attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                            melee_range=10, name="attack_0")
        raise AssertionError("Create Attack succeeded with no weapon or damage dice")
    except ValueError:
        pass


def test_Attack_constructor_should_work():
    attack_0 = get_dagger_Attack()
    assert attack_0.get_damage_dice().get_dice_tuple() == (1, 8)
    assert attack_0.get_attack_mod() == 5
    assert attack_0.get_damage_mod() == 2
    assert attack_0.get_damage_type() == "piercing"
    assert attack_0.get_range() == 120
    assert attack_0.get_melee_range() == 10
    assert attack_0.get_adv() == -1
    assert attack_0.get_name() == "attack_0"
    assert attack_0.get_max_hit() == 25
    assert isclose(attack_0.get_average_hit(), 7.175 + 5), attack_0.get_average_hit()
    assert attack_0.get_max_damage() == 10
    assert attack_0.get_average_damage() == 6.5, attack_0.get_average_damage()
    assert attack_0.get_min_hit() == 6
    assert isclose(attack_0.get_prob_hit(ac=6), 1)
    assert isclose(attack_0.get_dpr(ac=6), attack_0.get_average_damage())
    assert attack_0.get_dpr(ac=10) == 0.64 * attack_0.get_average_damage()


def test_Attack_copy_should_eq():
    attack_0 = get_dagger_Attack()
    attack_0_copy = copy(attack_0)
    assert attack_0_copy is not attack_0
    assert attack_0_copy == attack_0
    assert attack_0_copy.get_damage_dice().get_dice_tuple() == (1, 8)
    assert attack_0_copy.get_attack_mod() == 5
    assert attack_0_copy.get_damage_mod() == 2
    assert attack_0_copy.get_damage_type() == "piercing"
    assert attack_0_copy.get_range() == 120
    assert attack_0_copy.get_melee_range() == 10
    assert attack_0_copy.get_adv() == -1
    assert attack_0_copy.get_name() == "attack_0"
    assert attack_0_copy.get_max_hit() == 25
    assert isclose(attack_0_copy.get_average_hit(), 7.175 + 5), attack_0_copy.get_average_hit()
    assert attack_0_copy.get_max_damage() == 10
    assert attack_0_copy.get_average_damage() == 6.5, attack_0.get_average_damage()
    assert attack_0_copy.get_min_hit() == 6
    assert isclose(attack_0_copy.get_prob_hit(ac=6), 1)
    assert isclose(attack_0_copy.get_dpr(ac=6), attack_0_copy.get_average_damage())
    assert attack_0_copy.get_dpr(ac=10) == 0.64 * attack_0_copy.get_average_damage()


def test_dagger_should_have_attacks():
    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_mod=2, damage_mod=1, damage_type="piercing")

    combatant_0 = get_Combatant()

    combatant_0.add_weapon(dagger)
    assert dagger in combatant_0.get_weapons()
    assert dagger.get_owner() is combatant_0
    assert len(combatant_0.get_attacks()) == 3  # melee and two ranged

    dagger_attacks = combatant_0.get_attacks()

    dagger_attack = dagger_attacks[0]
    assert dagger_attack.get_name() == "dagger_range"
    assert dagger_attack == combatant_0.get_attack_by_name("dagger_range")
    assert dagger_attack.get_weapon() is dagger
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5  # 3 + 2
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 20
    assert dagger_attack.get_melee_range() == 0
    assert dagger_attack.get_adv() == 0

    dagger_attack = dagger_attacks[1]
    assert dagger_attack.get_name() == "dagger_range_disadvantage"
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 60
    assert dagger_attack.get_melee_range() == 0
    assert dagger_attack.get_adv() == -1

    dagger_attack = dagger_attacks[2]
    assert dagger_attack.get_name() == "dagger_melee"
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 0
    assert dagger_attack.get_melee_range() == 5
    assert dagger_attack.get_adv() == 0

    combatant_0.remove_weapon_attacks(dagger)
    assert not combatant_0.get_attacks()


def test_damage_dealt_should_be_accurate():
    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            damage_type="piercing")
    mace = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")

    combatant_0 = get_Combatant()
    combatant_0.add_weapon(dagger)
    combatant_1 = get_Combatant('combatant_1')
    combatant_1.add_weapon(mace)

    dagger_melee = combatant_0.get_attacks()[2]
    mace_melee = combatant_1.get_attacks()[0]

    damage_t0_dealt = 0
    for i in range(15):  # pylint: disable=unused-variable
        damage = combatant_0.send_attack(combatant_1, dagger_melee)
        if damage is not None:
            damage_t0_dealt += damage
        if combatant_1.has_condition("unconscious") or combatant_1.has_condition("dead"):  # pragma: no cover
            break
        combatant_1.send_attack(combatant_0, mace_melee)
        if combatant_0.has_condition("unconscious") or combatant_0.has_condition("dead"):  # pragma: no cover
            break
    assert combatant_0.get_damage_dealt() == damage_t0_dealt
    combatant_0.reset()
    assert combatant_0.get_damage_dealt() == 0


def test_MultiAttack():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`MultiAttack`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - NotImplementedError
        - :py:meth:`__eq__` and :py:meth:`current_eq`
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_attack`
        - :py:func:`test_healing_spell`
    :return: None
    """
    # TODO: clean this up
    attack_0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="attack_0")
    assert attack_0.get_adv() == -1
    assert attack_0.get_attack_dice().get_adv() == -1
    attack_1 = attack_class.Attack(damage_dice=(1, 12), attack_mod=2, damage_mod=3, damage_type="bludgeoning", melee_range=5, name="attack_1")
    attacks = [attack_0, attack_1]
    multiattack = attack_class.MultiAttack(attacks=attacks)
    assert multiattack.get_attacks() == attacks
    assert multiattack.get_attacks() is not attacks
    assert multiattack.get_attack_by_name("attack_0") is attack_0
    assert multiattack.get_attack_by_name("attack_1") is attack_1
    assert multiattack.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack.get_average_damage() == 6.5 + 9.5
    # low ac so that I know the attacks will hit
    combatant_0 = combatant.Combatant(ac=2, max_hp=100, current_hp=50, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="combatant_0", logger=TEST_LOGGER)
    combatant_1 = combatant.Combatant(ac=2, max_hp=200, current_hp=200, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="combatant_1", logger=TEST_LOGGER)
    for i in range(50):  # pylint: disable=unused-variable
        fail_num = 0
        try:
            multiattack.remove_attack("Cure Wounds")
            multiattack.make_attack(source=combatant_1, target=combatant_0)
            c0_hp = combatant_0.get_current_hp()
            multiattack.make_attack(source=combatant_1, target=[combatant_0, combatant_1])
            assert c0_hp - attack_0.get_max_damage() <= combatant_0.get_current_hp() < c0_hp
            c1_hp = combatant_1.get_current_hp()
            assert c1_hp - attack_1.get_max_damage() <= c1_hp < combatant_1.get_max_hp()
            c0_hp = combatant_0.get_current_hp()

            # make one attack a HealingSpell so that I can tell both were made appropriately
            cure_wounds = DnD_5e.attack_class.spell_attacks.HealingSpell(damage_dice=(1, 8), level=1, casting_time="1 action", components=["v", "s"],
                                                                         duration="instantaneous", name="Cure Wounds")
            multiattack.add_attack(cure_wounds)
            combatant_2 = Cleric(name="combatant_2", ac=2, strength_mod=0, dexterity_mod=1, constitution_mod=1, wisdom_mod=3,
                        intelligence_mod=2, charisma_mod=-2, level=7, spells=[cure_wounds], logger=TEST_LOGGER)
            multiattack.make_attack(source=combatant_2, target=[combatant_2, combatant_1, combatant_0])
            assert combatant_2.get_max_hp() - attack_0.get_max_damage() <= combatant_2.get_current_hp() < combatant_2.get_max_hp()
            assert c1_hp - attack_1.get_max_damage() <= combatant_1.get_current_hp() < c1_hp
            assert c0_hp < combatant_0.get_current_hp() <= c0_hp + cure_wounds.get_max_damage()
        except AssertionError:
            warnings.warn(traceback.format_exc())
            fail_num += 0
            if fail_num > 9:  # only a natural 1 will miss, so the probability of getting 10 or more failures is 0.00016
                raise ValueError("Too many failures occurred")

    multiattack.remove_attack("Cure Wounds")
    multiattack_1 = copy(multiattack)
    assert multiattack_1.get_attacks() == multiattack.get_attacks()
    assert multiattack_1.get_attacks() is not multiattack.get_attacks()
    assert multiattack_1.get_attack_by_name("attack_0") is attack_0
    assert multiattack_1.get_attack_by_name("attack_1") is attack_1
    assert multiattack_1.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack_1.get_average_damage() == 6.5 + 9.5
    assert multiattack_1 is not multiattack

    multiattack_2 = deepcopy(multiattack)
    assert multiattack_2.get_attacks() is not multiattack_1.get_attacks()  # pylint: disable=no-member
    assert multiattack_2.get_attack_by_name("attack_0") is not attack_0  # pylint: disable=no-member
    assert multiattack_2.get_attack_by_name("attack_1") is not attack_1  # pylint: disable=no-member
    assert multiattack_2.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack_2.get_average_damage() == 6.5 + 9.5


close_logging(None, CH)
