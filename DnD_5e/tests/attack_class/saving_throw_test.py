"""
:Test module: :py:mod:`attack_class.saving_throw_attacks`
:Test class: :py:class:`Attack`
:Untested classes/functions: run SaveOrDieAttack or HitAndSaveAttack
:Untested parts:
    - run SaveOrDieAttack
    - run HitAndSaveAttack
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_weapon`
    - :py:func:`test_attacks`
:return: None
"""
from copy import copy, deepcopy

import DnD_5e.attack_class.saving_throw_attacks as saving_throw_attacks  # pylint: disable=consider-using-from-import
from DnD_5e import weapons, combatant

from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.attack_class', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_SavingThrowAttack():
    # TODO: clean this up
    mace_1 = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")

    # Attack of the clones!
    combatant_1 = combatant.Combatant(ac=12, max_hp=60, current_hp=60, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='combatant_1', logger=TEST_LOGGER)
    combatant_1.add_weapon(mace_1)

    combatant_2 = copy(combatant_1)
    combatant_2.set_name("combatant_2")
    mace_2 = copy(mace_1)
    combatant_2.add_weapon(mace_2)

    sunburn_2 = saving_throw_attacks.SavingThrowAttack(damage_dice=(1, 8), dc=12, save_type="dexterity", damage_on_success=False,
                                                                           damage_mod=0, damage_type="radiant", name="Sunburn")
    assert sunburn_2.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn_2.get_dc() == 12
    assert sunburn_2.get_save_type() == "dexterity"
    assert not sunburn_2.get_damage_on_success()
    assert sunburn_2.get_attack_mod() == 0
    assert sunburn_2.get_damage_mod() == 0
    assert sunburn_2.get_damage_type() == "radiant"
    assert sunburn_2.get_name() == "Sunburn"
    assert sunburn_2.get_prob_hit(mod=1) == 0.5
    assert sunburn_2.get_dpr(target=combatant_1) == 8/20 * sunburn_2.get_average_damage()

    sunburn_success = saving_throw_attacks.SavingThrowAttack(damage_dice=(1, 8), dc=12, save_type="dexterity", damage_on_success=True,
                                                                                 damage_mod=0, damage_type="radiant", name="Sunburn")
    assert sunburn_success.get_damage_on_success()
    assert sunburn_success.get_dpr(target=combatant_1) == 8/20 * sunburn_success.get_average_damage() + \
           12/20 * (sunburn_success.get_average_damage() // 2)

    sunburn_3 = copy(sunburn_2)
    assert sunburn_3.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn_3.get_dc() == 12
    assert sunburn_3.get_save_type() == "dexterity"
    assert not sunburn_3.get_damage_on_success()
    assert sunburn_3.get_attack_mod() == 0
    assert sunburn_3.get_damage_mod() == 0
    assert sunburn_3.get_damage_type() == "radiant"
    assert sunburn_3.get_name() == "Sunburn"

    assert sunburn_2 == sunburn_3 == copy(sunburn_2)


def test_SaveOrDieAttack_constructor_should_work():
    save_or_die_attack = saving_throw_attacks.SaveOrDieAttack(damage_dice=(1, 8), damage_type="piercing", range=50,
                                                                                  melee_range=10, name="doom", save_type='constitution', dc=11,
                                                                                  threshold=51)
    assert save_or_die_attack.get_threshold() == 51
    assert save_or_die_attack.get_dc() == 11
    assert save_or_die_attack.get_save_type() == 'constitution'
    assert not save_or_die_attack.get_save_on_miss()

    save_or_die_copy = copy(save_or_die_attack)
    assert save_or_die_copy.get_threshold() == 51
    assert save_or_die_attack.get_dc() == 11
    assert save_or_die_attack.get_save_type() == 'constitution'
    assert not save_or_die_attack.get_save_on_miss()
    assert save_or_die_copy == save_or_die_attack


def test_HitAndSaveAttackShouldWork():
    hit_and_save_attack = saving_throw_attacks.HitAndSaveAttack(damage_dice=(1, 8), damage_type="piercing", range=50,
                                                                                    name="ouch", save_type='dexterity', dc=15, save_damage_dice="3d6", save_damage_mod=2,
                                                                                    save_damage_type="bludgeoning")
    assert hit_and_save_attack.get_save_damage_dice().get_dice_tuple() == (3, 6)
    assert hit_and_save_attack.get_save_damage_dice().get_modifier() == 2
    assert hit_and_save_attack.get_save_type() == "dexterity"
    assert hit_and_save_attack.get_save_damage_type() == "bludgeoning"

    hit_and_save_copy = copy(hit_and_save_attack)
    assert hit_and_save_copy.get_save_damage_dice() is hit_and_save_copy.get_save_damage_dice()
    assert hit_and_save_copy.get_save_type() == 'dexterity'
    assert hit_and_save_copy.get_save_damage_type() == 'bludgeoning'
    assert hit_and_save_attack == hit_and_save_copy

    hit_and_save_deepcopy = deepcopy(hit_and_save_attack)
    assert hit_and_save_deepcopy.get_save_damage_dice() is not hit_and_save_attack.get_save_damage_dice()
    assert hit_and_save_deepcopy.get_save_type() == 'dexterity'
    assert hit_and_save_deepcopy.get_save_damage_type() == 'bludgeoning'
    assert hit_and_save_deepcopy == hit_and_save_attack

close_logging(None, CH)
