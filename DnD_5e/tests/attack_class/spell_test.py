"""
:Test module: :py:mod:`attack_class.spell_attacks`
:Test class: :py:class:`Spell` and related subclasses
:Untested parts:
    - invalid input to constructor
    - casting spell (including at different levels) - spell is cast in other tests
    - :py:meth:`current_eq`
:Test dependencies:
    - :py:func:`test_combatant`
    - :py:func:`test_attack`
:return: None
"""

import random
from copy import copy

import DnD_5e.attack_class
from DnD_5e import combatant
from DnD_5e.combatant.spellcaster import SpellCaster

from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.attack_class', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_Spell():
    # TODO: clean this up
    spell0 = DnD_5e.attack_class.spell_attacks.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                                     school="magic", damage_dice=(1, 8), range=60, ritual=True, name="spell0")
    assert spell0.get_level() == 1
    assert spell0.get_casting_time() == 10
    assert spell0.get_components() == ["v", "s"]
    assert spell0.get_duration() == 0
    assert spell0.get_school() == "magic"
    assert spell0.get_ritual()

    spell1 = copy(spell0)
    assert spell1.get_level() == 1
    assert spell1.get_casting_time() == 10
    assert spell1.get_components() == ["v", "s"]
    assert spell1.get_duration() == 0
    assert spell1.get_school() == "magic"
    assert spell1.get_ritual()

    assert spell0 == spell1


def test_SavingThrowSpell():
    # TODO: clean this up
    sunburn0 = DnD_5e.attack_class.spell_attacks.SavingThrowSpell(damage_dice=(1, 8), level=0, school="evocation",
                                                                  casting_time="1 action", components=["v", "s"], duration="instantaneous",
                                                                  dc=12, save_type="dexterity", damage_on_success=False,
                                                                  damage_type="radiant", name="Sunburn")
    assert sunburn0.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn0.get_level() == 0
    assert sunburn0.get_school() == "evocation"
    assert sunburn0.get_casting_time() == "1 action"
    assert sunburn0.get_components() == ["v", "s"]
    assert sunburn0.get_duration() == 0
    assert sunburn0.get_dc() == 12
    assert sunburn0.get_save_type() == "dexterity"
    assert not sunburn0.get_damage_on_success()
    assert sunburn0.get_attack_mod() == 0
    assert sunburn0.get_damage_mod() == 0
    assert sunburn0.get_damage_type() == "radiant"
    assert sunburn0.get_name() == "Sunburn"

    spellcaster_0 = SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                                                             strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                                                             name="spellcaster_0", spell_ability="wisdom", spell_slots={1: 3, 2: 2, 3: 1}, proficiency_mod=2,
                                                             spells=[sunburn0], logger=TEST_LOGGER)
    spellcaster_1 = copy(spellcaster_0)
    spellcaster_1.set_name("spellcaster_1")
    sunburn1 = spellcaster_1.get_spells()[0]
    for i in range(30):  # pylint: disable=unused-variable
        spellcaster_0.send_attack(spellcaster_1, sunburn0)
        if spellcaster_1.has_condition("unconscious") or spellcaster_1.has_condition("dead"):  # pragma: no cover
            break
        spellcaster_1.send_attack(spellcaster_0, sunburn1)
        if spellcaster_0.has_condition("unconscious") or spellcaster_0.has_condition("dead"):  # pragma: no cover
            break


def test_HealingSpell():
    # TODO: clean this up
    cure_wounds = DnD_5e.attack_class.spell_attacks.HealingSpell(damage_dice=(1, 8), level=1, casting_time="1 action",
                                                                 components=["v", "s"],
                                                                 duration="instantaneous", name="Cure Wounds")
    cure_wounds_copy = copy(cure_wounds)
    assert cure_wounds_copy == cure_wounds

    combatant_0 = combatant.Combatant(ac=12, max_hp=70, current_hp=5, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='combatant_0', logger=TEST_LOGGER)
    combatant_1 = combatant.Combatant(ac=12, max_hp=70, current_hp=0, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='combatant_1', logger=TEST_LOGGER)
    spellcaster_0 = SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                                                             strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                                                             name="spellcaster_0", spell_ability="wisdom", spell_slots={1: 30}, proficiency_mod=2,
                                                             spells=[cure_wounds], logger=TEST_LOGGER)
    targets = [combatant_0, combatant_1]
    for i in range(30):  # pylint: disable=unused-variable
        target = random.choice(targets)
        spellcaster_0.send_attack(target=target, attack=cure_wounds)
        if combatant_0.is_hp_max() or combatant_1.is_hp_max():
            break

close_logging(None, CH)
