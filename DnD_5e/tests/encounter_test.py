"""
:Test module: :py:mod:`encounter`
:Test class: :py:class:`Encounter`
:Test dependencies:
    - :py:func:`test_tactics`
    - :py:func:`test_combatant`
    - :py:func:`test_team`
:return:
"""
from copy import copy, deepcopy

from DnD_5e import armory, encounter
from DnD_5e.combatant import Combatant
from DnD_5e.team import Team
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.encounter', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_combatants_for_team_C():
    combatant_1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                            name="combatant_1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_2 = deepcopy(combatant_1)
    combatant_2.set_name("combatant_2")
    return [combatant_1, combatant_2]


def get_combatants_for_team_D():
    combatant_d1 = Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="combatant_d1", weapons=[armory.Greatclub()], logger=TEST_LOGGER)
    combatant_d2 = deepcopy(combatant_d1)
    combatant_d2.set_name("combatant_d2")
    return [combatant_d1, combatant_d2]


def test_invalid_Encounter_with_invalid_teams_should_raise_exception():
    try:
        encounter.Encounter(name="e", logger=TEST_LOGGER)
        raise AssertionError("Allowed Encounter with no teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams=[], logger=TEST_LOGGER)
        raise AssertionError("Allowed Encounter with empty list of teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams=1, logger=TEST_LOGGER)
        raise AssertionError("Allowed Encounter with non-iterable teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams="cat", logger=TEST_LOGGER)
        raise AssertionError("Allowed Encounter with non-Teams")
    except ValueError:
        pass


def test_Encounter_constructor():
    combatants_c = get_combatants_for_team_C()
    [combatant_c1, combatant_c2] = combatants_c
    team_c = Team(combatant_list=combatants_c, name="team_c")
    combatants_d = get_combatants_for_team_D()
    [combatant_d1, combatant_d2] = combatants_d
    team_d = Team(combatant_list=combatants_d, name="team_d")

    encounter_1 = encounter.Encounter(name="encounter_1", teams=[team_c, team_d], logger=TEST_LOGGER)
    assert encounter_1.get_name() == "encounter_1"
    assert encounter_1.get_teams() == [team_c, team_d]
    assert encounter_1.get_combatants() == [combatant_c1, combatant_c2, combatant_d1, combatant_d2]
    assert encounter_1.get_round() == 1
    assert encounter_1.get_encounter_statnames() == ["rounds"]


def test_copy_should_eq():
    combatants_c = get_combatants_for_team_C()
    [combatant_c1, _] = combatants_c
    team_c = Team(combatant_list=combatants_c, name="team_c")
    combatants_d = get_combatants_for_team_D()
    [_, combatant_d2] = combatants_d
    team_d = Team(combatant_list=combatants_d, name="team_d")
    encounter_1 = encounter.Encounter(name="encounter_1", teams=[team_c, team_d], logger=TEST_LOGGER)

    e1_copy = copy(encounter_1)
    assert combatant_d2 == e1_copy.get_teams()[1].get_combatants()[1]
    assert combatant_c1 == e1_copy.get_teams()[0].get_combatants()[0]
    # TODO: re-enable when moving to built in equals method
    # assert e1_copy.get_combatants() == [combatant_c1, combatant_c2, combatant_d1, combatant_d2]
    assert encounter_1 == e1_copy
    assert e1_copy is not encounter_1


def test_deep_copy_should_eq():
    combatants_c = get_combatants_for_team_C()
    [combatant_c1, combatant_c2] = combatants_c
    team_c = Team(combatant_list=combatants_c, name="team_c")
    combatants_d = get_combatants_for_team_D()
    team_d = Team(combatant_list=combatants_d, name="team_d")
    encounter_1 = encounter.Encounter(name="encounter_1", teams=[team_c, team_d], logger=TEST_LOGGER)

    encounter_1_deep_copy = deepcopy(encounter_1)
    assert encounter_1_deep_copy.get_teams()[0].get_combatants()[0] is not combatant_c1  # copied Combatants
    assert encounter_1_deep_copy.get_teams()[0].get_combatants()[1] == combatant_c2
    assert encounter_1 == encounter_1_deep_copy
    assert encounter_1_deep_copy is not encounter_1


def test_run_encounter():
    # This is just to test output

    combatants_c = get_combatants_for_team_C()
    team_c = Team(combatant_list=combatants_c, name="team_c")
    combatants_d = get_combatants_for_team_D()
    team_d = Team(combatant_list=combatants_d, name="team_d")
    encounter_1 = encounter.Encounter(name="encounter_1", teams=[team_c, team_d], logger=TEST_LOGGER)

    for _ in range(0, 5):  # pylint: disable=unused-variable
        encounter_1.roll_initiative()

    encounter_1.run()
    assert encounter_1.get_round() != 0
    stats = encounter_1.get_stats()
    TEST_LOGGER.info("team {team}\ncombatants {combatants}\nrounds {rounds}".format_map(stats))
    encounter_1.reset()
    assert encounter_1.get_round() == 1
    for comb in encounter_1.get_combatants():
        assert comb.get_current_hp() == comb.get_max_hp()
        assert comb.get_conditions() == []


def test_reset():
    combatants_c = get_combatants_for_team_C()
    [combatant_c1, _] = combatants_c
    team_c = Team(combatant_list=combatants_c, name="team_c")
    combatants_d = get_combatants_for_team_D()
    team_d = Team(combatant_list=combatants_d, name="team_d")

    encounter_1 = encounter.Encounter(name="encounter_1", teams=[team_c, team_d], logger=TEST_LOGGER)
    original_combatants = encounter_1.get_combatants()
    original_teams = encounter_1.get_teams()
    encounter_1.run()
    stats = encounter_1.get_stats()
    encounter_1.reset()
    assert encounter_1.get_round() == 1
    assert encounter_1.get_stats() != stats
    new_teams = encounter_1.get_teams()
    assert new_teams == original_teams
    assert new_teams[0] is not team_c
    new_combatants = encounter_1.get_combatants()
    assert new_combatants == original_combatants
    assert new_combatants[0] is not combatant_c1
    for i, comb in enumerate(new_combatants):
        if not original_combatants[i].current_eq(comb):
            break
    else:
        raise AssertionError("At least one of these combatants should be different (e.g., in hp) from the original")

close_logging(None, CH)
