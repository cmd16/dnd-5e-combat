"""
:Test module: :py:mod:`utility_methods_dnd`
:Untested classes/functions:
    - :py:func:`roll_dice`
    - :py:func:`ability_from_abbreviation` - simple
:Test dependencies: None
:return: None
"""

from DnD_5e.tests.test_utils import setup_logging, close_logging
from DnD_5e.utility_methods_dnd import ability_to_mod, cr_to_xp, time_to_rounds, calc_advantage, \
    proficiency_bonus_per_level, proficency_bonus_by_cr

LOG_INFO = setup_logging('DnD_5e.tests.utility_methods', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_ability_to_mod():
    assert ability_to_mod(10) == 0
    assert ability_to_mod(11) == 0
    assert ability_to_mod(14) == 2
    assert ability_to_mod(8) == -1


def test_invalid_ability_to_mod_should_raise_exception():
    try:
        ability_to_mod(-5)
        raise AssertionError("Allowed low ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        ability_to_mod(70)
        raise AssertionError("Allowed high ability score")   # pragma: no cover
    except ValueError:
        pass


def test_cr_to_xp():
    assert cr_to_xp(0) == 10
    assert cr_to_xp(1/8) == 25
    assert cr_to_xp(1/4) == 50
    assert cr_to_xp(2) == 450


def test_invalid_cr_to_xp_should_raise_exception():
    try:
        cr_to_xp(34)
        raise AssertionError("Allowed invalid challenge rating")
    except ValueError:
        pass


def test_time_to_rounds():
    assert time_to_rounds("1 round") == 1
    assert time_to_rounds("10 rounds") == 10
    assert time_to_rounds("1 minute") == 10
    assert time_to_rounds("2 minutes") == 20
    assert time_to_rounds("1 hour") == 600
    assert time_to_rounds("2 hours") == 1200
    assert time_to_rounds((2, "minutes")) == 20
    assert time_to_rounds([3, "round"]) == 3
    assert time_to_rounds("instantaneous") == 0


def test_invalid_time_to_rounds_should_raise_exception():
    try:
        time_to_rounds("-1 hour")
        raise AssertionError("Allowed non-positive time")
    except ValueError:
        pass

    try:
        time_to_rounds("3 days")
        raise AssertionError("Allowed other unit of time")
    except ValueError:
        pass

    try:
        time_to_rounds("2")
        raise AssertionError("Allowed incomplete input")
    except ValueError:
        pass

    try:
        time_to_rounds(82)
        raise AssertionError("Allowed invalid input")
    except ValueError:
        pass


def test_calc_advantage():
    assert calc_advantage([0]) == 0
    assert calc_advantage([1, -1]) == 0
    assert calc_advantage([3, -2, 1]) == 1
    assert calc_advantage([-1, -1, 1]) == -1


def test_proficiency_bonus_per_level():
    assert proficiency_bonus_per_level(4) == 2
    assert proficiency_bonus_per_level(6) == 3
    assert proficiency_bonus_per_level(13) == 5


def test_invalid_proficiency_bonus_per_level():
    try:
        proficiency_bonus_per_level(0)
        raise AssertionError("Allowed level too low")
    except ValueError:
        pass

    try:
        proficiency_bonus_per_level(25)
        raise AssertionError("Allowed level too high")
    except ValueError:
        pass

    try:
        proficiency_bonus_per_level("nope")
        raise AssertionError("Allowed non-numerical level")
    except ValueError:
        pass


def test_proficiency_bonus_by_cr():
    assert proficency_bonus_by_cr(4) == 2
    assert proficency_bonus_by_cr(7) == 3
    assert proficency_bonus_by_cr(12) == 4
    assert proficency_bonus_by_cr(30) == 9


def test_invalid_proficiency_bonus_by_cr_should_rais_exception():
    try:
        proficency_bonus_by_cr(-2)
        raise AssertionError("Allowed negative cr")
    except ValueError:
        pass

    try:
        proficency_bonus_by_cr("hi")
        raise AssertionError("Allowed non-numerical cr")
    except ValueError:
        pass

close_logging(None, CH)
