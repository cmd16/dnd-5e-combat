"""
:Test module: :py:mod:`armory`
:Test class: :py:class:`Weapon` and subclasses
:Test functions: :py:func:`is_monk_weapon`
:Untested parts: not all weapons/classes are tested, just a sample
:Test dependencies: :py:func:`test_combatant`
:return: None
"""
from DnD_5e import armory, weapons, combatant
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.armory', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def get_Combatant_SimpleWeapon_and_WarPick():
    return combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                               strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                               proficiencies=["dexterity", "charisma", "simple weapons", "warpick"], proficiency_mod=2,
                               name='combatant_0',
                               logger=TEST_LOGGER)


def test_Dagger_should_haveCorrectProperties():
    dagger = armory.Dagger()
    assert isinstance(dagger, weapons.Weapon)
    assert isinstance(dagger, armory.SimpleWeapon)
    assert isinstance(dagger, armory.MeleeWeapon)
    assert dagger.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger.get_damage_type() == "piercing"
    assert dagger.has_prop("finesse")
    assert dagger.has_prop("light")
    assert dagger.get_melee_range() == 5
    assert dagger.get_range() == (20, 60)
    assert armory.is_monk_weapon(dagger)

    combatant_0 = get_Combatant_SimpleWeapon_and_WarPick()
    assert combatant_0.has_weapon_proficiency(dagger)


def test_WarPick_should_have_proficiency():
    combatant_0 = get_Combatant_SimpleWeapon_and_WarPick()

    war_pick = armory.WarPick()  # choose this one to make sure multi_word names are handled correctly for proficiency
    assert combatant_0.has_weapon_proficiency(war_pick)


def test_MartialWeapon_shouldNot_be_monk_weapon():
    war_pick = armory.WarPick()
    assert isinstance(war_pick, armory.MartialWeapon)
    assert isinstance(war_pick, armory.MeleeWeapon)
    assert not armory.is_monk_weapon(war_pick)


def test_RangedWeapon_shouldNot_be_monk_weapon():
    shortbow = armory.Shortbow()  # ranged simple
    assert isinstance(shortbow, armory.RangedWeapon)
    assert not armory.is_monk_weapon(shortbow)


def test_SimpleMeleeWeapon_should_be_monk_weapon():
    shortsword = armory.Shortsword()
    assert armory.is_monk_weapon(shortsword)

close_logging(None, CH)
