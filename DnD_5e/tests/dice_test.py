"""
:Test module: :py:mod:`dice`
:Test class: :py:class:`Dice` and subclasses
:Untested parts: rolling the dice
:Test dependencies: None
:return: None
"""
from copy import deepcopy
from math import isclose

from DnD_5e import dice
from DnD_5e.tests.test_utils import setup_logging, close_logging

LOG_INFO = setup_logging('DnD_5e.tests.dice', '')
TEST_LOGGER = LOG_INFO.get('logger')
CH = LOG_INFO.get('CH')


def test_invalid_dice_should_raise_exception():
    try:
        dice.Dice(dice_tuple=5.2)
        raise AssertionError("Allowed non-tuple dice_tuple")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_num=3.7)
        raise AssertionError("Allowed non-int dice_num")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_type=4.1)
        raise AssertionError("Allowed non-int dice_type")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_tuple=(1, 2, 3))
        raise AssertionError("Allowed dice_tuple of len != 2")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_tuple=("a", "b"))
        raise AssertionError("Allowed dice_tuple of non-integers")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="xdz")
        raise AssertionError("Allowed dice with non-integers in str_val")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="1d4d20")
        raise AssertionError("Allowed dice with multiple d in str_val")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="1d4", modifier="nope")
        raise AssertionError("Allowed dice with invalid modifier")  # pragma: no cover
    except ValueError:
        pass


def test_tuple_constructor_should_work():
    two_d3 = dice.Dice(dice_tuple=(2, 3))
    assert two_d3.get_dice_tuple() == (2, 3)
    assert two_d3.get_dice_num() == 2
    assert two_d3.get_dice_type() == 3
    assert two_d3.get_modifier() == 0
    assert two_d3.get_adv() == 0
    assert two_d3.get_critable() == False  # pylint: disable=singleton-comparison
    assert two_d3.get_max_value() == 6
    assert two_d3.get_min_value() == 2
    assert two_d3.get_average_value() == 4
    assert two_d3.get_max_value() == 6
    assert two_d3.get_prob(2) == 1/9
    assert two_d3.get_prob(3) == 2/9
    assert two_d3.get_prob(4) == 3/9
    assert two_d3.get_prob(5) == 2/9
    assert two_d3.get_prob(6) == 1/9


def test_str_constructor_should_work():
    three_d4 = dice.Dice(str_val="3d4")
    assert three_d4.get_dice_tuple() == (3, 4)
    assert three_d4.get_dice_num() == 3
    assert three_d4.get_dice_type() == 4
    assert three_d4.get_modifier() == 0
    assert three_d4.get_adv() == 0
    assert three_d4.get_critable() == False  # pylint: disable=singleton-comparison
    assert three_d4.get_max_value() == 12
    assert three_d4.get_min_value() == 3
    assert three_d4.get_average_value() == 7.5
    assert three_d4.get_max_value() == 12
    assert three_d4.get_prob(3) == 1/(4**3)
    assert three_d4.get_prob(4) == 3/(4**3)
    assert three_d4.get_prob(5) == 6/(4**3)


def test_keyword_constructor_should_work():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    assert one_d8_plus_3.get_dice_tuple() == (1, 8)
    assert one_d8_plus_3.get_modifier() == 3
    assert one_d8_plus_3.get_adv() == 0
    assert not one_d8_plus_3.get_critable()
    assert one_d8_plus_3.get_max_value() == 11
    assert one_d8_plus_3.get_min_value() == 4
    assert one_d8_plus_3.get_average_value() == 7.5
    assert one_d8_plus_3.get_prob(1) == 0
    assert one_d8_plus_3.get_prob(11) == 1/8
    assert one_d8_plus_3.get_prob(10) == 1/8
    assert one_d8_plus_3.get_prob(4) == 1/8


def test_copy_should_eq():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    one_d8_plus_3_copy = deepcopy(one_d8_plus_3)
    assert one_d8_plus_3_copy.get_dice_tuple() == (1, 8)
    assert one_d8_plus_3_copy.get_modifier() == 3
    assert one_d8_plus_3_copy.get_adv() == 0
    assert not one_d8_plus_3_copy.get_critable()
    assert one_d8_plus_3_copy == one_d8_plus_3


def test_invalid_modifier_should_raise_exception():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    try:
        one_d8_plus_3.set_modifier("why")
        raise AssertionError("Allowed set_modifier with non-int modifier")
    except ValueError:
        pass


def test_set_modifier_and_set_adv_and_shift_adv_should_work():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    one_d8_plus_3_copy = deepcopy(one_d8_plus_3)
    one_d8_plus_3.set_modifier(-1)
    assert one_d8_plus_3.get_modifier() == -1
    assert one_d8_plus_3.get_max_value() == 7
    assert one_d8_plus_3.get_min_value() == 0
    assert one_d8_plus_3.get_average_value() == 3.5
    assert one_d8_plus_3.get_prob(0) == 1/8
    assert one_d8_plus_3.get_prob(8) == 0
    assert one_d8_plus_3 != one_d8_plus_3_copy
    one_d8_plus_3.set_modifier(3)
    assert one_d8_plus_3 == one_d8_plus_3_copy

    one_d8_plus_3.set_adv(2)
    assert one_d8_plus_3.get_adv() == 2
    assert one_d8_plus_3 != one_d8_plus_3_copy
    one_d8_plus_3.shift_adv(-3)
    assert one_d8_plus_3.get_adv() == -1


def test_invalid_set_adv_should_raise_exception():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    try:
        one_d8_plus_3.set_adv("no")
        raise AssertionError("Allowed set_adv with non-int adv")
    except ValueError:
        pass


def test_invalid_shift_adv_should_rasie_exception():
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    try:
        one_d8_plus_3.shift_adv([])
        raise AssertionError("Allowed shift_adv with non-int adv")
    except ValueError:
        pass


def test_dice_num_type_constructor_should_work():
    four_d10 = dice.Dice(dice_num=4, dice_type=10)
    assert four_d10.get_dice_tuple() == (4, 10)
    assert four_d10.get_adv() == 0
    assert four_d10.get_max_value() == 40
    assert four_d10.get_min_value() == 4
    assert four_d10.get_average_value() == 22


def test_critable_dice_should_be_critable():
    d20_sample = dice.Dice(str_val="1d20", critable=True)
    assert d20_sample.get_dice_tuple() == (1, 20)
    assert d20_sample.get_adv() == 0
    assert d20_sample.get_critable()
    assert d20_sample.get_max_value() == 20
    assert d20_sample.get_min_value() == 1
    assert d20_sample.get_average_value() == 10.5
    assert isclose(d20_sample.get_prob(10, kind="ge"), 11/20), d20_sample.get_prob(10, kind="ge")
    assert isclose(d20_sample.get_prob(5, kind="le"), 5/20)
    assert isclose(d20_sample.get_prob(6, kind="gt"), 14/20)
    assert isclose(d20_sample.get_prob(4, kind="lt"), 3/20)


def test_adv_dice_should_have_increased_probabilities():
    d20_adv = dice.Dice(str_val="1d20", adv=1, critable=True)
    assert d20_adv.get_adv() == 1
    assert isclose(d20_adv.get_prob(5), 0.0225)  # from https://anydice.com/program/1203
    assert isclose(d20_adv.get_prob(15), 0.0725)  # from https://anydice.com/program/1203
    assert d20_adv.get_average_value() == 13.825, d20_adv.get_average_value()


def test_disadv_dice_should_have_decreased_probabilities():
    d20_disadv = dice.Dice(str_val="1d20", adv=-1)
    assert isclose(d20_disadv.get_prob(3), 0.0875)  # from https://anydice.com/program/1203
    assert isclose(d20_disadv.get_prob(17), 0.0175)  # from https://anydice.com/program/1203
    assert isclose(d20_disadv.get_average_value(), 7.175)
    assert isclose(d20_disadv.get_prob(5), 0.0775), d20_disadv.get_prob(5)
    assert isclose(d20_disadv.get_prob(10), 0.0525)
    assert isclose(d20_disadv.get_prob(15), 0.0275)
    d20_disadv_copy = deepcopy(d20_disadv)
    assert d20_disadv_copy.get_adv() == -1
    d20_disadv.set_modifier(2)
    assert isclose(d20_disadv.get_prob(5), 0.0875), d20_disadv.get_prob(5)
    assert isclose(d20_disadv.get_prob(10), 0.0625)
    assert isclose(d20_disadv.get_prob(15), 0.0375)


def test_invalid_shift_modifier_should_raise_exception():
    d20_sample = dice.Dice(str_val="1d20", critable=True)
    try:
        d20_sample.shift_modifier(9.0012)
        raise AssertionError("Allowed shift modifier with non-int modifier")
    except ValueError:
        pass


def test_shift_modifier_should_change_probability():
    d20_sample = dice.Dice(str_val="1d20", critable=True)
    d20_sample.shift_modifier(-3)
    assert d20_sample.get_modifier() == -3
    assert d20_sample.get_prob(20) == 0
    assert d20_sample.get_prob(-2) == 1/20
    d20_sample.shift_modifier(5)
    assert d20_sample.get_modifier() == 2


def test_invalid_dice_obj_should_raise_exception():
    try:
        dice.DamageDice(dice_obj="nah")
        raise AssertionError("Allowed DamageDice with invalid dice_obj")
    except ValueError:
        pass


def test_DamageDice():
    # TODO clean this up
    four_d10_lightning = dice.DamageDice(dice_num=4, dice_type=10, damage_type="lightning")
    assert four_d10_lightning.get_dice_tuple() == (4, 10)
    assert four_d10_lightning.get_damage_type() == "lightning"
    assert four_d10_lightning.has_damage_type("lightning")

    one_d4_plus_2_fire = dice.DamageDice(dice_tuple=(1, 4), modifier=2, damage_type="fire")
    assert one_d4_plus_2_fire.get_dice_tuple() == (1, 4)
    assert one_d4_plus_2_fire.get_modifier() == 2
    assert one_d4_plus_2_fire.get_damage_type() == "fire"
    assert one_d4_plus_2_fire.has_damage_type("fire")

    one_d4_plus_2_fire_copy = deepcopy(one_d4_plus_2_fire)
    assert one_d4_plus_2_fire_copy.get_damage_type() == "fire"
    assert one_d4_plus_2_fire_copy == one_d4_plus_2_fire
    assert one_d4_plus_2_fire_copy is not one_d4_plus_2_fire


def test_invalid_dice_list_should_raise_exception():
    try:
        dice.DiceBag(dice_list="nah")
        raise AssertionError("Allowed DiceBag with invalid dice list")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.DiceBag(dice_list=[2])
        raise AssertionError("Allowed DiceBag with dice list that doesn't contain dice")  # pragma: no cover
    except ValueError:
        pass

    d20_sample = dice.Dice(str_val="1d20", critable=True)
    try:
        dice.DiceBag(dice_list=[d20_sample])
        raise AssertionError("Allowed DiceBag with short dice list")  # pragma: no cover
    except ValueError:
        pass


def test_DiceBag():
    # TODO: clean this up
    two_d3 = dice.Dice(dice_tuple=(2, 3))
    three_d4 = dice.Dice(dice_tuple=(3, 4))
    one_d6_plus_2 = dice.Dice(dice_tuple=(1, 6), modifier=2)

    dice_list = [two_d3, three_d4, one_d6_plus_2]
    dice_bag_1 = dice.DiceBag(dice_list=dice_list)
    assert dice_bag_1.get_dice_list() == dice_list  # pylint: disable=no-member
    assert dice_bag_1.get_dice_list() is not dice_list  # pylint: disable=no-member
    assert dice_bag_1.get_dice_list()[0] is two_d3 and dice_bag_1.get_dice_list()[1] is three_d4 \
           and dice_bag_1.get_dice_list()[2] is one_d6_plus_2  # pylint: disable=no-member
    assert dice_bag_1.get_max_value() == 26  # 6 + 12 + 8
    assert dice_bag_1.get_min_value() == 8  # 2 + 3 + 3
    assert dice_bag_1.get_average_value() == 17  # 4 + 7.5 + 5.5
    assert isclose(dice_bag_1.get_prob(2+3+3), 1/((3**2)*(4**3)*6)), dice_bag_1.get_prob(2+3+3)  # rolling a 1 on every die

    dice_bag_1_deepcopy = deepcopy(dice_bag_1)
    assert dice_bag_1 == dice_bag_1_deepcopy
    assert dice_bag_1 is not dice_bag_1_deepcopy
    assert dice_bag_1_deepcopy.get_max_value() == 26  # 6 + 12 + 8
    assert dice_bag_1_deepcopy.get_min_value() == 8  # 2 + 3 + 3
    assert dice_bag_1_deepcopy.get_average_value() == 17  # 4 + 7.5 + 5.5
    assert dice_bag_1_deepcopy.get_dice_list()[0] is not two_d3  # pylint: disable=no-member
    assert dice_bag_1_deepcopy.get_dice_list()[1] is not three_d4  # pylint: disable=no-member
    assert dice_bag_1_deepcopy.get_dice_list()[2] is not one_d6_plus_2  # pylint: disable=no-member

    dice_bag_1.set_modifier(1)
    assert two_d3.get_modifier() == 1
    assert three_d4.get_modifier() == 1
    assert one_d6_plus_2.get_modifier() == 1

    one_d6_plus_2.set_modifier(2)
    dice_bag_1.shift_modifier(-2)
    assert two_d3.get_modifier() == -1
    assert three_d4.get_modifier() == -1
    assert one_d6_plus_2.get_modifier() == 0

    dice_bag_1.set_adv(1)
    for die in dice_bag_1.get_dice_list():
        assert die.get_adv() == 1
    dice_bag_1.shift_adv(2)
    for die in dice_bag_1.get_dice_list():
        assert die.get_adv() == 3


def test_invalid_method_DiceBag_should_raise_exception():
    two_d3 = dice.Dice(dice_tuple=(2, 3))
    three_d4 = dice.Dice(dice_tuple=(3, 4))

    dice_list = [two_d3, three_d4]
    dice_bag_1 = dice.DiceBag(dice_list=dice_list)

    try:
        dice_bag_1.get_dice_tuple()
        raise AssertionError("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_dice_num()
        raise AssertionError("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_dice_type()
        raise AssertionError("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_modifier()
        raise AssertionError("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_critable()
        raise AssertionError("Allowed non-implemented method access")
    except NotImplementedError:
        pass


def test_DiceBag_should_have_prob():
    dice_bag_2 = dice.DiceBag(dice_list=[dice.Dice(str_val="1d4"), dice.Dice(str_val="1d6")])
    assert dice_bag_2.get_prob(3) == 2/(4*6)


def test_DamageDiceBag_should_not_allow_non_damage_dice():
    four_d10_lightning = dice.DamageDice(str_val="4d10", damage_type="lightning")
    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    try:
        dice.DamageDiceBag(dice_list=[four_d10_lightning, one_d8_plus_3])
        raise AssertionError("Allowed DamageDiceBag with non-damage dice")
    except ValueError:
        pass

    one_d4_plus_2_fire = dice.DamageDice(dice_type=4, modifier=2, damage_type="fire")
    damage_dice_bag = dice.DamageDiceBag(dice_list=[four_d10_lightning, one_d4_plus_2_fire])
    try:
        damage_dice_bag.add_dice(one_d8_plus_3)
        raise AssertionError("Allowed adding non-damage dice to DamageDiceBag")
    except ValueError:
        pass


def test_DamageDiceBag_constructor_should_work():
    four_d10_lightning = dice.DamageDice(str_val="4d10", damage_type="lightning")
    one_d4_plus_2_fire = dice.DamageDice(dice_type=4, modifier=2, damage_type="fire")
    dice_list = [four_d10_lightning, one_d4_plus_2_fire]
    damage_dice_bag = dice.DamageDiceBag(dice_list=dice_list)
    assert damage_dice_bag.get_dice_list() == dice_list
    assert damage_dice_bag.get_dice_list() is not dice_list
    assert damage_dice_bag.get_damage_type_list() == ["lightning", "fire"]
    assert damage_dice_bag.has_damage_type("lightning")
    assert damage_dice_bag.has_damage_type("fire")

    four_d10_lightning_copy = deepcopy(four_d10_lightning)
    damage_dice_bag.add_dice(four_d10_lightning_copy)  # in normal use cases this is redundant, but it works for this test
    assert damage_dice_bag.get_damage_type_list() == ["lightning", "fire", "lightning"]
    assert damage_dice_bag.get_damage_type_set() == {"lightning", "fire"}
    assert damage_dice_bag.has_damage_type("lightning")
    assert damage_dice_bag.has_damage_type("fire")


def test_invalid_method_DamageDice_should_raise_exception():
    four_d10_lightning = dice.DamageDice(str_val="4d10", damage_type="lightning")
    one_d4_plus_2_fire = dice.DamageDice(dice_type=4, modifier=2, damage_type="fire")
    dice_list = [four_d10_lightning, one_d4_plus_2_fire]
    damage_dice_bag = dice.DamageDiceBag(dice_list=dice_list)
    try:
        damage_dice_bag.get_damage_type()
        raise AssertionError("Allowed get_damage_type for DamageDiceBag")
    except NotImplementedError:
        pass


def test_NullDice_should_be_0():
    null_dice = dice.NullDice(modifier=1)
    assert null_dice.get_dice_tuple() == (0, 0)
    assert null_dice.get_prob(0) == 1
    assert null_dice.get_modifier() == 0
    null_damage_dice = dice.NullDamageDice()
    assert null_damage_dice.get_damage_type() == ""
    assert null_damage_dice.get_dice_tuple() == (0, 0)
    null_lightning_dice = dice.NullDamageDice(damage_type="lightning")
    assert null_lightning_dice.get_dice_tuple() == (0, 0)
    assert null_lightning_dice.get_damage_type() == "lightning"

close_logging(None, CH)
