FROM continuumio/miniconda3:4.10.3p0-alpine

RUN apk add make

RUN apk add fontconfig

RUN apk add --update --no-cache graphviz font-bitstream-type1 ghostscript-fonts

ADD . /myapp

WORKDIR /myapp

RUN pip install --trusted-host pypi.python.org -r requirements.txt
