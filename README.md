# D&D 5e Combat Simulator (WIP)

*Script for simulating combat using D&D 5e mechanics and statistics.*

See (https://cmd16.gitlab.io/dnd-5e-combat/) for full documentation.

## Purposes/uses
Allow D&D 5e DMs and players to...

- See how difficult/powerful/etc. specific encounters, creatures, characters, weapons, etc. are and adjust them accordingly
- See how certain tactics are played out and how effective those tactics are in different situations
- Create, test, and share homebrew content

## Getting Started

*These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See Deployment for notes on how to deploy the project on a live system.*

### Prerequisites

*What things you need to install the software and how to install them*

See [requirements.txt](./requirements.txt)

### Installing

*A step by step series of examples that tell you how to get a development env running*

Clone the repository

```
git clone https://gitlab.com/cmd16/dnd-5e-combat.git
```

To update the code to the latest version, use git pull

```
git pull
```

## Running the tests

*Note: the pipeline is set up such that tests will be run every time you push commits to the master branch.
However, you may find it useful to run tests manually before committing and pushing your changes.*

To test the code, run [nosetests](http://nose.readthedocs.io/en/latest/) on package DnD_5e,
or add function calls to the functions you want to test and then run tests.py, whichever you prefer

Note: some tests may throw warnings. Several of the tests are designed to throw warnings, so this is good.
Any actual problems will cause Exceptions to be raised.

##### Run all tests (do this after each clone, pull, or significant code change and before pushing your code)
```nosetests --with-html -v```
* The -v flag makes the output verbose, which helps you see a breakdown of each test and its success/failure.
* The --with-html flag provides an html report detailing the success/failure of each test
* To run this without nosetests, uncomment the definition of and call to test_all, then run tests/__init__.py

##### Test a specific method (do this when changing a specific class or method)
Find the name of the test method, then use it. For example,
```nosetests -v tests/__init__.py:test_paladin```

To run this without nosetests, find the method, then call it and run [DnD_5e/tests/\_\_init\_\_.py]. E.g., test_paladin()

##### Test coverage (do this before pushing your code)

```nosetests --with-html --with-coverage --cover-package=DnD_5e  --cover-erase --cover-html --cover-tests -v```

To see the results, look at the directory named <em>cover</em>. 
The recommended way to look at the results is to open <em>index.html</em> in your web browser of choice and proceed from there.

Ideally, all the code you wrote is either covered or excluded - 
put ```# pragma: no cover``` at the end of any statement you think shouldn't be tested, 
along with an explanation of why you're excluding it

If you wrote tests to cover code that wasn't previously covered, check the reports to make sure the code in question is now covered.

##### Interpreting test results

* **Nosetests** - Nosetests runs every test (it finds them based on the name of the function) and tells you the results of each one.
If a test fails, you will see the stack trace leading up to the failure. 
If any test fails, GitLab CI will mark the job "nosetests" as failed. 
Clicking on the job will show you the output, including the stack trace.
* **Nosetests coverage** - Checks to see if every line of code is executed, except for lines that it is specifically told to ignore.
You can open up the nosetests.hmtl file (locally) or artifact (on GitLab CI) 
to see which lines were executed (green), missed (red), or skipped (grey).
* **Pylint** - Looks for code that is bad practice and/or bad style.
Each error/warning message will be output with the location (filename and line number) as well as a description of the message. 
Note: GitLab CI does not mark this job as failed, even if problems are found. You need to look at the pylint job output.

## Deployment

No extra steps needed

## How to use the code

* This is a work in progress, so you cannot run a simulation yet
* You can still play around with creating combatants, weapons, attacks, and spells, 
and you can even hardcode combatants attacking/healing each other
* The overarching code for running a simulation is not yet written, but check back periodically 
because the code is updated frequently

## Built With

* [Python 3.6.6](https://docs.python.org/3.6/) - The language this is written in
* [Pycharm](https://www.jetbrains.com/pycharm/documentation/) - The IDE used
* [GitLab](https://rometools.gitlab.com) - VCS and issue management
* [Nosetests](http://nose.readthedocs.io/en/latest/) - Testing
* [Pylint](http://pylint.readthedocs.io/en/latest/) - Code quality
* [Sphinx](http://sphinx-doc.org/) - Documentation
* [Docker](https://www.docker.com/) - Runner for continuous integration

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on the process for submitting merge requests (also known as pull requests).

## Versioning

Currently due to the minimal dependencies and the fact the project is not complete, no versioning system is currently implemented. If the version number is important, you can use the commit name/number listed on GitLab.

## Authors

* **Catherine DeJager** - *Initial work* - [cmd16](https://gitlab.com/cmd16)

See also the list of [contributors](contributors.md) who participated in this project.

## License

This project is licensed under the GNU GPL - see the [LICENSE](Licenses/LICENSE) file for details

D&D 5e content provided is part of the SRD,
as allowed by the Open Gaming License -
see the [OGL License](Licenses/OGL_License.md) file for details

## Acknowledgments
